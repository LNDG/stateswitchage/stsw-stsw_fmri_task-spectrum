currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.standards = fullfile(pn.root, 'data','A_standards');
pn.regressors = fullfile(pn.root,'..', 'design_timing', 'data', 'regressors');
pn.out = fullfile(pn.root, 'data','spectralPower');

IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2261'};

WelchPower_merged = [];
for indID = 1:numel(IDs)
    load(fullfile(pn.out, [IDs{indID}, '_WelchPower_byBlock.mat']), 'WelchPower', 'f')
    % sort according to block dimensionality
    curDims = [];
    for indRun = 1:4
        load(fullfile(pn.regressors, [IDs{indID}, '_Run',num2str(indRun),'_regressors.mat']))
        curDims = [curDims; Regressors(find(Regressors(:,4)),4)];
    end
    for indDim = 1:4
        WelchPower_merged(indID,indDim,:) = squeeze(nanmean(WelchPower(curDims==indDim,:),1));
    end
end

figure;
hold on;
for indDim = 1:4
    plot(f, log10(squeeze(nanmean(nanmean(WelchPower_merged(:,indDim,:),2),1))));
end

figure;
subplot(2,1,1);
hold on;
for indDim = 1:4
    plot(f, log10(squeeze(nanmean(nanmean(WelchPower_merged(1:47,indDim,:),2),1))));
end
subplot(2,1,2);
hold on;
for indDim = 1:4
    plot(f, log10(squeeze(nanmean(nanmean(WelchPower_merged(48:end,indDim,:),2),1))));
end

figure;
hold on;
plot(f, squeeze(nanmean(nanmean(WelchPower_merged(1:43,4,:),2),1))-...
    squeeze(nanmean(nanmean(WelchPower_merged(1:43,1,:),2),1)), 'k');
plot(f, squeeze(nanmean(nanmean(WelchPower_merged(44:end,4,:),2),1))-...
    squeeze(nanmean(nanmean(WelchPower_merged(44:end,1,:),2),1)), 'r');

for indID = 1:size(WelchPower_merged,1)
    for indTime = 1:size(WelchPower_merged,3)
        X = [1 1; 1 2; 1 3; 1 4];
        b = regress(squeeze(WelchPower_merged(indID, 1:4, indTime))', X);
        WelchPower_lin(indID,indTime) = b(2);
    end
end

figure;
subplot(2,1,1);
hold on;
l1 = plot(log10(f), squeeze(nanmean(nanmean(log10(WelchPower_merged(1:end,1,:)),2),1)), 'k', 'LineWidth', 2);
l4 = plot(log10(f), squeeze(nanmean(nanmean(log10(WelchPower_merged(1:end,4,:)),2),1)), 'r', 'LineWidth', 2);
xlim([-2 -.25]);
legend([l1, l4], {'L1'; 'L4'})
legend('boxoff')
xlabel('Frequency [Hz]')
ylabel('Spectral power')
subplot(2,1,2);
hold on;
lYA = plot(f, squeeze(nanmean(WelchPower_lin(1:43,:),1)), 'k', 'LineWidth', 2);
lOA = plot(f, squeeze(nanmean(WelchPower_lin(44:end,:),1)), 'r', 'LineWidth', 2);
xlim([0 .75]);
legend([lYA, lOA], {'YA'; 'OA'})
legend('boxoff')
xlabel('Frequency [Hz]')
ylabel('Linear Power change')
set(findall(gcf,'-property','FontSize'),'FontSize',16)

%% median split of YA according to drift rates

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
load([pn.dataOut, 'STSWD_summary_YAOA.mat'], 'STSWD_summary')

idx_sumID = ismember(STSWD_summary.IDs, IDs(1:43));
curDrift = STSWD_summary.HDDM_vt.driftEEG(idx_sumID,1);
[sortVal, sortIdx] = sort(curDrift,  'descend');

figure;
subplot(2,1,1);
hold on;
l1 = plot(f, squeeze(nanmean(nanmean(WelchPower_merged(sortIdx(1:floor(numel(sortIdx)/2)),1,:),2),1)), 'k', 'LineWidth', 2);
l4 = plot(f, squeeze(nanmean(nanmean(WelchPower_merged(sortIdx(1:floor(numel(sortIdx)/2)),4,:),2),1)), 'k--', 'LineWidth', 2);
l1 = plot(f, squeeze(nanmean(nanmean(WelchPower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),1,:),2),1)), 'r', 'LineWidth', 2);
l4 = plot(f, squeeze(nanmean(nanmean(WelchPower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),4,:),2),1)), 'r--', 'LineWidth', 2);
l1 = plot(f, squeeze(nanmean(nanmean(WelchPower_merged(44:end,1,:),2),1)), 'b', 'LineWidth', 2);
l4 = plot(f, squeeze(nanmean(nanmean(WelchPower_merged(44:end,4,:),2),1)), 'b--', 'LineWidth', 2);
xlim([0 .3]);
legend([l1, l4], {'L1'; 'L4'})
legend('boxoff')
xlabel('Frequency [Hz]')
ylabel('Spectral power')
subplot(2,1,2);
hold on;
lYA = plot(f, squeeze(nanmean(WelchPower_lin(sortIdx(1:floor(numel(sortIdx)/2)),:),1)), 'k', 'LineWidth', 2);
lYA = plot(f, squeeze(nanmean(WelchPower_lin(sortIdx(floor(numel(sortIdx)/2)+1:end),:),1)), 'r', 'LineWidth', 2);
lOA = plot(f, squeeze(nanmean(WelchPower_lin(44:end,:),1)), 'b', 'LineWidth', 2);
xlim([0 .3]);
legend([lYA, lOA], {'YA'; 'OA'})
legend('boxoff')
xlabel('Frequency [Hz]')
ylabel('Linear Power change')
set(findall(gcf,'-property','FontSize'),'FontSize',16)


% sort all subjects by drift rate

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
load([pn.dataOut, 'STSWD_summary_YAOA.mat'], 'STSWD_summary')

idx_sumID = ismember(STSWD_summary.IDs, IDs);
curDrift = squeeze(nanmean(STSWD_summary.HDDM_vt.driftEEG(idx_sumID,1:4),2));
[sortVal, sortIdx] = sort(curDrift,  'descend');

figure;
hold on;
l1 = plot(log10(f), squeeze(nanmean(nanmean(log10(WelchPower_merged(sortIdx(1:floor(numel(sortIdx)/2)),1:4,:)),2),1)), 'k', 'LineWidth', 2);
l2 = plot(log10(f), squeeze(nanmean(nanmean(log10(WelchPower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),1:4,:)),2),1)), 'r', 'LineWidth', 2);
l3 = plot(log10(f), squeeze(nanmean(nanmean(log10(WelchPower_merged(44:end,1:4,:)),2),1)), 'b', 'LineWidth', 2);
xlim([-2 -.25])
legend([l1, l2, l3], {'High YA'; 'Low YA'; 'OA'})
legend('boxoff')
xlabel('Frequency [Hz]')
ylabel('Spectral power')
set(findall(gcf,'-property','FontSize'),'FontSize',16)


figure;
hold on;
l1 = plot(f, squeeze(nanmean(nanmean(log10(WelchPower_merged(sortIdx(1:floor(numel(sortIdx)/2)),4,:)),2),1))-...
    squeeze(nanmean(nanmean(log10(WelchPower_merged(sortIdx(1:floor(numel(sortIdx)/2)),1,:)),2),1)), 'k', 'LineWidth', 2);
l2 = plot(f, squeeze(nanmean(nanmean(log10(WelchPower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),4,:)),2),1))-...
    squeeze(nanmean(nanmean(log10(WelchPower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),1,:)),2),1)), 'r', 'LineWidth', 2);
l3 = plot(f, squeeze(nanmean(nanmean(log10(WelchPower_merged(44:end,4,:)),2),1))-...
    squeeze(nanmean(nanmean(log10(WelchPower_merged(44:end,1,:)),2),1)), 'b', 'LineWidth', 2);
legend([l1, l2, l3], {'High YA'; 'Low YA'; 'OA'})
legend('boxoff')
xlabel('Frequency [Hz]')
ylabel('Spectral power')
set(findall(gcf,'-property','FontSize'),'FontSize',16)

keepFreq = f>.03 & f<.07 | f>.14 & f<.16 | f>.23 & f<.26 | f>.33;

figure;
hold on;
l1 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(WelchPower_merged(sortIdx(1:floor(numel(sortIdx)/2)),1:4,keepFreq)),2),1)), 'k', 'LineWidth', 2);
l2 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(WelchPower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),1:4,keepFreq)),2),1)), 'r', 'LineWidth', 2);
l3 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(WelchPower_merged(44:end,1:4,keepFreq)),2),1)), 'b', 'LineWidth', 2);
legend([l1, l2, l3], {'High YA'; 'Low YA'; 'OA'})
xlim([-1.4 -.20])
legend('boxoff')
xlabel('Frequency [Hz]')
ylabel('Spectral power')
set(findall(gcf,'-property','FontSize'),'FontSize',16)

figure;
hold on;
l1 = plot(f(keepFreq), squeeze(nanmean(nanmean(log10(WelchPower_merged(sortIdx(1:floor(numel(sortIdx)/2)),1:4,keepFreq)),2),1)), 'k', 'LineWidth', 2);
l2 = plot(f(keepFreq), squeeze(nanmean(nanmean(log10(WelchPower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),1:4,keepFreq)),2),1)), 'r', 'LineWidth', 2);
l3 = plot(f(keepFreq), squeeze(nanmean(nanmean(log10(WelchPower_merged(44:end,1:4,keepFreq)),2),1)), 'b', 'LineWidth', 2);
legend([l1, l2, l3], {'High YA'; 'Low YA'; 'OA'})
legend('boxoff')
xlabel('Frequency [Hz]')
ylabel('Spectral power')
set(findall(gcf,'-property','FontSize'),'FontSize',16)


%% perform a linear fit

freqs = log10(f(keepFreq));
X = [ones(numel(freqs),1), freqs];

for indID = 1:size(WelchPower_merged,1)
    curData = squeeze(nanmean(log10(WelchPower_merged(indID,1:4,keepFreq)),2));
    b = regress(curData, X);
    SpectralSlopes(indID) = b(2);
end

figure; scatter(SpectralSlopes(sortIdx(3:end)), sortVal(3:end), 'filled')
[r,p] = corrcoef(SpectralSlopes(sortIdx(3:end)), sortVal(3:end)')

idx_sumID = ismember(STSWD_summary.IDs, IDs);

idxYA = 1:43; idxOA = 44:numel(IDs);
figure; cla; hold on;
x = SpectralSlopes;
y = squeeze(nanmean(STSWD_summary.HDDM_vt.driftEEG(idx_sumID,1:4),2));
%y = sortVal(3:end);
scatter(x, y, 1, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[.2 .2 .2], 'LineWidth', 3);
scatter(x(idxYA), y(idxYA), 70, 'filled', 'MarkerFaceColor', [1 .6 .6]);
scatter(x(idxOA), y(idxOA), 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]);
[r, p, RL, RU] = corrcoef(x, y);
title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']'])
ylabel({'Drift rate L1234'}); xlabel('Aperiodic Slope')
ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])

%% perform a linear fit for each condition

for indID = 1:size(WelchPower_merged,1)
    for indCond = 1:4
        curData = squeeze(nanmean(log10(WelchPower_merged(indID,indCond,keepFreq)),2));
        b = regress(curData, X);
        SpectralSlopesCond(indID,indCond) = b(2);
    end
end

figure;
hold on;
bar(mean(SpectralSlopesCond(1:floor(numel(sortIdx)/2),:),1), 'k')
bar(mean(SpectralSlopesCond(floor(numel(sortIdx)/2)+1:end,:),1), 'r')
bar(mean(SpectralSlopesCond(44:end,:),1), 'g')
ylim([-.95 -.6])

ttest(SpectralSlopesCond(1:43,1), SpectralSlopesCond(1:43,2))

idx_sumID = ismember(STSWD_summary.IDs, IDs(1:43));
curDrift = STSWD_summary.HDDM_vt.driftEEG(idx_sumID,1);
[sortVal, sortIdx] = sort(curDrift,  'descend');

figure;
subplot(2,1,1);
hold on;
l1 = plot(f, squeeze(nanmean(nanmean(WelchPower_merged(sortIdx(1:floor(numel(sortIdx)/2)),1,:),2),1)), 'k', 'LineWidth', 2);
l4 = plot(f, squeeze(nanmean(nanmean(WelchPower_merged(sortIdx(1:floor(numel(sortIdx)/2)),4,:),2),1)), 'k--', 'LineWidth', 2);
l1 = plot(f, squeeze(nanmean(nanmean(WelchPower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),1,:),2),1)), 'r', 'LineWidth', 2);
l4 = plot(f, squeeze(nanmean(nanmean(WelchPower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),4,:),2),1)), 'r--', 'LineWidth', 2);
l1 = plot(f, squeeze(nanmean(nanmean(WelchPower_merged(44:end,1,:),2),1)), 'b', 'LineWidth', 2);
l4 = plot(f, squeeze(nanmean(nanmean(WelchPower_merged(44:end,4,:),2),1)), 'b--', 'LineWidth', 2);

%% split based on spectral slopes in EEG

figure;
hold on;
idx_sumID = ismember(STSWD_summary.IDs, IDs_YA);
curDrift = squeeze(nanmean(STSWD_summary.OneFslope.data(idx_sumID,1:4),2));
[sortVal, sortIdx] = sort(curDrift,  'descend');
l1 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(WelchPower_merged(sortIdx(1:floor(numel(sortIdx)/2)),1:4,keepFreq)),2),1)), 'k', 'LineWidth', 2);
l2 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(WelchPower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),1:4,keepFreq)),2),1)), 'k--', 'LineWidth', 2);
idx_sumID = ismember(STSWD_summary.IDs, IDs_OA);
curDrift = squeeze(nanmean(STSWD_summary.OneFslope.data(idx_sumID,1:4),2));
[sortVal, sortIdx] = sort(curDrift,  'descend');
l3 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(WelchPower_merged(43+sortIdx(1:floor(numel(sortIdx)/2)),1:4,keepFreq)),2),1)), 'r', 'LineWidth', 2);
l4 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(WelchPower_merged(43+sortIdx(floor(numel(sortIdx)/2)+1:end),1:4,keepFreq)),2),1)), 'r--', 'LineWidth', 2);
legend([l1, l2, l3], {'High YA'; 'Low YA'; 'OA'})
legend('boxoff')
xlabel('Frequency [Hz]')
ylabel('Spectral power')
set(findall(gcf,'-property','FontSize'),'FontSize',16)

idx_sumID = ismember(STSWD_summary.IDs, [IDs_YA; IDs_OA]);
%curDrift = squeeze(nanmean(STSWD_summary.OneFslope.data(idx_sumID,1:4),2));
curDrift = squeeze(nanmean(STSWD_summary.HDDM_vt.driftEEG(idx_sumID,1:4),2));
[sortVal, sortIdx] = sort(curDrift,  'descend');

figure; imagesc(squeeze(nanmean(log10(WelchPower_merged(sortIdx,:,keepFreq)),2)))