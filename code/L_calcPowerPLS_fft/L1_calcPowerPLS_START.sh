#!/bin/bash

fun_name="L1_calcPowerPLS"
job_name="stsw_varpls_fft"

rootpath="$(pwd)/../.."
rootpath=$(builtin cd $rootpath; pwd)

mkdir ${rootpath}/log

# path to the text file with all subject ids:
path_ids="${rootpath}/code/id_list_mr.txt"
# read subject ids from the list of the text file
IDS=$(cat ${path_ids} | tr '\n' ' ')

#for subj in ${IDS}; do
for subj in "2131" "2237"; do
	sbatch \
  		--job-name ${job_name}_${subj} \
  		--cpus-per-task 1 \
  		--mem 60gb \
  		--time 02:00:00 \
  		--output ${rootpath}/log/${job_name}_${subj}.out \
  		--workdir . \
  		--wrap=". /etc/profile ; module load matlab/R2020a; matlab -nodisplay -r \"${fun_name}('${rootpath}','${subj}')\""
done