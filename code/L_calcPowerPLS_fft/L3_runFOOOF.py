# coding: utf-8

# import sys
#!{sys.executable} -m pip install fooof
import numpy as np
import os as os
from scipy.io import loadmat, savemat
from fooof import FOOOFGroup
import sys

print(sys.argv[0])  # returns scriptname
print(sys.argv[1])  # returns rootdir
print(sys.argv[2])  # returns ID

# Set up current paths etc.
print("Setting up paths ...\n")
rootdir = sys.argv[1] #'/Users/kosciessa/Desktop/tardis/LNDG/stateswitch/stsw_fmri_task/spectrum/'
curSub = sys.argv[2]
datadir = os.path.join(rootdir, 'data','spectralPower', 'freq')
if not os.path.isdir(datadir):
    os.mkdir(datadir)

# Load the mat file
print("Loading backgrounds from .mat ...\n")
filename = os.path.join(datadir, curSub + '_freq.mat')
dat = loadmat(filename)

# Unpack data from dictionary, and squeeze into numpy arrays
psds = np.squeeze(dat['freq']['powspctrm'][0][0]).astype('float')
freqs = np.squeeze(dat['freq']['freq'][0][0]).astype('float')
# ^Note: this also explicitly enforces type as float (type casts to float64, instead of float32)
#  This is not strictly necessary for fitting, but is for saving out as json from FOOOF, if you want to do that
# FOOOF expects psds to be nChan*nFreq, no preference for 1D freqs

# Initialize FOOOF object
fg = FOOOFGroup(peak_width_limits=[0.005, 0.025], max_n_peaks =8, peak_threshold = 4, min_peak_height = 0.1, aperiodic_mode='fixed')

# Fit the FOOOF model on all PSDs, and report
fg.report(freqs, psds, [0.035, 0.45])

for channel in range(len(fg.get_params('error'))):
    fm = fg.get_fooof(channel, regenerate=True)
    fm.plot(plt_log=True, save_fig=True, file_name=curSub+'_fit_'+str(channel+1), file_path=os.path.join(rootdir, 'data','spectralPower', 'freq', 'fits'))

# Save out a specific FOOOF measure of interest - for example, slopes
exps = fg.get_params('aperiodic_params', 'exponent')
ints = fg.get_params('aperiodic_params', 'offset')
savemat(os.path.join(datadir, 'exps'+'_'+curSub+'.mat'), {'exps': exps, 'ints': ints})


# Save out fooof results to json file
#  There is a utility file to load this json file directly into Matlab
fg.save(os.path.join(datadir, 'fooof_results'+'_'+curSub), save_results=True)

print(curSub+'Done!')
