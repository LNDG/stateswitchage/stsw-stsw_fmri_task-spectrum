function L1_calcPowerPLS(rootpath, ID)

    % extract temporal traces from all voxels that are part of behavioral PLS
    
    if ismac
        currentFile = mfilename('fullpath');
        [pathstr,~,~] = fileparts(currentFile);
        cd(fullfile(pathstr,'..', '..'))
        rootpath = pwd;
        ID = '1117';
    end
    
    pn.tools = fullfile(rootpath, 'tools');
        addpath(fullfile(pn.tools, 'preprocessing_tools'))
        addpath(fullfile(pn.tools, 'nifti_toolbox'))
    pn.standards = fullfile(rootpath, 'data', 'standards', 'data');
    pn.varpls = fullfile(rootpath, '..', 'pls_vartbx', 'data', 'pls');
    pn.BOLD = fullfile(rootpath, '..', 'extended_preproc', 'data', 'data_clean');
    pn.regressors = fullfile(rootpath, '..', 'design_timing', 'data', 'regressors');
    pn.out = fullfile(rootpath, 'data', 'spectralPower');
    
    pn.mask = fullfile(pn.varpls, 'taskPLS_STSWD_VarTbx_YA_OA_3mm_1000P1000B_BfMRIbsr_lv1.img');
    [mask] = double(S_load_nii_2d(pn.mask));
        
    if strcmp(ID, '2131') || strcmp(ID, '2237')
        numOfRuns = 2;
    else
        numOfRuns = 4;
    end
    
    disp(['Processing subject ', ID, '.']);

    %fftpower = NaN(32,numel(find(mask)), 129);
    for indRun = 1:numOfRuns

        % load nifti file for this run; %(x by y by z by time)
        % check for nii or nii.gz, unzip .gz and reshape to 2D

        % if proc on tardis: change path to tardis preproc directory
       
        fileName = [ID, '_run-',num2str(indRun),'_regressed.nii'];
        fname = fullfile(pn.BOLD, fileName);
        
        if ~exist(fname)
            warning(['File not available: ', ID, ' Run ', num2str(indRun)]);
            continue;
        end
        
        % If using preprocessing tools
        [img] = double(S_load_nii_2d(fname));
        
        % get time series within mask
        masksignal = img(mask~=0,:); clear img;
        
        %% cut into 8-stimulus blocks
               
        load(fullfile(pn.regressors, [ID, '_Run',num2str(indRun),'_regressors.mat']))
        
        if strcmp(ID, '2132') && indRun == 2
            Regressors(1041-128:end) = [];
        end
        
        % get stimulus onsets
        onset_samples = find(Regressors(:,1));
        for indBlock = 1:numel(onset_samples)
            tic
            curSignal = masksignal(:,onset_samples(indBlock):onset_samples(indBlock)+128);
            curSignal = [-1.*fliplr(curSignal), curSignal, -1.*fliplr(curSignal)]';
            [pxx,f] = pspectrum(curSignal,1/0.645);
            fftpower((indRun-1)*8+indBlock,:,:) = pxx';
            disp(['Block ', num2str(indBlock), ' finished: ', num2str(toc)])
        end
        
        clear masksignal;
        
    end
    
    %% WIP: get spectral slopes by voxel (but initially averaged across blocks of same dimensionality)
    
    % average across blocks
    fftpower_merged = [];
    % sort according to block dimensionality
    curDims = [];
    for indRun = 1:numOfRuns
        load(fullfile(pn.regressors, [ID, '_Run',num2str(indRun),'_regressors.mat']))
        curDims = [curDims; Regressors(find(Regressors(:,4)),4)];
    end
    for indDim = 1:4
        fftpower_merged(indDim,:,:) = squeeze(nanmean(fftpower(curDims==indDim,:,:),1));
    end
    
    keepFreq = f>.03 & f<.07 | f>.14 & f<.16 | f>.23 & f<.26 | f>.33 & f<.38 | f>.42;

    freqs = log10(f(keepFreq));
    X = [ones(numel(freqs),1), freqs];

    for indBlock = 1:size(fftpower_merged,1)
        for indVoxel = 1:size(fftpower,2)
            curData = squeeze(nanmean(log10(fftpower_merged(indBlock,indVoxel,keepFreq)),2));
            b = regress(curData, X);
            SpectralSlopes(indBlock,indVoxel) = b(2);
        end
    end
    
    %% average spectral power for plotting
    
    fftpower = squeeze(nanmean(fftpower,2));
    
    save(fullfile(pn.out, [ID, '_varpls_fft_byBlock.mat']), 'fftpower', 'fftpower_merged', 'f', 'SpectralSlopes')

end