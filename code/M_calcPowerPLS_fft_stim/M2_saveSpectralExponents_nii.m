    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..', '..'))
    pn.root = pwd;

    pn.standards = fullfile(pn.root, 'data','standards', 'data');
    pn.spectra = fullfile(pn.root, 'data','spectralPower');
        
    addpath(fullfile(pn.root, 'tools','nifti_toolbox'));
    addpath(fullfile(pn.root, 'tools','preprocessing_tools'));
    
    filename = fullfile(pn.root, 'code', 'id_list_mr.txt');
    fileID = fopen(filename);
    IDs = textscan(fileID,'%s');
    fclose(fileID);
    IDs = IDs{1};
    
    pn.GMmask = fullfile(pn.standards, 'mni_icbm152_nlin_sym_09c','mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii');

    for indID = 1:numel(IDs)
        load(fullfile(pn.spectra, [IDs{indID}, '_varpls_fft_stim_allvoxs.mat']), 'fftpower_merged', 'f')
        PeakAvg = squeeze(nanmean(fftpower_merged,3));
        T1w = load_nii(pn.GMmask);
        [GMmask] = double(S_load_nii_2d(pn.GMmask));
        for indLoad = 1:4
            nii_coords=zeros(size(T1w.img,1),size(T1w.img,2),size(T1w.img,3));
            nii_coords(GMmask==1) = PeakAvg(indLoad,:);
            T1w.img = nii_coords;
            save_nii(T1w, fullfile(pn.spectra, [IDs{indID}, '_Peak_L',num2str(indLoad),'.nii']))
        end
    end

    %% average slopes across all subjects
    
    for indID = 1:numel(IDs)
        for indLoad = 1:4
            dataIn = load_nii(fullfile(pn.spectra, [IDs{indID}, '_Slopes_L',num2str(indLoad),'.nii']));
            dataMerged(indID,indLoad,:,:,:) = dataIn.img;
        end
    end
    dataIn.img = squeeze(nanmean(nanmean(dataMerged,2),1));
    save_nii(dataIn, fullfile(pn.spectra, 'Peak_all.nii'))
    