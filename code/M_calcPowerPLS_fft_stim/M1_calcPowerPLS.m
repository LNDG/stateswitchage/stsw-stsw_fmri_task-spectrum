function M1_calcPowerPLS(rootpath, ID)

    % extract temporal traces from all voxels that are part of behavioral PLS
    
    if ismac
        currentFile = mfilename('fullpath');
        [pathstr,~,~] = fileparts(currentFile);
        cd(fullfile(pathstr,'..', '..'))
        rootpath = pwd;
        ID = '1117';
    end
    
    pn.tools = fullfile(rootpath, 'tools');
        addpath(fullfile(pn.tools, 'preprocessing_tools'))
        addpath(fullfile(pn.tools, 'nifti_toolbox'))
    pn.standards = fullfile(rootpath, 'data', 'standards', 'data');
    pn.varpls = fullfile(rootpath, '..', 'pls_vartbx', 'data', 'pls');
    pn.BOLD = fullfile(rootpath, '..', 'extended_preproc', 'data', 'data_clean');
    pn.regressors = fullfile(rootpath, '..', 'design_timing', 'data', 'regressors');
    pn.out = fullfile(rootpath, 'data', 'spectralPower');
    
    pn.mask = fullfile(pn.standards,'mni_icbm152_nlin_sym_09c','mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii');
    %pn.mask = fullfile(pn.varpls, 'taskPLS_STSWD_VarTbx_YA_OA_3mm_1000P1000B_BfMRIbsr_lv1.img');
    [mask] = double(S_load_nii_2d(pn.mask));
        
    if strcmp(ID, '2131') || strcmp(ID, '2237')
        numOfRuns = 2;
    else
        numOfRuns = 4;
    end
    
    disp(['Processing subject ', ID, '.']);
    
    %fftpower = NaN(32,numel(find(mask)), 129);
    for indRun = 1:numOfRuns

        % load nifti file for this run; %(x by y by z by time)
        % check for nii or nii.gz, unzip .gz and reshape to 2D

        % if proc on tardis: change path to tardis preproc directory
       
        fileName = [ID, '_run-',num2str(indRun),'_regressed.nii'];
        fname = fullfile(pn.BOLD, fileName);
        
        if ~exist(fname)
            warning(['File not available: ', ID, ' Run ', num2str(indRun)]);
            continue;
        end
        
        % If using preprocessing tools
        [img] = double(S_load_nii_2d(fname));
        
        % get time series within mask
        masksignal = img(mask~=0,:); clear img;
        
        %% cut into 8-stimulus blocks
               
        load(fullfile(pn.regressors, [ID, '_Run',num2str(indRun),'_regressors.mat']))
        
        if strcmp(ID, '2132') && indRun == 2
            Regressors(1041-128:end) = [];
        end
        
        % get stimulus onsets
        onset_samples = find(Regressors(:,1));
        for indBlock = 1:numel(onset_samples)
            tic
            curSignal = masksignal(:,onset_samples(indBlock):onset_samples(indBlock)+128);
            curSignal = [-1.*fliplr(curSignal), curSignal, -1.*fliplr(curSignal)]';
            curSignal(isnan(curSignal)) = 0;
            [pxx,f] = pspectrum(curSignal,1/0.645);
            keepFreq = f>.08 & f<.125;
            fftpower((indRun-1)*8+indBlock,:,:) = pxx(keepFreq,:)';
            disp(['Block ', num2str(indBlock), ' finished: ', num2str(toc)])
        end
        
        clear masksignal;
        
    end
    
    %% WIP: get spectral slopes by voxel (but initially averaged across blocks of same dimensionality)
    
    % average across blocks
    fftpower_merged = [];
    % sort according to block dimensionality
    curDims = [];
    for indRun = 1:numOfRuns
        load(fullfile(pn.regressors, [ID, '_Run',num2str(indRun),'_regressors.mat']))
        curDims = [curDims; Regressors(find(Regressors(:,4)),4)];
    end
    for indDim = 1:4
        fftpower_merged(indDim,:,:) = squeeze(nanmean(fftpower(curDims==indDim,:,:),1));
    end
    
    %% average spectral power across voxels for plotting
    
    fftpower = squeeze(nanmean(fftpower,2));
    
    save(fullfile(pn.out, [ID, '_varpls_fft_stim_allvoxs.mat']), 'fftpower', 'fftpower_merged', 'f')

end