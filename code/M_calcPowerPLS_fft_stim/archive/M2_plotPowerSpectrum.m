currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
pn.root = pwd;

pn.standards = fullfile(pn.root, 'data','A_standards');
pn.regressors = fullfile(pn.root,'..', 'design_timing', 'data', 'regressors');
pn.out = fullfile(pn.root, 'data','spectralPower');
pn.spectral = fullfile(pn.root, 'data','spectralPower', 'freq'); mkdir(pn.spectral);
pn.summary = fullfile(pn.root, '..', '..', 'stsw_multimodal');
pn.figures = fullfile(pn.root, 'figures');
    addpath(fullfile(pn.root, 'tools', 'BrewerMap'))
    addpath(fullfile(pn.root, 'tools', 'shadedErrorBar'))
    addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')))

cBrew = brewermap(4,'RdBu');

filename = fullfile(pn.root, 'code', 'id_list_mr.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

idx_YA = find(cellfun(@str2num, IDs)<2000);
idx_OA = find(cellfun(@str2num, IDs)>2000);

fftpower_merged = [];
for indID = 1:numel(IDs)
    disp(['processing ', num2str(indID)]);
    load(fullfile(pn.out, [IDs{indID}, '_varpls_fft_stim_allvoxs.mat']), 'fftpower', 'fftpower_merged', 'f')
    % sort according to block dimensionality
    curDims = [];
    if strcmp(IDs{indID}, '2131') || strcmp(IDs{indID}, '2237')
        numOfRuns = 2;
    else
        numOfRuns = 4;
    end
    for indRun = 1:numOfRuns
        load(fullfile(pn.regressors, [IDs{indID}, '_Run',num2str(indRun),'_regressors.mat']))
        curDims = [curDims; Regressors(find(Regressors(:,4)),4)];
    end
    for indDim = 1:4
        fftpower_merged(indID,indDim,:) = squeeze(nanmean(fftpower(curDims==indDim,:),1));
    end
    
    % save in freq structure
    freq = [];
    freq.freq = f';
    freq.powspctrm = squeeze(fftpower_merged(indID,:,:));
    save(fullfile(pn.spectral, [IDs{indID}, '_freq.mat']), 'freq');
    
    % load exponents
    load(fullfile(pn.spectral, ['exps_', IDs{indID}, '.mat']), 'exps', 'ints');

    for indDim = 1:4
        pv = [-1.*exps(indDim),ints(indDim)];
        aperiodic=10.^(polyval(pv,log10(f)));       

        % figure;
        % plot(f, squeeze(fftpower_merged(indID,1,:))-aperiodic);
        % xlim([0.0175, 0.77])

        exponents(indID, indDim) = -1.*exps(indDim); % INVERT!!!
        aperiodic_merged(indID, indDim,:) = aperiodic;
        residual_merged(indID, indDim,:) = squeeze(fftpower_merged(indID,indDim,:))-aperiodic;
    end
end

% linear changes by age group
X = [1 1; 1 2; 1 3; 1 4]; b=X\exponents'; exponents_load = b(2,:);
[~, p] = ttest(exponents_load(idx_YA))
[~, p] = ttest(exponents_load(idx_OA))

% comparison between age groups
[h,p] = ttest2(nanmean(exponents(idx_YA,1:4),2), nanmean(exponents(idx_OA,1:4),2))

%% plot overall spectrum

h = figure('units','normalized','position',[.1 .1 .5 .2]);
subplot(1,3,1);
    hold on;
    condAverage = squeeze(nanmean(log10(fftpower_merged(1:end,[1:4],:)),2));
    curData = squeeze(nanmean(log10(fftpower_merged(1:end,1,:)),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(f),nanmean(curData,1),standError,'lineprops', {'color', cBrew(1,:), 'linewidth', 1});
    curData = squeeze(nanmean(log10(fftpower_merged(1:end,4,:)),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(f),nanmean(curData,1),standError,'lineprops', {'color', cBrew(4,:), 'linewidth', 1});
    xlim([-2 -.2]);
    legend([l1.mainLine, l4.mainLine], {'L1'; 'L4'}, 'location', 'SouthWest')
    legend('boxoff')
    xlabel('Frequency [Hz]')
    ylabel('Spectral power')
subplot(1,3,2); cla;
    hold on;
    curData = squeeze(nanmean(log10(fftpower_merged(idx_YA,1:4,:)),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    lYA = shadedErrorBar(log10(f),nanmean(curData,1),standError,'lineprops', {'color', cBrew(1,:), 'linewidth', 1});
    curData = squeeze(nanmean(log10(fftpower_merged(idx_OA,1:4,:)),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    lOA = shadedErrorBar(log10(f), nanmean(curData,1),standError,'lineprops', {'color', cBrew(4,:), 'linewidth', 1});
    xlim([-2 -.2]);
    ylim([0.75 2.225])
    legend([lYA.mainLine, lOA.mainLine], {'YA'; 'OA'},'location', 'SouthWest')
    legend('boxoff')
    xlabel('Frequency [Hz]')
    ylabel('Linear Power change')
    set(findall(gcf,'-property','FontSize'),'FontSize',16)
subplot(1,3,3); cla;
    hold on;
    curData = squeeze(log10(fftpower_merged(idx_YA,4,:))-log10(fftpower_merged(idx_YA,1,:)));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    lYA = shadedErrorBar(f,nanmean(curData,1),standError,'lineprops', {'color', cBrew(1,:), 'linewidth', 1});
    curData = squeeze(log10(fftpower_merged(idx_OA,4,:))-log10(fftpower_merged(idx_OA,1,:)));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    lOA = shadedErrorBar(f, nanmean(curData,1),standError,'lineprops', {'color', cBrew(4,:), 'linewidth', 1});
	xlim([0 .75]);
    legend([lYA.mainLine, lOA.mainLine], {'YA'; 'OA'})
    legend('boxoff')
    xlabel('Frequency [Hz]')
    ylabel('L4 - L1')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = ['l2_spectrum_all'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% plot spectrum for aperiodic

h = figure('units','normalized','position',[.1 .1 .5 .2]);
subplot(1,3,1);
    hold on;
    condAverage = squeeze(nanmean(log10(aperiodic_merged(1:end,[1:4],:)),2));
    curData = squeeze(nanmean(log10(aperiodic_merged(1:end,1,:)),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(f),nanmean(curData,1),standError,'lineprops', {'color', cBrew(1,:), 'linewidth', 1});
    curData = squeeze(nanmean(log10(aperiodic_merged(1:end,2,:)),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(log10(f),nanmean(curData,1),standError,'lineprops', {'color', cBrew(2,:), 'linewidth', 1});
    curData = squeeze(nanmean(log10(aperiodic_merged(1:end,3,:)),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(log10(f),nanmean(curData,1),standError,'lineprops', {'color', cBrew(3,:), 'linewidth', 1});
    curData = squeeze(nanmean(log10(aperiodic_merged(1:end,4,:)),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(f),nanmean(curData,1),standError,'lineprops', {'color', cBrew(4,:), 'linewidth', 1});
    xlim([-2 -.2]);
    legend([l1.mainLine, l4.mainLine], {'L1'; 'L4'}, 'location', 'SouthWest')
    legend('boxoff')
    xlabel('Frequency [Hz]')
    ylabel('Spectral power')
subplot(1,3,2); cla;
    hold on;
    curData = squeeze(nanmean(log10(aperiodic_merged(idx_YA,1:4,:)),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    lYA = shadedErrorBar(log10(f),nanmean(curData,1),standError,'lineprops', {'color', cBrew(1,:), 'linewidth', 1});
    curData = squeeze(nanmean(log10(aperiodic_merged(idx_OA,1:4,:)),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    lOA = shadedErrorBar(log10(f), nanmean(curData,1),standError,'lineprops', {'color', cBrew(4,:), 'linewidth', 1});
    xlim([-2 -.2]);
    %ylim([0.75 2.225])
    legend([lYA.mainLine, lOA.mainLine], {'YA'; 'OA'},'location', 'SouthWest')
    legend('boxoff')
    xlabel('Frequency [Hz]')
    ylabel('Linear Power change')
    set(findall(gcf,'-property','FontSize'),'FontSize',16)
subplot(1,3,3); cla;
    hold on;
    curData = squeeze(log10(aperiodic_merged(idx_YA,4,:))-log10(aperiodic_merged(idx_YA,1,:)));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    lYA = shadedErrorBar(f,nanmean(curData,1),standError,'lineprops', {'color', cBrew(1,:), 'linewidth', 1});
    curData = squeeze(log10(aperiodic_merged(idx_OA,4,:))-log10(aperiodic_merged(idx_OA,1,:)));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    lOA = shadedErrorBar(f, nanmean(curData,1),standError,'lineprops', {'color', cBrew(4,:), 'linewidth', 1});
	xlim([0 .75]);
    legend([lYA.mainLine, lOA.mainLine], {'YA'; 'OA'})
    legend('boxoff')
    xlabel('Frequency [Hz]')
    ylabel('L4 - L1')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = ['l2_spectrum_aperiodic'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

h = figure('units','normalized','position',[.1 .1 .15 .2]);
    hold on;
    condAverage = squeeze(nanmean(log10(aperiodic_merged(idx_YA,[1:4],:)),2));
    curData = squeeze(nanmean(log10(aperiodic_merged(idx_YA,1,:)),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(f),nanmean(curData,1),standError,'lineprops', ...
        {'color', cBrew(1,:), 'linewidth', 4});
    curData = squeeze(nanmean(log10(aperiodic_merged(idx_YA,4,:)),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(f),nanmean(curData,1),standError,'lineprops', ...
        {'color', cBrew(4,:), 'linewidth', 4});
    condAverage = squeeze(nanmean(log10(aperiodic_merged(idx_OA,[1:4],:)),2));
    curData = squeeze(nanmean(log10(aperiodic_merged(idx_OA,1,:)),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1_OA = shadedErrorBar(log10(f),nanmean(curData,1),standError,'lineprops', ...
        {'color', cBrew(1,:), 'linestyle', ':', 'linewidth', 4});
    curData = squeeze(nanmean(log10(aperiodic_merged(idx_OA,4,:)),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4_OA = shadedErrorBar(log10(f),nanmean(curData,1),standError,'lineprops', ...
        {'color', cBrew(4,:), 'linestyle', '--', 'linewidth', 4});
    xlim([-2 -.2]);
    legend([l1.mainLine, l4.mainLine], {'L1'; 'L4'}, 'location', 'SouthWest')
    legend('boxoff')
    xlabel('Frequency [Hz]')
    ylabel('Spectral power')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = ['l2_spectrum_aperiodic_YAOA'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% plot spectrum for residuals

h = figure('units','normalized','position',[.1 .1 .5 .2]);
subplot(1,3,1);
    hold on;
    condAverage = squeeze(nanmean(log10(residual_merged(1:end,[1,4],:)),2));
    curData = squeeze(nanmean(log10(residual_merged(1:end,1,:)),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(f),nanmean(curData,1),standError,'lineprops', {'color', cBrew(1,:), 'linewidth', 1});
    curData = squeeze(nanmean(log10(residual_merged(1:end,4,:)),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(f),nanmean(curData,1),standError,'lineprops', {'color', cBrew(4,:), 'linewidth', 1});
    xlim([-2 -.2]);
    legend([l1.mainLine, l4.mainLine], {'L1'; 'L4'}, 'location', 'SouthWest')
    legend('boxoff')
    xlabel('Frequency [Hz]')
    ylabel('Spectral power')
subplot(1,3,2); cla;
    hold on;
    curData = squeeze(nanmean(log10(residual_merged(idx_YA,1:4,:)),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    lYA = shadedErrorBar(log10(f),nanmean(curData,1),standError,'lineprops', {'color', cBrew(1,:), 'linewidth', 1});
    curData = squeeze(nanmean(log10(residual_merged(idx_OA,1:4,:)),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    lOA = shadedErrorBar(log10(f), nanmean(curData,1),standError,'lineprops', {'color', cBrew(4,:), 'linewidth', 1});
    xlim([-2 -.2]);
    %ylim([0.75 2.225])
    legend([lYA.mainLine, lOA.mainLine], {'YA'; 'OA'},'location', 'SouthWest')
    legend('boxoff')
    xlabel('Frequency [Hz]')
    ylabel('Linear Power change')
    set(findall(gcf,'-property','FontSize'),'FontSize',16)
subplot(1,3,3); cla;
    hold on;
    curData = squeeze(log10(residual_merged(idx_YA,4,:))-log10(residual_merged(idx_YA,1,:)));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    lYA = shadedErrorBar(f,nanmean(curData,1),standError,'lineprops', {'color', cBrew(1,:), 'linewidth', 1});
    curData = squeeze(log10(residual_merged(idx_OA,4,:))-log10(residual_merged(idx_OA,1,:)));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    lOA = shadedErrorBar(f, nanmean(curData,1),standError,'lineprops', {'color', cBrew(4,:), 'linewidth', 1});
	xlim([0 .75]);
    legend([lYA.mainLine, lOA.mainLine], {'YA'; 'OA'})
    legend('boxoff')
    xlabel('Frequency [Hz]')
    ylabel('L4 - L1')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = ['l2_spectrum_residual'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% plot peak following aperiodic removal

h = figure('units','normalized','position',[.1 .1 .15 .2]);
    hold on;
    condAverage = squeeze(nanmean(residual_merged(1:end,[1,4],:),2));
    curData = squeeze(nanmean(residual_merged(1:end,1,:),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(f,nanmean(curData,1),standError,'lineprops', {'color', cBrew(1,:), 'linewidth', 1});
    curData = squeeze(nanmean(residual_merged(1:end,4,:),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(f,nanmean(curData,1),standError,'lineprops', {'color', cBrew(4,:), 'linewidth', 1});
    xlim([0.08 0.13]);
    legend([l1.mainLine, l4.mainLine], {'L1'; 'L4'}, 'location', 'NorthWest')
    legend('boxoff')
    xlabel('Frequency [Hz]')
    ylabel('Spectral power')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = ['l2_periodic_spectrum'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

% find peak

spectrum = squeeze(nanmean(nanmean(residual_merged,2),1));
search_space = f>0.08 & f<0.13;

[maxVal, maxInd] = max(spectrum(search_space)); maxInd = maxInd + find(search_space, 1, 'first');

peakpower = squeeze(nanmean(residual_merged(:,:,maxInd),3));

% linear changes by age group
X = [1 1; 1 2; 1 3; 1 4]; b=X\peakpower'; peakpower_load = b(2,:);

h = figure('units','normalized','position',[.1 .1 .25 .2]);
subplot(1,2,1); hold on;
    condAverage = squeeze(nanmean(residual_merged(idx_YA,[1,4],:),2));
    curData = squeeze(nanmean(residual_merged(idx_YA,1,:),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(f,nanmean(curData,1),standError,'lineprops', {'color', cBrew(1,:), 'linewidth', 1});
    curData = squeeze(nanmean(residual_merged(idx_YA,4,:),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(f,nanmean(curData,1),standError,'lineprops', {'color', cBrew(4,:), 'linewidth', 1});
    xlim([0.08 0.13]);
    legend([l1.mainLine, l4.mainLine], {'L1'; 'L4'}, 'location', 'NorthWest')
    legend('boxoff')
    xlabel('Frequency [Hz]')
    ylabel('Spectral power')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(1,2,2); hold on;
    condAverage = squeeze(nanmean(residual_merged(idx_OA,[1,4],:),2));
    curData = squeeze(nanmean(residual_merged(idx_OA,1,:),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(f,nanmean(curData,1),standError,'lineprops', {'color', cBrew(1,:), 'linewidth', 1});
    curData = squeeze(nanmean(residual_merged(idx_OA,4,:),2));
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(f,nanmean(curData,1),standError,'lineprops', {'color', cBrew(4,:), 'linewidth', 1});
    xlim([0.08 0.13]);
    legend([l1.mainLine, l4.mainLine], {'L1'; 'L4'}, 'location', 'NorthWest')
    legend('boxoff')
    xlabel('Frequency [Hz]')
    %ylabel('Spectral power')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = ['l2_periodic_spectrum_byAge'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% visualize exponent differences

cBrew(1,:) = 2.*[.3 .1 .1];
cBrew(2,:) = 2.*[.3 .3 .3];

uData{1} = exponents(idx_YA,:)';
uData{2} = exponents(idx_OA,:)';

idx_outlier = cell(1); idx_standard = cell(1);
for indGroup = 1:2
    dataToPlot = uData{indGroup}';
    % define outlier as lin. modulation that is more than three scaled median absolute deviations (MAD) away from the median
    X = [1 1; 1 2; 1 3; 1 4]; b=X\dataToPlot'; IndividualSlopes = b(2,:);
    outliers = isoutlier(IndividualSlopes, 'median');
    idx_outlier{indGroup} = find(outliers);
    idx_standard{indGroup} = find(outliers==0);
end

h = figure('units','centimeter','position',[0 0 25 10]);
for indGroup = 1:2
    dataToPlot = uData{indGroup}';
    % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = dataToPlot(:,i);
            % individually demean for within-subject visualization
            data_ws{i, j} = dataToPlot(:,i)-...
                nanmean(dataToPlot(:,:),2)+...
                repmat(nanmean(nanmean(dataToPlot(:,:),2),1),size(dataToPlot(:,:),1),1);
            data_nooutlier{i, j} = data{i, j};
            data_nooutlier{i, j}(idx_outlier{indGroup}) = [];
            data_ws_nooutlier{i, j} = data_ws{i, j};
            data_ws_nooutlier{i, j}(idx_outlier{indGroup}) = [];
            % sort outliers to back in original data for improved plot overlap
            data_ws{i, j} = [data_ws{i, j}(idx_standard{indGroup}); data_ws{i, j}(idx_outlier{indGroup})];
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    subplot(1,2,indGroup);
    set(gcf,'renderer','Painters')
        cla;
        cl = cBrew(indGroup,:);
        [~, dist] = rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1);
        h_rc = rm_raincloud_fixedSpacing(data_ws_nooutlier, cl,1,[],[],[],dist);
        view([90 -90]);
        axis ij
    box(gca,'off')
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./2, yticks(4)+(yticks(2)-yticks(1))./1.5]);

    minmax = [min(min(cat(2,data_ws{:}))), max(max(cat(2,data_ws{:})))];
    xlim(minmax+[-0.2*diff(minmax), 0.2*diff(minmax)])
    ylabel('targets'); xlabel({'Exponent'})

    % test linear effect
    curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}, data_nooutlier{3, 1}, data_nooutlier{4, 1}];
    X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
    [~, p, ci, stats] = ttest(IndividualSlopes);
    title(['M:', num2str(round(mean(IndividualSlopes),3)), '; p=', num2str(round(p,3))])
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = ['l2_exponents'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% visualize peak power differences

cBrew(1,:) = 2.*[.3 .1 .1];
cBrew(2,:) = 2.*[.1 .1 .3];

uData{1} = peakpower(idx_YA,:)';
uData{2} = peakpower(idx_OA,:)';

idx_outlier = cell(1); idx_standard = cell(1);
for indGroup = 1:2
    dataToPlot = uData{indGroup}';
    % define outlier as lin. modulation that is more than three scaled median absolute deviations (MAD) away from the median
    X = [1 1; 1 2; 1 3; 1 4]; b=X\dataToPlot'; IndividualSlopes = b(2,:);
    outliers = isoutlier(IndividualSlopes, 'median');
    idx_outlier{indGroup} = find(outliers);
    idx_standard{indGroup} = find(outliers==0);
end

h = figure('units','centimeter','position',[0 0 25 10]);
for indGroup = 1:2
    dataToPlot = uData{indGroup}';
    % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = dataToPlot(:,i);
            % individually demean for within-subject visualization
            data_ws{i, j} = dataToPlot(:,i)-...
                nanmean(dataToPlot(:,:),2)+...
                repmat(nanmean(nanmean(dataToPlot(:,:),2),1),size(dataToPlot(:,:),1),1);
            data_nooutlier{i, j} = data{i, j};
            data_nooutlier{i, j}(idx_outlier{indGroup}) = [];
            data_ws_nooutlier{i, j} = data_ws{i, j};
            data_ws_nooutlier{i, j}(idx_outlier{indGroup}) = [];
            % sort outliers to back in original data for improved plot overlap
            data_ws{i, j} = [data_ws{i, j}(idx_standard{indGroup}); data_ws{i, j}(idx_outlier{indGroup})];
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    subplot(1,2,indGroup);
    set(gcf,'renderer','Painters')
        cla;
        cl = cBrew(indGroup,:);
        [~, dist] = rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1);
        h_rc = rm_raincloud_fixedSpacing(data_ws_nooutlier, cl,1,[],[],[],dist);
        view([90 -90]);
        axis ij
    box(gca,'off')
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./2, yticks(4)+(yticks(2)-yticks(1))./1.5]);

    minmax = [min(min(cat(2,data_ws{:}))), max(max(cat(2,data_ws{:})))];
    xlim(minmax+[-0.2*diff(minmax), 0.2*diff(minmax)])
    ylabel('targets'); xlabel({'Exponent'})

    % test linear effect
    curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}, data_nooutlier{3, 1}, data_nooutlier{4, 1}];
    X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
    [~, p, ci, stats] = ttest(IndividualSlopes);
    title(['M:', num2str(round(mean(IndividualSlopes),3)), '; p=', num2str(round(p,3))])
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = ['l2_peakpower'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% correlate linear slope changes with drift rates

load(fullfile(pn.summary, 'data', 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

idx_sumID = ismember(STSWD_summary.IDs, IDs);

idxYA = idx_YA; idxOA = idx_OA;

%color = [.5 .7 .8];
color = [.7 .2 .2];

h = figure('units','centimeters','position',[10 10 8 7]);
cla; hold on;
x = exponents_load';
y = squeeze(nanmean(STSWD_summary.HDDM_vat.driftMRI(idx_sumID,1:4),2));
scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color);  
[r, p, RL, RU] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
if p(2)>10^-3; tit = [', p = ', num2str(round(p(2),2))]; else; tit = sprintf(', p = %.1e',p(2)); end
title(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
    ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
    tit, ])
xlabel({'exp. mod.'}); ylabel({'drift rate';'(avg.)'})
ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
set(h,'Color','w')
for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
set(findall(gcf,'-property','FontSize'),'FontSize',14)

figureName = ['l2_exponent_drift_corr'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

disp(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])
    
% partialcorr:
h = figure('units','centimeters','position',[10 10 8 7]);
X = [x,y, cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[r, p] = partialcorr(X);
X = [repmat(1,numel(x),1),cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[b, tmp, x_r] = regress(x,X);
[b, tmp, y_r] = regress(y,X);
scatter(x_r, y_r, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
[r, ~, RL, RU] = corrcoef(x_r, y_r);
if p(2)>10^-3; tit = [' p = ', num2str(round(p(2),2))]; else; tit = sprintf(' p = %.1e',p(2)); end
disp(['r(',num2str(numel(x)-1-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit])

%% correlate linear peak changes with drift rates

load(fullfile(pn.summary, 'data', 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

idx_sumID = ismember(STSWD_summary.IDs, IDs);

idxYA = idx_YA; idxOA = idx_OA;

%color = [.5 .7 .8];
color = [.7 .2 .2];

h = figure('units','centimeters','position',[10 10 8 7]);
cla; hold on;
x = peakpower_load';
y = squeeze(nanmean(STSWD_summary.HDDM_vat.driftMRI(idx_sumID,1:4),2));
scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color);  
[r, p, RL, RU] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
if p(2)>10^-3; tit = [', p = ', num2str(round(p(2),2))]; else; tit = sprintf(', p = %.1e',p(2)); end
title(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
    ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
    tit, ])
xlabel({'exp. mod.'}); ylabel({'drift rate';'(avg.)'})
ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
set(h,'Color','w')
for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
set(findall(gcf,'-property','FontSize'),'FontSize',14)

figureName = ['l2_exponent_drift_corr'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

disp(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])
    
% partialcorr:
h = figure('units','centimeters','position',[10 10 8 7]);
X = [x,y, cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[r, p] = partialcorr(X);
X = [repmat(1,numel(x),1),cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[b, tmp, x_r] = regress(x,X);
[b, tmp, y_r] = regress(y,X);
scatter(x_r, y_r, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
[r, ~, RL, RU] = corrcoef(x_r, y_r);
if p(2)>10^-3; tit = [' p = ', num2str(round(p(2),2))]; else; tit = sprintf(' p = %.1e',p(2)); end
disp(['r(',num2str(numel(x)-1-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit])
    
%% correlate linear slope changes with behavioral pls brainscore (sd bold)

load(fullfile(pn.root, '..', 'pls_vartbx', 'data', 'pls', 'behavPLS_LV1.mat'), 'LV1')

idx_sumID = ismember(LV1.IDs, IDs);
idx_bsID = ismember(IDs,LV1.IDs);

LV1.IDs(ismember(LV1.IDs, IDs))

idxYA = 1:41; idxOA = 42:numel(find(idx_sumID));

%color = [.5 .7 .8];
color = [.7 .2 .2];

h = figure('units','centimeters','position',[10 10 8 7]);
cla; hold on;
x = exponents_load(idx_bsID)';
y = squeeze(LV1.BrainS(idx_sumID));
scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color);  
[r, p, RL, RU] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
if p(2)>10^-3; tit = [', p = ', num2str(round(p(2),2))]; else; tit = sprintf(', p = %.1e',p(2)); end
title(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
    ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
    tit, ])
xlabel({'exp. mod.'}); ylabel({'drift rate';'(avg.)'})
ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
set(h,'Color','w')
for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
set(findall(gcf,'-property','FontSize'),'FontSize',14)

disp(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])
    
% partialcorr:
h = figure('units','centimeters','position',[10 10 8 7]);
X = [x,y, cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[r, p] = partialcorr(X);
X = [repmat(1,numel(x),1),cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[b, tmp, x_r] = regress(x,X);
[b, tmp, y_r] = regress(y,X);
scatter(x_r, y_r, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
[r, ~, RL, RU] = corrcoef(x_r, y_r);
if p(2)>10^-3; tit = [' p = ', num2str(round(p(2),2))]; else; tit = sprintf(' p = %.1e',p(2)); end
disp(['r(',num2str(numel(x)-1-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit])
    
%% avg. exponents are correlated with drift across age groups, but not after age partialing

h = figure('units','centimeters','position',[10 10 8 7]);
cla; hold on;
x = squeeze(nanmean(exponents,2));
y = squeeze(nanmean(STSWD_summary.HDDM_vat.driftMRI(idx_sumID,1:4),2));
scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color);  
[r, p, RL, RU] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
if p(2)>10^-3; tit = [', p = ', num2str(round(p(2),2))]; else; tit = sprintf(', p = %.1e',p(2)); end
title(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
    ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
    tit, ])
xlabel({'exp. mod.'}); ylabel({'drift rate';'(avg.)'})
ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
set(h,'Color','w')
for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
set(findall(gcf,'-property','FontSize'),'FontSize',14)

disp(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])
    
% partialcorr:
h = figure('units','centimeters','position',[10 10 8 7]);
X = [x,y, cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[r, p] = partialcorr(X);
X = [repmat(1,numel(x),1),cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[b, tmp, x_r] = regress(x,X);
[b, tmp, y_r] = regress(y,X);
scatter(x_r, y_r, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
[r, ~, RL, RU] = corrcoef(x_r, y_r);
if p(2)>10^-3; tit = [' p = ', num2str(round(p(2),2))]; else; tit = sprintf(' p = %.1e',p(2)); end
disp(['r(',num2str(numel(x)-1-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])

%% median split of YA according to drift rates

% load(fullfile(pn.summary, 'data', 'STSWD_summary_YAOA.mat'), 'STSWD_summary')
% 
% % idx_sumID = ismember(STSWD_summary.IDs, IDs(idx_YA));
% % curDrift = STSWD_summary.HDDM_vat.driftMRI(idx_sumID,1);
% % [sortVal, sortIdx] = sort(curDrift,  'descend');
% % 
% % idx_sumID = ismember(STSWD_summary.IDs, IDs(idx_OA));
% % curDrift = STSWD_summary.HDDM_vat.driftMRI(idx_sumID,1);
% % [sortValOA, sortIdxOA] = sort(curDrift,  'descend');
% % 
% % figure;
% % subplot(2,1,1);
% % hold on;
% % l1 = plot(f, squeeze(nanmean(nanmean(fftpower_merged(sortIdx(1:floor(numel(sortIdx)/2)),1,:),2),1)), 'k', 'LineWidth', 2);
% % l4 = plot(f, squeeze(nanmean(nanmean(fftpower_merged(sortIdx(1:floor(numel(sortIdx)/2)),4,:),2),1)), 'k--', 'LineWidth', 2);
% % plot(f, squeeze(nanmean(nanmean(fftpower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),1,:),2),1)), 'r', 'LineWidth', 2);
% % plot(f, squeeze(nanmean(nanmean(fftpower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),4,:),2),1)), 'r--', 'LineWidth', 2);
% % plot(f, squeeze(nanmean(nanmean(fftpower_merged(43+sortIdxOA(1:floor(numel(sortIdxOA)/2)),1,:),2),1)), 'b', 'LineWidth', 2);
% % plot(f, squeeze(nanmean(nanmean(fftpower_merged(43+sortIdxOA(1:floor(numel(sortIdxOA)/2)),4,:),2),1)), 'b--', 'LineWidth', 2);
% % plot(f, squeeze(nanmean(nanmean(fftpower_merged(43+sortIdxOA(floor(numel(sortIdxOA)/2)+1:end),1,:),2),1)), 'g', 'LineWidth', 2);
% % plot(f, squeeze(nanmean(nanmean(fftpower_merged(43+sortIdxOA(floor(numel(sortIdxOA)/2)+1:end),4,:),2),1)), 'g--', 'LineWidth', 2);
% % xlim([0 .3]);
% % legend([l1, l4], {'L1'; 'L4'})
% % legend('boxoff')
% % xlabel('Frequency [Hz]')
% % ylabel('Spectral power')
% % subplot(2,1,2);
% % hold on;
% % lYAhigh = plot(f, squeeze(nanmean(fftpower_lin(sortIdx(1:floor(numel(sortIdx)/2)),:),1)), 'k', 'LineWidth', 2);
% % lYAlow = plot(f, squeeze(nanmean(fftpower_lin(sortIdx(floor(numel(sortIdx)/2)+1:end),:),1)), 'r', 'LineWidth', 2);
% % lOAhigh = plot(f, squeeze(nanmean(fftpower_lin(43+sortIdxOA(1:floor(numel(sortIdxOA)/2)),:),1)), 'b', 'LineWidth', 2);
% % lOAlow = plot(f, squeeze(nanmean(fftpower_lin(43+sortIdxOA(floor(numel(sortIdxOA)/2)+1:end),:),1)), 'g', 'LineWidth', 2);
% % %lOA = plot(f, squeeze(nanmean(fftpower_lin(idx_OA,:),1)), 'b', 'LineWidth', 2);
% % xlim([0 .3]);
% % legend([lYAhigh, lYAlow, lOAhigh], {'YA large drift';'YA small drift'; 'OA'})
% % legend('boxoff')
% % xlabel('Frequency [Hz]')
% % ylabel('Linear Power change')
% % set(findall(gcf,'-property','FontSize'),'FontSize',16)
% 
% % sort all subjects by drift rate
% 
% idx_sumID = ismember(STSWD_summary.IDs, IDs(idx_YA));
% curDrift = squeeze(nanmean(STSWD_summary.HDDM_vat.driftMRI(idx_sumID,1:4),2));
% [sortVal, sortIdx] = sort(curDrift,  'descend');
% 
% idx_sumID = ismember(STSWD_summary.IDs, IDs(idx_OA));
% curDrift = STSWD_summary.HDDM_vat.driftMRI(idx_sumID,1);
% [sortValOA, sortIdxOA] = sort(curDrift,  'descend');
% 
% % plot median split of young performers avg. across conditions
% 
% figure;
% hold on;
% l1 = plot(log10(f), squeeze(nanmean(nanmean(log10(fftpower_merged(sortIdx(1:floor(numel(sortIdx)/2)),1:4,:)),2),1)), 'k', 'LineWidth', 2);
% l2 = plot(log10(f), squeeze(nanmean(nanmean(log10(fftpower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),1:4,:)),2),1)), 'r', 'LineWidth', 2);
% l3 = plot(log10(f), squeeze(nanmean(nanmean(log10(fftpower_merged(43+sortIdxOA(1:floor(numel(sortIdxOA)/2)),1:4,:)),2),1)), 'b', 'LineWidth', 2);
% l4 = plot(log10(f), squeeze(nanmean(nanmean(log10(fftpower_merged(43+sortIdxOA(floor(numel(sortIdxOA)/2)+1:end),1:4,:)),2),1)), 'b--', 'LineWidth', 2);
% %l3 = plot(log10(f), squeeze(nanmean(nanmean(log10(fftpower_merged(idx_OA,1:4,:)),2),1)), 'b', 'LineWidth', 2);
% xlim([-2 -.25])
% legend([l1, l2, l3, l4], {'High YA'; 'Low YA'; 'High OA'; 'Low OA'})
% legend('boxoff')
% xlabel('Frequency [Hz]')
% ylabel('Spectral power')
% set(findall(gcf,'-property','FontSize'),'FontSize',16)
% 
% 
% figure;
% hold on;
% l1 = plot(f, squeeze(nanmean(nanmean(log10(fftpower_merged(sortIdx(1:floor(numel(sortIdx)/2)),4,:)),2),1))-...
%     squeeze(nanmean(nanmean(log10(fftpower_merged(sortIdx(1:floor(numel(sortIdx)/2)),1,:)),2),1)), 'k', 'LineWidth', 2);
% l2 = plot(f, squeeze(nanmean(nanmean(log10(fftpower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),4,:)),2),1))-...
%     squeeze(nanmean(nanmean(log10(fftpower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),1,:)),2),1)), 'r', 'LineWidth', 2);
% l3 = plot(f, squeeze(nanmean(nanmean(log10(fftpower_merged(idx_OA,4,:)),2),1))-...
%     squeeze(nanmean(nanmean(log10(fftpower_merged(idx_OA,1,:)),2),1)), 'b', 'LineWidth', 2);
% legend([l1, l2, l3], {'High YA'; 'Low YA'; 'OA'})
% legend('boxoff')
% xlabel('Frequency [Hz]')
% ylabel('Spectral power')
% set(findall(gcf,'-property','FontSize'),'FontSize',16)
% 
% keepFreq = f>.03 & f<.07 | f>.14 & f<.16 | f>.23 & f<.26 | f>.33;
% 
% figure;
% hold on;
% l1 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(fftpower_merged(sortIdx(1:floor(numel(sortIdx)/2)),1:4,keepFreq)),2),1)), 'k', 'LineWidth', 2);
% l2 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(fftpower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),1:4,keepFreq)),2),1)), 'r', 'LineWidth', 2);
% l3 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(fftpower_merged(43+sortIdxOA(1:floor(numel(sortIdxOA)/2)),1:4,keepFreq)),2),1)), 'b', 'LineWidth', 2);
% l4 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(fftpower_merged(43+sortIdxOA(floor(numel(sortIdxOA)/2)+1:end),1:4,keepFreq)),2),1)), 'b:', 'LineWidth', 2);
% %l3 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(fftpower_merged(idx_OA,1:4,keepFreq)),2),1)), 'b', 'LineWidth', 2);
% legend([l1, l2, l3], {'High YA'; 'Low YA'; 'OA'})
% xlim([-1.4 -.20])
% legend('boxoff')
% xlabel('Frequency [Hz]')
% ylabel('Spectral power')
% set(findall(gcf,'-property','FontSize'),'FontSize',16)
% 
% figure;
% hold on;
% l1 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(fftpower_merged(sortIdx(1:floor(numel(sortIdx)/2)),4,keepFreq))-...
%     log10(fftpower_merged(sortIdx(1:floor(numel(sortIdx)/2)),1,keepFreq)),2),1)), 'k', 'LineWidth', 2);
% l2 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(fftpower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),4,keepFreq))-...
%     log10(fftpower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),1:4,keepFreq)),2),1)), 'r', 'LineWidth', 2);
% l3 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(fftpower_merged(idx_OA,4,keepFreq))-...
%     log10(fftpower_merged(idx_OA,1,keepFreq)),2),1)), 'b', 'LineWidth', 2);
% legend([l1, l2, l3], {'High YA'; 'Low YA'; 'OA'})
% xlim([-1.4 -.20])
% legend('boxoff')
% xlabel('Frequency [Hz]')
% ylabel('Spectral power')
% set(findall(gcf,'-property','FontSize'),'FontSize',16)
% 
% figure;
% hold on;
% l1 = plot(f(keepFreq), squeeze(nanmean(nanmean(log10(fftpower_merged(sortIdx(1:floor(numel(sortIdx)/2)),1:4,keepFreq)),2),1)), 'k', 'LineWidth', 2);
% l2 = plot(f(keepFreq), squeeze(nanmean(nanmean(log10(fftpower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),1:4,keepFreq)),2),1)), 'r', 'LineWidth', 2);
% l3 = plot(f(keepFreq), squeeze(nanmean(nanmean(log10(fftpower_merged(idx_OA,1:4,keepFreq)),2),1)), 'b', 'LineWidth', 2);
% legend([l1, l2, l3], {'High YA'; 'Low YA'; 'OA'})
% xlim([.05 .7])
% legend('boxoff')
% xlabel('Frequency [Hz]')
% ylabel('Spectral power')
% set(findall(gcf,'-property','FontSize'),'FontSize',16)
% 
% %% perform a linear fit
% 
% freqs = log10(f(keepFreq));
% X = [ones(numel(freqs),1), freqs];
% 
% for indID = 1:size(fftpower_merged,1)
%     curData = squeeze(nanmean(log10(fftpower_merged(indID,1:4,keepFreq)),2));
%     b = regress(curData, X);
%     SpectralSlopes(indID) = b(2);
% end
% 
% figure; scatter(SpectralSlopes(sortIdx(3:end)), sortVal(3:end), 'filled')
% [r,p] = corrcoef(SpectralSlopes(sortIdx(3:end)), sortVal(3:end)')
% 
% idx_sumID = ismember(STSWD_summary.IDs, IDs);
% 
% idxYA = idx_YA; idxOA = idx_OA;
% figure; cla; hold on;
% x = SpectralSlopes;
% y = squeeze(nanmean(STSWD_summary.HDDM_vat.driftEEG(idx_sumID,1:4),2));
% %y = sortVal(3:end);
% scatter(x, y, 1, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[.2 .2 .2], 'LineWidth', 3);
% scatter(x(idxYA), y(idxYA), 70, 'filled', 'MarkerFaceColor', [1 .6 .6]);
% scatter(x(idxOA), y(idxOA), 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]);
% [r, p, RL, RU] = corrcoef(x, y);
% title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']'])
% ylabel({'Drift rate L1234'}); xlabel('Aperiodic Slope')
% ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
% xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
% 
% %% perform a linear fit for each condition
% 
% for indID = 1:size(fftpower_merged,1)
%     for indCond = 1:4
%         curData = squeeze(nanmean(log10(fftpower_merged(indID,indCond,keepFreq)),2));
%         b = regress(curData, X);
%         SpectralSlopesCond(indID,indCond) = b(2);
%     end
% end
% 
% figure;
% hold on;
% bar(mean(SpectralSlopesCond(1:floor(numel(sortIdx)/2),:),1), 'k')
% bar(mean(SpectralSlopesCond(floor(numel(sortIdx)/2)+1:end,:),1), 'r')
% bar(mean(SpectralSlopesCond(idx_OA,:),1), 'g')
% ylim([-.95 -.6])
% 
% ttest(SpectralSlopesCond(idx_YA,1), SpectralSlopesCond(idx_YA,2))
% 
% idx_sumID = ismember(STSWD_summary.IDs, IDs(idx_YA));
% curDrift = STSWD_summary.HDDM_vat.driftEEG(idx_sumID,1);
% [sortVal, sortIdx] = sort(curDrift,  'descend');
% 
% figure;
% subplot(2,1,1);
% hold on;
% l1 = plot(f, squeeze(nanmean(nanmean(fftpower_merged(sortIdx(1:floor(numel(sortIdx)/2)),1,:),2),1)), 'k', 'LineWidth', 2);
% l4 = plot(f, squeeze(nanmean(nanmean(fftpower_merged(sortIdx(1:floor(numel(sortIdx)/2)),4,:),2),1)), 'k--', 'LineWidth', 2);
% l1 = plot(f, squeeze(nanmean(nanmean(fftpower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),1,:),2),1)), 'r', 'LineWidth', 2);
% l4 = plot(f, squeeze(nanmean(nanmean(fftpower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),4,:),2),1)), 'r--', 'LineWidth', 2);
% l1 = plot(f, squeeze(nanmean(nanmean(fftpower_merged(idx_OA,1,:),2),1)), 'b', 'LineWidth', 2);
% l4 = plot(f, squeeze(nanmean(nanmean(fftpower_merged(idx_OA,4,:),2),1)), 'b--', 'LineWidth', 2);
% 
% %% split based on spectral slopes in EEG
% % 
% % figure;
% % hold on;
% % idx_sumID = ismember(STSWD_summary.IDs, IDs_YA);
% % curDrift = squeeze(nanmean(STSWD_summary.OneFslope.data(idx_sumID,1:4),2));
% % [sortVal, sortIdx] = sort(curDrift,  'descend');
% % l1 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(fftpower_merged(sortIdx(1:floor(numel(sortIdx)/2)),1:4,keepFreq)),2),1)), 'k', 'LineWidth', 2);
% % l2 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(fftpower_merged(sortIdx(floor(numel(sortIdx)/2)+1:end),1:4,keepFreq)),2),1)), 'k--', 'LineWidth', 2);
% % idx_sumID = ismember(STSWD_summary.IDs, IDs_OA);
% % curDrift = squeeze(nanmean(STSWD_summary.OneFslope.data(idx_sumID,1:4),2));
% % [sortVal, sortIdx] = sort(curDrift,  'descend');
% % l3 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(fftpower_merged(43+sortIdx(1:floor(numel(sortIdx)/2)),1:4,keepFreq)),2),1)), 'r', 'LineWidth', 2);
% % l4 = plot(log10(f(keepFreq)), squeeze(nanmean(nanmean(log10(fftpower_merged(43+sortIdx(floor(numel(sortIdx)/2)+1:end),1:4,keepFreq)),2),1)), 'r--', 'LineWidth', 2);
% % legend([l1, l2, l3], {'High YA'; 'Low YA'; 'OA'})
% % legend('boxoff')
% % xlabel('Frequency [Hz]')
% % ylabel('Spectral power')
% % set(findall(gcf,'-property','FontSize'),'FontSize',16)
% % 
% % idx_sumID = ismember(STSWD_summary.IDs, [IDs_YA; IDs_OA]);
% % %curDrift = squeeze(nanmean(STSWD_summary.OneFslope.data(idx_sumID,1:4),2));
% % curDrift = squeeze(nanmean(STSWD_summary.HDDM_vat.driftEEG(idx_sumID,1:4),2));
% % [sortVal, sortIdx] = sort(curDrift,  'descend');
% % 
% % figure; imagesc(squeeze(nanmean(log10(fftpower_merged(sortIdx,:,keepFreq)),2)))