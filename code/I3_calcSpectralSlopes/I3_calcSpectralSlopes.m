function I3_calcSpectralSlopes(ID)

    % extract temporal traces from all voxels in bilateral V5 mask to
    % subject to decoding analysis
    
    if ismac
        pn.root = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/L_V5motion/';
        pn.rootBOLD = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/G_GLM/';
        % add tools (precompiled on cluster)
        pn.tools_1 = [pn.rootBOLD, 'T_tools/NIFTI_toolbox']; addpath(genpath(pn.tools_1))
        pn.tools_2 = [pn.rootBOLD, 'T_tools/preprocessing_tools']; addpath(genpath(pn.tools_2))
    else
        pn.root = '/home/mpib/LNDG/StateSwitch/WIP/L_V5motion/';
        pn.rootBOLD = '/home/mpib/LNDG/StateSwitch/WIP/G_GLM/';
    end
    pn.standards = [pn.root, 'B_data/A_standards/'];
    pn.BOLD = [pn.rootBOLD, 'B_data/BOLDin/'];
    pn.regressors = [pn.rootBOLD, 'B_data/A_regressors/'];
    pn.out = [pn.root, 'B_data/I_spectralPower/'];
    
    pn.mask = [pn.standards, 'mni_icbm152_nlin_sym_09c/mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii'];
    [mask] = double(S_load_nii_2d(pn.mask));
    
    numConds_raw = 4; % number of conditions may have changed in PLS input structure during preproc
    
    disp(['Processing subject ', ID, '.']);

    WelchPower = NaN(32,numel(find(mask)), 129);
    for indRun = 1:numConds_raw

        % load nifti file for this run; %(x by y by z by time)
        % check for nii or nii.gz, unzip .gz and reshape to 2D

        % if proc on tardis: change path to tardis preproc directory
       
        fileName = [ID, '_run-',num2str(indRun),'_regressed.nii'];
        fname = [pn.BOLD, fileName];
        
        if ~exist(fname)
            warning(['File not available: ', ID, ' Run ', num2str(indRun)]);
            continue;
        end
        
        % If using preprocessing tools
        [img] = double(S_load_nii_2d(fname));
        
        % get time series within mask
        masksignal = img(mask==1,:); clear img;
        
        %% cut into 8-stimulus blocks
               
        load([pn.regressors, ID, '_Run',num2str(indRun),'_regressors.mat'])
        
        if strcmp(ID, '2132') && indRun == 2
            Regressors(1041-128:end) = [];
        end
        
        % get stimulus onsets
        onset_samples = find(Regressors(:,1));
        for indBlock = 1:numel(onset_samples)
            tic
            for indVoxel = 1:size(masksignal,1)
                curSignal = masksignal(indVoxel,onset_samples(indBlock):onset_samples(indBlock)+128);
                curSignal = [-1.*fliplr(curSignal), curSignal, -1.*fliplr(curSignal)];
                [pxx,f] = pwelch(curSignal,[],[],[],1/0.645);
                WelchPower((indRun-1)*8+indBlock,indVoxel,:) = pxx;
            end
            toc
        end
        
        clear masksignal;
        
    end
    
    %% WIP: get spectral slopes by voxel (but initially averaged across blocks of same dimensionality)
    
    % average across blocks
    WelchPower_merged = [];
    % sort according to block dimensionality
    curDims = [];
    for indRun = 1:4
        load([pn.regressors, ID, '_Run',num2str(indRun),'_regressors.mat'])
        curDims = [curDims; Regressors(find(Regressors(:,4)),4)];
    end
    for indDim = 1:4
        WelchPower_merged(indDim,:,:) = squeeze(nanmean(WelchPower(curDims==indDim,:,:),1));
    end
    
    keepFreq = f>.03 & f<.07 | f>.14 & f<.16 | f>.23 & f<.26 | f>.33 & f<.38 | f>.42;

%     figure;
%     for indVox = 1:100
%         plot(log10(f(keepFreq)), log10(squeeze(nanmean(WelchPower_merged(1,indVox,keepFreq),1))));
%         pause(.5);
%     end
    
    freqs = log10(f(keepFreq));
    X = [ones(numel(freqs),1), freqs];

    for indBlock = 1:size(WelchPower_merged,1)
        for indVoxel = 1:size(WelchPower,2)
            curData = squeeze(nanmean(log10(WelchPower_merged(indBlock,indVoxel,keepFreq)),2));
            b = regress(curData, X);
            SpectralSlopes(indBlock,indVoxel) = b(2);
        end
    end
    
    %% average spectral power for plotting
    
    WelchPower = squeeze(nanmean(WelchPower,2));
    
    save([pn.out, ID, '_WelchPower_byBlock.mat'], 'WelchPower', 'f', 'SpectralSlopes')
        