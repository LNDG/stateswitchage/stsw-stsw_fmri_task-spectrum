clear all; clc; restoredefaultpath

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
pn.root = pwd;

pn.plstoolbox = fullfile(pn.root, 'tools', 'pls'); addpath(genpath(pn.plstoolbox));

cd(fullfile(pn.root, 'data', 'pls'));

%batch_plsgui('behavPLS_STSWD_SPM_YA_OA_2Group_v4_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('taskPLS_STSWD_Slopes_YA_OA_2Group_3mm_1000P1000B_BfMRIanalysis.txt')

plsgui