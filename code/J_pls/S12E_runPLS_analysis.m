clear all; clc; restoredefaultpath

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
pn.root = pwd;

pn.plstoolbox = fullfile(pn.root, 'tools', 'pls'); addpath(genpath(pn.plstoolbox));

cd(fullfile(pn.root, 'data', 'pls'));

% batch_plsgui('meanPLS_STSWD_Mean_fullModel_3mm_1000P1000B_BfMRIanalysis.txt')
% batch_plsgui('meanPLS_STSWD_FunctDim_YA_OA_3mm_1000P1000B_BfMRIanalysis.txt')
% batch_plsgui('meanPLS_STSWD_FunctDim_YA_OA_type2_3mm_1000P1000B_BfMRIanalysis.txt')
% batch_plsgui('meanPLS_STSWD_FunctDim_YA_OA_type3_3mm_1000P1000B_BfMRIanalysis.txt')
% % behavioral PLS
% batch_plsgui('behavPLS_STSWD_Mean_fullModel_3mm_1000P1000B_BfMRIanalysis.txt')
% batch_plsgui('behavPLS_STSWD_FunctDim_YA_OA_3mm_1000P1000B_BfMRIanalysis.txt')
% batch_plsgui('behavPLS_STSWD_FunctDim_bl_YA_OA_3mm_1000P1000B_BfMRIanalysis.txt')

plsgui