    
    if ismac
        pn.root = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/L_V5motion/';
        pn.rootBOLD = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/G_GLM/';
    else
        pn.root = '/home/mpib/LNDG/StateSwitch/WIP/L_V5motion/';
        pn.rootBOLD = '/home/mpib/LNDG/StateSwitch/WIP/G_GLM/';
    end
    pn.standards = [pn.root, 'B_data/A_standards/'];
    pn.BOLD = [pn.rootBOLD, 'B_data/BOLDin/'];
    pn.regressors = [pn.rootBOLD, 'B_data/A_regressors/'];
    pn.in = [pn.root, 'B_data/B_singleTrialV5/'];
    pn.out = [pn.root, 'B_data/I_spectralPower/'];
    
    addpath([pn.rootBOLD, 'T_tools/NIFTI_toolbox']);
    addpath([pn.rootBOLD, 'T_tools/preprocessing_tools']);
    
    IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2261'};

    pn.vcMask = [pn.standards, 'VisualCortex_unified_thr_MNI_3mm.nii'];
    [vcMask] = double(S_load_nii_2d(pn.vcMask));
    
    pn.GMmask = [pn.standards, 'mni_icbm152_nlin_sym_09c/mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii'];
    [GMmask] = double(S_load_nii_2d(pn.GMmask));

%     for indID = 1:numel(IDs)
%         load([pn.in, IDs{indID}, '_VC_WelchPower_byBlock.mat'], 'SpectralSlopes')
%         T1w = load_nii([pn.rootBOLD, 'B_data/E_standards/mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii']);
%         for indLoad = 1:4
%             nii_coords=zeros(size(T1w.img,1),size(T1w.img,2),size(T1w.img,3));
%             nii_coords(vcMask==1) = SpectralSlopes(indLoad,:);
%             T1w.img = nii_coords;
%             save_nii(T1w, [pn.out, IDs{indID}, '_VC_Slopes_L',num2str(indLoad),'.nii'])
%         end
%     end
    
    for indID = 1:numel(IDs)
        load([pn.out, IDs{indID}, '_WelchPower_byBlock.mat'], 'SpectralSlopes')
        T1w = load_nii([pn.rootBOLD, 'B_data/E_standards/mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii']);
        for indLoad = 1:4
            nii_coords=zeros(size(T1w.img,1),size(T1w.img,2),size(T1w.img,3));
            nii_coords(GMmask==1) = SpectralSlopes(indLoad,:);
            T1w.img = nii_coords;
            save_nii(T1w, [pn.out, IDs{indID}, '_Slopes_L',num2str(indLoad),'.nii'])
        end
    end

    %% average slopes across all subjects
    
    for indID = 1:numel(IDs)
        for indLoad = 1:4
            dataIn = load_nii([pn.out, IDs{indID}, '_Slopes_L',num2str(indLoad),'.nii']);
            dataMerged(indID,indLoad,:,:,:) = dataIn.img;
        end
    end
    dataIn.img = squeeze(nanmean(nanmean(dataMerged,2),1));
    save_nii(dataIn, [pn.out, 'Slopes_all.nii'])
    