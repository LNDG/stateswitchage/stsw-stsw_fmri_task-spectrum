function K1_calcPowerPLS(rootpath, ID)

    % extract temporal traces from all voxels that are part of behavioral PLS
    
    if ismac
        currentFile = mfilename('fullpath');
        [pathstr,~,~] = fileparts(currentFile);
        cd(fullfile(pathstr,'..', '..'))
        rootpath = pwd;
        ID = '1117';
    end
    
    pn.tools = fullfile(rootpath, 'tools');
        addpath(fullfile(pn.tools, 'preprocessing_tools'))
        addpath(fullfile(pn.tools, 'nifti_toolbox'))
    pn.standards = fullfile(rootpath, 'data', 'standards', 'data');
    pn.varpls = fullfile(rootpath, '..', 'pls_vartbx', 'data', 'pls');
    pn.BOLD = fullfile(rootpath, '..', 'extended_preproc', 'data', 'data_clean');
    pn.regressors = fullfile(rootpath, '..', 'design_timing', 'data', 'regressors');
    pn.out = fullfile(rootpath, 'data', 'spectralPower');
    
    pn.mask = fullfile(pn.varpls, 'taskPLS_STSWD_VarTbx_YA_OA_3mm_1000P1000B_BfMRIbsr_lv1.img');
    [mask] = double(S_load_nii_2d(pn.mask));
    
    numConds_raw = 4; % number of conditions may have changed in PLS input structure during preproc
    
    disp(['Processing subject ', ID, '.']);

    WelchPower = NaN(32,numel(find(mask)), 129);
    for indRun = 1:numConds_raw

        % load nifti file for this run; %(x by y by z by time)
        % check for nii or nii.gz, unzip .gz and reshape to 2D

        % if proc on tardis: change path to tardis preproc directory
       
        fileName = [ID, '_run-',num2str(indRun),'_regressed.nii'];
        fname = fullfile(pn.BOLD, fileName);
        
        if ~exist(fname)
            warning(['File not available: ', ID, ' Run ', num2str(indRun)]);
            continue;
        end
        
        % If using preprocessing tools
        [img] = double(S_load_nii_2d(fname));
        
        % get time series within mask
        masksignal = img(mask~=0,:); clear img;
        
        %% cut into 8-stimulus blocks
               
        load(fullfile(pn.regressors, [ID, '_Run',num2str(indRun),'_regressors.mat']))
        
        if strcmp(ID, '2132') && indRun == 2
            Regressors(1041-128:end) = [];
        end
        
        % get stimulus onsets
        onset_samples = find(Regressors(:,1));
        for indBlock = 1:numel(onset_samples)
            tic
            for indVoxel = 1:size(masksignal,1)
                curSignal = masksignal(indVoxel,onset_samples(indBlock):onset_samples(indBlock)+128);
                curSignal = [-1.*fliplr(curSignal), curSignal, -1.*fliplr(curSignal)];
                [pxx,f] = pwelch(curSignal,[],[],[],1/0.645);
                WelchPower((indRun-1)*8+indBlock,indVoxel,:) = pxx;
            end
            disp(['Block ', num2str(indBlock), ' finished: ', num2str(toc)])
        end
        
        clear masksignal;
        
    end
    
    %% WIP: get spectral slopes by voxel (but initially averaged across blocks of same dimensionality)
    
    % average across blocks
    WelchPower_merged = [];
    % sort according to block dimensionality
    curDims = [];
    for indRun = 1:4
        load(fullfile(pn.regressors, [ID, '_Run',num2str(indRun),'_regressors.mat']))
        curDims = [curDims; Regressors(find(Regressors(:,4)),4)];
    end
    for indDim = 1:4
        WelchPower_merged(indDim,:,:) = squeeze(nanmean(WelchPower(curDims==indDim,:,:),1));
    end
    
    keepFreq = f>.03 & f<.07 | f>.14 & f<.16 | f>.23 & f<.26 | f>.33 & f<.38 | f>.42;

    freqs = log10(f(keepFreq));
    X = [ones(numel(freqs),1), freqs];

    for indBlock = 1:size(WelchPower_merged,1)
        for indVoxel = 1:size(WelchPower,2)
            curData = squeeze(nanmean(log10(WelchPower_merged(indBlock,indVoxel,keepFreq)),2));
            b = regress(curData, X);
            SpectralSlopes(indBlock,indVoxel) = b(2);
        end
    end
    
    %% average spectral power for plotting
    
    WelchPower = squeeze(nanmean(WelchPower,2));
    
%     TrialSignal = []; 
%     for indRun = 1:numConds_raw
% 
%         % load nifti file for this run; %(x by y by z by time)
%         % check for nii or nii.gz, unzip .gz and reshape to 2D
% 
%         % if proc on tardis: change path to tardis preproc directory
%        
%         fname = fullfile(pn.BOLD, [ID, '_run-',num2str(indRun),'_regressed.nii']);
%         
%         if ~exist(fname)
%             warning(['File not available: ', ID, ' Run ', num2str(indRun)]);
%             continue;
%         end
%         
%         % If using preprocessing tools
%         [img] = double(S_load_nii_2d(fname));
%         
%         % get time series within mask
%         targetsignals = img(Mask~=0,:);
%         
%         %% cut into 8-stimulus blocks
%                
%         load(fullfile(pn.regressors, [ID, '_Run',num2str(indRun),'_regressors.mat']))
%         
%         if strcmp(ID, '2132') && indRun == 2
%             Regressors(1041-128:end) = [];
%         end
%         
%         % get stimulus onsets
%         onset_samples = find(Regressors(:,1));
%         for indOnset = 1:numel(onset_samples)            
%             BlockSignal((indRun-1)*64+indOnset,:,:) = targetsignals(:,onset_samples(indOnset):onset_samples(indOnset)+128);
%         end
%         
%     end
%     
%     %% spectral power estimation
%     
%     %WelchPower = NaN(size(BlockSignal,1), size(BlockSignal,2), 129);
%     parfor indBlock = 1:size(BlockSignal,1)
%         tmp = [];
%         for indVoxel = 1:size(BlockSignal,2)
%             curSignal = squeeze(BlockSignal(indBlock, indVoxel,:))';
%             curSignal = [-1.*fliplr(curSignal), curSignal, -1.*fliplr(curSignal)];
%             [pxx,f] = pwelch(curSignal,[],[],[],1/0.645);
%             tmp(indVoxel,:) = pxx;
%         end
%         WelchPower(indBlock,:,:) = tmp;
%     end
%     
%     %% WIP: get spectral slopes by voxel (but initially averaged across blocks of same dimensionality)
%     
%     % average across blocks
%     WelchPower_merged = [];
%     % sort according to block dimensionality
%     curDims = [];
%     for indRun = 1:4
%         load(fullfile(pn.regressors, [ID, '_Run',num2str(indRun),'_regressors.mat']))
%         curDims = [curDims; Regressors(find(Regressors(:,4)),4)];
%     end
%     for indDim = 1:4
%         WelchPower_merged(indDim,:,:) = squeeze(nanmean(WelchPower(curDims==indDim,:,:),1));
%     end
%     
%     keepFreq = f>.03 & f<.07 | f>.14 & f<.16 | f>.23 & f<.26 | f>.33 & f<.38 | f>.42;
% 
% %     figure;
% %     for indVox = 1:100
% %         plot(log10(f(keepFreq)), log10(squeeze(nanmean(WelchPower_merged(1,indVox,keepFreq),1))));
% %         pause(.5);
% %     end
%     
%     freqs = log10(f(keepFreq));
%     X = [ones(numel(freqs),1), freqs];
% 
%     for indBlock = 1:size(WelchPower_merged,1)
%         for indVoxel = 1:size(WelchPower,2)
%             curData = squeeze(nanmean(log10(WelchPower_merged(indBlock,indVoxel,keepFreq)),2));
%             b = regress(curData, X);
%             SpectralSlopes(indBlock,indVoxel) = b(2);
%         end
%     end
%     
%     %% average spectral power for plotting
%     
%     WelchPower = squeeze(nanmean(WelchPower,2));
%     
% %     figure; 
% %     hold on;
% %     for indBlock = 1:8
% %         plot(log10(f), log10(squeeze(nanmean(WelchPower(indBlock,:),1))));
% %     end
% %        
% %     
% %     figure; 
% %     hold on;
% %     for indBlock = 1:8
% %         plot(f, squeeze(nanmean(WelchPower(indBlock,:,:),1)));
% %     end
    
    save(fullfile(pn.out, [ID, '_varpls_WelchPower_byBlock.mat']), 'WelchPower', 'WelchPower_merged', 'f', 'SpectralSlopes')

end