%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wrapper for timelock & frequency analysis.  %
% Last modified: Jan. 15, 2014                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_TimelockFreq(BuilderMat, AnalysisType)


% Make sure java paths added:
UseProgBar = CheckParforJavaPaths;


% Load builder .mat file:
Builder      = load(BuilderMat);
name         = Builder.name;
paths        = Builder.paths;
time         = Builder.time;
FTcfg        = Builder.FTcfg;


% Clear existing Errorlog & Diary:
switch AnalysisType
    case 'Timelock'
        RunSection = 'TimelockAnalysis';
    case 'Freq'
        RunSection = 'FreqAnalysis';
end

if exist(['ErrorLog_',RunSection,'.txt'], 'file')
    system(['rm ErrorLog_',RunSection,'.txt']);
end
if exist(['Diary_',RunSection,'.txt'], 'file')
    system(['rm Diary_',RunSection,'.txt']);
end

diary(['Diary_',RunSection,'.txt']);
ErrLog = fopen(['ErrorLog_',RunSection,'.txt'], 'a');



%=====================================%
% RUN TIMELOCK OR FREQUENCY ANALYSIS: %
%=====================================%

for g = 1:length(name.GroupID)
    NumSubj = length(name.SubjID{g});
    
    if UseProgBar == 1
        ppm = ParforProgMon...
            (['TIMELOCK / FREQUENCY ANALYSIS:  ',name.GroupID{g},'.  '], NumSubj, 1, 700, 80);
    end
    
	parfor s = 1:length(name.SubjID{g})
		for c = 1:length(name.CondID)
            
            CheckInput = CheckPipelineMat(paths.MEGdata{g}{s,c}, RunSection);
            if CheckInput == 0
                continue;
            end
            
            
            % Resize input data to analysis time:
            cfgResize           = [];
            cfgResize.toilim    = [time.Start, time.End];
            cfgResize.inputfile = paths.MEGdata{g}{s,c};
            
            ResizedData = ft_redefinetrial(cfgResize)
			
            
            % Run timelock & GMFP analysis:
            if strcmp(AnalysisType, 'Timelock')
                CheckSavePerms(paths.Timelock{g}{s,c}, RunSection);
                
                cfgTimelock                   = [];
                cfgTimelock                   = FTcfg.Timelock;
                cfgTimelock.outputfile        = paths.Timelock{g}{s,c};
                cfgTimelock.outputfilepresent = 'overwrite';
                
                disp('Running timelock analysis:')
                ft_timelockanalysis(cfgTimelock, ResizedData)
                
                
                if exist(paths.Timelock{g}{s,c}, 'file')
                    CheckSavePerms(paths.GlobalMeanField{g}{s,c}, RunSection);
                    
                    cfgGFP                   = [];
                    cfgGFP                   = FTcfg.GFP;
                    cfgGFP.inputfile         = paths.Timelock{g}{s,c};
                    cfgGFP.outputfile        = paths.GlobalMeanField{g}{s,c};
                    cfgGFP.outputfilepresent = 'overwrite';
                    
                    ft_globalmeanfield(cfgGFP)  % Compute GMFP from new timelock file.
                end
            end
            
            
            % Run frequency analysis:
            if strcmp(AnalysisType, 'Freq')
                CheckSavePerms(paths.Freq{g}{s,c}, RunSection);
                
                cfgFreq                   = [];
                cfgFreq                   = FTcfg.Freq;
                cfgFreq.outputfile        = paths.Freq{g}{s,c};
                cfgFreq.outputfilepresent = 'overwrite';
                
                disp('Running frequency analysis:')
                ft_freqanalysis(cfgFreq, ResizedData)
            end
            
            ResizedData = [];  % Free memory
			
		end  % Cond
        
        if UseProgBar == 1 && mod(s, 1) == 0
            ppm.increment();  % move up progress bar
        end
        
	end  % Subj
    if UseProgBar == 1
        ppm.delete();
    end
    
end  % Group



%=========================%
% CHECK FOR OUTPUT FILES: %
%=========================%

for g = 1:length(name.GroupID)
	for s = 1:length(name.SubjID{g})
		for c = 1:length(name.CondID)
            
            switch AnalysisType
                case 'Timelock'
                    if ~exist(paths.Timelock{g}{s,c}, 'file')
                        fprintf(ErrLog, ['ERROR: Output timelock file missing:'...
                            '\n %s \n\n'], paths.Timelock{g}{s,c});
                    end
                    if ~exist(paths.GlobalMeanField{g}{s,c}, 'file')
                        fprintf(ErrLog, ['ERROR: Output global mean field file missing:'...
                            '\n %s \n\n'], paths.GlobalMeanField{g}{s,c});
                    end
                    
                case 'Freq'
                    if ~exist(paths.Freq{g}{s,c}, 'file')
                        fprintf(ErrLog, ['ERROR: Output freq analysis file missing:'...
                            '\n %s \n\n'], paths.Freq{g}{s,c});
                    end
            end
            
		end  % Cond
	end  % Subj
end  % Group



%=================%

if exist([pwd,'/ErrorLog_',RunSection,'.txt'], 'file')
    LogCheck = dir(['ErrorLog_',RunSection,'.txt']);
    if LogCheck.bytes ~= 0  % File not empty
        open(['ErrorLog_',RunSection,'.txt']);
    else
        delete(['ErrorLog_',RunSection,'.txt']);
    end
end

fclose(ErrLog);
diary off

end
