%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Converts functional NIFTI files to AFNI:     %
% Inputs: Normalised NIFTI source volumes.     %
% Last modified: Jan. 15, 2014                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_Nifti4DFunc2AFNI_Wrap(BuilderMat, TargetNiftiType)


% Make sure java paths added:
UseProgBar = CheckParforJavaPaths;


% Load builder .mat file:
Builder      = load(BuilderMat);
name         = Builder.name;
paths        = Builder.paths;
time         = Builder.time;
PipeSettings = Builder.PipeSettings;


% Clear existing Errorlog & Diary:
switch TargetNiftiType
    case 'Nifti4DSource'
        RunSection = 'MakeAfni4DSource';
        
    case 'Nifti4DNormSource'
        RunSection = 'MakeAfni4DNormSource';
        
    case 'Nifti4DGrpAvg'
        RunSection = 'MakeAfni4DGrpAvg';
end

if exist(['ErrorLog_',RunSection,'.txt'], 'file')
    system(['rm ErrorLog_',RunSection,'.txt']);
end
if exist(['Diary_',RunSection,'.txt'], 'file')
    system(['rm Diary_',RunSection,'.txt']);
end

diary(['Diary_',RunSection,'.txt']);
ErrLog = fopen(['ErrorLog_',RunSection,'.txt'], 'a');



%===========================================%
% CONVERTS 4D-NIFTI FILES INTO AFNI-FORMAT: %
%===========================================%

for g = 1:length(name.GroupID)
    NumCond = length(name.CondID);
    
    if UseProgBar == 1
        ppm = ParforProgMon...
            (['CONVERTING SOURCE FILES TO AFNI:  ',name.GroupID{g},'.  '], NumCond, 1, 700, 80);
    end
    
    % Note: Do NOT parfor this due to 3dWarp command.
    for c = 1:length(name.CondID)
        for s = 1:length(name.SubjID{g})
            
            
            %--- Make 4D AFNI file for interp. source files: ---%
            %---------------------------------------------------%
            
            if strcmp(TargetNiftiType, 'Nifti4DSource')
                
                % Convert to AFNI:
                if ~exist(paths.Nifti4DSource{g}{s,c}, 'file')
                    fprintf(ErrLog, ['ERROR: Input 4D-NIFTI file missing:'...
                        '\n %s \n\n'], paths.Nifti4DSource{g}{s,c});
                    continue;
                end
                
                CheckSavePath(paths.Afni4DSource{g}{s,c}, RunSection);
                
                MEGpipeline_Nifti2Afni_Guts(PipeSettings, ...
                    paths.Nifti4DSource{g}{s,c}, paths.Afni4DSource{g}{s,c})
                
                
                % Deoblique non-normalised sources for viewing:
                % Note: Deoblique command outputs file "warped+tlrc.BRIK/HEAD" in current dir.
                if exist('warped+tlrc.BRIK', 'file') || exist('warped+tlrc.HEAD', 'file')
                    delete('warped+tlrc.BRIK');
                    delete('warped+tlrc.HEAD');
                    
                    if exist('warped+tlrc.BRIK', 'file')
                        disp('ERROR: Could not remove remnant "warped+tlrc" files.');
                        fprintf(ErrLog, ['ERROR: Failed to deoblique non-normalised source file:'...
                            '\n %s \n\n'], paths.Afni4DSource{g}{s,c});
                        continue;
                    end
                end
                
                system(['3dWarp -deoblique ',paths.Afni4DSource{g}{s,c}]);
                
                
                % Remove and replace oblique files with deobliqued ones:
                [TargetFolder, TargetFile, ~] = fileparts(paths.Afni4DSource{g}{s,c});
                delete([TargetFolder,'/',TargetFile,'.BRIK']);
                delete([TargetFolder,'/',TargetFile,'.HEAD']);
                
                if exist(paths.Afni4DSource{g}{s,c}, 'file')
                    disp('ERROR: Could not replace oblique MRI files with deobliqued ones.');
                    fprintf(ErrLog, ['ERROR: Failed to deoblique non-normalised source file:'...
                            '\n %s \n\n'], paths.Afni4DSource{g}{s,c});
                    continue;
                end
                
                system(['3dcopy warped+tlrc.BRIK ',paths.Afni4DSource{g}{s,c}]);
                delete('warped+tlrc.BRIK');
                delete('warped+tlrc.HEAD');    
            end
            
            
            %--- Make 4D AFNI file for norm source files: ---%
            %------------------------------------------------%
            
            if strcmp(TargetNiftiType, 'Nifti4DNormSource')
                
                if ~exist(paths.Nifti4DNormSource{g}{s,c}, 'file')
                    fprintf(ErrLog, ['ERROR: Input 4D-NIFTI file missing:'...
                        '\n %s \n\n'], paths.Nifti4DNormSource{g}{s,c});
                    continue;
                end
                
                CheckSavePath(paths.Afni4DNormSource{g}{s,c}, RunSection);
                
                MEGpipeline_Nifti2Afni_Guts(PipeSettings, ...
                    paths.Nifti4DNormSource{g}{s,c}, paths.Afni4DNormSource{g}{s,c})
            end
            
        end  % Subj
        
        
        %--- Make 4D AFNI file for group average normsource files: ---%
        %-------------------------------------------------------------%
        
        if strcmp(TargetNiftiType, 'Nifti4DGrpAvg')
            
            if ~exist(paths.Nifti4DGrpAvg{g}{c}, 'file')
                fprintf(ErrLog, ['ERROR: Input 4D-NIFTI file missing:'...
                    '\n %s \n\n'], paths.Nifti4DGrpAvg{g}{c});
                continue;
            end
            
            CheckSavePath(paths.Afni4DGrpAvg{g}{c}, RunSection);
            
            MEGpipeline_Nifti2Afni_Guts(PipeSettings, ...
                paths.Nifti4DGrpAvg{g}{c}, paths.Afni4DGrpAvg{g}{c})
        end
        
        
        if UseProgBar == 1 && mod(c, 1) == 0
            ppm.increment();  % move up progress bar
        end
        
    end  % Cond
    if UseProgBar == 1
        ppm.delete();
    end
    
end  % Group



%==========================================================%
% GENERATES TIME-LEGEND FOR INDICES IN NIFTI & AFNI FILES: %
%==========================================================%

if exist([paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt'], 'file')
	system(['rm ',paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt']);
end
TimeLegend = fopen('NiftiAfni4D_TimeLegend.txt', 'a');

fprintf(TimeLegend, ['/// IMPORTANT: ///'...
	'\n - In the AFNI-VIEWER, the time-index for images starts at 0.     '...
	'\n - In the data however, the time-index for images starts at 1. \n '...
	'\n - Therefore, when selecting time-indices for use in the data,    '...
	'\n   be careful NOT use the index from the viewer. \n\n']);

fprintf(TimeLegend, ['/// TIME LEGEND: ///'...
	'\n AFNI-Viewer ------- AFNI/NIFTI ------- TIME-Interval:'...
	'\n ViewerIndex ------- Data Index ------- In seconds:']);

for t = 1:size(time.Windows, 1)
	fprintf(TimeLegend, '\n---- %s --------------- %s ------------ %s',...
		num2str(t-1), num2str(t), time.str.Windows{t,3});
end

fclose(TimeLegend);
if ~isequal(pwd, paths.AnalysisID)
    movefile('NiftiAfni4D_TimeLegend.txt', [paths.AnalysisID,'/'], 'f');
end



%=========================%
% CHECK FOR OUTPUT FILES: %
%=========================%

for g = 1:length(name.GroupID)
    for c = 1:length(name.CondID)
        for s = 1:length(name.SubjID{g})
            
            if strcmp(TargetNiftiType, 'Nifti4DSource')
                if ~exist(paths.Afni4DSource{g}{s,c}, 'file')
                    fprintf(ErrLog, ['ERROR: Output 4D-AFNI file missing:'...
                        '\n %s \n\n'], paths.Afni4DSource{g}{s,c});
                end
            
            elseif strcmp(TargetNiftiType, 'Nifti4DNormSource')
                if ~exist(paths.Afni4DNormSource{g}{s,c}, 'file')
                    fprintf(ErrLog, ['ERROR: Output 4D-AFNI file missing:'...
                        '\n %s \n\n'], path.Afni4DNormSource{g}{s,c});
                end
                
            end
        end  % Subj
        
        if strcmp(TargetNiftiType, 'Nifti4DGrpAvg')
            if ~exist(paths.Afni4DGrpAvg{g}{c}, 'file')
                fprintf(ErrLog, ['ERROR: Output 4D-AFNI file missing:'...
                        '\n %s \n\n'], path.Afni4DGrpAvg{g}{c});
            end
        end
        
    end  % Cond
end  % Group

if ~exist([paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt'], 'file')
	fprintf(ErrLog, ['ERROR: Time-legend file for NIFTI/AFNI files missing:'...
		'\n %s \n\n'], [paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt']);
end



%=================%

if exist([pwd,'/ErrorLog_',RunSection,'.txt'], 'file')
    LogCheck = dir(['ErrorLog_',RunSection,'.txt']);
    if LogCheck.bytes ~= 0  % File not empty
        open(['ErrorLog_',RunSection,'.txt']);
    else
        delete(['ErrorLog_',RunSection,'.txt']);
    end
end

if exist([pwd,'/ErrorLog_Nifti2Afni.txt'], 'file')
    LogCheck = dir('ErrorLog_Nifti2Afni.txt');
    if LogCheck.bytes ~= 0  % File not empty
        open('ErrorLog_Nifti2Afni.txt');
    else
        delete('ErrorLog_Nifti2Afni.txt');
    end
end

fclose(ErrLog);
diary off
