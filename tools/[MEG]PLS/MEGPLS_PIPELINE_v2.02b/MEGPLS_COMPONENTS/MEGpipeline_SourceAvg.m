%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wrapper for group-averaging normsource files. %
% Inputs: Processed MEG data                    %
% Last modified: Jan. 15, 2014                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_SourceAvg(BuilderMat)


% Make sure java paths added:
% Note: Using parfor progress bar even though no parfor.
% - Other components already using it, and check needs to be run anyway.
UseProgBar = CheckParforJavaPaths;


% Load builder .mat file:
Builder      = load(BuilderMat);
name         = Builder.name;
paths        = Builder.paths;
time         = Builder.time;
FTcfg        = Builder.FTcfg;


% Clear existing Errorlog & Diary:
if exist('ErrorLog_SourceAvg.txt', 'file')
    system('rm ErrorLog_SourceAvg.txt');
end
if exist('Diary_SourceAvg.txt', 'file')
    system('rm Diary_SourceAvg.txt');
end

diary Diary_SourceAvg.txt
ErrLog = fopen('ErrorLog_SourceAvg.txt', 'a');



%=============================================%
% GROUP-AVERAGE NORMSOURCE FILES ACROSS SUBJ: %
%=============================================%

AvgParam = FTcfg.SrcAvg.parameter;
if strcmp(AvgParam(1:4), 'avg.')
    AvgParam(1:4) = [];
end


for g = 1:length(name.GroupID)
    NumCond = length(name.CondID);
    
    if UseProgBar == 1
        ppm = ParforProgMon...
            (['GROUP-AVERAGING SOURCE FILES:  ',name.GroupID{g},'.  '], NumCond, 1, 700, 80);
    end
    
    % Note: Do NOT parfor here, want proper ordering.
	for c = 1:length(name.CondID)
        
        
        %--- Get new "inside" data for group-average: ---%
        %------------------------------------------------%
        
        % Only keep "inside" voxels shared by all subjects in the group:
        % If any errors occur here, output file will not have an "inside" field.
        clear LoadSource NewInsideData NumDatasets
        
        NewInsideData = [];
        MaskErrors    = 0;
        
        for s = 1:length(name.SubjID{g})
            LoadSource = LoadFTmat(paths.NormSource{g}{s,c,1}, 'SourceAvg');
            
            if isempty(LoadSource)  % Note: Only need to load first time-window.
                MaskErrors = 1;     % - All time-windows for given g, s, c should share same inside.
                continue;
            end
            
            if isempty(NewInsideData)
                NewInsideData = LoadSource.inside;
                NumDatasets   = 1;
                
            else
                if ~isequal(size(NewInsideData), size(LoadSource.inside))
                    fprintf(ErrLog, ['ERROR: NormSource dataset has different dimensions.'...
                        '\n Cannot acquire "inside" data for group-average mask.'...
                        '\n %s \n\n'], paths.NormSource{g}{s,c,1});
                    
                    MaskErrors = 1;  % Make sure dims are the same
                    continue;
                end
                
                NewInsideData = NewInsideData + LoadSource.inside;  % Sum all inside
                NumDatasets   = NumDatasets + 1;
            end
            
            LoadSource = [];  % Free memory
        end  % Subj
        
        if MaskErrors == 0
            NewInsideData = NewInsideData ./ NumDatasets;  % Average
            NewInsideData(find(NewInsideData < 1)) = 0;   % Only keep those that 100% subjs in grp have.
            NewInsideData = logical(NewInsideData);
        
        else
            fprintf(ErrLog, ['ERROR: Failed to acquire new "inside" indices for grp-avg.'...
                '\n Output files will contain voxels not shared by all subjects in the group.'...
                '\n As a result, masking around the edges of the grp-avg image may be odd.']);
        end
        
        
        %--- Compile datasets & group-average: ---%
        %-----------------------------------------%
        
		for t = 1:size(time.Windows, 1)
            SrcAvg         = [];  % Reset input
            SourceAvgInput = [];
            MissingFiles   = 0;
			
            
			% Make sure subject files exist for each time:			
            for s = 1:length(name.SubjID{g})
                if ~exist(paths.NormSource{g}{s,c,t}, 'file')
                    if MissingFiles == 0
                        MissingFiles = 1;
                        fprintf(ErrLog, '\nERROR: NormSource .mat file(s) missing: \n');
                    end
                    
                    fprintf(ErrLog, ' - File: %s \n', paths.NormSource{g}{s,c,t});
                    continue;
                end
            end
            
            
            % Load & compile data to average:
            if MissingFiles == 0
                for s = 1:length(name.SubjID{g})
                    SourceAvgInput{s} = LoadFTmat(paths.NormSource{g}{s,c,t}, 'SourceAvg');
                    
                    if isempty(SourceAvgInput{s})
                        MissingFiles = 1;
                        continue;
                    end
                    
                    % Only avg indices that all subjects in group share:
                    if MaskErrors == 0
                        SourceAvgInput{s}.avg.OutputStat(find(NewInsideData < 1)) = NaN;
                    end
                    
                    % Rmv these fields to prevent warnings flooding command window:
                    % Note: These fields won't be used in the group-average.
                    SourceAvgInput{s} = rmfield(SourceAvgInput{s}, 'coordsys');
                    SourceAvgInput{s} = rmfield(SourceAvgInput{s}, 'initial');
                    SourceAvgInput{s} = rmfield(SourceAvgInput{s}, 'params');
                    SourceAvgInput{s} = rmfield(SourceAvgInput{s}, 'inside');
                    SourceAvgInput{s} = rmfield(SourceAvgInput{s}, 'anatomy');
                    
                    % Rename "avg.OutputStat" field to "avg.nai" for now (prevents warnings):
                    if strcmp(AvgParam, 'OutputStat')
                        SourceAvgInput{s}.avg.nai = SourceAvgInput{s}.avg.OutputStat;
                        SourceAvgInput{s}.avg     = rmfield(SourceAvgInput{s}.avg, 'OutputStat');
                    end
                    
                end  % Subj
            end
            
            
            % If any files are missing, skip iteration:
            if MissingFiles == 1
                fprintf(ErrLog, ['ERROR: Failed to grp-avg source data for time-window'...
                    '(missing subj data): \n %s \n\n'], paths.GrpAvg{g}{c,t});
                
				disp('ERROR: Missing .mat file(s) for GPAVG. Skipping time-window.')
				continue;
            end
			
            
			% Average NormSource files:
            cfgSrcAvg = [];
            cfgSrcAvg = FTcfg.SrcAvg;
            
            if strcmp(AvgParam, 'OutputStat')
                cfgSrcAvg.parameter = 'nai';  % Since we renamed it earlier on
            else
                cfgSrcAvg.parameter = AvgParam;
            end
            
            SrcAvg = ft_sourcegrandaverage(cfgSrcAvg, SourceAvgInput{:})
            
            
            % Load new "inside" and replace fields:
            if MaskErrors == 0
                SrcAvg.inside = NewInsideData;
            else
                SrcAvg = rmfield(SrcAvg, 'inside');
            end
            
            if strcmp(AvgParam, 'OutputStat')
                SrcAvg.OutputStat       = SrcAvg.nai;  % Rename field back to OutputStat
                SrcAvg.OutputStatdimord = SrcAvg.naidimord;
                SrcAvg.cfg.parameter    = 'OutputStat';
                
                SrcAvg = rmfield(SrcAvg, 'nai');
                SrcAvg = rmfield(SrcAvg, 'naidimord');
            end
            
            CheckSavePath(paths.GrpAvg{g}{c,t}, 'SourceAvg');
            save(paths.GrpAvg{g}{c,t}, 'SrcAvg');
            
            SrcAvg      = [];  % Free memory
            SrcAvgInput = [];
			
		end  % Time
        
        if UseProgBar == 1 && mod(c, 1) == 0
            ppm.increment();  % move up progress bar
        end
        
	end  % Cond
    if UseProgBar == 1
        ppm.delete();
    end
    
end  % Group



%=========================%
% CHECK FOR OUTPUT FILES: %
%=========================%

for g = 1:length(name.GroupID)
	for c = 1:length(name.CondID)
		for t = 1:size(time.Windows, 1)
			
			if ~exist(paths.GrpAvg{g}{c,t}, 'file')
				fprintf(ErrLog, ['ERROR: Output source group-avg file missing:'...
					'\n %s \n\n'], paths.GrpAvg{g}{c,t});
			end
			
		end  % Time
	end  % Cond
end  % Group



%=================%

if exist([pwd,'/ErrorLog_SourceAvg.txt'], 'file')
    LogCheck = dir('ErrorLog_SourceAvg.txt');
    if LogCheck.bytes ~= 0  % File not empty
        open('ErrorLog_SourceAvg.txt');
    else
        delete('ErrorLog_SourceAvg.txt');
    end
end

fclose(ErrLog);
diary off
