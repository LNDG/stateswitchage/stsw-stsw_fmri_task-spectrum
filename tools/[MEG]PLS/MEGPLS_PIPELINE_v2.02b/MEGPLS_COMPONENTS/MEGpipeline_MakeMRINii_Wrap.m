%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Creates NIFTI files for anatomical MRIs.    %
% Last modified: Jan. 15, 2014                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_MakeMRINii_Wrap(BuilderMat, TargetMRIType)


% Make sure java paths added:
UseProgBar = CheckParforJavaPaths;


% Load builder .mat file:
Builder      = load(BuilderMat);
name         = Builder.name;
paths        = Builder.paths;
MainFTcfg    = Builder.FTcfg;
PipeSettings = Builder.PipeSettings;


% Clear existing Errorlog & Diary:
switch TargetMRIType
    case 'PreprocMRI'
        RunSection = 'MakePreprocMRINii';
        
    case 'NormMRI'
        RunSection = 'MakeNormMRINii';
end

if exist(['ErrorLog_',RunSection,'.txt'], 'file')
    system(['rm ErrorLog_',RunSection,'.txt']);
end
if exist(['Diary_',RunSection,'.txt'], 'file')
    system(['rm Diary_',RunSection,'.txt']);
end

diary(['Diary_',RunSection,'.txt']);
ErrLog = fopen(['ErrorLog_',RunSection,'.txt'], 'a');



%======================================%
% GENERATE NIFTI FILES FOR PREPROCMRI: %
%======================================%

for g = 1:length(name.GroupID)
    NumSubj = length(name.SubjID{g});
    
    if UseProgBar == 1
        ppm = ParforProgMon...
            (['GENERATING MRI NIFTI FILES:  ',name.GroupID{g},'.  '], NumSubj, 1, 700, 80);
    end
    
    parfor s = 1:length(name.SubjID{g})
        
        if strcmp(TargetMRIType, 'PreprocMRI')
            LoadedMRI  = LoadFTmat(paths.MRIdata{g}{s}, RunSection); 
            OutputPath = paths.MRIdataNii{g}{s};
            
        elseif strcmp(TargetMRIType, 'NormMRI')
            LoadedMRI  = LoadFTmat(paths.NormMRI{g}{s}, RunSection);
            OutputPath = paths.NormMRINii{g}{s};
        end
        
        if isempty(LoadedMRI)
            continue;
        end
        
        % For PreprocMRI, do approximate conversion to RAS orientation for viewing:
        if strcmp(TargetMRIType, 'PreprocMRI')
            LoadedMRI = ft_convert_coordsys(LoadedMRI, 'spm');
        end
        
        % Write 3D NIFTI:
        cfgWriteMRI           = [];
        cfgWriteMRI           = MainFTcfg.WriteNormMRI;
        cfgWriteMRI.parameter = 'anatomy';
        cfgWriteMRI.datatype  = 'double';
        cfgWriteMRI.scaling   = 'no';
        
        CheckSavePath(OutputPath, RunSection);
        MEGpipeline_MakeNifti3D_Guts(cfgWriteMRI, LoadedMRI, OutputPath)
        
        LoadedMRI = [];  % Free memory
        
        if UseProgBar == 1 && mod(s, 1) == 0
            ppm.increment();  % move up progress bar
        end
        
    end  % Subj
    if UseProgBar == 1
        ppm.delete();
    end
    
end  % Group



%==========================%
% CHECKS FOR OUTPUT FILES: %
%==========================%

for g = 1:length(name.GroupID)
    for s = 1:length(name.SubjID{g})
        
        if strcmp(TargetMRIType, 'PreprocMRI')
            if ~exist(paths.MRIdataNii{g}{s}, 'file')
                fprintf(ErrLog, ['ERROR: Output 3D-NIFTI file missing:'...
                    '\n %s \n\n'], paths.MRIdataNii{g}{s});
            end
            
        elseif strcmp(TargetMRIType, 'NormMRI')
            if ~exist(paths.NormMRINii{g}{s}, 'file')
                fprintf(ErrLog, ['ERROR: Output 3D-NIFTI file missing:'...
                    '\n %s \n\n'], paths.NormMRINii{g}{s});
            end
        end
        
    end  % Subj
end  %Group

        

%=================%

if exist([pwd,'/ErrorLog_',RunSection,'.txt'], 'file')
    LogCheck = dir(['ErrorLog_',RunSection,'.txt']);
    if LogCheck.bytes ~= 0  % File not empty
        open(['ErrorLog_',RunSection,'.txt']);
    else
        delete(['ErrorLog_',RunSection,'.txt']);
    end
end

if exist([pwd,'/ErrorLog_MakeNifti3D.txt'], 'file')
    LogCheck = dir('ErrorLog_MakeNifti3D.txt');
    if LogCheck.bytes ~= 0  % File not empty
        open('ErrorLog_MakeNifti3D.txt');
    else
        delete('ErrorLog_MakeNifti3D.txt');
    end
end

fclose(ErrLog);
diary off
