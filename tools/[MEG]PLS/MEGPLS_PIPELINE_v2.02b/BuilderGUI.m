function varargout = BuilderGUI(varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compiles input files & analysis settings for pipeline. %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


% Last Modified by GUIDE v2.5 13-Mar-2014 14:35:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @BuilderGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @BuilderGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


%--- Executes just before BuilderGUI is made visible. ---%
%--------------------------------------------------------%
function BuilderGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to BuilderGUI (see VARARGIN)

% Choose default command line output for BuilderGUI
handles.output = hObject;


% Make sure toolbox paths are added:
[PipelineDir, ~, ~] = fileparts(which('BuilderGUI.m'));
handles.PipelineDir = PipelineDir;

addpath(genpath(PipelineDir));
rmpath([PipelineDir,'/DEFAULT_SETTINGS']);  % Make sure its calling from AnalysisID
rmpath([PipelineDir,'/TEMPORARY_FIXES']);   % Make sure its calling from FT toolbox

CheckToolboxPaths(PipelineDir);

if exist('ErrorLog_BuilderGUI.txt', 'file')
    system('rm ErrorLog_BuilderGUI.txt');
end


% Initializes variables being displayed:
handles.paths.Rootpath   = [];
handles.name.AnalysisID  = [];
handles.paths.AnalysisID = [];

handles.name.GroupID = [];
handles.name.SubjID  = [];
handles.name.CondID  = [];

handles.gui.MEGdataFiletype = 'Fieldtrip';
handles.gui.AnatID          = [];
handles.gui.InputAnatFiles  = [];
handles.gui.InputDataFiles  = [];
handles.gui.StatusAnat      = [];
handles.gui.StatusData      = [];
handles.gui.CheckICAclean   = [];  % For input FT files, checks if ICAcleaned (for check at end).

handles.gui.CommonFilterCondID  = [];
handles.gui.ControlCondID       = [];

handles.gui.CommonFiltDataFiles = [];
handles.gui.ControlDataFiles    = [];

handles.time         = [];
handles.FTcfg        = [];
handles.PipeSettings = [];

% Update handles structure
guidata(hObject, handles);


% UIWAIT makes BuilderGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


%--- Outputs from this function are returned to the command line. ---%
%--------------------------------------------------------------------%
function varargout = BuilderGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;




%=================================%
% FUNCTIONS FOR SETTING ROOTPATH: %
%=================================%

%--- Executes on button press in ButtonSetRootpath. ---%
%------------------------------------------------------%
function ButtonSetRootpath_Callback(hObject, eventdata, handles)
if ~isempty(handles.name.AnalysisID)
	prompt = {'WARNING:';
		'Changing root directory will clear all current settings!'; '';
		'Do you wish to continue?'};
	
	ChangeDir = questdlg(prompt, 'WARNING:', 'YES', 'NO', 'NO');
	if strcmp(ChangeDir, 'NO')
		return;
	end
end
	
% Select and set Rootpath:
SelectedPath = uigetdir([], 'Select target directory:');
if SelectedPath == 0
	return;  % If user cancels
end

% Check if selected path has spaces (AFNI functions cannot read spaces):
CheckSpaces = strfind(SelectedPath, ' ');
if ~isempty(CheckSpaces)
    message = {'Error: Target directory selected contains spaces.';
        'AFNI functions cannot read folder & file paths with spaces.';
        'AFNI functions are required for file conversions later on.'};
    msgbox(message, 'Error:')
    return;
end

handles.paths.Rootpath = SelectedPath;
set(handles.TextboxRootpath, 'String', handles.paths.Rootpath);

% Start Waitbox:
WaitBox = StartWaitBox('SETTING TARGET DIRECTORY:');

% Disable buttons for settings .m files: ** Upgrade to subgui
set(handles.ButtonHdmAdvSettings,           'Enable', 'off');
set(handles.ButtonTimelockFreqAdvSettings,  'Enable', 'off');
set(handles.ButtonSourceAdvSettings,        'Enable', 'off');
set(handles.ButtonNormSettings,             'Enable', 'off');

% Reset AnalysisID:
handles.name.AnalysisID  = [];
handles.paths.AnalysisID = [];
set(handles.TextboxAnalysisID, 'String', 'Not Selected.');

% Reset all input and settings:
handles = ResetGroupIDSettings	 (handles);
handles = ResetSubjIDSettings	 (handles);
handles = ResetCondIDSettings	 (handles);
handles = ResetInputMRI			 (handles);
handles = ResetInputMEG			 (handles);

handles = ResetTimeSettings		 (handles);
handles = ResetHdmLeadSettings	 (handles);
handles = ResetTimeFreqSettings	 (handles);
handles = ResetSourceSettings	 (handles);
handles = ResetNiftiAfniSettings (handles);

% Redetect RootpathFolders & available CondIDs:
handles = DetectRootpathFolders (handles);
handles = DetectCondIDFolders   (handles);

% Update input and settings:
handles = UpdateGroupIDSettings	  (handles);
handles = UpdateSubjIDSettings	  (handles);
handles = UpdateCondIDSettings	  (handles);
handles = UpdateInputMRI		  (handles);
handles = UpdateInputMEG		  (handles);

handles = UpdateTimeSettings	  (handles);
handles = UpdateHdmLeadSettings	  (handles);
handles = UpdateTimeFreqSettings  (handles);
handles = UpdateSourceSettings	  (handles);
handles = UpdateNormaliseSettings (handles);
handles = UpdateNiftiAfniSettings (handles);

% Save handles:
guidata(hObject, handles);
close(WaitBox);


%--- Textbox to display selected Rootpath: ---%
%---------------------------------------------%
function TextboxRootpath_Callback(hObject, eventdata, handles)
EnteredText = get(handles.TextboxRootpath, 'String');

if ~isequal(EnteredText, handles.paths.Rootpath)
	set(handles.TextboxRootpath, 'String', handles.paths.Rootpath);
	msgbox('Note: Use button to change Rootpath.')
end




%===============================================%
% FUNCTIONS FOR CREATING OR LOADING ANALYSISID: %
%===============================================%

%--- Executes on button press in ButtonCreateAnalysisID. ---%
%-----------------------------------------------------------%
function ButtonCreateAnalysisID_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Select root directory first.', 'Warning:');
	return;
end

% Enter name of AnalysisID:
UserInput = inputdlg('Enter AnalysisID name:', 'Create new AnalysisID:');
UserInput = deblank(UserInput);
UserInput = regexprep(UserInput, '\s+', '_');
if isempty(char(UserInput))
	return;  % If user cancels
end

PathInputID = [handles.paths.Rootpath,'/AnalysisID_',UserInput{1}];
if exist(PathInputID, 'dir')
	msgbox('Error: Specified AnalysisID already exists!', 'Error:')
	return;
end

% Create AnalysisID & advanced settings folder:
status = mkdir([PathInputID,'/SETTINGS/']);
if status == 0  % If mkdir failed
	msgbox('Error: Failed to create AnalysisID folder.', 'Error:')
	return;
end

% Create advanced settings .m files:
DefaultSettingsDir = [handles.PipelineDir,'/DEFAULT_SETTINGS'];

AdvSettingsFiles   = {
	'Settings_SegHdmLeadfield.m';
	'Settings_TimelockFreqAnalysis.m';
    'Settings_TimelockFreqPlot.m';
	'Settings_SourceAnalysis.m';
	'Settings_SourceNormalise.m'};

for f = 1:length(AdvSettingsFiles)
	status = copyfile([DefaultSettingsDir,'/',AdvSettingsFiles{f}], ...
		[PathInputID,'/SETTINGS/'], 'f');
	
	if status == 0
		message = {'ERROR:'; '';
			'Failed to create advanced settings .m files for AnalysisID.';
            'Could not copy files from [MEG]PLS "DEFAULT_SETTINGS" directory.';
            'Check for proper folder and file permissions.'};
		msgbox(message, 'ERROR:')
        
        rmdir(PathInputID, 's');  % Remove remnant folder
		return;
	end
end

set(handles.ButtonHdmAdvSettings,           'Enable', 'on');
set(handles.ButtonTimelockFreqAdvSettings,  'Enable', 'on');
set(handles.ButtonSourceAdvSettings,        'Enable', 'on');
set(handles.ButtonNormSettings,             'Enable', 'on');

% Set as current AnalysisID:
handles.name.AnalysisID  = UserInput{1};
handles.paths.AnalysisID = PathInputID;
set(handles.TextboxAnalysisID, 'String', handles.name.AnalysisID);

% Start Waitbox:
WaitBox = StartWaitBox('CREATING ANALYSIS ID:');

% Reset all input and settings:
handles = ResetGroupIDSettings	 (handles);
handles = ResetSubjIDSettings	 (handles);
handles = ResetCondIDSettings	 (handles);
handles = ResetInputMRI			 (handles);
handles = ResetInputMEG			 (handles);

handles = ResetTimeSettings		 (handles);
handles = ResetHdmLeadSettings	 (handles);
handles = ResetTimeFreqSettings	 (handles);
handles = ResetSourceSettings	 (handles);
handles = ResetNiftiAfniSettings (handles);

% Redetect RootpathFolders & available CondIDs:
handles = DetectRootpathFolders (handles);
handles = DetectCondIDFolders   (handles);

% Update input and settings:
handles = UpdateGroupIDSettings	  (handles);
handles = UpdateSubjIDSettings	  (handles);
handles = UpdateCondIDSettings	  (handles);
handles = UpdateInputMRI		  (handles);
handles = UpdateInputMEG		  (handles);

handles = UpdateTimeSettings	  (handles);
handles = UpdateHdmLeadSettings	  (handles);
handles = UpdateTimeFreqSettings  (handles);
handles = UpdateSourceSettings	  (handles);
handles = UpdateNormaliseSettings (handles);
handles = UpdateNiftiAfniSettings (handles);

% Save handles:
guidata(hObject, handles);
close(WaitBox);


%--- Executes on button press in ButtonLoadAnalysisID. ---%
%---------------------------------------------------------%
function ButtonLoadAnalysisID_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Select root directory first.', 'Warning:');
	return;
end

% Acquire AnalysisID folders in currently selected rootpath:
DetectedFolders = dir([handles.paths.Rootpath,'/AnalysisID_*']);
RemoveIndex = [];
for d = 1:length(DetectedFolders)
    if DetectedFolders(d).isdir ~= 1
        RemoveIndex = [RemoveIndex, d];  % Get unwanted indices
    end
end
DetectedFolders(RemoveIndex) = [];
if isempty(DetectedFolders)
    msgbox('Error: No AnalysisID folders detected inside root dir.', 'Error:');
    return;
end

% List detected AnalysisID folders:
DetectedFolders = {DetectedFolders.name};
SelectedIndex = listdlg('PromptString', 'Select AnalysisID to load:',...
    'ListSize', [300, 300], 'SelectionMode', 'single', 'ListString', DetectedFolders);
if isempty(SelectedIndex)
    return;  % If user cancels.
end

SelectedAnalysisID		 = DetectedFolders{SelectedIndex};
SelectedAnalysisID(1:11) = [];  % Remove "AnalysisID_" prefix to get just the ID.
PathAnalysisID			 = [handles.paths.Rootpath,'/AnalysisID_',SelectedAnalysisID];

% Check for advanced settings .m files:
AdvSettingsFiles = {
	'Settings_SegHdmLeadfield.m';
	'Settings_TimelockFreqAnalysis.m';
    'Settings_TimelockFreqPlot.m';
	'Settings_SourceAnalysis.m';
	'Settings_SourceNormalise.m'};

for f = 1:length(AdvSettingsFiles)
	if ~exist([PathAnalysisID,'/SETTINGS/',AdvSettingsFiles{f}], 'file')
		message = {'ERROR:'; '';
			'Failed to find advanced settings .m files for AnalysisID.';
			'Create new AnalysisID or manually import settings .m files.'};
		msgbox(message, 'ERROR:');
		return;
	end
end

set(handles.ButtonHdmAdvSettings,          'Enable', 'on');
set(handles.ButtonTimelockFreqAdvSettings, 'Enable', 'on');
set(handles.ButtonSourceAdvSettings,       'Enable', 'on');
set(handles.ButtonNormSettings,            'Enable', 'on');

% Set as current AnalysisID:
handles.name.AnalysisID  = SelectedAnalysisID;
handles.paths.AnalysisID = PathAnalysisID;
set(handles.TextboxAnalysisID, 'String', handles.name.AnalysisID);

% Start Waitbox:
WaitBox = StartWaitBox('LOADING ANALYSIS ID:');

% Reset all input and settings:
handles = ResetGroupIDSettings	 (handles);
handles = ResetSubjIDSettings	 (handles);
handles = ResetCondIDSettings	 (handles);
handles = ResetInputMRI			 (handles);
handles = ResetInputMEG			 (handles);

handles = ResetTimeSettings		 (handles);
handles = ResetHdmLeadSettings	 (handles);
handles = ResetTimeFreqSettings	 (handles);
handles = ResetSourceSettings	 (handles);
handles = ResetNiftiAfniSettings (handles);

% Load Builder .mat for AnalysisID:
BuilderFile = [handles.paths.AnalysisID,'/Builder_',handles.name.AnalysisID,'.mat'];

if exist(BuilderFile, 'file')
    LoadedMat = load(BuilderFile);

	handles = LoadGroupIDSettings	(handles, LoadedMat);
	handles = LoadSubjIDSettings	(handles, LoadedMat);
	handles = LoadCondIDSettings	(handles, LoadedMat);
	handles = LoadInputMRI			(handles, LoadedMat);
	handles = LoadInputMEG			(handles, LoadedMat);
	
	handles = LoadTimeSettings		(handles, LoadedMat);
	handles = LoadHdmLeadSettings	(handles, LoadedMat);
	handles = LoadTimeFreqSettings	(handles, LoadedMat);
	handles = LoadSourceSettings	(handles, LoadedMat);
    handles = LoadNormaliseSettings (handles, LoadedMat);
	handles = LoadNiftiAfniSettings (handles, LoadedMat);
	
else
    message = {'WARNING:'; ''; 
		'Builder .mat was not found for loaded AnalysisID.';
        'Input and settings will need to be reselected.'};
    msgbox(message, 'WARNING:')
end

% Redetect RootpathFolders & available CondIDs:
handles = DetectRootpathFolders (handles);
handles = DetectCondIDFolders   (handles);

% Update input and settings:
handles = UpdateGroupIDSettings	  (handles);
handles = UpdateSubjIDSettings	  (handles);
handles = UpdateCondIDSettings	  (handles);
handles = UpdateInputMRI		  (handles);  % DetectInputMRI already called in LoadInputMRI
handles = UpdateInputMEG		  (handles);  % DetectInputMEG already called in LoadInputMEG

handles = UpdateTimeSettings	  (handles);
handles = UpdateHdmLeadSettings	  (handles);
handles = UpdateTimeFreqSettings  (handles);
handles = UpdateSourceSettings	  (handles);
handles = UpdateNormaliseSettings (handles);
handles = UpdateNiftiAfniSettings (handles);

% Save handles:
guidata(hObject, handles);
close(WaitBox);


%--- Textbox to display current AnalysisID: ---%
%----------------------------------------------%
function TextboxAnalysisID_Callback(hObject, eventdata, handles)
EnteredText = get(handles.TextboxAnalysisID, 'String');

if ~isequal(EnteredText, handles.name.AnalysisID)
	set(handles.TextboxAnalysisID, 'String', handles.name.AnalysisID);
	msgbox('Note: Use button to change AnalysisID.')
end




%========================================%
% FUNCTIONS TO IMPORT OR RESET SETTINGS: %
%========================================%

%--- Executes on button press in ButtonImportSettings. ---%
%---------------------------------------------------------%
function ButtonImportSettings_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Select root directory first.', 'Warning:');
	return;
end
if isempty(handles.name.AnalysisID) || isempty(handles.paths.AnalysisID)
    msgbox('Warning: Create or load AnalysisID first.', 'Warning:');
    return;
end

% Confirm load:
prompt = {'WARNING:'; '';
	'Loaded settings will overwrite preprocessing settings for current AnalysisID!';
	'Advanced settings .m files for current AnalysisID will also be overwritten.'; '';
	'Do you wish to continue?'};
ContinueLoad = questdlg(prompt, 'WARNING:', 'YES', 'NO', 'NO');
if strcmp(ContinueLoad, 'NO')
    return;
end

% Get AnalysisID folders in current Rootpath:
DetectedFolders = dir([handles.paths.Rootpath,'/AnalysisID_*']);
RemoveIndex = [];
for d = 1:length(DetectedFolders)
    if DetectedFolders(d).isdir ~= 1
        RemoveIndex = [RemoveIndex, d];  % Get unwanted indices
    end
end
DetectedFolders(RemoveIndex) = [];
if isempty(DetectedFolders)
    msgbox('Error: No AnalysisID folders detected inside root dir.', 'Error:');
    return;
end

% List AnalysisID folders to import settings from:
DetectedFolders = {DetectedFolders.name};
SelectedIndex   = listdlg('PromptString', 'Import settings from:',...
    'ListSize', [300, 300], 'SelectionMode', 'single', 'ListString', DetectedFolders);
if isempty(SelectedIndex)
    return;  % If user cancels.
end

SelectedAnalysisID		 = DetectedFolders{SelectedIndex};
SelectedAnalysisID(1:11) = [];  % Remove "AnalysisID_" prefix to get just the ID.
PathSelectedAnalysisID	 = [handles.paths.Rootpath,'/AnalysisID_',SelectedAnalysisID];

% Check if files exist for selected AnalysisID:
BuilderFile = [PathSelectedAnalysisID,'/Builder_',SelectedAnalysisID,'.mat'];
if ~exist(BuilderFile, 'file')
	msgbox('Error: Failed to find Builder .mat for selected AnalysisID.', 'Error:')
	return;
end

AdvSettingsFiles = {
	'Settings_SegHdmLeadfield.m';
	'Settings_TimelockFreqAnalysis.m';
    'Settings_TimelockFreqPlot.m';
	'Settings_SourceAnalysis.m';
	'Settings_SourceNormalise.m'};

for f = 1:length(AdvSettingsFiles)
	if ~exist([PathSelectedAnalysisID,'/SETTINGS/',AdvSettingsFiles{f}], 'file')
		msgbox('Error: Adv. settings .m file(s) missing for selected AnalysisID.', 'Error:')
		return;
	end
end

% Start Waitbox:
WaitBox = StartWaitBox('LOADING PREPROC SETTINGS:');

% Reset basic settings:
handles = ResetTimeSettings		 (handles);
handles = ResetHdmLeadSettings	 (handles);
handles = ResetTimeFreqSettings	 (handles);
handles = ResetSourceSettings	 (handles);
handles = ResetNormaliseSettings (handles);
handles = ResetNiftiAfniSettings (handles);

% Import basic settings from builder .mat:
LoadedMat = load(BuilderFile);
handles = LoadTimeSettings		(handles, LoadedMat);
handles = LoadHdmLeadSettings	(handles, LoadedMat);
handles = LoadTimeFreqSettings	(handles, LoadedMat);
handles = LoadSourceSettings	(handles, LoadedMat);
handles = LoadNormaliseSettings (handles, LoadedMat);
handles = LoadNiftiAfniSettings (handles, LoadedMat);

% Import advanced settings .m files:
for f = 1:length(AdvSettingsFiles)
    delete([handles.paths.AnalysisID,'/SETTINGS/',AdvSettingsFiles{f}]);
	
	status = copyfile([PathSelectedAnalysisID,'/SETTINGS/',AdvSettingsFiles{f}], ...
		[handles.paths.AnalysisID,'/SETTINGS/'], 'f');
	
	if status == 0
		msgbox('Error: Failed to import adv. settings from AnalysisID.', 'Error:')
	end
end

% Update settings:
handles = UpdateTimeSettings	  (handles);
handles = UpdateHdmLeadSettings	  (handles);
handles = UpdateTimeFreqSettings  (handles);
handles = UpdateSourceSettings	  (handles);
handles = UpdateNormaliseSettings (handles);
handles = UpdateNiftiAfniSettings (handles);

% Save handles:
guidata(hObject, handles);
close(WaitBox);


%--- Executes on button press in ButtonResetSettings. ---%
%--------------------------------------------------------%
function ButtonResetSettings_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Select root directory first.', 'Warning:');
	return;
end
if isempty(handles.name.AnalysisID) || isempty(handles.paths.AnalysisID)
    msgbox('Warning: Create or load AnalysisID first.', 'Warning:');
    return;
end

% Confirm reset:
prompt = {'WARNING:'; '';
	'You are about to reset all preprocessing settings for this AnalysisID.';
	'Advanced settings .m files will for selected AnalysisID will also be reset.'; '';
	'Do you wish to continue?'};
ContinueReset = questdlg(prompt, 'WARNING:', 'YES', 'NO', 'NO');
if strcmp(ContinueReset, 'NO')
    return;
end

% Reset advanced settings .m files:
DefaultSettingsDir = [handles.PipelineDir,'/DEFAULT_SETTINGS'];

AdvSettingsFiles   = {
	'Settings_SegHdmLeadfield.m';
	'Settings_TimelockFreqAnalysis.m';
    'Settings_TimelockFreqPlot.m';
	'Settings_SourceAnalysis.m';
	'Settings_SourceNormalise.m'};

for f = 1:length(AdvSettingsFiles)
    delete([handles.paths.AnalysisID,'/SETTINGS/',AdvSettingsFiles{f}]);
	
	status = copyfile([DefaultSettingsDir,'/',AdvSettingsFiles{f}], ...
		[handles.paths.AnalysisID,'/SETTINGS/'], 'f');

	if status == 0
		msgbox('Error: Failed to reset adv. settings .m file(s).', 'Error:')
		return;
	end
end

% Start Waitbox:
WaitBox = StartWaitBox('RESETTING PREPROC SETTINGS:');

% Reset settings:
handles = ResetTimeSettings		 (handles);
handles = ResetHdmLeadSettings	 (handles);
handles = ResetTimeFreqSettings	 (handles);
handles = ResetSourceSettings	 (handles);
handles = ResetNormaliseSettings (handles);
handles = ResetNiftiAfniSettings (handles);

% Update settings:
handles = UpdateTimeSettings	  (handles);
handles = UpdateHdmLeadSettings	  (handles);
handles = UpdateTimeFreqSettings  (handles);
handles = UpdateSourceSettings	  (handles);
handles = UpdateNormaliseSettings (handles);
handles = UpdateNiftiAfniSettings (handles);

% Save handles:
guidata(hObject, handles);
close(WaitBox);




%==========================================%
% FUNCTIONS FOR ADDING OR REMOVING GROUPS: %
%==========================================%

%--- Executes on selection change in ListboxGroupID. ---%
%-------------------------------------------------------%
function ListboxGroupID_Callback(hObject, eventdata, handles)
handles = UpdateSubjIDSettings (handles);
handles = UpdateInputMRI	   (handles);
handles = UpdateInputMEG	   (handles);
guidata(hObject, handles);


%--- Executes on button press in ButtonAddGroupID. ---%
%-----------------------------------------------------%
function ButtonAddGroupID_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Select target directory first.', 'Warning:');
	return;
end
if isempty(handles.name.AnalysisID) || isempty(handles.paths.AnalysisID)
    msgbox('Warning: Create or load AnalysisID first.', 'Warning:');
    return;
end

% Enter name of GroupID:
UserInput = inputdlg('Enter GroupID name:', 'Manually specify GroupID:');
UserInput = deblank(UserInput);
UserInput = regexprep(UserInput, '\s+', '_');
if isempty(char(UserInput))
	return;  % If user cancels
end

InputGroupID = UserInput{1};
if ismember(InputGroupID, handles.name.GroupID)
    msgbox('Note: GroupID has already been added.', 'Note:')
	return;
end

% Set GroupID:
handles.name.GroupID = [handles.name.GroupID; cellstr(InputGroupID)];

NewGroupIndex = length(handles.name.GroupID);
handles.name.SubjID{NewGroupIndex} = [];  % Initialize
set(handles.ListboxGroupID, 'Value', NewGroupIndex);

handles.gui.InputAnatFiles {NewGroupIndex} = [];
handles.gui.StatusAnat     {NewGroupIndex} = [];

handles.gui.InputDataFiles {NewGroupIndex} = [];
handles.gui.StatusData     {NewGroupIndex} = [];
handles.gui.CheckICAclean  {NewGroupIndex} = [];

% Update input files:
handles = UpdateGroupIDSettings (handles);
handles = UpdateSubjIDSettings  (handles);
handles = UpdateInputMRI	    (handles);  % Only initialize, don't detect for now
handles = UpdateInputMEG	    (handles);  % so user does not have to wait between add.

% Save handles:
guidata(hObject, handles);


%--- Executes on button press in ButtonRmvGroupID. ---%
%-----------------------------------------------------%
function ButtonRmvGroupID_Callback(hObject, eventdata, handles)
if isempty(handles.name.GroupID)
    return;
end

SelectedIndex = get(handles.ListboxGroupID, 'Value');
handles.name.GroupID (SelectedIndex) = [];
handles.name.SubjID  (SelectedIndex) = [];

if ~isempty(handles.gui.InputAnatFiles)
    handles.gui.InputAnatFiles (SelectedIndex) = [];
    handles.gui.StatusAnat     (SelectedIndex) = [];
end
if ~isempty(handles.gui.InputDataFiles)
    handles.gui.InputDataFiles (SelectedIndex) = [];
    handles.gui.StatusData     (SelectedIndex) = [];
    handles.gui.CheckICAclean  (SelectedIndex) = [];
end

% Update GUI & input files:
handles = UpdateGroupIDSettings (handles);
handles = UpdateSubjIDSettings  (handles);
handles = UpdateInputMRI	    (handles);
handles = UpdateInputMEG	    (handles);

% Save handles:
guidata(hObject, handles);


%--- Executes on button press in ButtonLoadGroupSubj. ---%
%--------------------------------------------------------%
function ButtonLoadGroupSubj_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Select root directory first.', 'Warning:');
	return;
end
if isempty(handles.name.AnalysisID) || isempty(handles.paths.AnalysisID)
    msgbox('Warning: Create or load AnalysisID first.', 'Warning:');
    return;
end

% Load PreprocInput or Builder .mat:
[matfile, matpath] = uigetfile({[handles.paths.Rootpath,...
    '/PreprocInputMRI_*.mat; PreprocInputMEG_*; Builder_*.mat']},...
	'Load GroupID & SubjID from .mat file:', 'MultiSelect', 'off');

if matfile == 0
    return;  % If user cancels
else
	LoadedMat = load([matpath,matfile]);
end

% Clear conflicting groups:
if strfind(matfile, 'PreprocInput') == 1
    Overlap = find(ismember(handles.name.GroupID, LoadedMat.name.CurrentGroupID));
    
elseif strfind(matfile, 'Builder_') == 1
	if ~isempty(handles.name.GroupID)
		Overlap = find(ismember(handles.name.GroupID, LoadedMat.name.GroupID));
	else
		Overlap = [];
	end
    
else
    msgbox('Error: Unrecognized .mat type.', 'Error:')
    return;
end

if ~isempty(Overlap)
	handles.name.GroupID (Overlap) = [];
	handles.name.SubjID  (Overlap) = [];
end

% Start Waitbox:
WaitBox = StartWaitBox('LOADING GROUP & SUBJ INPUT:');

% Load GroupID and SubjID:
if strfind(matfile, 'PreprocInput') == 1
	handles.name.GroupID = [handles.name.GroupID; cellstr(LoadedMat.name.CurrentGroupID)];
	handles.name.SubjID  = [handles.name.SubjID,  {LoadedMat.name.SubjID}];
	
elseif strfind(matfile, 'Builder_') == 1
	handles.name.GroupID = [handles.name.GroupID; LoadedMat.name.GroupID];
	handles.name.SubjID  = [handles.name.SubjID,  LoadedMat.name.SubjID];
	
else
    msgbox('Error: Unrecognized .mat type.', 'Error:')
    return;
end

% Update Group & SubjIDs:
handles = UpdateGroupIDSettings (handles);
handles = UpdateSubjIDSettings  (handles);

% Detect input files & update status:
for g = 1:length(handles.name.GroupID)
    DetectWaitbar = waitbar(0, ...
        ['Scanning input MRI files for: ',handles.name.GroupID{g}], 'Windowstyle', 'modal');
    
    for s = 1:length(handles.name.SubjID{g})
        waitbar(s/length(handles.name.SubjID{g}), DetectWaitbar);
        handles = DetectInputMRI(handles, g, s);
    end
    delete(DetectWaitbar)
end

for g = 1:length(handles.name.GroupID)
    DetectWaitbar = waitbar(0, ...
        ['Scanning input MEG files for: ',handles.name.GroupID{g}], 'Windowstyle', 'modal');
    
    for s = 1:length(handles.name.SubjID{g})
        waitbar(s/length(handles.name.SubjID{g}), DetectWaitbar);
        
        for c = 1:size(handles.gui.AddedCondIDs, 1)
            handles = DetectInputMEG(handles, g, s, c);
        end
    end
    delete(DetectWaitbar)
end

handles = UpdateInputMRI (handles);
handles = UpdateInputMEG (handles);

% Save handles:
guidata(hObject, handles);
close(WaitBox);


%--- Update GroupID settings & GUI panel: ---%
%--------------------------------------------%
function OutputHandles = UpdateGroupIDSettings(InputHandles)
handles = InputHandles;

% Update listbox:
set(handles.ListboxGroupID, 'String', handles.name.GroupID);

CurrentIndex  = get(handles.ListboxGroupID, 'Value');
MaxGroupIndex = length(handles.name.GroupID);

if isempty(CurrentIndex) || CurrentIndex == 0 || CurrentIndex > MaxGroupIndex
	set(handles.ListboxGroupID, 'Value', MaxGroupIndex);
end

% Set output handles:
OutputHandles = handles;


%--- Load GroupID settings & GUI panel: ---%
%------------------------------------------%
function OutputHandles = LoadGroupIDSettings(InputHandles, LoadedMat)
handles = InputHandles;

% Load GroupID and SubjID (Note: Overwrites unlike LoadGroupSubj):
handles.name.GroupID = LoadedMat.name.GroupID;
handles.name.SubjID  = LoadedMat.name.SubjID;

% Load GUI panel:
set(handles.ListboxGroupID, 'String', handles.name.GroupID);
set(handles.ListboxGroupID, 'Value',  1);

set(handles.ListboxSubjID,  'String', handles.name.SubjID{1});
set(handles.ListboxSubjID,  'Value',  1);

% Set output handles:
OutputHandles = handles;


%--- Reset GroupID settings & GUI panel: ---%
%-------------------------------------------%
function OutputHandles = ResetGroupIDSettings(InputHandles)
handles = InputHandles;

handles.name.GroupID = [];
set(handles.ListboxGroupID, 'String', []);
set(handles.ListboxGroupID, 'Value',  1);

% Set output handles:
OutputHandles = handles;




%=========================================%
% FUNCTIONS FOR ADDING/REMOVING SUBJECTS: %
%=========================================%

%--- Executes on selection change in ListboxSubjID. ---%
%------------------------------------------------------%
function ListboxSubjID_Callback(hObject, eventdata, handles)
SelectedIndex = get(handles.ListboxSubjID, 'Value');
if ~isempty(handles.gui.InputAnatFiles)
	set(handles.ListboxInputMRI, 'Value', SelectedIndex);
end
if ~isempty(handles.gui.InputDataFiles)
	set(handles.ListboxInputMEG, 'Value', SelectedIndex);
end


%--- User enters subject to add into this textbox. ---%
%-----------------------------------------------------%
function TextboxSubjID_Callback(hObject, eventdata, handles)
keypress = get(gcf,'CurrentCharacter');
if isequal(keypress, char(13))
    ButtonAddSubjID_Callback(hObject, eventdata, handles);
end


%--- Executes on button press in ButtonAddSubjID. ---%
%----------------------------------------------------%
function ButtonAddSubjID_Callback(hObject, eventdata, handles)
if isempty(handles.name.AnalysisID) || isempty(handles.paths.AnalysisID)
    msgbox('Warning: Create or load AnalysisID first.', 'Warning:');
    return;
end
if isempty(handles.name.GroupID)
	msgbox('Warning: Create or load a GroupID first.', 'Warning:');
	return;
end

% Read entered SubjID:
InputSubjID = get(handles.TextboxSubjID, 'String');
InputSubjID = deblank(InputSubjID);
InputSubjID = regexprep(InputSubjID, '\s+', '_');
InputSubjID = cellstr(InputSubjID);
if isempty(InputSubjID{1})
	return;
end

GroupIndex = get(handles.ListboxGroupID, 'Value');
if ismember(InputSubjID{1}, handles.name.SubjID{GroupIndex})
	return;  % If SubjID already added
end

% Add SubjID for selected group:
handles.name.SubjID{GroupIndex} = [handles.name.SubjID{GroupIndex}; InputSubjID];

NewSubjIndex = length(handles.name.SubjID{GroupIndex});
handles.gui.InputAnatFiles {GroupIndex}{NewSubjIndex} = [];
handles.gui.StatusAnat     {GroupIndex}{NewSubjIndex} = [];

for c = 1:length(handles.name.CondID)
    handles.gui.InputDataFiles {GroupIndex}{NewSubjIndex,c} = [];
    handles.gui.StatusData     {GroupIndex}{NewSubjIndex,c} = [];
    handles.gui.CheckICAclean  {GroupIndex}{NewSubjIndex,c} = [];
end

% Update input files:
handles = UpdateSubjIDSettings (handles);
handles = UpdateInputMRI	   (handles);  % Only initialize for now, don't detect
handles = UpdateInputMEG	   (handles);  % so user doesn't have to wait between adds.

% Save handles:
set(handles.ListboxSubjID, 'Value', NewSubjIndex);
if ~isempty(handles.gui.InputAnatFiles)
    set(handles.ListboxInputMRI, 'Value', NewSubjIndex);
end
if ~isempty(handles.gui.InputDataFiles)
    set(handles.ListboxInputMEG, 'Value', NewSubjIndex);
end
guidata(hObject, handles);


%--- Executes on button press in ButtonRmvSubjID. ---%
%----------------------------------------------------%
function ButtonRmvSubjID_Callback(hObject, eventdata, handles)
if isempty(handles.name.SubjID)
    return;
end

GroupIndex = get(handles.ListboxGroupID, 'Value');
SubjIndex  = get(handles.ListboxSubjID,  'Value');
if isempty(handles.name.SubjID{GroupIndex})
    return;
end

handles.name.SubjID{GroupIndex}(SubjIndex) = [];

if ~isempty(handles.gui.InputAnatFiles)
    handles.gui.InputAnatFiles{GroupIndex}(SubjIndex) = [];
    handles.gui.StatusAnat{GroupIndex}(SubjIndex)     = [];
end

if ~isempty(handles.name.CondID)
    handles.gui.InputDataFiles{GroupIndex}(SubjIndex,:) = [];
    handles.gui.StatusData{GroupIndex}(SubjIndex,:)     = [];
    handles.gui.CheckICAclean{GroupIndex}(SubjIndex,:)  = [];
end

% Update input files:
handles = UpdateSubjIDSettings (handles);
handles = UpdateInputMRI	   (handles);
handles = UpdateInputMEG	   (handles);

% Save handles:
guidata(hObject, handles);


%--- Update SubjID settings & GUI panel: ---%
%-------------------------------------------%
function OutputHandles = UpdateSubjIDSettings(InputHandles)
handles = InputHandles;

% Update listbox:
GroupIndex = get(handles.ListboxGroupID, 'Value');
if isempty(handles.name.SubjID)
    set(handles.ListboxSubjID, 'String', []);
    MaxSubjIndex = 0;
else
    set(handles.ListboxSubjID, 'String', handles.name.SubjID{GroupIndex});
    MaxSubjIndex = length(handles.name.SubjID{GroupIndex});
end

SubjIndex = get(handles.ListboxSubjID, 'Value');
if isempty(SubjIndex) || SubjIndex == 0 || SubjIndex > MaxSubjIndex
	set(handles.ListboxSubjID, 'Value', MaxSubjIndex);
	
	if ~isempty(handles.gui.InputAnatFiles)
		set(handles.ListboxInputMRI, 'Value', MaxSubjIndex);
	end
	if ~isempty(handles.gui.InputDataFiles)
		set(handles.ListboxInputMEG, 'Value', MaxSubjIndex);
	end
end

% Set output handles:
OutputHandles = handles;


%--- Load SubjID settings & GUI panel: ---%
%-----------------------------------------%
function OutputHandles = LoadSubjIDSettings(InputHandles, LoadedMat)
handles = InputHandles;

handles.name.SubjID = LoadedMat.name.SubjID;
set(handles.ListboxSubjID,  'String', handles.name.SubjID{1});
set(handles.ListboxSubjID,  'Value',  1);

% Set output handles:
OutputHandles = handles;


%--- Reset SubjID settings & GUI panel: ---%
%------------------------------------------%
function OutputHandles = ResetSubjIDSettings(InputHandles)
handles = InputHandles;

handles.name.SubjID = [];
set(handles.ListboxSubjID, 'String', []);
set(handles.ListboxSubjID, 'Value',  1);

% Set output handles:
OutputHandles = handles;




%========================================%
% FUNCTIONS FOR MRI SELECTION & DISPLAY: %
%========================================%

%--- Executes on selection change in ListboxInputMRI. ---%
%------------------------------------------------------------%
function ListboxInputMRI_Callback(hObject, eventdata, handles)
if isempty(handles.gui.InputAnatFiles)
	return;
end

SelectedIndex = get(handles.ListboxInputMRI, 'Value');
set(handles.ListboxSubjID, 'Value', SelectedIndex);

if ~isempty(handles.gui.InputDataFiles)
	set(handles.ListboxInputMEG, 'Value', SelectedIndex);
end


%--- Executes on button press in CheckboxShowPathMRI. ---%
%------------------------------------------------------------%
function CheckboxShowPathMRI_Callback(hObject, eventdata, handles)
handles = UpdateInputMRI(handles);
guidata(hObject, handles);


%--- Executes on button press in ButtonSelectAnatIDFolder. ---%
%-------------------------------------------------------------%
function ButtonSelectAnatIDFolder_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Select root directory first.', 'Warning:');
	return;
end
if isempty(handles.name.AnalysisID) || isempty(handles.paths.AnalysisID)
    msgbox('Warning: Create or load AnalysisID first.', 'Warning:');
    return;
end

% Get AnatID folders in current rootpath:
DetectedFolders = dir([handles.paths.Rootpath,'/AnatID_*']);
RemoveIndex = [];
for d = 1:length(DetectedFolders)
	if DetectedFolders(d).isdir ~= 1
		RemoveIndex = [RemoveIndex, d];  % Get unwanted indices
	end
end
DetectedFolders(RemoveIndex) = [];
if isempty(DetectedFolders)
	msgbox('Error: No AnatID folders detected inside root dir.', 'Error:');
	return;
end

% List AnatID folders for selection:
DetectedFolders = {DetectedFolders.name};
SelectedIndex = listdlg('PromptString', 'Select AnatID to load:',...
	'ListSize', [300, 300], 'SelectionMode', 'single', 'ListString', DetectedFolders);
if isempty(SelectedIndex)
	return;  % If user cancels
end

% Set as current AnatID:
SelectedAnatID		= DetectedFolders{SelectedIndex};
SelectedAnatID(1:7) = [];  % Remove "AnatID_" prefix to get AnatID.
handles.gui.AnatID	= SelectedAnatID;

% Detect input MRI files & update status:
for g = 1:length(handles.name.GroupID)
    DetectWaitbar = waitbar(0, ...
        ['Scanning input MRI files for: ',handles.name.GroupID{g}], 'Windowstyle', 'modal');
    
    for s = 1:length(handles.name.SubjID{g})
        waitbar(s/length(handles.name.SubjID{g}), DetectWaitbar);
        handles = DetectInputMRI(handles, g, s);
    end
    delete(DetectWaitbar)
end

handles = UpdateInputMRI(handles);

% Save handles:
guidata(hObject, handles);


%--- Executes on button press in ButtonDetectInputMRI. ---%
%---------------------------------------------------------%
function ButtonDetectInputMRI_Callback(hObject, eventdata, handles)

% Detect input MRI files & update status:
for g = 1:length(handles.name.GroupID)
    DetectWaitbar = waitbar(0, ...
        ['Scanning input MRI files for: ',handles.name.GroupID{g}], 'Windowstyle', 'modal');
    
    for s = 1:length(handles.name.SubjID{g})
        waitbar(s/length(handles.name.SubjID{g}), DetectWaitbar);
        handles = DetectInputMRI(handles, g, s);
    end
    delete(DetectWaitbar)
end

handles = UpdateInputMRI(handles);
guidata(hObject, handles);


%--- Detect input MRI files & status: ---%
%----------------------------------------%
function OutputHandles = DetectInputMRI(InputHandles, g, s)
handles = InputHandles;

% Update input MRI paths and status:
handles.gui.InputAnatFiles{g}{s} = [];
handles.gui.StatusAnat{g}{s}     = [];

if ~isempty(handles.gui.AnatID)
    Rootpath      = handles.paths.Rootpath;
    AnatIDFolder  = ['AnatID_',handles.gui.AnatID];
    GroupIDFolder = ['GroupID_',handles.name.GroupID{g}];
    Filename      = [handles.name.SubjID{g}{s},'_PreprocMRI.mat'];
    
    handles.gui.InputAnatFiles{g}{s} = ...
        [Rootpath,'/',AnatIDFolder,'/',GroupIDFolder,'/',Filename];
    
    if exist(handles.gui.InputAnatFiles{g}{s}, 'file')
        LoadMRI = LoadFTmat(handles.gui.InputAnatFiles{g}{s}, 'BuilderGUI');
        
        if isempty(LoadMRI)
            StatusMsg = 'PreprocMRI file found, but contains errors. See ErrorLog.';
            
        else
            if isfield(LoadMRI, 'coordsys')
                CoordSys = LoadMRI.coordsys;
            else
                CoordSys = 'Unknown';
            end
            
            DimStr    = num2str(LoadMRI.dim);
            StatusMsg = ['PreprocMRI found. Dim: ',DimStr,'. Coordsys: ',CoordSys];
        end
        
    else
        StatusMsg = 'Error: PreprocMRI file missing.';
    end
    
    handles.gui.StatusAnat{g}{s} = StatusMsg;
    
end  % If AnatID

% Set output handles:
OutputHandles = handles;


%--- Updates input MRI data and GUI panel: ---%
%---------------------------------------------%
function OutputHandles = UpdateInputMRI(InputHandles)
handles = InputHandles;

% Make sure cells initialized:
for g = 1:length(handles.name.GroupID)
    for s = 1:length(handles.name.SubjID{g})
        
        try
            handles.gui.InputAnatFiles{g}{s};
            handles.gui.StatusAnat{g}{s};
        catch
            handles.gui.InputAnatFiles{g}{s} = [];
            handles.gui.StatusAnat{g}{s}     = [];
        end
        
    end
end

% Update listbox:
GroupIndex = get(handles.ListboxGroupID, 'Value');

if isempty(handles.gui.InputAnatFiles) || isempty(handles.gui.InputAnatFiles{GroupIndex})
	set(handles.ListboxInputMRI, 'String', []);
    set(handles.ListboxInputMRI, 'Value',  0);
else
    if get(handles.CheckboxShowPathMRI, 'Value') == 1
        set(handles.ListboxInputMRI, 'String', handles.gui.InputAnatFiles{GroupIndex});
    else
        set(handles.ListboxInputMRI, 'String', handles.gui.StatusAnat{GroupIndex});
    end
end

if ~isempty(handles.gui.InputAnatFiles) && ~isempty(handles.gui.InputAnatFiles{GroupIndex})
    CurrentIndex = get(handles.ListboxInputMRI, 'Value');
    MaxIndex     = length(handles.gui.InputAnatFiles{GroupIndex});
    
    if isempty(CurrentIndex) || CurrentIndex == 0 || CurrentIndex > MaxIndex
        set(handles.ListboxInputMRI, 'Value', MaxIndex);
        set(handles.ListboxSubjID,   'Value', MaxIndex);
        
        if ~isempty(handles.gui.InputDataFiles)
            set(handles.ListboxInputMEG, 'Value', MaxIndex);
        end
    end
end

% Enable/Disable GUI components:
if isempty(handles.gui.AnatID)
	set(handles.ListboxInputMRI,	 'Enable', 'off');
	set(handles.CheckboxShowPathMRI, 'Enable', 'off');
else
	set(handles.ListboxInputMRI,	 'Enable', 'on');
	set(handles.CheckboxShowPathMRI, 'Enable', 'on');
end

% Set output handles:
OutputHandles = handles;


%--- Load input MRI & GUI panel: ---%
%-----------------------------------%
function OutputHandles = LoadInputMRI(InputHandles, LoadedMat)
handles = InputHandles;

% Load AnatID (Note: Need to run update fcn after):
handles.gui.AnatID		   = LoadedMat.gui.AnatID;
handles.gui.InputAnatFiles = LoadedMat.gui.InputAnatFiles;

% Update to get input files and status:
for g = 1:length(handles.name.GroupID)
    DetectWaitbar = waitbar(0, ...
        ['Scanning input MRI files for: ',handles.name.GroupID{g}], 'Windowstyle', 'modal');
    
    for s = 1:length(handles.name.SubjID{g})
        waitbar(s/length(handles.name.SubjID{g}), DetectWaitbar);
        handles = DetectInputMRI(handles, g, s);
    end
    delete(DetectWaitbar)
end

handles = UpdateInputMRI (handles);

% Set output handles:
OutputHandles = handles;


%--- Reset input MRI & GUI panel: ---%
%------------------------------------%
function OutputHandles = ResetInputMRI(InputHandles)
handles = InputHandles;

% Reset input MRI:
handles.gui.AnatID         = [];
handles.gui.InputAnatFiles = [];
handles.gui.StatusAnat     = [];

% Reset GUI panel:
set(handles.ListboxInputMRI,	 'String', []);
set(handles.ListboxInputMRI,	 'Value',  1);
set(handles.CheckboxShowPathMRI, 'Value',  0);

% Set output handles:
OutputHandles = handles;



%=======================================================%
% FUNCTIONS FOR MEG INPUT DISPLAY & FILETYPE SELECTION: %
%=======================================================%

%--- Executes on selection change in ListboxInputMEG. ---%
%------------------------------------------------------------%
function ListboxInputMEG_Callback(hObject, eventdata, handles)
if isempty(handles.gui.InputDataFiles)
	return;
end

SelectedIndex = get(handles.ListboxInputMEG, 'Value');
set(handles.ListboxSubjID, 'Value', SelectedIndex);

if ~isempty(handles.gui.InputAnatFiles)
	set(handles.ListboxInputMRI,  'Value', SelectedIndex);
end


%--- Executes on button press in CheckboxShowPathMEG. ---%
%--------------------------------------------------------%
function CheckboxShowPathMEG_Callback(hObject, eventdata, handles)
handles = UpdateInputMEG(handles);
guidata(hObject, handles);


%--- Executes on selection change in DropdownMEGdataFiletype. ---%
%----------------------------------------------------------------%
function DropdownMEGdataFiletype_Callback(hObject, eventdata, handles)

handles = ResetCondIDSettings (handles);
handles.gui.InputDataFiles  = [];  % Note: Do not use ResetInputMEG here or
handles.gui.StatusData      = [];  %  filetype will also be reset.
handles.gui.CheckICAclean   = [];

handles = UpdateInputMEG        (handles);  % Update dropdown box.
handles = DetectRootpathFolders (handles);
handles = DetectCondIDFolders   (handles);
handles = UpdateCondIDSettings  (handles);

% Detect input MEG files & update status:
for g = 1:length(handles.name.GroupID)
    DetectWaitbar = waitbar(0, ...
        ['Scanning input MEG files for: ',handles.name.GroupID{g}], 'Windowstyle', 'modal');
    
    for s = 1:length(handles.name.SubjID{g})
        waitbar(s/length(handles.name.SubjID{g}), DetectWaitbar);
        
        for c = 1:size(handles.gui.AddedCondIDs, 1)
            handles = DetectInputMEG(handles, g, s, c);
        end
    end
    delete(DetectWaitbar)
end

handles = UpdateInputMEG(handles);

% Save handles:
guidata(hObject, handles);


%--- Executes on button press in ButtonDetectInputMEG. ---%
%---------------------------------------------------------%
function ButtonDetectInputMEG_Callback(hObject, eventdata, handles)

% Detect input MEG files & update status:
for g = 1:length(handles.name.GroupID)
    DetectWaitbar = waitbar(0, ...
        ['Scanning input MEG files for: ',handles.name.GroupID{g}], 'Windowstyle', 'modal');
    
    for s = 1:length(handles.name.SubjID{g})
        waitbar(s/length(handles.name.SubjID{g}), DetectWaitbar);
        
        for c = 1:size(handles.gui.AddedCondIDs, 1)
            handles = DetectInputMEG(handles, g, s, c);
        end
    end
    delete(DetectWaitbar)
end

handles = UpdateInputMEG (handles);
guidata(hObject, handles);


%--- Detect input MEG files & status: ---%
%----------------------------------------%
function OutputHandles = DetectInputMEG(InputHandles, g, s, c)
handles = InputHandles;

% Update input MEG paths and status:
handles.gui.InputDataFiles{g}{s,c} = [];
handles.gui.StatusData{g}{s,c}     = [];
handles.gui.CheckICAclean{g}{s,c}  = [];

Rootpath	   = handles.paths.Rootpath;
RootpathFolder = handles.gui.AddedCondIDs{c,2};
GroupIDFolder  = ['GroupID_',handles.name.GroupID{g}];
CondIDFolder   = handles.gui.AddedCondIDs{c,1};

switch handles.gui.MEGdataFiletype
    case 'Fieldtrip'
        MainFolder = [Rootpath,'/',RootpathFolder];
        Filename   = [handles.name.SubjID{g}{s},'_PreprocMEG.mat'];
    case 'NIFTI'
        MainFolder = [Rootpath,'/',RootpathFolder,'/NORM_SOURCE_Nifti4D'];
        Filename   = [handles.name.SubjID{g}{s},'_Nifti4D.nii'];
    case 'AFNI'
        MainFolder = [Rootpath,'/',RootpathFolder,'/NORM_SOURCE_Afni4D'];
        Filename   = [handles.name.SubjID{g}{s},'_Afni4D+tlrc.BRIK'];
end

handles.gui.InputDataFiles{g}{s,c} = ...
    [MainFolder,'/',GroupIDFolder,'/',CondIDFolder,'/',Filename];

CheckICA = [];  % For FT files, logs whether ICA cleaning was done on it

if exist(handles.gui.InputDataFiles{g}{s,c}, 'file')
    if strcmp(handles.gui.MEGdataFiletype, 'Fieldtrip')
        LoadData  = LoadFTmat(handles.gui.InputDataFiles{g}{s,c}, 'BuilderGUI');
        
        if isempty(LoadData)
            StatusMsg = 'PreprocMEG file found, but contains errors. See ErrorLog.';
        else
            NumTrials = size(LoadData.trial, 2);
            StatusMsg = ['PreprocMEG dataset found. Trials: ',num2str(NumTrials)];
            
            if isfield(LoadData, 'ICAcompRemoved') && ~isempty(LoadData.ICAcompRemoved)
                CheckICA = 1;
            end
        end
    else
        StatusMsg = 'Dataset found.';
    end
else
    StatusMsg = 'Error: Dataset could not be found. Check paths.';
end

handles.gui.StatusData{g}{s,c}    = StatusMsg;
handles.gui.CheckICAclean{g}{s,c} = CheckICA;


% Set output handles:
OutputHandles = handles;


%--- Updates input MEG data & GUI panel: ---%
%-------------------------------------------%
function OutputHandles = UpdateInputMEG(InputHandles)
handles = InputHandles;

% Update input MEG filetype:
DropdownOptions = get(handles.DropdownMEGdataFiletype, 'String');
MEGdataFiletype = DropdownOptions{get(handles.DropdownMEGdataFiletype, 'Value')};
ViableOptions   = {'Fieldtrip', 'NIFTI', 'AFNI'};

if ismember(MEGdataFiletype, ViableOptions)
    handles.gui.MEGdataFiletype = MEGdataFiletype;
else
    error('Unrecognized Option')  % error-check
end

% Make sure cells initialized:
for g = 1:length(handles.name.GroupID)
    for s = 1:length(handles.name.SubjID{g})
        for c = 1:length(handles.name.CondID)
            
            try
                handles.gui.InputDataFiles{g}{s,c};
                handles.gui.StatusData{g}{s,c};
                handles.gui.CheckICAclean{g}{s,c};
            catch
                handles.gui.InputDataFiles{g}{s,c} = [];
                handles.gui.StatusData{g}{s,c}     = [];
                handles.gui.CheckICAclean{g}{s,c}  = [];
            end
            
        end
    end
end

% Update listbox:
GroupIndex = get(handles.ListboxGroupID,      'Value');
CondIndex  = get(handles.ListboxAddedCondIDs, 'Value');

if isempty(handles.gui.InputDataFiles) || ...
        isempty(handles.gui.InputDataFiles{GroupIndex}) || isempty(handles.name.CondID)
	set(handles.ListboxInputMEG, 'String', []);
    set(handles.ListboxInputMEG, 'Value',  0);
else
    if get(handles.CheckboxShowPathMEG, 'Value') == 1
        set(handles.ListboxInputMEG, 'String', ...
            handles.gui.InputDataFiles{GroupIndex}(:,CondIndex));
    else
        set(handles.ListboxInputMEG, 'String', ...
            handles.gui.StatusData{GroupIndex}(:,CondIndex));
    end
end

if ~isempty(handles.gui.InputDataFiles) && ...
        ~isempty(handles.gui.InputDataFiles{GroupIndex}) && ~isempty(handles.name.CondID)
    CurrentIndex = get(handles.ListboxInputMEG, 'Value');
    MaxIndex     = length(handles.gui.InputDataFiles{GroupIndex}(:,CondIndex));
    
    if isempty(CurrentIndex) || CurrentIndex == 0 || CurrentIndex > MaxIndex
        set(handles.ListboxInputMEG, 'Value', MaxIndex);
        set(handles.ListboxSubjID,   'Value', MaxIndex);
        
        if ~isempty(handles.gui.InputAnatFiles)
            set(handles.ListboxInputMRI, 'Value', MaxIndex);
        end
    end
end

% Enable/Disable GUI components:
if isempty(handles.gui.AddedCondIDs)
	set(handles.ListboxInputMEG,	 'Enable', 'off');
	set(handles.CheckboxShowPathMEG, 'Enable', 'off');
else
	set(handles.ListboxInputMEG,	 'Enable', 'on');
	set(handles.CheckboxShowPathMEG, 'Enable', 'on');
end
			
% Set output handles:
OutputHandles = handles;


%--- Load input MEG data & GUI panel: ---%
%----------------------------------------%
function OutputHandles = LoadInputMEG(InputHandles, LoadedMat)
handles = InputHandles;

% Load input MEG settings:
handles.gui.MEGdataFiletype = LoadedMat.gui.MEGdataFiletype;
handles.gui.InputDataFiles  = LoadedMat.gui.InputDataFiles;
handles.gui.AddedCondIDs    = LoadedMat.gui.AddedCondIDs;
handles.name.CondID         = LoadedMat.name.CondID;

% Load GUI panel:
DropdownOptions = get(handles.DropdownMEGdataFiletype, 'String');
DropdownValue   = find(strcmp(DropdownOptions, handles.gui.MEGdataFiletype));
set(handles.DropdownMEGdataFiletype, 'Value', DropdownValue);
if DropdownValue == 0
	error('Option does not exist in dropdown list.')  % error-check
end

set(handles.ListboxAddedCondIDs, 'String', handles.gui.AddedCondIDs(:,3));
set(handles.ListboxAddedCondIDs, 'Value',  1);

% Update to get input files and status:
for g = 1:length(handles.name.GroupID)
    DetectWaitbar = waitbar(0, ...
        ['Scanning input MEG files for: ',handles.name.GroupID{g}], 'Windowstyle', 'modal');
    
    for s = 1:length(handles.name.SubjID{g})
        waitbar(s/length(handles.name.SubjID{g}), DetectWaitbar);
        
        for c = 1:size(handles.gui.AddedCondIDs, 1)
            handles = DetectInputMEG(handles, g, s, c);
        end
    end
    delete(DetectWaitbar)
end

handles = UpdateInputMEG(handles);

% Set output handles:
OutputHandles = handles;


%--- Reset input MEG data & GUI panel: ---%
%-----------------------------------------%
function OutputHandles = ResetInputMEG(InputHandles)
handles = InputHandles;

% Reset input MEG:
handles.gui.MEGdataFiletype = 'Fieldtrip';
handles.gui.InputDataFiles  = [];
handles.gui.StatusData      = [];
handles.gui.CheckICAclean   = [];

handles.gui.AddedCondIDs    = [];
handles.name.CondID         = [];

% Reset GUI panel:
set(handles.DropdownMEGdataFiletype, 'Value',  1);
set(handles.CheckboxShowPathMEG,     'Value',  0);

set(handles.ListboxInputMEG,		 'String', []);
set(handles.ListboxInputMEG,		 'Value',  1);

set(handles.ListboxAddedCondIDs,	 'String', []);
set(handles.ListboxAddedCondIDs,	 'Value',  1);

% Set output handles:
OutputHandles = handles;




%=======================================================%
% FUNCTIONS FOR DETECTING ROOTPATH FOLDERS AND CONDIDs: %
%=======================================================%

%--- Executes on selection change in ListboxRootpathFolders. ----%
%----------------------------------------------------------------%
function ListboxRootpathFolders_Callback(hObject, eventdata, handles)
handles = DetectCondIDFolders(handles);
guidata(hObject, handles);


%--- Executes on selection change in ListboxDetectedCondIDs. ---%
%---------------------------------------------------------------%
function ListboxDetectedCondIDs_Callback(hObject, eventdata, handles)
guidata(hObject, handles);


%--- Detect DataID or AnalysisID folders in Rootpath: ---%
%--------------------------------------------------------%
function OutputHandles = DetectRootpathFolders(InputHandles)
handles = InputHandles;

% Get DataID / AnalysisID folders in current rootpath:
if strcmp(handles.gui.MEGdataFiletype, 'Fieldtrip')
    DetectedFolders = dir([handles.paths.Rootpath,'/DataID_*']);
	DetectedFolders = [DetectedFolders; dir([handles.paths.Rootpath,'/DataID-ICAclean_*'])];
else
    DetectedFolders = dir([handles.paths.Rootpath,'/AnalysisID_*']);
end

RemoveIndex = [];
for d = 1:length(DetectedFolders)
    if DetectedFolders(d).isdir ~= 1
        RemoveIndex = [RemoveIndex, d];  % Get unwanted indices
    end
end
DetectedFolders(RemoveIndex) = [];

% Set detected rootpath folders:
DetectedFolders				= {DetectedFolders.name};
handles.gui.RootpathFolders = DetectedFolders;

% Update listbox:
set(handles.ListboxRootpathFolders, 'String', handles.gui.RootpathFolders);

CurrentIndex = get(handles.ListboxRootpathFolders, 'Value');
MaxIndex     = length(handles.gui.RootpathFolders);
if isempty(CurrentIndex) || CurrentIndex == 0 || CurrentIndex > MaxIndex
	set(handles.ListboxRootpathFolders, 'Value', MaxIndex);
end

% Set as output handles:
OutputHandles = handles;


%--- Detect CondID folders for selection: ---%
%--------------------------------------------%
function OutputHandles = DetectCondIDFolders(InputHandles)
handles = InputHandles;

% Get CondID folders in selected RootpathFolder:
FolderIndex    = get(handles.ListboxRootpathFolders, 'Value');
if FolderIndex == 0 || isempty(FolderIndex)
    OutputHandles = handles;
    return;
end

RootpathFolder = handles.gui.RootpathFolders{FolderIndex};
Rootpath	   = handles.paths.Rootpath;

% Set search path:
switch handles.gui.MEGdataFiletype
    case 'Fieldtrip'
        SearchPath = [Rootpath,'/',RootpathFolder];
    case 'NIFTI'
        SearchPath = [Rootpath,'/',RootpathFolder,'/NORM_SOURCE_Nifti4D'];
    case 'AFNI'
        SearchPath = [Rootpath,'/',RootpathFolder,'/NORM_SOURCE_Afni4D'];
end

% Get GroupID folders in RootpathFolder to search within:
GroupFolders = dir([SearchPath,'/GroupID_*']);

RemoveIndex = [];
for d = 1:length(GroupFolders)
    if GroupFolders(d).isdir ~= 1 || ismember(GroupFolders(d).name, {'.', '..'})
        RemoveIndex = [RemoveIndex, d];
    end
end
GroupFolders(RemoveIndex) = [];
GroupFolders = {GroupFolders.name};

% Search GroupID folders for available CondIDs:
CondFolders = [];
for g = 1:length(GroupFolders)
    SearchGroup = dir([SearchPath,'/',GroupFolders{g},'/*']);
    CondFolders = [CondFolders; (SearchGroup)];
end

RemoveIndex = [];
for d = 1:length(CondFolders)
    if CondFolders(d).isdir ~= 1 || ismember(CondFolders(d).name, {'.', '..'})
        RemoveIndex = [RemoveIndex, d];
    end
end
CondFolders(RemoveIndex) = [];

% Set detected CondID folders:
if ~isempty(CondFolders)
    CondFolders	= {CondFolders.name};
    CondFolders	= unique(CondFolders);
end

if isempty(CondFolders)
    handles.gui.DetectedCondIDs = [];
else
    handles.gui.DetectedCondIDs = CondFolders;
end

% Update listbox:
set(handles.ListboxDetectedCondIDs, 'String', handles.gui.DetectedCondIDs);

CurrentIndex = get(handles.ListboxDetectedCondIDs, 'Value');
MaxIndex	 = length(handles.gui.DetectedCondIDs);
if isempty(CurrentIndex) || CurrentIndex == 0 || CurrentIndex > MaxIndex
	set(handles.ListboxDetectedCondIDs, 'Value', MaxIndex);
end

% Set as output handles:
OutputHandles = handles;




%==================================================%
% FUNCTIONS FOR SELECTING CONDITIONS AND DATASETS: %
%==================================================%

%--- Executes on selection change in ListboxAddedCondIDs. ---%
%------------------------------------------------------------%
function ListboxAddedCondIDs_Callback(hObject, eventdata, handles)
handles = UpdateInputMEG(handles);
guidata(hObject, handles);


%--- Executes on button press in ButtonAddCondID. ---%
%----------------------------------------------------%
function ButtonAddCondID_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Select root directory first.', 'Warning:');
	return;
end
if isempty(handles.name.AnalysisID) || isempty(handles.paths.AnalysisID)
    msgbox('Warning: Create or load AnalysisID first.', 'Warning:');
    return;
end
if isempty(handles.name.GroupID)
	msgbox('Warning: Create or load a GroupID first.', 'Warning:');
	return;
end
if isempty(handles.gui.DetectedCondIDs)
    return;
end

% Get selected RootpathFolder and DetectedCondID:
FolderIndex    = get(handles.ListboxRootpathFolders, 'Value');
RootpathFolder = handles.gui.RootpathFolders{FolderIndex};

CondIndex		 = get(handles.ListboxDetectedCondIDs, 'Value');
TargetCondID	 = handles.gui.DetectedCondIDs{CondIndex};
FolderCondString = [RootpathFolder,':  ',TargetCondID];

if ismember(TargetCondID, handles.name.CondID)
	msgbox('Error: CondID with the same name has already been added.', 'Error:')
    return;
end

% Add CondID folder:
handles.name.CondID = [handles.name.CondID; cellstr(TargetCondID)];
NewCondIndex		= length(handles.name.CondID);

handles.gui.AddedCondIDs{NewCondIndex,1} = TargetCondID;
handles.gui.AddedCondIDs{NewCondIndex,2} = RootpathFolder;
handles.gui.AddedCondIDs{NewCondIndex,3} = FolderCondString;

% Update settings & GUI:
handles = UpdateCondIDSettings (handles);

for g = 1:length(handles.name.GroupID)
    DetectWaitbar = waitbar(0, ...
        ['Scanning input MEG files for: ',handles.name.GroupID{g}], 'Windowstyle', 'modal');
    
    for s = 1:length(handles.name.SubjID{g})
        waitbar(s/length(handles.name.SubjID{g}), DetectWaitbar);
        
        for c = NewCondIndex  % Only update for new cond index
            handles = DetectInputMEG(handles, g, s, c);
        end
    end
    delete(DetectWaitbar)
end

handles = UpdateInputMEG(handles);

% Save handles:
guidata(hObject, handles);


%--- Executes on button press in ButtonRmvCondID. ---%
%----------------------------------------------------%
function ButtonRmvCondID_Callback(hObject, eventdata, handles)
if isempty(handles.name.CondID)
    return;
end

SelectedIndex = get(handles.ListboxAddedCondIDs, 'Value');
handles.name.CondID      (SelectedIndex)   = [];
handles.gui.AddedCondIDs (SelectedIndex,:) = [];

for g = 1:length(handles.name.GroupID)
    handles.gui.InputDataFiles{g}(:,SelectedIndex) = [];
    handles.gui.StatusData{g}(:,SelectedIndex)     = [];
    handles.gui.CheckICAclean{g}(:,SelectedIndex)  = [];
end

% Update settings & GUI:
handles = UpdateCondIDSettings (handles);
handles = UpdateInputMEG	   (handles);

% Save handles:
guidata(hObject, handles);


%--- Updates CondID settings & GUI panel: ---%
%--------------------------------------------%
function OutputHandles = UpdateCondIDSettings(InputHandles)
handles = InputHandles;

% Update listbox:
if isempty(handles.gui.AddedCondIDs)
	set(handles.ListboxAddedCondIDs, 'String', []);
else
	set(handles.ListboxAddedCondIDs, 'String', handles.gui.AddedCondIDs(:,3));
end

CurrentIndex = get(handles.ListboxAddedCondIDs, 'Value');
MaxIndex	 = size(handles.gui.AddedCondIDs, 1);
if isempty(CurrentIndex) || CurrentIndex == 0 || CurrentIndex > MaxIndex
	set(handles.ListboxAddedCondIDs, 'Value', MaxIndex);
end

% Set output handles:
OutputHandles = handles;


%--- Load CondID settings & GUI panel: ---%
%-----------------------------------------%
function OutputHandles = LoadCondIDSettings(InputHandles, LoadedMat)
handles = InputHandles;

% Load CondID settings:
handles.name.CondID		 = LoadedMat.name.CondID;
handles.gui.AddedCondIDs = LoadedMat.gui.AddedCondIDs;

% Reset GUI panel:
set(handles.ListboxAddedCondIDs, 'String', handles.gui.AddedCondIDs(:,3));
set(handles.ListboxAddedCondIDs, 'Value',  1);

% Set output handles:
OutputHandles = handles;


%--- Reset CondID settings & GUI panel: ---%
%------------------------------------------%
function OutputHandles = ResetCondIDSettings(InputHandles)
handles = InputHandles;

% Reset CondID settings:
handles.name.CondID            = [];
handles.gui.AddedCondIDs       = [];

handles.gui.CommonFilterCondID = [];
handles.gui.ControlCondID	   = [];

handles.gui.DetectedCondIDs    = [];
handles.gui.RootpathFolders    = [];

% Reset GUI panel:
set(handles.ListboxAddedCondIDs,          'String', []);
set(handles.ListboxAddedCondIDs,          'Value',  1);

set(handles.CheckboxEstNoiseCommonFilt,   'Value',  0);
set(handles.TextboxCommonFiltControlCond, 'String', 'None Selected');

set(handles.ListboxDetectedCondIDs,		  'String', []);
set(handles.ListboxDetectedCondIDs,		  'Value',  1);
set(handles.ListboxRootpathFolders,		  'String', []);
set(handles.ListboxRootpathFolders,		  'Value',  1);

% Set output handles:
OutputHandles = handles;




%=======================================%
% FUNCTIONS FOR ANALYSIS TIME SETTINGS: %
%=======================================%

%--- Textbox for user to enter analysis time start: ---%
%------------------------------------------------------%
function TextboxTimeStart_Callback(hObject, eventdata, handles)
StartTime = str2num(get(handles.TextboxTimeStart,  'String'));
EndTime   = str2num(get(handles.TextboxTimeEnd,    'String'));

if isempty(StartTime)
    msgbox('Error: Start time must be specified.', 'Error:');
    return;
end
if StartTime > EndTime
    msgbox('Error: Start time cannot be greater than end time.', 'Error:');
    set(handles.TextboxTimeStart, 'String', []);
    return;
end

% Adjust custom CovCsd start time:
set(handles.TextboxCovCsdTimeStart, 'String', num2str(StartTime, '%1.3f'));

% Save handles:
handles = UpdateTimeSettings(handles);
guidata(hObject, handles);


%--- Textbox for user to enter analysis time end: ---%
%----------------------------------------------------%
function TextboxTimeEnd_Callback(hObject, eventdata, handles)
StartTime = str2num(get(handles.TextboxTimeStart,  'String'));
EndTime   = str2num(get(handles.TextboxTimeEnd,    'String'));

if isempty(EndTime)
    msgbox('Error: End time must be specified.', 'Error:');
    return;
end
if EndTime < StartTime
    msgbox('Error: End time cannot be less than start time.', 'Error:');
    set(handles.TextboxTimeEnd, 'String', []);
    return;
end

% Adjust custom CovCsd end time:
set(handles.TextboxCovCsdTimeEnd, 'String', num2str(EndTime, '%1.3f'));

% Save handles:
handles = UpdateTimeSettings(handles);
guidata(hObject, handles);


%--- Textbox for user to enter sliding window-size: ---%
%------------------------------------------------------%
function TextboxWindowSize_Callback(hObject, eventdata, handles)
StartTime  = str2num(get(handles.TextboxTimeStart,  'String'));
EndTime    = str2num(get(handles.TextboxTimeEnd,    'String'));
WindowSize = str2num(get(handles.TextboxWindowSize, 'String'));

if isempty(WindowSize)
    msgbox('Error: Window size must be specified.', 'Error:');
    return;
end
if WindowSize > (EndTime - StartTime)
    msgbox('Error: Window size cannot be greater than epoch length.', 'Error:');
    set(handles.TextboxWindowSize, 'String', []);
    return;
end

handles = UpdateTimeSettings(handles);
guidata(hObject, handles);


%--- Textbox for user to enter time-step: ---%
%--------------------------------------------%
function TextboxTimeStep_Callback(hObject, eventdata, handles)
TimeStep = str2num(get(handles.TextboxTimeStep, 'String'));

if isempty(TimeStep)
	msgbox('Error: Time step must be specified.', 'Error:');
    set(handles.TextboxTimeStep, 'String', num2str(0.020, '%1.3f'));
	return;
end
if TimeStep == 0
    msgbox('Error: Time step cannot be 0.', 'Error:');
    set(handles.TextboxTimeStep, 'String', num2str(0.020, '%1.3f'));
    return;
end

handles = UpdateTimeSettings(handles);
guidata(hObject, handles);


%--- Updates analysis time settings & GUI panel: ---%
%---------------------------------------------------%
function OutputHandles = UpdateTimeSettings(InputHandles)
handles = InputHandles;
time    = handles.time;

% Read GUI to update settings:
time.Start      = str2num(get(handles.TextboxTimeStart,  'String'));
time.End        = str2num(get(handles.TextboxTimeEnd,    'String'));
time.WindowSize = str2num(get(handles.TextboxWindowSize, 'String'));
time.Step       = str2num(get(handles.TextboxTimeStep,   'String'));

% Set starting window & index:
WindowStart = time.Start;
WindowEnd   = time.Start + time.WindowSize;
t = 1;

% Set time window data:
time.Windows = [];

while WindowEnd < time.End | abs(WindowEnd - time.End) < 0.001
	time.Windows(t,1) = WindowStart;
	time.Windows(t,2) = WindowEnd;
	
	WindowStart = WindowStart + time.Step;
	WindowEnd   = WindowEnd   + time.Step;
	t = t + 1;
	
	WindowStart = round(WindowStart*10000)/10000;
	WindowEnd   = round(WindowEnd*10000)/10000;
end

% Create strings:
time.str.StartEnd = [];
time.str.Windows  = [];

if ~isempty(time.Start) && ~isempty(time.End)
    time.str.StartEnd = ...
        [num2str(time.Start, '%1.3f'),'s_',num2str(time.End, '%1.3f'),'s'];
end

for t = 1:size(time.Windows, 1)
	time.str.Windows{t,1} = num2str(time.Windows(t,1), '%1.3f');
	time.str.Windows{t,2} = num2str(time.Windows(t,2), '%1.3f');
	time.str.Windows{t,3} = ...
        [time.str.Windows{t,1},'s_',time.str.Windows{t,2},'s'];
end

% Set output handles:
handles.time  = time;
OutputHandles = handles;


%--- Load analysis time settings & GUI panel: ---%
%------------------------------------------------%
function OutputHandles = LoadTimeSettings(InputHandles, LoadedMat)
handles = InputHandles;
time    = handles.time;

% Load analysis time settings:
time.Start		= LoadedMat.time.Start;
time.End		= LoadedMat.time.End;
time.WindowSize = LoadedMat.time.WindowSize;
time.Step		= LoadedMat.time.Step;

% Load analysis GUI panel:
set(handles.TextboxTimeStart,  'String', num2str(time.Start,	  '%1.3f'));
set(handles.TextboxTimeEnd,    'String', num2str(time.End,		  '%1.3f'));
set(handles.TextboxWindowSize, 'String', num2str(time.WindowSize, '%1.3f'));
set(handles.TextboxTimeStep,   'String', num2str(time.Step,       '%1.3f'));

% Update to recompile time windows:
handles = UpdateTimeSettings(handles);

% Set output handles:
handles.time  = time;
OutputHandles = handles;


%--- Reset analysis time settings & GUI panel: ---%
%-------------------------------------------------%
function OutputHandles = ResetTimeSettings(InputHandles)
handles = InputHandles;
time    = handles.time;

% Reset analysis time settings:
time.Start      = 0.000;
time.End        = 1.000;
time.WindowSize = 0.040;
time.Step       = 0.020;

% Reset GUI panel:
set(handles.TextboxTimeStart,  'String', num2str(0.000, '%1.3f'));
set(handles.TextboxTimeEnd,    'String', num2str(1.000, '%1.3f'));
set(handles.TextboxWindowSize, 'String', num2str(0.040, '%1.3f'));
set(handles.TextboxTimeStep,   'String', num2str(0.020, '%1.3f'));

% Update to recompile time windows:
handles = UpdateTimeSettings(handles);

% Set output handles:
handles.time  = time;
OutputHandles = handles;




%===============================================%
% FUNCTIONS FOR HEADMODEL & LEADFIELD SETTINGS: %
%===============================================%

%--- GUI functions for headmodel and leadfield: ---%
%--------------------------------------------------%
function DropdownHdmMethod_Callback(hObject, eventdata, handles)
handles = UpdateHdmLeadSettings(handles);
guidata(hObject, handles);

function TextboxLeadfieldRes_Callback(hObject, eventdata, handles)
if isempty(get(handles.TextboxLeadfieldRes, 'String'));
    msgbox('Note: Leadfield resolution not specified. Default set to 5mm.');
	set(handles.TextboxLeadfieldRes, 'String', num2str(5));
end

handles = UpdateHdmLeadSettings(handles);
guidata(hObject, handles);


%--- Executes on button press in ButtonTimelockFreqAdvSettings. ---%
%------------------------------------------------------------------%
function ButtonHdmAdvSettings_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Select root directory first.', 'Warning:');
	return;
end
if isempty(handles.name.AnalysisID) || isempty(handles.paths.AnalysisID)
    msgbox('Warning: Create or load AnalysisID first.', 'Warning:');
    return;
end
%** Opens settings .m file: Upgrade to sub-GUI later (Make it modal).
% - Can run child GUI, then call: uiwait(gcf) OR waitfor(childGUI)
% - Have updatefcns read adv.settings output to update settings accordingly.
open([handles.paths.AnalysisID,'/SETTINGS/Settings_SegHdmLeadfield.m']);


%--- Updates headmodel & leadfield settings & GUI panel: ---%
%-----------------------------------------------------------%
function OutputHandles = UpdateHdmLeadSettings(InputHandles)
handles = InputHandles;
FTcfg   = handles.FTcfg;

% Read GUI to update settings:
DropdownOptions = get(handles.DropdownHdmMethod, 'String');
HdmMethod       = DropdownOptions{get(handles.DropdownHdmMethod, 'Value')};
ViableOptions   = {'singleshell'; 'localspheres'; 'singlesphere'; 'infinite'};

if ismember(HdmMethod, ViableOptions)
	FTcfg.Hdm.method = HdmMethod;
else
	error('Unrecognized Option');  % error-check
end

LeadfieldRes = str2num(get(handles.TextboxLeadfieldRes, 'String'));
FTcfg.Lead.grid.resolution = LeadfieldRes;

% Set output handles:
handles.FTcfg = FTcfg;
OutputHandles = handles;


%--- Load headmodel & leadfield settings & GUI panel: ---%
%--------------------------------------------------------%
function OutputHandles = LoadHdmLeadSettings(InputHandles, LoadedMat)
handles = InputHandles;
FTcfg   = handles.FTcfg;

% Load settings:
FTcfg.Hdm.method		   = LoadedMat.FTcfg.Hdm.method;
FTcfg.Lead.grid.resolution = LoadedMat.FTcfg.Lead.grid.resolution;

% Load GUI panel:
DropdownOptions = get(handles.DropdownHdmMethod, 'String');
DropdownValue   = find(strcmp(DropdownOptions, FTcfg.Hdm.method));
set(handles.DropdownHdmMethod, 'Value', DropdownValue);

if isempty(DropdownValue)
	error('Option does not exist in dropdown list.');  % error-check
end

set(handles.TextboxLeadfieldRes, 'String', num2str(FTcfg.Lead.grid.resolution));

% Set output handles:
handles.FTcfg = FTcfg;
OutputHandles = handles;


%--- Reset headmodel & leadfield settings & GUI panel: ---%
%---------------------------------------------------------%
function OutputHandles = ResetHdmLeadSettings(InputHandles)
handles = InputHandles;
FTcfg   = handles.FTcfg;

% Reset settings:
FTcfg.Hdm.method           = 'singleshell';
FTcfg.Lead.grid.resolution = 5;

% Reset GUI panel:
set(handles.DropdownHdmMethod,   'Value',  1);
set(handles.TextboxLeadfieldRes, 'String', num2str(5));

% Set output handles:
handles.FTcfg = FTcfg;
OutputHandles = handles;




%============================================%
% FUNCTIONS FOR TIME/FREQ ANALYSIS SETTINGS: %
%============================================%

%--- GUI functions for COV & CSD settings: ---%
%---------------------------------------------%

function CheckboxCovDemean_Callback(hObject, eventdata, handles)
handles = UpdateTimeFreqSettings(handles);
guidata(hObject, handles);

function DropdownCovCsdTime_Callback(hObject, eventdata, handles)
handles = UpdateTimeFreqSettings(handles);
guidata(hObject, handles);


% User enters custom start-time for COV/CSD here:
function TextboxCovCsdTimeStart_Callback(hObject, eventdata, handles)
TimeStart = str2num(get(handles.TextboxCovCsdTimeStart, 'String'));
TimeEnd   = str2num(get(handles.TextboxCovCsdTimeEnd,   'String'));

if isempty(TimeStart)
    msgbox('Error: Custom COV/CSD start time not specified.', 'Error:');
    return
end
if TimeStart > TimeEnd
    msgbox('Error: COV/CSD start time cannot be greater than end time.', 'Error:');
    set(handles.TextboxCovCsdTimeStart, 'String', []);
    return;
end

handles = UpdateTimeFreqSettings(handles);
guidata(hObject, handles);


% User enters custom end-time for COV/CSD here:
function TextboxCovCsdTimeEnd_Callback(hObject, eventdata, handles)
TimeStart = str2num(get(handles.TextboxCovCsdTimeStart, 'String'));
TimeEnd   = str2num(get(handles.TextboxCovCsdTimeEnd,   'String'));

if isempty(TimeEnd)
    msgbox('Error: Custom COV/CSD end time not specified.', 'Error:');
    return;
end
if TimeEnd < TimeStart
    msgbox('Error: COV/CSD end time cannot be less than start time.', 'Error:');
    set(handles.TextboxCovCsdTimeEnd, 'String', []);
    return;
end
        
handles = UpdateTimeFreqSettings(handles);
guidata(hObject, handles);


%--- GUI functions for frequency analysis settings: ---%
%------------------------------------------------------%

% Executes on selection change in DropdownFreqMethod.
function DropdownFreqMethod_Callback(hObject, eventdata, handles)
DropdownOptions = get(handles.DropdownFreqMethod, 'String');
FreqMethod      = DropdownOptions{get(handles.DropdownFreqMethod, 'Value')};
FreqRes         = str2num(get(handles.TextboxFreqRes, 'String'));

if strcmp(FreqMethod, 'mtmconvol: Fixed Time Win.')
    message = {'WARNING:'; '';
        'For "mtmconvol" with fixed time-windows:'; 
        'Frequency resolution determines the size of the fixed time-window.'; '';
        ['Current freq. resolution: ',num2str(FreqRes),'Hz'];
        ['Fixed time-window size: ',num2str(1 / FreqRes),'s']};
    msgbox(message, 'WARNING:');
end

handles = UpdateTimeFreqSettings(handles);
guidata(hObject, handles);


%--- User enters frequency resolution for freq. analysis here: ---%
%-----------------------------------------------------------------%
function TextboxFreqRes_Callback(hObject, eventdata, handles)
FreqStart = str2num(get(handles.TextboxFreqFoiStart, 'String'));
FreqEnd   = str2num(get(handles.TextboxFreqFoiEnd,   'String'));
FreqRes   = str2num(get(handles.TextboxFreqRes,      'String'));

if isempty(FreqRes)
    msgbox('Error: Frequency resolution must be specified.', 'Error:');
    return;
end

DropdownOptions = get(handles.DropdownFreqMethod, 'String');
FreqMethod      = DropdownOptions{get(handles.DropdownFreqMethod, 'Value')};

if strcmp(FreqMethod, 'mtmconvol: Fixed Time Win.')
    TotalAnalysisTime = handles.time.End - handles.time.Start;
    FixedTimeWinSize  = 1 / FreqRes;
    
    if FixedTimeWinSize > TotalAnalysisTime
        message = {'ERROR:'; '';
            'The size of the fixed time-window for frequency analysis is larger';
            ['than the current total analysis time (',num2str(TotalAnalysisTime),' s).']; '';
            'For the method "mtmconvol" with fixed time-windows:';
            'Frequency resolution determines the size of the fixed time-window.'; '';
            ['Current freq. resolution: ',num2str(FreqRes),'Hz'];
            ['Fixed time-window size: ',num2str(1 / FreqRes),'s']; '';
            'Select a larger FreqRes value, or increase length of analysis window.'; ''};
        msgbox(message, 'ERROR:');
        
        set(handles.TextboxFreqRes, 'String', []);
        return;
    end
end

handles = UpdateTimeFreqSettings(handles);
guidata(hObject, handles);


%--- User enters start frequency for frequency analysis here: ---%
%----------------------------------------------------------------%
function TextboxFreqFoiStart_Callback(hObject, eventdata, handles)
FreqStart = str2num(get(handles.TextboxFreqFoiStart, 'String'));
FreqEnd   = str2num(get(handles.TextboxFreqFoiEnd,   'String'));

if isempty(FreqStart)
    msgbox('Error: Start frequency must be specified.', 'Error:');
    return;
end
if FreqStart > FreqEnd
    msgbox('Error: Start frequency cannot be greater than end frequency.', 'Error:');
    set(handles.TextboxFreqFoiStart, 'String', []);
    return;
end

handles = UpdateTimeFreqSettings(handles);
guidata(hObject, handles);


%--- User enters end frequency for frequency analysis here: ---%
%--------------------------------------------------------------%
function TextboxFreqFoiEnd_Callback(hObject, eventdata, handles)
FreqStart = str2num(get(handles.TextboxFreqFoiStart, 'String'));
FreqEnd   = str2num(get(handles.TextboxFreqFoiEnd,   'String'));

if isempty(FreqEnd)
    msgbox('Error: End frequency must be specified.', 'Error:');
    return;
end
if FreqEnd < FreqStart
    msgbox('Error: End frequency cannot be less than start frequency.', 'Error:');
    set(handles.TextboxFreqFoiEnd, 'String', []);
    return;
end

handles = UpdateTimeFreqSettings(handles);
guidata(hObject, handles);



%--- Executes on button press in ButtonTimelockFreqAdvSettings. ---%
%------------------------------------------------------------------%
function ButtonTimelockFreqAdvSettings_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Please select root directory first.', 'Warning:');
	return;
end
if isempty(handles.name.AnalysisID) || isempty(handles.paths.AnalysisID)
    msgbox('Warning: Create or load AnalysisID first.', 'Warning:');
    return;
end
%** Opens settings .m file: Upgrade to sub-GUI later (Make it modal).
% - Can run child GUI, then call: uiwait(gcf) OR waitfor(childGUI)
% - Have updatefcns read adv.settings output to update settings accordingly.
open([handles.paths.AnalysisID,'/SETTINGS/Settings_TimelockFreqAnalysis.m']);


%--- Updates time/freq analysis settings: ---%
%--------------------------------------------%
function OutputHandles = UpdateTimeFreqSettings(InputHandles)
handles      = InputHandles;
FTcfg        = handles.FTcfg;
PipeSettings = handles.PipeSettings;


% Read GUI to update COV & CSD settings:
if get(handles.CheckboxCovDemean, 'Value') == 1
	FTcfg.Timelock.removemean = 'yes';
else
	FTcfg.Timelock.removemean = 'no';
end


DropdownOptions = get(handles.DropdownCovCsdTime, 'String');
CovCsdWindow    = DropdownOptions{get(handles.DropdownCovCsdTime, 'Value')};

switch CovCsdWindow
	case 'All Time: Recommended'
		PipeSettings.Freq.CsdWindow     = 'all';
		FTcfg.Timelock.covariancewindow = 'all';
        
	case 'Analysis Window Only'
		PipeSettings.Freq.CsdWindow     = [handles.time.Start, handles.time.End];
		FTcfg.Timelock.covariancewindow = [handles.time.Start, handles.time.End];
        
	case 'Custom'
		CovTimeStart = str2num(get(handles.TextboxCovCsdTimeStart, 'String'));
		CovTimeEnd   = str2num(get(handles.TextboxCovCsdTimeEnd,   'String'));
		
        PipeSettings.Freq.CsdWindow     = [CovTimeStart, CovTimeEnd];
		FTcfg.Timelock.covariancewindow = [CovTimeStart, CovTimeEnd];
        
	otherwise
		error('Unrecognized Option');
end


% Read GUI to update frequency analysis settings:
DropdownOptions = get(handles.DropdownFreqMethod, 'String');
FreqMethod      = DropdownOptions{get(handles.DropdownFreqMethod, 'Value')};

FreqStart = str2num(get(handles.TextboxFreqFoiStart, 'String'));
FreqEnd   = str2num(get(handles.TextboxFreqFoiEnd,   'String'));
FreqRes   = str2num(get(handles.TextboxFreqRes,      'String'));

switch FreqMethod
    case 'mtmconvol: Fixed Time Win.'
        FTcfg.Freq.method = 'mtmconvol';
        
        message = {'Warning:'; '';
                'For mtmconvol with fixed time-windows, start and end frequency';
                'should be divisible by the frequency resolution.'};
            
        if round(FreqStart / FreqRes) ~= (FreqStart / FreqRes)
            FreqStart = FreqRes;
            set(handles.TextboxFreqFoiStart, 'String', num2str(FreqStart));
            msgbox(message, 'Warning:');
        end
        if round(FreqEnd / FreqRes) ~= (FreqEnd / FreqRes)
            FreqEnd = round(FreqEnd / FreqRes) * FreqRes;
            set(handles.TextboxFreqFoiEnd, 'String', num2str(FreqEnd));
            msgbox(message, 'Warning:');
        end

    case 'mtmconvol: Freq. Dep. Time Win.'
        FTcfg.Freq.method = 'mtmconvol';
        
    case 'mtmfft'
        FTcfg.Freq.method = 'mtmfft';
        
    case 'wavelet'
        FTcfg.Freq.method = 'wavelet';
        
    case 'tfr'
        FTcfg.Freq.method = 'tfr';
        
    otherwise
        error('Unrecognized option.'); 
end

handles.gui.FreqMethod = FreqMethod;
handles.gui.FreqStart  = FreqStart;
handles.gui.FreqEnd    = FreqEnd;
handles.gui.FreqRes    = FreqRes;


% Enable/Disable GUI components:
set(findall(handles.PanelTimeFreqAnalysis, '-property', 'Enable'), 'Enable', 'on');

if ~strcmp(CovCsdWindow, 'Custom')
	set(handles.TextboxCovCsdTimeStart,     'Enable', 'off');
	set(handles.TextboxCovCsdTimeEnd,       'Enable', 'off');
	set(handles.TextCovCsdTimeStart,        'Enable', 'off');
	set(handles.TextCovCsdTimeEnd,          'Enable', 'off');
	set(handles.TextCovCsdCustomTimeHeader, 'Enable', 'off');
end

% Set output handles:
handles.PipeSettings = PipeSettings;
handles.FTcfg        = FTcfg;
OutputHandles        = handles;


%--- Load time/freq analysis settings: ---%
%-----------------------------------------%
function OutputHandles = LoadTimeFreqSettings(InputHandles, LoadedMat)
handles		 = InputHandles;
time         = handles.time;
FTcfg        = handles.FTcfg;
PipeSettings = handles.PipeSettings;

% Load settings:
FTcfg.Timelock.removemean        = LoadedMat.FTcfg.Timelock.removemean;
FTcfg.Timelock.covariancewindow  = LoadedMat.FTcfg.Timelock.covariancewindow;
PipeSettings.Freq.CsdWindow      = LoadedMat.PipeSettings.Freq.CsdWindow;

FTcfg.Freq.method      = LoadedMat.FTcfg.Freq.method;
handles.gui.FreqMethod = LoadedMat.gui.FreqMethod;
handles.gui.FreqStart  = LoadedMat.gui.FreqStart;
handles.gui.FreqEnd    = LoadedMat.gui.FreqEnd;
handles.gui.FreqRes    = LoadedMat.gui.FreqRes;


% Load COV demean:
if strcmp(FTcfg.Timelock.removemean, 'yes')
	set(handles.CheckboxCovDemean, 'Value', 1);
else
	set(handles.CheckboxCovDemean, 'Value', 0);
end

% Load time selection type for COV/CSD:
if strcmp(FTcfg.Timelock.covariancewindow, 'all')
    CovCsdTime = 'All Time: Recommended';
elseif isequal(FTcfg.Timelock.covariancewindow, [time.Start, time.End])
    CovCsdTime = 'Analysis Window Only';
else
    CovCsdTime = 'Custom';
end

DropdownOptions = get(handles.DropdownCovCsdTime, 'String');
DropdownValue   = find(strcmp(DropdownOptions, CovCsdTime));
set(handles.DropdownCovCsdTime, 'Value', DropdownValue);

if isempty(DropdownValue)
	error('Option does not exist in dropdown list.');  % error-check
end

% Load custom COV/CSD time:
if strcmp(CovCsdTime, 'Custom')
	TimeStart = num2str(FTcfg.Timelock.covariancewindow(1), '%1.3f');
	TimeEnd   = num2str(FTcfg.Timelock.covariancewindow(2), '%1.3f');
	set(handles.TextboxCovCsdTimeStart, 'String', TimeStart);
    set(handles.TextboxCovCsdTimeEnd,   'String', TimeEnd);
end

% Load freq analysis settings:
DropdownOptions = get(handles.DropdownFreqMethod, 'String');
DropdownValue   = find(strcmp(DropdownOptions, LoadedMat.gui.FreqMethod));
set(handles.DropdownFreqMethod, 'Value', DropdownValue);

if isempty(DropdownValue)
    error('Option does not exist in dropdown list.');  % error-check
end

set(handles.TextboxFreqFoiStart, 'String', num2str(LoadedMat.gui.FreqStart));
set(handles.TextboxFreqFoiEnd,   'String', num2str(LoadedMat.gui.FreqEnd));
set(handles.TextboxFreqRes,      'String', num2str(LoadedMat.gui.FreqRes));


% Set output handles:
handles.PipeSettings = PipeSettings;
handles.FTcfg        = FTcfg;
OutputHandles        = handles;


%--- Reset time/freq analysis settings: ---%
%------------------------------------------%
function OutputHandles = ResetTimeFreqSettings(InputHandles)
handles      = InputHandles;
FTcfg        = handles.FTcfg;
PipeSettings = handles.PipeSettings;

% Reset settings:
FTcfg.Timelock.removemean        = 'no';
FTcfg.Timelock.covariancewindow  = 'all';
PipeSettings.Freq.CsdWindow      = 'all';

FTcfg.Freq.method      = 'mtmfft';
handles.gui.FreqMethod = 'mtmfft';
handles.gui.FreqStart  = 2;
handles.gui.FreqEnd    = 50;
handles.gui.FreqRes    = 2;

% Reset GUI panel:
set(handles.CheckboxCovDemean,      'Value',  0);
set(handles.DropdownCovCsdTime,     'Value',  1);
set(handles.TextboxCovCsdTimeStart, 'String', []);
set(handles.TextboxCovCsdTimeEnd,   'String', []);

set(handles.DropdownFreqMethod,  'Value', 1);
set(handles.TextboxFreqFoiStart, 'String', num2str(handles.gui.FreqStart));
set(handles.TextboxFreqFoiEnd,   'String', num2str(handles.gui.FreqEnd));
set(handles.TextboxFreqRes,      'String', num2str(handles.gui.FreqRes));

% Set output handles:
handles.PipeSettings = PipeSettings;
handles.FTcfg        = FTcfg;
OutputHandles        = handles;




%=========================================%
% FUNCTIONS FOR SOURCE ANALYSIS SETTINGS: %
%=========================================%

%--- GUI functions for source analysis: ---%
%------------------------------------------%

% User selects source method here:
function DropdownSourceMethod_Callback(hObject, eventdata, handles)
DropdownOptions = get(handles.DropdownSourceMethod, 'String');
SourceMethod    = DropdownOptions{get(handles.DropdownSourceMethod, 'Value')};

if ismember(SourceMethod, {'dics', 'pcc'})
    message = {'Warning:'; '';
        'DICS and PCC source methods run on frequency-domain data';
        'output from frequency analysis (ft_freqanalysis).'; '';
        'All other methods are for time-domain data output from';
        'timelocked analysis (ft_timelockanalysis).'};
    msgbox(message, 'Warning:')
    
else
    message = {'Note:'; '';
        'All source methods except for DICS and PCC run on time-domain data';
        'output from timelocked analysis (ft_timelockanalysis).'; '';
        'DICS and PCC source methods run on frequency-domain data.'};
    msgbox(message, 'Note:')
end
    
if strcmp(SourceMethod, 'mne')
    message = {'Warning:';
        'Freesurfer & MNESuite mesh generation not implemented yet.';
        'Mesh from headmodel generation will be used instead.'};
    msgbox(message, 'Warning:')
end

handles = UpdateSourceSettings(handles);
guidata(hObject, handles);


% User selects source output statistic here:
function DropdownSourceOutput_Callback(hObject, eventdata, handles)
handles = UpdateSourceSettings(handles);
guidata(hObject, handles);

% User selects type of source contrast here:
function PanelSourceAnalysis_SelectionChangeFcn(hObject, eventdata, handles)
handles = UpdateSourceSettings(handles);
guidata(hObject, handles);

% Checkbox for EstNoise Common Filter:
function CheckboxEstNoiseCommonFilt_Callback(hObject, eventdata, handles)
handles = UpdateSourceSettings(handles);
guidata(hObject, handles);


%--- User enters start time for contrasting baseline: ---%
%--------------------------------------------------------%
function TextboxDiffBaseStart_Callback(hObject, eventdata, handles)
BaselineStart = str2num(get(handles.TextboxDiffBaseStart, 'String'));
BaselineEnd   = str2num(get(handles.TextboxDiffBaseEnd,   'String'));
    
if isempty(BaselineStart)
    msgbox('Error: Baseline time for DiffBase not specified.', 'Error:')
    return;
end
if BaselineStart > BaselineEnd
    msgbox('Error: Baseline start is greater than end.', 'Error:')
    set(handles.TextboxDiffBaseStart, 'String', []);
    return;
end

handles = UpdateSourceSettings(handles);
guidata(hObject, handles);


%--- User enters end time for contrasting baseline: ---%
%------------------------------------------------------%
function TextboxDiffBaseEnd_Callback(hObject, eventdata, handles)
BaselineStart = str2num(get(handles.TextboxDiffBaseStart, 'String'));
BaselineEnd   = str2num(get(handles.TextboxDiffBaseEnd,   'String'));
    
if isempty(BaselineEnd)
    msgbox('Error: Baseline end time not specified.', 'Error:')
    set(handles.TextboxDiffBaseEnd, 'String', []);
    return;
end
if BaselineEnd < BaselineStart
    msgbox('Error: Baseline end cannot be less than start.', 'Error:')
    set(handles.TextboxDiffBaseEnd, 'String', []);
    return;
end

handles = UpdateSourceSettings(handles);
guidata(hObject, handles);


%--- Executes on button press in ButtonCommonFilterCond. ---%
%--------------------------------------------------------------%
function ButtonCommonFilterCond_Callback(hObject, eventdata, handles)
if isempty(handles.gui.RootpathFolders) || isempty(handles.gui.DetectedCondIDs)
    return;
end

% Set common filter CondID for EstNoise:
FolderIndex    = get(handles.ListboxRootpathFolders, 'Value');
RootpathFolder = handles.gui.RootpathFolders{FolderIndex};

CondIndex        = get(handles.ListboxDetectedCondIDs, 'Value');
TargetCondID     = handles.gui.DetectedCondIDs{CondIndex};
FolderCondString = [RootpathFolder,': ',TargetCondID];

handles.gui.CommonFilterCondID{1} = TargetCondID;
handles.gui.CommonFilterCondID{2} = RootpathFolder;
handles.gui.CommonFilterCondID{3} = FolderCondString;

% Update GUI:
handles = UpdateSourceSettings(handles);

% Save handles:
guidata(hObject, handles);


%--- Executes on button press in ButtonDiffCondControl. ---%
%-------------------------------------------------------------%
function ButtonDiffCondControl_Callback(hObject, eventdata, handles)
if isempty(handles.gui.RootpathFolders) || isempty(handles.gui.DetectedCondIDs)
    return;
end

% Set control CondID for DiffCond:
FolderIndex    = get(handles.ListboxRootpathFolders, 'Value');
RootpathFolder = handles.gui.RootpathFolders{FolderIndex};

CondIndex        = get(handles.ListboxDetectedCondIDs, 'Value');
TargetCondID     = handles.gui.DetectedCondIDs{CondIndex};
FolderCondString = [RootpathFolder,': ',TargetCondID];

handles.gui.ControlCondID{1} = TargetCondID;
handles.gui.ControlCondID{2} = RootpathFolder;
handles.gui.ControlCondID{3} = FolderCondString;

% Update GUI:
handles = UpdateSourceSettings(handles);

% Save handles:
guidata(hObject, handles);


%--- Detect input MEG files for common-filter / control: ---%
%-----------------------------------------------------------%
function OutputHandles = DetectCommonFiltControlData(InputHandles)
handles = InputHandles;

% Start Waitbox:
WaitBox = StartWaitBox('UPDATING COMMON-FILTER / CONTROL DATA:');

% Update common-filter & control dataset paths:
handles.gui.CommonFiltDataFiles = [];
handles.gui.ControlDataFiles    = [];

for g = 1:length(handles.name.GroupID)
    for s = 1:length(handles.name.SubjID{g})
        
        if ~isempty(handles.gui.CommonFilterCondID)
            Rootpath       = handles.paths.Rootpath;
            RootpathFolder = handles.gui.CommonFilterCondID{2};
            
            GroupFolder    = ['GroupID_',handles.name.GroupID{g}];
            CondID         = handles.gui.CommonFilterCondID{1};
            NameData	   = [handles.name.SubjID{g}{s},'_PreprocMEG.mat'];
            
            handles.gui.CommonFiltDataFiles{g}{s} = ...
                [Rootpath,'/',RootpathFolder,'/',GroupFolder,'/',CondID,'/',NameData];
        end
        
        if ~isempty(handles.gui.ControlCondID)
            Rootpath = handles.paths.Rootpath;
            RootpathFolder = handles.gui.ControlCondID{2};
            
            GroupFolder = ['GroupID_',handles.name.GroupID{g}];
            CondID      = handles.gui.ControlCondID{1};
            NameData    = [handles.name.SubjID{g}{s},'_PreprocMEG.mat'];
            
            handles.gui.ControlDataFiles{g}{s} = ...
                [Rootpath,'/',RootpathFolder,'/',GroupFolder,'/',CondID,'/',NameData];
        end
        
    end  % Subj
end  % Group

% Set output handles:
OutputHandles = handles;
close(WaitBox);


%--- Textbox to display selected common-filter or control CondID: ---%
%--------------------------------------------------------------------%
function TextboxCommonFiltControlCond_Callback(hObject, eventdata, handles)
PipeSettings = handles.PipeSettings;
EnteredText  = get(handles.TextboxCommonFiltControlCond, 'String');

if strcmp(PipeSettings.Source.Contrast, 'EstNoise') && ...
        strcmp(PipeSettings.Source.EstNoiseCommonFilt, 'yes')
    
    if ~isempty(handles.gui.CommonFilterCondID) && ...
            ~isequal(EnteredText, handles.gui.CommonFilterCondID{3})
        
        set(handles.TextboxCommonFiltControlCond, 'String', handles.gui.CommonFilterCondID{3});
        msgbox('Note: Use button to change common-filter CondID.')
    end

elseif strcmp(PipeSettings.Source.Contrast, 'DiffCond')
    if ~isempty(handles.gui.ControlCondID) && ...
            ~isequal(EnteredText, handles.gui.ControlCondID{3})
        
        set(handles.TextboxCommonFiltControlCond, 'String', handles.gui.ControlCondID{3});
        msgbox('Note: Use button to change control CondID.')
    end
end


%--- Executes on button press in ButtonSourceAdvSettings. ---%
%------------------------------------------------------------%
function ButtonSourceAdvSettings_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Please select root directory first.', 'Warning:');
	return;
end
if isempty(handles.name.AnalysisID) || isempty(handles.paths.AnalysisID)
    msgbox('Warning: Create or load AnalysisID first.', 'Warning:');
    return;
end
%** Opens settings .m file: Upgrade to sub-GUI later (Make it modal).
% - Can run child GUI, then call: uiwait(gcf) OR waitfor(childGUI)
% - Have updatefcns read adv.settings output to update settings accordingly.
open([handles.paths.AnalysisID,'/SETTINGS/Settings_SourceAnalysis.m']);


%--- Updates source analysis settings: ---%
%-----------------------------------------%
function OutputHandles = UpdateSourceSettings(InputHandles)
handles      = InputHandles;
FTcfg        = handles.FTcfg;
PipeSettings = handles.PipeSettings;

% Read GUI to update source method:
DropdownOptions = get(handles.DropdownSourceMethod, 'String');
SourceMethod    = DropdownOptions{get(handles.DropdownSourceMethod, 'Value')};

ViableOptions = {'lcmv', 'sam', 'dics', 'pcc', 'loreta', 'mne', 'rv', 'music', 'mvl'};
if ismember(SourceMethod, ViableOptions)
	FTcfg.Source.method = SourceMethod;
else
	error('Unrecognized Option.');  % error-check
end

% Read GUI to update source contrast:
if get(handles.ButtonSourceEstNoise, 'Value') == 1
	PipeSettings.Source.Contrast = 'EstNoise';
    
    if get(handles.CheckboxEstNoiseCommonFilt, 'Value') == 1
        PipeSettings.Source.EstNoiseCommonFilt = 'yes';
        
        if ~isempty(handles.gui.CommonFilterCondID)
            set(handles.TextboxCommonFiltControlCond,...
                'String', handles.gui.CommonFilterCondID{3});
        else
            set(handles.TextboxCommonFiltControlCond, 'String', 'None Selected.');
        end
        
    else
        PipeSettings.Source.EstNoiseCommonFilt = 'no';
        set(handles.TextboxCommonFiltControlCond, 'String', 'None Selected.');
    end
    
elseif get(handles.ButtonSourceDiffCond, 'Value') == 1
	PipeSettings.Source.Contrast = 'DiffCond';
    
    if ~isempty(handles.gui.ControlCondID)
        set(handles.TextboxCommonFiltControlCond, 'String', handles.gui.ControlCondID{3});
    else
        set(handles.TextboxCommonFiltControlCond, 'String', 'None Selected.');
    end
    
elseif get(handles.ButtonSourceDiffBase, 'Value') == 1
	BaselineStart = str2num(get(handles.TextboxDiffBaseStart, 'String'));
	BaselineEnd   = str2num(get(handles.TextboxDiffBaseEnd,   'String'));

    PipeSettings.Source.Contrast     = 'DiffBase';
	PipeSettings.Source.DiffBaseTime = [BaselineStart, BaselineEnd];
    
else
	error('Unrecognized Option');
end

% Read GUI to update source output:
DropdownOptions = get(handles.DropdownSourceOutput, 'String');
SourceOutput    = DropdownOptions{get(handles.DropdownSourceOutput, 'Value')};
switch SourceOutput
	case 'PseudoZ'
		PipeSettings.Source.Output = 'PseudoZ';
	case 'PseudoT'
		PipeSettings.Source.Output = 'PseudoT';
	case 'PseudoF'
		PipeSettings.Source.Output = 'PseudoF';
	otherwise
		error('Unrecognized Option');  % error-check
end

if ~strcmp(PipeSettings.Source.Output, 'PseudoZ') &&...
		get(handles.ButtonSourceEstNoise, 'Value') == 1
    message = {'Warning:'; 'Output for EstNoise must be in PseudoZ.'};
    msgbox(message, 'Warning:');
    
    PipeSettings.Source.Output = 'PseudoZ';
    DropdownValue = find(strcmp(DropdownOptions, 'PseudoZ'));
    set(handles.DropdownSourceOutput, 'Value', DropdownValue); 
    
elseif strcmp(PipeSettings.Source.Output, 'PseudoZ') &&...
		get(handles.ButtonSourceEstNoise, 'Value') == 0
    message = {'Warning:'; 'PseudoZ output is only available for EstNoise.'};
	msgbox(message, 'Warning:')
    
	PipeSettings.Source.Output = 'PseudoF';
    DropdownValue = find(strcmp(DropdownOptions, 'PseudoF'));
    set(handles.DropdownSourceOutput, 'Value', DropdownValue);
end

% Enable/Disable GUI components:
set(findall(handles.PanelSourceAnalysis, '-property', 'Enable'), 'Enable', 'on');

if ~strcmp(PipeSettings.Source.Contrast, 'EstNoise')
    set(handles.CheckboxEstNoiseCommonFilt, 'Enable', 'off');
    set(handles.ButtonCommonFilterCond,     'Enable', 'off');
else
    if get(handles.CheckboxEstNoiseCommonFilt, 'Value') == 0
        set(handles.ButtonCommonFilterCond, 'Enable', 'off');
    end
end

if ~strcmp(PipeSettings.Source.Contrast, 'DiffCond')
    set(handles.ButtonDiffCondControl, 'Enable', 'off')
end

if ~ismember(PipeSettings.Source.Contrast, {'EstNoise', 'DiffCond'})
    set(handles.TextboxCommonFiltControlCond,     'Enable', 'off');
	set(handles.TextCommonFiltControlCondHeader,  'Enable', 'off');
    
elseif strcmp(PipeSettings.Source.Contrast, 'EstNoise') && ...
        get(handles.CheckboxEstNoiseCommonFilt, 'Value') == 0
    set(handles.TextboxCommonFiltControlCond,     'Enable', 'off');
	set(handles.TextCommonFiltControlCondHeader,  'Enable', 'off');
end

if ~strcmp(PipeSettings.Source.Contrast, 'DiffBase')
    set(handles.TextboxDiffBaseStart, 'Enable', 'off');
	set(handles.TextboxDiffBaseEnd,   'Enable', 'off');
	set(handles.TextDiffBaseStart,    'Enable', 'off');
	set(handles.TextDiffBaseEnd,      'Enable', 'off');
end

% Redetect paths for common-filter & control datasets:
handles = DetectCommonFiltControlData(handles);

% Set output handles:
handles.PipeSettings = PipeSettings;
handles.FTcfg        = FTcfg;
OutputHandles        = handles;


%--- Load source analysis settings: ---%
%--------------------------------------%
function OutputHandles = LoadSourceSettings(InputHandles, LoadedMat)
handles      = InputHandles;
FTcfg        = handles.FTcfg;
PipeSettings = handles.PipeSettings;

% Load source settings:
FTcfg.Source.method                    = LoadedMat.FTcfg.Source.method;
PipeSettings.Source.Output             = LoadedMat.PipeSettings.Source.Output;

PipeSettings.Source.Contrast           = LoadedMat.PipeSettings.Source.Contrast;
PipeSettings.Source.EstNoiseCommonFilt = LoadedMat.PipeSettings.Source.EstNoiseCommonFilt;
PipeSettings.Source.DiffBaseTime       = LoadedMat.PipeSettings.Source.DiffBaseTime;

handles.gui.CommonFilterCondID         = LoadedMat.gui.CommonFilterCondID;
handles.gui.ControlCondID              = LoadedMat.gui.ControlCondID;

% Load GUI source method:
DropdownOptions = get(handles.DropdownSourceMethod, 'String');
DropdownValue   = find(strcmp(DropdownOptions, FTcfg.Source.method));
set(handles.DropdownSourceMethod, 'Value', DropdownValue);

if isempty(DropdownValue)
    error('Option does not exist in dropdown list.');  % error-check
end

% Load GUI source output:
switch PipeSettings.Source.Output
    case 'PseudoZ'
        SourceOutput = 'PseudoZ';
    case 'PseudoT'
        SourceOutput = 'PseudoT';
    case 'PseudoF'
        SourceOutput = 'PseudoF';
    otherwise
        error('Unrecognized Option.');  % error-check  
end

DropdownOptions = get(handles.DropdownSourceOutput, 'String');
DropdownValue   = find(strcmp(DropdownOptions, SourceOutput));
set(handles.DropdownSourceOutput, 'Value', DropdownValue);

if isempty(DropdownValue)
    error('Option does not exist in dropdown list.');  % error-check
end

% Load GUI source contrast:
switch PipeSettings.Source.Contrast
    case 'EstNoise'
        set(handles.ButtonSourceEstNoise, 'Value', 1);
		
		if strcmp(PipeSettings.Source.EstNoiseCommonFilt, 'yes')
			set(handles.CheckboxEstNoiseCommonFilt, 'Value', 1);
            
            if ~isempty(handles.gui.CommonFilterCondID)
                set(handles.TextboxCommonFiltControlCond,...
                    'String', handles.gui.CommonFilterCondID{3});
            else
                set(handles.TextboxCommonFiltControlCond, 'String', 'None Selected.');
            end
            
		else
			set(handles.CheckboxEstNoiseCommonFilt,   'Value',  0);
            set(handles.TextboxCommonFiltControlCond, 'String', 'None Selected.');
		end
		
    case 'DiffCond'
        set(handles.ButtonSourceDiffCond,  'Value', 1);
        
        if ~isempty(handles.gui.ControlCondID)
            set(handles.TextboxCommonFiltControlCond,...
                'String', handles.gui.ControlCondID{3});
        else
            set(handles.TextboxCommonFiltControlCond, 'String', 'None Selected.');
        end
		
    case 'DiffBase'
		BaseStart = num2str(PipeSettings.Source.DiffBaseTime(1), '%1.3f');
		BaseEnd   = num2str(PipeSettings.Source.DiffBaseTime(2), '%1.3f');
		
		set(handles.ButtonSourceDiffBase, 'Value',  1);
		set(handles.TextboxDiffBaseStart, 'String', BaseStart);
		set(handles.TextboxDiffBaseEnd,   'String', BaseEnd);
		
    otherwise
        error('Unrecognized Option.');  % error-check
end

% Redetect paths for common-filter & control datasets:
handles = DetectCommonFiltControlData(handles);

% Set output handles:
handles.PipeSettings = PipeSettings;
handles.FTcfg        = FTcfg;
OutputHandles        = handles;


%--- Reset source analysis settings: ---%
%---------------------------------------%
function OutputHandles = ResetSourceSettings(InputHandles)
handles      = InputHandles;
FTcfg        = handles.FTcfg;
PipeSettings = handles.PipeSettings;

% Reset source analysis settings:
FTcfg.Source.method                    = 'lcmv';
PipeSettings.Source.Output             = 'PseudoZ';

PipeSettings.Source.Contrast           = 'EstNoise';
PipeSettings.Source.EstNoiseCommonFilt = 'no';
PipeSettings.Source.DiffBaseTime       = [];

handles.gui.CommonFilterCondID         = [];
handles.gui.ControlCondID              = [];

handles.gui.CommonFiltDataFiles = [];
handles.gui.ControlDataFiles    = [];

% Reset GUI panel:
set(handles.DropdownSourceMethod,         'Value',  1);
set(handles.DropdownSourceOutput,         'Value',  1);

set(handles.ButtonSourceEstNoise,         'Value',  1);
set(handles.CheckboxEstNoiseCommonFilt,   'Value',  0);
set(handles.TextboxCommonFiltControlCond, 'String', 'None Selected');

set(handles.TextboxDiffBaseStart,         'String', []);
set(handles.TextboxDiffBaseEnd,           'String', []);

% Set output handles:
handles.PipeSettings = PipeSettings;
handles.FTcfg        = FTcfg;
OutputHandles        = handles;




%==============================================%
% FUNCTIONS FOR SOURCE NORMALISATION SETTINGS: %
%==============================================%

%--- Executes on button press in CheckboxResliceMri2Src. ---%
%-----------------------------------------------------------%
function CheckboxResliceMri2Src_Callback(hObject, eventdata, handles)
handles = UpdateNormaliseSettings(handles);
guidata(hObject, handles);


%--- Executes on button press in ButtonNormSettings. ---%
%-------------------------------------------------------%
function ButtonNormSettings_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Select root directory first.', 'Warning:');
	return;
end
if isempty(handles.name.AnalysisID) || isempty(handles.paths.AnalysisID)
    msgbox('Warning: Create or load AnalysisID first.', 'Warning:');
    return;
end
%** Opens settings .m file: Upgrade to sub-GUI later (Make it modal).
% - Can run child GUI, then call: uiwait(gcf) OR waitfor(childGUI)
% - Have updatefcns read adv.settings output to update settings accordingly.
open([handles.paths.AnalysisID,'/SETTINGS/Settings_SourceNormalise.m']);


%--- Update source norm. settings & GUI panel: ---%
%-------------------------------------------------%
function OutputHandles = UpdateNormaliseSettings(InputHandles)
handles      = InputHandles;
PipeSettings = handles.PipeSettings;

if get(handles.CheckboxResliceMri2Src, 'Value') == 1
    PipeSettings.NormSPM.ResliceMri2Src = 'yes';
else
    PipeSettings.NormSPM.ResliceMri2Src = 'no';
end

% Set output handles:
handles.PipeSettings = PipeSettings;
OutputHandles        = handles;


%--- Load source norm. settings & GUI panel: ---%
%-----------------------------------------------%
function OutputHandles = LoadNormaliseSettings(InputHandles, LoadedMat)
handles      = InputHandles;
PipeSettings = handles.PipeSettings;

% Load settings:
PipeSettings.NormSPM.ResliceMri2Src = LoadedMat.PipeSettings.NormSPM.ResliceMri2Src;

% Load GUI panel:
if strcmp(PipeSettings.NormSPM.ResliceMri2Src, 'yes')
    set(handles.CheckboxResliceMri2Src, 'Value', 1);
else
    set(handles.CheckboxResliceMri2Src, 'Value', 0);
end

% Set output handles:
handles.PipeSettings = PipeSettings;
OutputHandles        = handles;


%--- Reset source norm. settings & GUI panel: ---%
%------------------------------------------------%
function OutputHandles = ResetNormaliseSettings(InputHandles)
handles      = InputHandles;
PipeSettings = handles.PipeSettings;

PipeSettings.NormSPM.ResliceMri2Src = 'no';
set(handles.CheckboxResliceMri2Src, 'Value', 0);

% Set output handles:
handles.PipeSettings = PipeSettings;
OutputHandles        = handles;




%======================================%
% FUNCTIONS FOR NIFTI & AFNI SETTINGS: %
%======================================%

%--- GUI functions for NIFTI & AFNI settings: ---%
%------------------------------------------------%
function CheckboxKeepNifti3DFiles_Callback(hObject, eventdata, handles)
handles = UpdateNiftiAfniSettings(handles);
guidata(hObject, handles);

function CheckboxAfniNaNFix_Callback(hObject, eventdata, handles)
handles = UpdateNiftiAfniSettings(handles);
guidata(hObject, handles);

function DropdownAfniTemplate_Callback(hObject, eventdata, handles)
message = {'Warning: If files were normalised using this pipeline,';
	'keep the AFNI template in MNI!'};
msgbox(message, 'Warning:')

handles = UpdateNiftiAfniSettings(handles);
guidata(hObject, handles);


%--- Updates NIFTI & AFNI conversion settings: ---%
%-------------------------------------------------%
function OutputHandles = UpdateNiftiAfniSettings(InputHandles)
handles      = InputHandles;
PipeSettings = handles.PipeSettings;

% Read GUI to update settings:
if get(handles.CheckboxKeepNifti3DFiles, 'Value') == 1
    PipeSettings.Afni4D.KeepNifti3D = 'yes';
else
    PipeSettings.Afni4D.KeepNifti3D = 'no';
end

if get(handles.CheckboxAfniNaNFix, 'Value') == 1
	PipeSettings.Afni4D.NaNFix = 'yes';
else
	PipeSettings.Afni4D.NaNFix = 'no';
end

DropdownOptions = get(handles.DropdownAfniTemplate, 'String');
AfniTemplate    = DropdownOptions{get(handles.DropdownAfniTemplate, 'Value')};
ViableOptions   = {'mni', 'tlrc', 'orig'};

if ismember(AfniTemplate, ViableOptions)
    PipeSettings.Afni4D.Template = AfniTemplate;
else
    error('Unrecognized Option.');  % error-check
end

% Set output handles:
handles.PipeSettings = PipeSettings;
OutputHandles        = handles;


%--- Load NIFTI & AFNI conversion settings: ---%
%----------------------------------------------%
function OutputHandles = LoadNiftiAfniSettings(InputHandles, LoadedMat)
handles		 = InputHandles;
PipeSettings = handles.PipeSettings;

% Load settings:
PipeSettings.Afni4D.KeepNifti3D = LoadedMat.PipeSettings.Afni4D.KeepNifti3D;
PipeSettings.Afni4D.NaNFix      = LoadedMat.PipeSettings.Afni4D.NaNFix;
PipeSettings.Afni4D.Template    = LoadedMat.PipeSettings.Afni4D.Template;

% Load GUI panel:
if strcmp(PipeSettings.Afni4D.KeepNifti3D, 'yes')
    set(handles.CheckboxKeepNifti3DFiles, 'Value', 1);
else
    set(handles.CheckboxKeepNifti3DFiles, 'Value', 0);
end

if strcmp(PipeSettings.Afni4D.NaNFix, 'yes')
    set(handles.CheckboxAfniNaNFix, 'Value', 1);
else
    set(handles.CheckboxAfniNaNFix, 'Value', 0);
end

DropdownOptions = get(handles.DropdownAfniTemplate, 'String');
DropdownValue   = find(strcmp(DropdownOptions, PipeSettings.Afni4D.Template));
set(handles.DropdownAfniTemplate, 'Value', DropdownValue);

if isempty(DropdownValue)
    error('Option does not exist in dropdown list.');  % error-check
end

% Set output handles:
handles.PipeSettings = PipeSettings;
OutputHandles        = handles;


%--- Reset NIFTI & AFNI conversion settings: ---%
%-----------------------------------------------%
function OutputHandles = ResetNiftiAfniSettings(InputHandles)
handles		 = InputHandles;
PipeSettings = handles.PipeSettings;

% Reset settings:
PipeSettings.Afni4D.KeepNifti3D = 'no';
PipeSettings.Afni4D.NaNFix      = 'yes';
PipeSettings.Afni4D.Template    = 'mni';

% Reset GUI panel:
set(handles.CheckboxAfniNaNFix,   'Value', 1);
set(handles.DropdownAfniTemplate, 'Value', 1);

% Set output handles:
handles.PipeSettings = PipeSettings;
OutputHandles        = handles;




%=======================================%
% FUNCTIONS TO GENERATE PIPELINE PATHS: %
%=======================================%

function OutputHandles = UpdatePipelinePaths(InputHandles)
handles		 = InputHandles;

paths		 = handles.paths;
time		 = handles.time;
FTcfg		 = handles.FTcfg;
PipeSettings = handles.PipeSettings;

Rootpath	 = handles.paths.Rootpath;
AnalysisPath = handles.paths.AnalysisID;

% Get FreqMethod:
CurrentDir = pwd;
cd([paths.AnalysisID,'/SETTINGS/']);

[~, ~, cfgFreq, ~] = Settings_TimelockFreqAnalysis (handles);
FreqMethod         = cfgFreq.method;

cd(CurrentDir);


%--- Set paths for input MEG & MRI data: ---%
%-------------------------------------------%
if ~isempty(handles.gui.AnatID)
	paths.MRIdata = handles.gui.InputAnatFiles;
end

switch handles.gui.MEGdataFiletype
	case 'Fieldtrip'
		paths.MEGdata = handles.gui.InputDataFiles;
	case 'NIFTI'
		paths.Nifti4DNormSource = handles.gui.InputDataFiles;
	case 'AFNI'
		paths.Afni4DNormSource  = handles.gui.InputDataFiles;
end


%--- Set paths for segmented & normalised MRI's: ---%
%---------------------------------------------------%
for g = 1:length(handles.name.GroupID)
	for s = 1:length(handles.name.SubjID{g})
		
		GroupFolder = ['GroupID_',handles.name.GroupID{g}];
		NameSegMRI  = [handles.name.SubjID{g}{s},'_SegMRI.mat'];
		NameNormMRI = [handles.name.SubjID{g}{s},'_NormMRI.mat'];
        
        NameNormMRINii  = [handles.name.SubjID{g}{s},'_NormMRI.nii'];
        NameNormMRIAfni = [handles.name.SubjID{g}{s},'_NormMRI+tlrc.BRIK'];
        
        NameMRINii  = [handles.name.SubjID{g}{s},'_PreprocMRI_RASorient.nii'];
        NameMRIAfni = [handles.name.SubjID{g}{s},'_PreprocMRI_RASorient+tlrc.BRIK'];
		
        
        % Segmented MRI paths:
		paths.SegMRI{g}{s} = ...
			[AnalysisPath,'/SEG_MRI/',GroupFolder,'/',NameSegMRI];
		
        
        % Normalised MRI paths:
		paths.NormMRI{g}{s} = ...
			[AnalysisPath,'/NORM_MRI/',GroupFolder,'/',NameNormMRI];
        
        paths.NormMRINii{g}{s} = ...
            [AnalysisPath,'/NORM_MRI_Nifti/',GroupFolder,'/',NameNormMRINii];
        
        paths.NormMRIAfni{g}{s} = ...
            [AnalysisPath,'/NORM_MRI_Afni/',GroupFolder,'/',NameNormMRIAfni];
        
        
        % Also get paths for PreprocMRI RASorient NIFTI & AFNI files:
        paths.MRIdataNii{g}{s} = ...
            [AnalysisPath,'/MRI_RASorient_Nifti/',GroupFolder,'/',NameMRINii];
        
        paths.MRIdataAfni{g}{s} = ...
            [AnalysisPath,'/MRI_RASorient_Afni/',GroupFolder,'/',NameMRIAfni];
		
	end
end


%--- Set paths for Timelock & Freq analysis: ---%
%-----------------------------------------------%
for g = 1:length(handles.name.GroupID)
	for s = 1:length(handles.name.SubjID{g})
		for c = 1:length(handles.name.CondID)
            
            GroupFolder = ['GroupID_',handles.name.GroupID{g}];
			SubjID		= handles.name.SubjID{g}{s};
			CondID		= handles.name.CondID{c};
            
			TimeFolder = ['TIMELOCK_',time.str.StartEnd];
			NameTime   = [SubjID,'_timelock.mat'];
            NameGFP    = [SubjID,'_globalmeanfield.mat'];
			
			FreqFolder = ['FREQ_',FreqMethod,'_',time.str.StartEnd];
			NameFreq   = [SubjID,'_freq.mat'];
			
			paths.Timelock{g}{s,c} = ...
				[AnalysisPath,'/',TimeFolder,'/',GroupFolder,'/',CondID,'/',NameTime];
            
            paths.GlobalMeanField{g}{s,c} = ...
                [AnalysisPath,'/',TimeFolder,'/',GroupFolder,'/',CondID,'/',NameGFP];
			
			paths.Freq{g}{s,c} = ...
				[AnalysisPath,'/',FreqFolder,'/',GroupFolder,'/',CondID,'/',NameFreq];
            
        end
    end
end


% Group-averaged timelock & freq analysis paths:
for g = 1:length(handles.name.GroupID)
	for c = 1:length(handles.name.CondID)
        
        GroupFolder = ['GroupID_',handles.name.GroupID{g}];
        CondID		= handles.name.CondID{c};
        NumSubjStr = num2str(length(handles.name.SubjID{g}));
        
        TimeFolder = ['TIMELOCK_',time.str.StartEnd];
        NameTime   = ['GrpAvg_',NumSubjStr,'subj_timelock.mat'];
        NameGFP    = ['GrpAvg_',NumSubjStr,'subj_globalmeanfield.mat'];
        
        FreqFolder = ['FREQ_',FreqMethod,'_',time.str.StartEnd];
        NameFreq   = ['GrpAvg_',NumSubjStr,'subj_freq.mat'];
        
        paths.TimelockGrpAvg{g}{c} = ...
            [AnalysisPath,'/',TimeFolder,'/',GroupFolder,'/',CondID,'/',NameTime];
        
        paths.GlobalMeanFieldGrpAvg{g}{c} = ...
            [AnalysisPath,'/',TimeFolder,'/',GroupFolder,'/',CondID,'/',NameGFP];
        
        paths.FreqGrpAvg{g}{c} = ...
            [AnalysisPath,'/',FreqFolder,'/',GroupFolder,'/',CondID,'/',NameFreq];
        
    end  % Cond
end  % Group


%--- Set paths for headmodel to source sections: ---%
%---------------------------------------------------%
for g = 1:length(handles.name.GroupID)
	for s = 1:length(handles.name.SubjID{g})
		for c = 1:length(handles.name.CondID)
			
			GroupFolder = ['GroupID_',handles.name.GroupID{g}];
			SubjID		= handles.name.SubjID{g}{s};
			CondID		= handles.name.CondID{c};
			
			
			% Headmodel & Leadfield paths:
			HdmFolder   = ['HDMLEAD_',FTcfg.Hdm.method,'_',num2str(FTcfg.Lead.grid.resolution),'mm'];
			NameHdm     = [SubjID,'_hdm.mat'];
			NameLead    = [SubjID,'_lead.mat'];
			
			paths.Hdm{g}{s,c} = ...
				[AnalysisPath,'/',HdmFolder,'/',GroupFolder,'/',CondID,'/',NameHdm];
			
			paths.Lead{g}{s,c} = ...
				[AnalysisPath,'/',HdmFolder,'/',GroupFolder,'/',CondID,'/',NameLead];
			
			
			% Source filter paths:
			switch PipeSettings.Source.Contrast
				case 'EstNoise'
					if strcmp(PipeSettings.Source.EstNoiseCommonFilt, 'yes')
						CommonFiltCondID = handles.gui.CommonFilterCondID{1};
						FilterFolder     = ['CommonFilter_',CommonFiltCondID];
					else
						FilterFolder = ['Filter_',CondID];
					end
					
                case 'DiffCond'
                    ControlCondID = handles.gui.ControlCondID{1};
                    FilterFolder  = ['CommonFilter_',CondID,'_',ControlCondID];
					
				case 'DiffBase'
					BaseStart    = [num2str(PipeSettings.Source.DiffBaseTime(1), '%1.3f')];
					BaseEnd      = [num2str(PipeSettings.Source.DiffBaseTime(2), '%1.3f')];
					FilterFolder = ['CommonFilter_',CondID,'_BaseTime_',BaseStart,'s_',BaseEnd,'s'];
			end
			
			paths.SourceFilter{g}{s,c} = [AnalysisPath,'/SOURCE_FILTER/',...
				GroupFolder,'/',FilterFolder,'/',SubjID,'_SourceFilter.mat'];
            
            
            % Source COV / CSD file paths:
            if ismember(FTcfg.Source.method, {'dics', 'pcc'})
                SourceMethodDomain = 'Freq';  % DICS & PCC are for freq-domain data.
            else
                SourceMethodDomain = 'Time';  % All other methods are for time-domain data.
            end
            
			switch SourceMethodDomain
				case 'Time'
					paths.SourceCovCsd{g}{s,c} = [AnalysisPath,'/SOURCE_FILTER/',...
						GroupFolder,'/',FilterFolder,'/',SubjID,'_COV.mat'];
					
				case 'Freq'
					paths.SourceCovCsd{g}{s,c} = [AnalysisPath,'/SOURCE_FILTER/',...
						GroupFolder,'/',FilterFolder,'/',SubjID,'_CSD_',FreqMethod,'.mat'];
			end
			
			
			% Source analysis paths:
			SrcMethod   = FTcfg.Source.method;
			SrcContrast = PipeSettings.Source.Contrast;
			SrcOutput   = PipeSettings.Source.Output;
			SrcParams   = [SrcMethod,'_',SrcContrast,'_',SrcOutput,'_',time.str.StartEnd];
			
			paths.SourceFolder{g}{s,c} = ...
				[AnalysisPath,'/SOURCE_',SrcParams,'/',GroupFolder,'/',CondID,'/',SubjID];
            
            paths.SourceNiiFolder{g}{s,c} = ...
                [AnalysisPath,'/SOURCE_RASorient_Nifti3D/',GroupFolder,'/',CondID,'/',SubjID];
			
			paths.NormSourceFolder{g}{s,c} = ...
				[AnalysisPath,'/NORM_SOURCE/',GroupFolder,'/',CondID,'/',SubjID];
            
            paths.NormSourceNiiFolder{g}{s,c} = ...
                [AnalysisPath,'/NORM_SOURCE_Nifti3D/',GroupFolder,'/',CondID,'/',SubjID];

			for t = 1:size(time.Windows, 1)
				paths.Source{g}{s,c,t} = [paths.SourceFolder{g}{s,c}, ...
					'/Source_',time.str.Windows{t,3},'.mat'];
                
                paths.SourceNii{g}{s,c,t} = [paths.SourceNiiFolder{g}{s,c}, ...
                    '/InterpSourceRAS_',time.str.Windows{t,3},'.nii'];
				
				paths.NormSource{g}{s,c,t} = [paths.NormSourceFolder{g}{s,c}, ...
					'/NormSource_',time.str.Windows{t,3},'.mat'];
                
                paths.NormSourceNii{g}{s,c,t} = [paths.NormSourceNiiFolder{g}{s,c}, ...
                    '/NormSource_',time.str.Windows{t,3},'.nii'];
			end
	
		end  % Cond
	end  % Subj
end  % Group


%--- Set paths for group-averaged normsource files: ---%
%------------------------------------------------------%
for g = 1:length(handles.name.GroupID)
	for c = 1:length(handles.name.CondID)
		
		NumSubjStr  = num2str(length(handles.name.SubjID{g}));
		GroupFolder = ['GroupID_',handles.name.GroupID{g}];
		CondID		= handles.name.CondID{c};
		
		paths.GrpAvgFolder{g}{c} = [AnalysisPath, ...
            '/NORM_SOURCE/',GroupFolder,'/',CondID,'/GrpAvg_',NumSubjStr,'subj'];
        
        paths.GrpAvgNiiFolder{g}{c} = [AnalysisPath, ...
            '/NORM_SOURCE_Nifti3D/',GroupFolder,'/',CondID,'/GrpAvg_',NumSubjStr,'subj'];
		
		for t = 1:size(time.Windows, 1)
			paths.GrpAvg{g}{c,t} = [paths.GrpAvgFolder{g}{c}, ...
				'/GrpAvg_',time.str.Windows{t,3},'.mat'];
            
            paths.GrpAvgNii{g}{c,t} = [paths.GrpAvgNiiFolder{g}{c}, ...
                '/GrpAvg_',time.str.Windows{t,3},'.nii'];
		end
		
	end  % Cond
end  % Group	


%--- Set paths for Custom-Filter & Control datasets: ---%
%-------------------------------------------------------%

% Set paths for common-filter datasets:
if ~isempty(handles.gui.CommonFilterCondID)
    paths.MEGdataCommonFilt = handles.gui.CommonFiltDataFiles;
end


% Set paths for control condition datasets:
if ~isempty(handles.gui.ControlCondID)
    paths.MEGdataControl = handles.gui.ControlDataFiles;
    
    for g = 1:length(handles.name.GroupID)
        for s = 1:length(handles.name.SubjID{g})
            
            GroupFolder = ['GroupID_',handles.name.GroupID{g}];
            SubjID      = handles.name.SubjID{g}{s};
            CondID      = handles.gui.ControlCondID{1};
            
            % Source analysis paths:
            paths.SourceControlFolder{g}{s} = ...
                [AnalysisPath,'/SOURCE_',SrcParams,'/',GroupFolder,'/',CondID,'/',SubjID];
            
            for t = 1:size(time.Windows, 1)
                paths.SourceControl{g}{s,t} = [paths.SourceControlFolder{g}{s}, ...
                    '/Source_',time.str.Windows{t,3},'.mat'];
            end
            
        end  % Subj
    end  % Group
end


%--- Set paths for NIFTI4D and AFNI4D files: ---%
%-----------------------------------------------%
for g = 1:length(handles.name.GroupID)
	for s = 1:length(handles.name.SubjID{g})
		for c = 1:length(handles.name.CondID)
			
			GroupFolder = ['GroupID_',handles.name.GroupID{g}];
			SubjID		= handles.name.SubjID{g}{s};
			CondID		= handles.name.CondID{c};
			
            % NIFTI 4D paths for source & normsource:
            Nii4DSrcFolder = ...
                [AnalysisPath,'/SOURCE_RASorient_Nifti4D/',GroupFolder,'/',CondID];
            
            Nii4DNormSrcFolder = ...
                [AnalysisPath,'/NORM_SOURCE_Nifti4D/',GroupFolder,'/',CondID];
            
            NameNii4D = [SubjID,'_Nifti4D.nii'];
            
			if ~strcmp(handles.gui.MEGdataFiletype, 'NIFTI')
				paths.Nifti4DSource{g}{s,c}     = [Nii4DSrcFolder,'/',NameNii4D];
                paths.Nifti4DNormSource{g}{s,c} = [Nii4DNormSrcFolder,'/',NameNii4D];
            end
			
            % AFNI 4D paths for source & normsource:
            Afni4DSrcFolder = ...
                [AnalysisPath,'/SOURCE_RASorient_Afni4D/',GroupFolder,'/',CondID];
            
            Afni4DNormSrcFolder = ...
                [AnalysisPath,'/NORM_SOURCE_Afni4D/',GroupFolder,'/',CondID];
            
            NameAfni4D = [SubjID,'_Afni4D+tlrc.BRIK'];
            
			if ~strcmp(handles.gui.MEGdataFiletype, 'AFNI')
				paths.Afni4DSource{g}{s,c}     = [Afni4DSrcFolder,'/',NameAfni4D];
                paths.Afni4DNormSource{g}{s,c} = [Afni4DNormSrcFolder,'/',NameAfni4D];
            end
			
		end  % Cond
	end  % Subj
end  % Group


% NIFTI and AFNI 4D paths for group-average:
for g = 1:length(handles.name.GroupID)
    for c = 1:length(handles.name.CondID)
        
        NumSubjStr  = num2str(length(handles.name.SubjID{g}));
		GroupFolder = ['GroupID_',handles.name.GroupID{g}];
		CondID		= handles.name.CondID{c};
        
        Nii4DGrpAvgFolder = [AnalysisPath,'/NORM_SOURCE_Nifti4D/',GroupFolder,'/',CondID];
        NameNii4D         = ['GrpAvg_',NumSubjStr,'subj_Nifti4D.nii'];
        
        Afni4DGrpAvgFolder = [AnalysisPath,'/NORM_SOURCE_Afni4D/',GroupFolder,'/',CondID];
        NameAfni4D         = ['GrpAvg_',NumSubjStr,'subj_Afni4D+tlrc.BRIK'];
        
        paths.Nifti4DGrpAvg{g}{c} = [Nii4DGrpAvgFolder,'/',NameNii4D];
        paths.Afni4DGrpAvg{g}{c}  = [Afni4DGrpAvgFolder,'/',NameAfni4D];
        
    end  % Cond
end  % Group


% Set output handles:
handles.paths = paths;
OutputHandles = handles;


%--- Reset pipeline paths: ---%
%-----------------------------%
function OutputHandles = ResetPipelinePaths(InputHandles)
handles = InputHandles;
paths   = handles.paths;

% Reset paths:
paths.MRIdata = [];
paths.MEGdata = [];

paths.MRIdataNii  = [];
paths.MRIdataAfni = [];

paths.SegMRI      = [];
paths.NormMRI     = [];
paths.NormMRINii  = [];
paths.NormMRIAfni = [];

paths.Hdm  = [];
paths.Lead = [];

paths.Timelock        = [];
paths.GlobalMeanField = [];
paths.Freq            = [];

paths.TimelockGrpAvg        = [];
paths.GlobalMeanFieldGrpAvg = [];
paths.FreqGrpAvg            = [];

paths.SourceCovCsd = [];
paths.SourceFilter = [];

paths.SourceFolder	  = [];
paths.Source          = [];
paths.SourceNiiFolder = [];
paths.SourceNii       = [];

paths.NormSourceFolder    = [];
paths.NormSource		  = [];
paths.NormSourceNiiFolder = [];
paths.NormSourceNii       = [];

paths.GrpAvgFolder    = [];
paths.GrpAvg          = [];
paths.GrpAvgNiiFolder = [];
paths.GrpAvgNii       = [];

paths.MEGdataCommonFilt   = [];
paths.MEGdataControl      = [];
paths.SourceControlFolder = [];
paths.SourceControl       = [];

paths.Nifti4DSource     = [];
paths.Nifti4DNormSource	= [];
paths.Nifti4DGrpAvg     = [];
paths.Afni4DSource      = [];
paths.Afni4DNormSource  = [];
paths.Afni4DGrpAvg      = [];

% Set output handles:
handles.paths = paths;
OutputHandles = handles;




%================================%
% FUNCTION TO SAVE BUILDER .MAT: %
%================================%

%--- Executes on button press in ButtonSaveBuilderMat. ---%
%---------------------------------------------------------%
function ButtonSaveBuilderMat_Callback(hObject, eventdata, handles)
PipeSettings = handles.PipeSettings;
FTcfg        = handles.FTcfg;
time         = handles.time;

SourceContrast     = PipeSettings.Source.Contrast;
EstNoiseCommonFilt = PipeSettings.Source.EstNoiseCommonFilt;


% Check initial paths:
if isempty(handles.paths.Rootpath)
	msgbox('Error: Root directory has not been selected.', 'Error:');
	return;
end
if isempty(handles.name.AnalysisID) || isempty(handles.paths.AnalysisID)
    msgbox('Error: AnalysisID has not been specified.', 'Error:');
    return;
end

% Make sure Rootpath & AnalysisID don't have spaces:
CheckSpaces1 = strfind(handles.paths.Rootpath, ' ');
CheckSpaces2 = strfind(handles.paths.AnalysisID, ' ');
if ~isempty(CheckSpaces1) || ~isempty(CheckSpaces2)
    message = {'Error: Target directory or AnalysisID contains spaces.';
        'AFNI functions cannot read folder & file paths with spaces.';
        'AFNI functions are required for file conversions later on.'};
    msgbox(message, 'Error:')
end

% Check for basic input settings:
if isempty(handles.name.GroupID)
	msgbox('Error: Groups have not been added.', 'Error:');
	return;
end

if isempty(handles.name.SubjID)
	msgbox('Error: Subjects have not been added.', 'Error:')
	return;
else
    for g = 1:length(handles.name.GroupID)
        if isempty(handles.name.SubjID{g})
            msgbox('Error: Not all Groups have Subjects added.', 'Error:')
            return;
        end
    end
end

if isempty(handles.name.CondID)
	msgbox('Error: Conditions have not been added.', 'Error:')
	return;
end

% Check input MEG and MRI files:
if isempty(handles.gui.AnatID)
    msgbox('Error: AnatID has not been specified.', 'Error:');
    return;
end
if isempty(handles.gui.InputAnatFiles) || isempty(handles.gui.StatusAnat)
    msgbox('Error: Input MRI files not specified.', 'Error:');
    return;
end
if isempty(handles.gui.InputDataFiles) || isempty(handles.gui.StatusData)
    msgbox('Error: Input MEG files not specified.', 'Error:');
    return;
end

for g = 1:length(handles.name.GroupID)
    if max(cellfun(@isempty, handles.gui.InputAnatFiles{g})) > 0
        msgbox('Warning: Update input MRI files & status first.', 'Warning:');
        return;
    end
    if max(cellfun(@isempty, handles.gui.StatusAnat{g})) > 0
        msgbox('Warning: Update input MRI files & status first.', 'Warning:');
        return;
    end
    if max(cellfun(@isempty, handles.gui.InputDataFiles{g})) > 0
        msgbox('Warning: Update input MEG files & status first.', 'Warning:');
        return;
    end
    if max(cellfun(@isempty, handles.gui.StatusData{g})) > 0
        msgbox('Warning: Update input MEG files & status first.', 'Warning:');
        return;
    end
end


% Load sample dataset to get time "boundaries":
SampleData = LoadFTmat(handles.gui.InputDataFiles{1}{1,1}, 'BuilderGUI');
if isempty(SampleData)
    msgbox('Error: Could not load first dataset to check time-boundaries.', 'Error:')
    return;
end

DataStart  = SampleData.time{1}(1);
DataEnd    = SampleData.time{1}(end);
Tolerance  = (1 / SampleData.fsample) * 2;  % tolerance of 2 samples

% Check time settings (Most already checked in previous sections):
StartTime  = str2num(get(handles.TextboxTimeStart,  'String'));
EndTime    = str2num(get(handles.TextboxTimeEnd,    'String'));
WindowSize = str2num(get(handles.TextboxWindowSize, 'String'));
TimeStep   = str2num(get(handles.TextboxTimeStep,   'String'));

if isempty(StartTime)
    msgbox('Error: Start time must be specified.', 'Error:');
    return;
end
if isempty(EndTime)
    msgbox('Error: End time must be specified.', 'Error:');
    return;
end
if isempty(WindowSize)
    msgbox('Error: Window size must be specified.', 'Error:');
    return;
end
if isempty(TimeStep)
	msgbox('Error: Time step must be specified.', 'Error:');
	return;
end

if StartTime < (DataStart - Tolerance) || StartTime > DataEnd
    msgbox('Error: Start analysis time is outside of bounds of dataset time.', 'Error:');
    set(handles.TextboxTimeStart, 'String', num2str(DataStart - Tolerance, '%1.3f'));
    return;
end
if EndTime > (DataEnd + Tolerance) || EndTime < DataStart
    msgbox('Error: End analysis time is outside bounds of dataset time.', 'Error:');
    set(handles.TextboxTimeEnd, 'String', num2str(DataEnd + Tolerance, '%1.3f'));
    return;
end

% Check freq settings:
FreqStart = str2num(get(handles.TextboxFreqFoiStart, 'String'));
FreqEnd   = str2num(get(handles.TextboxFreqFoiEnd,   'String'));
FreqRes   = str2num(get(handles.TextboxFreqRes,      'String'));

if isempty(FreqStart)
    msgbox('Error: Start frequency must be specified.', 'Error:');
    return;
end
if isempty(FreqEnd)
    msgbox('Error: End frequency must be specified.', 'Error:');
    return;
end
if isempty(FreqRes)
    msgbox('Error: Frequency resolution must be specified.', 'Error:');
    return;
end

if strcmp(handles.gui.FreqMethod, 'mtmconvol: Fixed Time Win.')
    TotalAnalysisTime = handles.time.End - handles.time.Start;
    FixedTimeWinSize  = 1 / FreqRes;
    
    if FixedTimeWinSize > TotalAnalysisTime
        message = {'ERROR:'; '';
            'The size of the fixed time-window for frequency analysis is larger';
            ['than the current total analysis time (',num2str(TotalAnalysisTime),' s).']; '';
            'For the method "mtmconvol" with fixed time-windows:';
            'Frequency resolution determines the size of the fixed time-window.'; '';
            ['Current freq. resolution: ',num2str(FreqRes),'Hz'];
            ['Fixed time-window size: ',num2str(1 / FreqRes),'s']; '';
            'Select a larger FreqRes value, or increase length of analysis window.'; ''};
        msgbox(message, 'ERROR:');
        
        set(handles.TextboxFreqRes, 'String', []);
        return;
    end
end

% Check custom COV / CSD time settings:
DropdownOptions = get(handles.DropdownCovCsdTime, 'String');
CovCsdWindow    = DropdownOptions{get(handles.DropdownCovCsdTime, 'Value')};

if strcmp(CovCsdWindow, 'Custom')
    CovCsdStart = str2num(get(handles.TextboxCovCsdTimeStart, 'String'));
    CovCsdEnd   = str2num(get(handles.TextboxCovCsdTimeEnd,   'String'));
    
    if isempty(CovCsdStart)
        msgbox('Error: Custom COV/CSD start time not specified.', 'Error:');
        return
    end
    if isempty(CovCsdEnd)
        msgbox('Error: Custom COV/CSD end time not specified.', 'Error:');
        return;
    end
    
    if CovCsdStart < (DataStart - Tolerance) || CovCsdStart > DataEnd
        msgbox('Error: Cov/Csd start time is outside of bounds of dataset time.', 'Error:')
        set(handles.TextboxCovCsdTimeStart, 'String', num2str(DataStart - Tolerance, '%1.3f'));
        return;
    end
    if CovCsdEnd > (DataEnd + Tolerance) || CovCsdEnd < DataStart
        msgbox('Error: Cov/Csd end time is outside of bounds of dataset time.', 'Error:')
        set(handles.TextboxCovCsdTimeEnd, 'String', num2str(DataEnd + Tolerance, '%1.3f'));
        return;
    end
end

% Check DiffBase time settings:
if strcmp(PipeSettings.Source.Contrast, 'DiffBase')
    BaselineStart = str2num(get(handles.TextboxDiffBaseStart, 'String'));
    BaselineEnd   = str2num(get(handles.TextboxDiffBaseEnd,   'String'));
    
    if isempty(BaselineStart)
        msgbox('Error: Baseline time for DiffBase not specified.', 'Error:')
        return;
    end
    if isempty(BaselineEnd)
        msgbox('Error: Baseline end time not specified.', 'Error:')
        return;
    end
    
    if BaselineStart < (DataStart - Tolerance) || BaselineStart > DataEnd
        msgbox('Error: DiffBase baseline start is outside bounds of dataset time.', 'Error:')
        set(handles.TextboxDiffBaseStart, 'String', num2str(DataStart - Tolerance, '%1.3f'));
        return;
    end
    if BaselineEnd > (DataEnd + Tolerance) || BaselineEnd < DataStart
        msgbox('Error: DiffBase baseline end is outside bounds of dataset time.', 'Error:')
        set(handles.TextboxDiffBaseEnd, 'String', num2str(DataEnd + Tolerance, '%1.3f'));
        return;
    end
end


% Check analysis settings:
if isempty(time.Windows)
	msgbox('Error: Time parameters not specified properly.', 'Error:')
	return;
end

if isempty(FTcfg.Lead.grid.resolution)
    msgbox('Error: Leadfield resolution not specified.', 'Error:')
    return;
end

if strcmp(CovCsdWindow, 'Custom')
    if isempty(FTcfg.Timelock.covariancewindow) || isempty(PipeSettings.Freq.CsdWindow)
        msgbox('Error: Times for custom COV/CSD window not specified.', 'Error:')
        return;
    end
end

switch SourceContrast
	case 'EstNoise'
		if strcmp(EstNoiseCommonFilt, 'yes') && isempty(handles.gui.CommonFilterCondID)
			msgbox('Error: Common filter CondID not selected.', 'Error:')
			return;
		end
		
	case 'DiffCond'
		if isempty(handles.gui.ControlCondID)
			msgbox('Error: Control CondID not selected.', 'Error:')
			return;
		end
		
	case 'DiffBase'
		if isempty(PipeSettings.Source.DiffBaseTime)
			msgbox('Error: DiffBase baseline time not specified.', 'Error:')
			return;
		end
end

% Check gradiometer info:
GradMismatch = CallCheckGradInfo(handles);

if GradMismatch == 1
    open('ErrorLog_BuilderGUI.txt');
    return;
end


% Confirm save:
ConfirmSave = questdlg('Save current settings into Builder .mat?',...
    'CONFIRM:', 'YES', 'NO', 'YES');
if strcmp(ConfirmSave, 'NO')
	return;
end

BuilderPath = [handles.paths.AnalysisID,'/Builder_',handles.name.AnalysisID,'.mat'];

if exist(BuilderPath, 'file')
	prompt = {'WARNING:'; '';
		'An existing Builder .mat file has been detected for this AnalysisID.'; '';
		'Do you wish to update and overwrite old configuration?'; ''};
    
	OverwriteFile = questdlg(prompt, 'WARNING:', 'YES', 'NO', 'NO');
	if strcmp(OverwriteFile, 'NO')
		return;
	end
end


% Start Waitbox:
WaitBox = StartWaitBox('SAVING SETTINGS TO BUILDER .MAT:');

% Update settings:
handles = UpdateGroupIDSettings	  (handles);
handles = UpdateSubjIDSettings	  (handles);
handles = UpdateCondIDSettings	  (handles);
handles = UpdateInputMRI		  (handles);
handles = UpdateInputMEG		  (handles);

handles = UpdateTimeSettings	  (handles);
handles = UpdateHdmLeadSettings	  (handles);
handles = UpdateTimeFreqSettings  (handles);
handles = UpdateSourceSettings	  (handles);
handles = UpdateNormaliseSettings (handles);
handles = UpdateNiftiAfniSettings (handles);

% Generate paths:
handles = ResetPipelinePaths  (handles);
handles = UpdatePipelinePaths (handles);


% Compile input & analysis settings:
name  = handles.name;
paths = handles.paths;
time  = handles.time;

gui          = handles.gui;
FTcfg        = handles.FTcfg;           % Fields: Hdm, Lead, Timelock, Source
PipeSettings = handles.PipeSettings;    % Fields: TimelockFreq, Source, Afni4D, NormSPM

% Acquire advanced settings from .m files:
CurrentDir = pwd;
cd([paths.AnalysisID,'/SETTINGS/']);

[FTcfg.SegMRI, FTcfg.Hdm, FTcfg.Lead]                                     = Settings_SegHdmLeadfield(FTcfg);
[FTcfg.Timelock, FTcfg.TimelockAvg, FTcfg.GFP, FTcfg.Freq, FTcfg.FreqAvg] = Settings_TimelockFreqAnalysis(handles);
[FTcfg.Source, FTcfg.InterpSrc, FTcfg.WriteSrc, FTcfg.SrcAvg]             = Settings_SourceAnalysis(FTcfg);
[SPMcfg, FTcfg.WriteNormMRI, FTcfg.WriteNormSrc]                          = Settings_SourceNormalise;

cd(CurrentDir);


% If ICAcleaned FT files selected, check if lambda on:
LambdaRequired = 0;

for g = 1:length(handles.name.GroupID)
    if ~isempty(find(~cellfun(@isempty, handles.gui.CheckICAclean{g})))
        LambdaRequired = 1;
    end
end

if LambdaRequired == 1 && ~isfield(FTcfg.Source.(FTcfg.Source.method), 'lambda')
    prompt = {'WARNING:'; '';
        'Detected ICA cleaning in one or more of the input MEG files.';
        'However, no REGULARIZATION (LAMBDA) has been set for source analysis.'; '';
        'For ICA cleaned data, it is HIGHLY RECOMMENDED to regularize the data';
        'for source analysis (Lambda = ''5%'' is a common value).'; '';
        'Do you wish to go back and set a regularization (lambda) value?'; ''};
    
    CancelSave = questdlg(prompt, 'WARNING:', 'YES', 'NO', 'YES');
    if strcmp(CancelSave, 'YES')
        close(WaitBox);
        return;
    end
end


% Save settings:
CheckSavePath(BuilderPath, 'BuilderGUI');
save(BuilderPath, 'name', 'paths', 'time', 'gui', 'PipeSettings', 'FTcfg', 'SPMcfg');

% Feedback:
if exist(BuilderPath, 'file')
    msgbox('Builder .mat file successfully saved.')
end

% Check for ErrorLog:
if exist([pwd,'/ErrorLog_BuilderGUI.txt'], 'file')
    LogCheck = dir('ErrorLog_BuilderGUI.txt');
    if LogCheck.bytes == 0  % File empty
        delete('ErrorLog_BuilderGUI.txt');
    end
end

close(WaitBox);




%==================================%
% FUNCTION TO CALL CHECKGRADINFO.m %
%==================================%
function GradMismatch = CallCheckGradInfo(InputHandles)
handles = InputHandles;

SourceContrast     = handles.PipeSettings.Source.Contrast;
EstNoiseCommonFilt = handles.PipeSettings.Source.EstNoiseCommonFilt;

% Start Waitbox:
WaitBox = StartWaitBox('CHECKING GRADIOMETER INFO:');

% Check gradiometer info for Custom / Control datasets:
% Note: GradMismatch value is required for GeneratePipelinePaths.
% Therefore, we need to call paths from GUI fields.
TempHandles.name                = handles.name;
TempHandles.gui.MEGdataFiletype = handles.gui.MEGdataFiletype;
TempHandles.PipeSettings.Source = handles.PipeSettings.Source;

TempHandles.paths.MEGdata           = handles.gui.InputDataFiles;
TempHandles.paths.MEGdataCommonFilt = handles.gui.CommonFiltDataFiles;
TempHandles.paths.MEGdataControl    = handles.gui.ControlDataFiles;

GradMismatch = CheckGradInfo(TempHandles, 'BuilderGUI');

if strcmp(SourceContrast, 'EstNoise') && ...
        strcmp(EstNoiseCommonFilt, 'yes') && GradMismatch == 1
    ErrMsg = {'========== ERROR: ==========';                           '';
        'Datasets from which common-filters are computed from do NOT have';
        'the same gradiometer info as their respective input datasets.   ';
        'Therefore, common-filters cannot be applied.                    ';
        '';
        'For each subject, the common-filter dataset and its respective  ';
        'input datasets must have matching gradiometer info (usually     ';
        'originating from the same MEG session).                         ';
        '';
        'For a log of which input dataset(s) did not match with their    ';
        'respective custom-filter datasets, see ErrorLog_BuilderGUI.     '};
    
    msgbox(ErrMsg, 'ERROR: Gradiometer Mismatch')
end

if strcmp(SourceContrast, 'DiffCond') && GradMismatch == 1
    WarnMsg = {'========== ERROR: ==========';                        '';
        'Control datasets do NOT have the same gradiometer information  ';
        'as their respective active condition datasets. Control and     ';
        'active conditions cannot be merged for common-filter.          ';
        '';
        'As a result, the DiffCond setting (vs. Control Cond) cannot be ';
        'used. Gradiometer info must match (usually originating from    ';
        'the same MEG session) in order for common-filter to be computed';
        'across active and control datasets.'
        '';
        'For a log of which input dataset(s) did not match with their   ';
        'respective control datasets, see ErrorLog_BuilderGUI.          '};
    
    msgbox(WarnMsg, 'ERROR: Gradiometer Mismatch')
end

close(WaitBox);

    
    

%=================================%
% FUNCTION TO OPEN MODAL WAITBOX: %
%=================================%
function WaitBox = StartWaitBox(TextString)

WaitBox = dialog('Units', 'pixels', 'Position', [500 500 300 40],...
    'Windowstyle', 'modal', 'NextPlot', 'new', 'Name', 'Please Wait:');

uicontrol('Parent', WaitBox, 'Units', 'pixels', 'Position', [20 10 250 20],...
    'Style', 'text', 'String', TextString, 'FontWeight', 'bold');

movegui(WaitBox, 'center');




%============================%
% GUIDE CREATEFCN FUNCTIONS: %
%============================%

% --- Executes during object creation, after setting all properties.
function TextboxRootpath_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxAnalysisID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function ListboxGroupID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function ListboxSubjID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxSubjID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function DropdownMEGdataFiletype_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function ListboxInputMEG_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function ListboxInputMRI_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function ListboxRootpathFolders_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function ListboxDetectedCondIDs_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function ListboxAddedCondIDs_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function TextboxTimeStart_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxWindowSize_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxTimeStep_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxTimeEnd_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function DropdownHdmMethod_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxLeadfieldRes_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function DropdownCovCsdTime_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxCovCsdTimeStart_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxCovCsdTimeEnd_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function DropdownFreqMethod_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxFreqRes_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxFreqFoiStart_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxFreqFoiEnd_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function DropdownSourceMethod_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function DropdownSourceOutput_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxCommonFiltControlCond_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxDiffBaseStart_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxDiffBaseEnd_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function DropdownAfniTemplate_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
