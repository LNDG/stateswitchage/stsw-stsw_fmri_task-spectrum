function varargout = DriverGUI(varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Configures PLS settings & calls analysis sections to run. %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


% Last Modified by GUIDE v2.5 24-Sep-2014 01:06:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DriverGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @DriverGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


%--- Executes just before DriverGUI is made visible. ---%
%-------------------------------------------------------%
function DriverGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DriverGUI (see VARARGIN)

% Choose default command line output for DriverGUI
handles.output = hObject;


% Make sure toolbox paths are added:
[PipelineDir, ~, ~] = fileparts(which('DriverGUI.m'));

addpath(genpath(PipelineDir));
rmpath([PipelineDir,'/DEFAULT_SETTINGS']);  % Make sure its calling from AnalysisID
rmpath([PipelineDir,'/TEMPORARY_FIXES']);   % Make sure its calling from FT toolbox

CheckToolboxPaths(PipelineDir);


% Initialize variables and default settings:
handles.gui.BuilderMat        = [];
handles.gui.ManualTimeIndices = [];

handles.gui.DefaultBrainMask  = ...
    [PipelineDir,'/MEGPLS_TOOLBOX/Masks_Templates/mask_T1+tlrc.BRIK'];

handles.gui.DefaultROIMask    = ...
    [PipelineDir,'/MEGPLS_TOOLBOX/Masks_Templates/area_coords_RAI.xls'];

handles.name  = [];
handles.time  = [];
handles.paths = [];

handles.PLSmat.OrigBuilder  = [];
handles.PLSmat.SessionID    = [];
handles.PLSmat.name.GroupID = [];
handles.PLSmat.name.SubjID  = [];
handles.PLSmat.name.CondID  = [];

handles.PLSmat.paths.Afni4D     = [];  % For PLS datamat input
handles.PLSmat.paths.NormSource = [];  % For data-specific mask generation
handles.PLSmat.paths.AnalysisID = [];

handles.PLSmat.time				= [];
handles.PLSmat.TimeIndices      = [];  % Default is epoch, but no builder loaded yet
handles.PLSmat.ManualTimeSelect = 'no';

handles.PLSmat.MaskType          = 'BrainMask';
handles.PLSmat.DataSpecificMask  = 'yes';
handles.PLSmat.MaskKeepVoxThresh = 0.90;  % 90% datasets need to share voxel for it to be included into mask.
handles.PLSmat.BrainMaskFile     = handles.gui.DefaultBrainMask;
handles.PLSmat.ROIMaskFile       = handles.gui.DefaultROIMask;
handles.PLSmat.ROIBlobSize       = [-1:1];

handles.PLSmat.BhvSeedID    = [];
handles.PLSmat.BhvFile      = [];
handles.PLSmat.SeedFile     = [];
handles.PLSmat.SeedBlobSize = [];

handles.PLSrun.NRContrast   = [];
handles.PLSrun.NRContrastID = [];

handles.PLSrun.Method      = 'MCENT';
handles.PLSrun.SplitHalf   = 'no';
handles.PLSrun.SingleBhv   = 'no';
handles.PLSrun.KeepDatamat = 'no';

handles.PLSrun.NumPerm   = 500;
handles.PLSrun.NumBoot   = 500;
handles.PLSrun.NumSplit  = 0;

handles.PLSrun.CorrMode  = 0;
handles.PLSrun.PermType  = 0;  % is_struct field
handles.PLSrun.MCentType = 0;
handles.PLSrun.BootType  = 'strat';

% Update handles structure
guidata(hObject, handles);


% UIWAIT makes DriverGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


%--- Outputs from this function are returned to the command line. ---%
%--------------------------------------------------------------------%
function varargout = DriverGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;




%=====================================%
% FUNCTIONS FOR LOADING BUILDER .MAT: %
%=====================================%

%--- Executes on button press in ButtonLoadBuilderMat. ---%
%---------------------------------------------------------%
function ButtonLoadBuilderMat_Callback(hObject, eventdata, handles)

[BuilderFile, BuilderPath] = uigetfile('Builder_*.mat',...
	'Select Builder .mat file to load:', 'MultiSelect', 'off');

if BuilderFile == 0
	return;  % If user cancels
else
    handles.gui.BuilderMat = [BuilderPath,BuilderFile];
	set(handles.TextboxBuilderMat, 'String', handles.gui.BuilderMat);
end

% Start Waitbox:
WaitBox = StartWaitBox('LOADING BUILDER .MAT FILE:');

% Load selected builder (for GUI purposes):
LoadBuilder   = load(handles.gui.BuilderMat);
handles.name  = LoadBuilder.name;
handles.time  = LoadBuilder.time;
handles.paths = LoadBuilder.paths;

% Reset PLS settings & GUI panels:
handles = ResetPLSmatInputData  (handles);
handles = ResetTimeSettings     (handles);
handles = ResetMaskSettings     (handles);
handles = ResetBehaviorSettings (handles);
handles = ResetPLSrunSettings   (handles);

% Load builder settings into PLSmat (for MakePLSmat & PLSrun input):
handles.PLSmat.OrigBuilder      = LoadBuilder;
handles.PLSmat.name             = LoadBuilder.name;
handles.PLSmat.time             = LoadBuilder.time;
handles.PLSmat.paths.Afni4D     = LoadBuilder.paths.Afni4DNormSource;
handles.PLSmat.paths.NormSource = LoadBuilder.paths.NormSource;
handles.PLSmat.paths.AnalysisID = LoadBuilder.paths.AnalysisID;

% Reset run section values:
set(handles.CheckboxTimelockAnalysis, 'Value', 0);
set(handles.CheckboxTimelockGrpAvg,   'Value', 0);
set(handles.CheckboxFreqAnalysis,     'Value', 0);
set(handles.CheckboxFreqGrpAvg,       'Value', 0);
set(handles.CheckboxSegmentMRI,	      'Value', 0);
set(handles.CheckboxHdmLeadfield,     'Value', 0);
set(handles.CheckboxSourceFilter,     'Value', 0);
set(handles.CheckboxSourceAnalysis,	  'Value', 0);
set(handles.CheckboxMakeSourceNifti,  'Value', 0);
set(handles.CheckboxMakeSourceAfni,   'Value', 0);
set(handles.CheckboxNormaliseSource,  'Value', 0);
set(handles.CheckboxMakeNormSrcNifti, 'Value', 0);
set(handles.CheckboxMakeNormSrcAfni,  'Value', 0);
set(handles.CheckboxGroupAvgNormSrc,  'Value', 0);
set(handles.CheckboxMakeGrpAvgNifti,  'Value', 0);
set(handles.CheckboxMakeGrpAvgAfni,   'Value', 0);
set(handles.CheckboxMakePLSmat,       'Value', 0);
set(handles.CheckboxRunPLS,           'Value', 0);

% Enable/disable run section panel:
set(findall(handles.PanelRunTimelockFreq, '-property', 'Enable'), 'Enable', 'on');
set(findall(handles.PanelRunSource,       '-property', 'Enable'), 'Enable', 'on');
set(findall(handles.PanelRunPLS,          '-property', 'Enable'), 'Enable', 'on');

if ~strcmp(LoadBuilder.gui.MEGdataFiletype, 'Fieldtrip')
    set(handles.CheckboxTimelockAnalysis, 'Enable', 'off');
    set(handles.CheckboxTimelockGrpAvg,   'Enable', 'off');
    set(handles.CheckboxFreqAnalysis,     'Enable', 'off');
    set(handles.CheckboxFreqGrpAvg,       'Enable', 'off');
    set(handles.CheckboxSegmentMRI,	      'Enable', 'off');
    set(handles.CheckboxHdmLeadfield,     'Enable', 'off');
    set(handles.CheckboxSourceFilter,     'Enable', 'off');
    set(handles.CheckboxSourceAnalysis,	  'Enable', 'off');
    set(handles.CheckboxMakeSourceNifti,  'Enable', 'off');
    set(handles.CheckboxNormaliseSource,  'Enable', 'off');
    set(handles.CheckboxMakeNormSrcNifti, 'Enable', 'off');
    set(handles.CheckboxGroupAvgNormSrc,  'Enable', 'off');
    set(handles.CheckboxMakeGrpAvgNifti,  'Enable', 'off');
end

if strcmp(LoadBuilder.gui.MEGdataFiletype, 'AFNI')
	set(handles.CheckboxMakeSourceAfni,	 'Enable', 'off');
    set(handles.CheckboxMakeNormSrcAfni, 'Enable', 'off');
    set(handles.CheckboxMakeGrpAvgAfni,  'Enable', 'off');
end

% Update PLS settings & GUI panels:
handles = UpdatePLSmatInputData  (handles);
handles = UpdateTimeSettings     (handles);
handles = UpdateMaskSettings     (handles);
handles = UpdateBehaviorSettings (handles);
handles = UpdatePLSrunSettings   (handles);

% Save handles:
guidata(hObject, handles);
close(WaitBox);


%--- Textbox to display selected path of Builder: ---%
%----------------------------------------------------%
function TextboxBuilderMat_Callback(hObject, eventdata, handles)
EnteredText = get(handles.TextboxBuilderMat, 'String');

if ~isequal(EnteredText, handles.gui.BuilderMat)
    set(handles.TextboxBuilderMat, 'String', handles.gui.BuilderMat);
    msgbox('Note: Use the button to change Builder .mat file.')
end




%=======================================%
% FUNCTIONS FOR SELECTING RUN SECTIONS: %
%=======================================%

%--- Checkboxes for timelock & frequency analysis: ---%
%-----------------------------------------------------%

% --- Executes on button press in CheckboxTimelockAnalysis.
function CheckboxTimelockAnalysis_Callback(hObject, eventdata, handles)
if get(handles.CheckboxTimelockAnalysis, 'Value') == 1
    set(handles.CheckboxTimelockGrpAvg, 'Value', 1);
end

% --- Executes on button press in CheckboxTimelockGrpAvg.
function CheckboxTimelockGrpAvg_Callback(hObject, eventdata, handles)


% --- Executes on button press in CheckboxFreqAnalysis.
function CheckboxFreqAnalysis_Callback(hObject, eventdata, handles)
if get(handles.CheckboxFreqAnalysis, 'Value') == 1
    set(handles.CheckboxFreqGrpAvg, 'Value', 1);
end

% --- Executes on button press in CheckboxFreqGrpAvg.
function CheckboxFreqGrpAvg_Callback(hObject, eventdata, handles)


%--- Checkboxes for source analysis stream: ---%
%----------------------------------------------%

% --- Executes on button press in CheckboxSegmentMRI.
function CheckboxSegmentMRI_Callback(hObject, eventdata, handles)

% --- Executes on button press in CheckboxHdmLeadfield.
function CheckboxHdmLeadfield_Callback(hObject, eventdata, handles)

% --- Executes on button press in CheckboxSourceFilter.
function CheckboxSourceFilter_Callback(hObject, eventdata, handles)


% --- Executes on button press in CheckboxSourceAnalysis.
function CheckboxSourceAnalysis_Callback(hObject, eventdata, handles)
% if get(handles.CheckboxSourceAnalysis, 'Value') == 1    %** Disabled for now
%     set(handles.CheckboxMakeSourceNifti, 'Value',  1);  %   Saves time
%     set(handles.CheckboxMakeSourceAfni,  'Value',  1);
% else
%     set(handles.CheckboxMakeSourceNifti, 'Value',  0);
%     set(handles.CheckboxMakeSourceAfni,  'Value',  0);
% end

% --- Executes on button press in CheckboxMakeSourceNifti.
function CheckboxMakeSourceNifti_Callback(hObject, eventdata, handles)

% --- Executes on button press in CheckboxMakeSourceAfni.
function CheckboxMakeSourceAfni_Callback(hObject, eventdata, handles)


% --- Executes on button press in CheckboxNormaliseSource.
function CheckboxNormaliseSource_Callback(hObject, eventdata, handles)
if get(handles.CheckboxNormaliseSource, 'Value') == 1
    set(handles.CheckboxMakeNormSrcNifti, 'Value',  1);
    set(handles.CheckboxMakeNormSrcAfni,  'Value',  1);
else
    set(handles.CheckboxMakeNormSrcNifti, 'Value',  0);
    set(handles.CheckboxMakeNormSrcAfni,  'Value',  0);
end

% --- Executes on button press in CheckboxMakeNormSrcNifti.
function CheckboxMakeNormSrcNifti_Callback(hObject, eventdata, handles)

% --- Executes on button press in CheckboxMakeNormSrcAfni.
function CheckboxMakeNormSrcAfni_Callback(hObject, eventdata, handles)


% --- Executes on button press in CheckboxGroupAvgNormSrc. ---%
function CheckboxGroupAvgNormSrc_Callback(hObject, eventdata, handles)
if get(handles.CheckboxGroupAvgNormSrc, 'Value') == 1
    set(handles.CheckboxMakeGrpAvgNifti, 'Value',  1);
    set(handles.CheckboxMakeGrpAvgAfni,  'Value',  1);
else
    set(handles.CheckboxMakeGrpAvgNifti, 'Value',  0);
    set(handles.CheckboxMakeGrpAvgAfni,  'Value',  0);
end

% --- Executes on button press in CheckboxMakeGrpAvgNifti.
function CheckboxMakeGrpAvgNifti_Callback(hObject, eventdata, handles)

% --- Executes on button press in CheckboxMakeGrpAvgAfni.
function CheckboxMakeGrpAvgAfni_Callback(hObject, eventdata, handles)


%--- Executes on button press in CheckboxMakePLSmat. ---%
%-------------------------------------------------------%
function CheckboxMakePLSmat_Callback(hObject, eventdata, handles)
if get(handles.CheckboxMakePLSmat, 'Value') == 0
    set(handles.CheckboxRunPLS, 'Value', 0);
end

% Update GUI panels (to enable/disable):
handles = UpdatePLSmatInputData  (handles);
handles = UpdateTimeSettings	 (handles);
handles = UpdateMaskSettings	 (handles);
handles = UpdateBehaviorSettings (handles);
handles = UpdatePLSrunSettings	 (handles);

% Save handles:
guidata(hObject, handles);


%--- Executes on button press in CheckboxRunPLS. ---%
%---------------------------------------------------%
function CheckboxRunPLS_Callback(hObject, eventdata, handles)
if get(handles.CheckboxRunPLS, 'Value') == 1
    set(handles.CheckboxMakePLSmat, 'Value', 1);
end

% Update GUI panels (to enable/disable):
handles = UpdatePLSmatInputData  (handles);
handles = UpdateTimeSettings	 (handles);
handles = UpdateMaskSettings	 (handles);
handles = UpdateBehaviorSettings (handles);
handles = UpdatePLSrunSettings	 (handles);

% Save handles:
guidata(hObject, handles);




%======================================%
% FUNCTIONS FOR OPENING VISUALIZATION: %
%======================================%

%--- Executes on button press in ButtonViewTimelockFreq. ---%
%-----------------------------------------------------------%
function ButtonViewTimelockFreq_Callback(hObject, eventdata, handles)
VisualizeTimeFreq(handles.gui.BuilderMat);


%--- Executes on button press in ButtonViewSources. ---%
%------------------------------------------------------%
function ButtonViewSources_Callback(hObject, eventdata, handles)
VisualizeSourceData(handles.gui.BuilderMat);


%--- Executes on button press in ButtonViewPLS. ---%
%--------------------------------------------------%
function ButtonViewPLS_Callback(hObject, eventdata, handles)
ViewPLSresults




%===================================%
% FUNCTIONS FOR PLS INPUT SETTINGS: %
%===================================%

% --- Executes on selection change in ListboxPLSmatGroupID.
function ListboxPLSmatGroupID_Callback(hObject, eventdata, handles)
handles = UpdatePLSmatInputData(handles);
guidata(hObject, handles);

% --- Executes on selection change in ListboxPLSmatSubjID.
function ListboxPLSmatSubjID_Callback(hObject, eventdata, handles)

% --- Executes on selection change in ListboxPLSmatCondID.
function ListboxPLSmatCondID_Callback(hObject, eventdata, handles)


%--- Executes on button press in ButtonRmvPLSmatGroupID. ---%
%--------------------------------------------------------%
function ButtonRmvPLSmatGroupID_Callback(hObject, eventdata, handles)
PLSmat = handles.PLSmat;

if isempty(PLSmat.name.GroupID)
	return;
end
if length(PLSmat.name.GroupID) <= 1
    msgbox('Error: Cannot remove the last Group.', 'Error:')
    return;
end

% Exclude selected GroupID & data from PLSmat:
SelectedIndex = get(handles.ListboxPLSmatGroupID, 'Value');

PLSmat.name.GroupID     (SelectedIndex) = [];
PLSmat.name.SubjID      (SelectedIndex) = [];
PLSmat.paths.Afni4D     (SelectedIndex) = [];
PLSmat.paths.NormSource (SelectedIndex) = [];

% Update GUI:
handles.PLSmat = PLSmat;
handles = UpdatePLSmatInputData(handles);

% Save handles:
guidata(hObject, handles);


%--- Executes on button press in ButtonRmvPLSmatSubjID. ---%
%----------------------------------------------------------%
function ButtonRmvPLSmatSubjID_Callback(hObject, eventdata, handles)
PLSmat = handles.PLSmat;

if isempty(PLSmat.name.SubjID)
	return;
end

GroupIndex = get(handles.ListboxPLSmatGroupID, 'Value');
if length(PLSmat.name.SubjID{GroupIndex}) <= 1
    msgbox('Error: Cannot remove the last Subject.', 'Error:');
    return;
end

% Exclude selected SubjID & data from PLSmat:
SubjIndex = get(handles.ListboxPLSmatSubjID,  'Value');

PLSmat.name.SubjID      {GroupIndex}(SubjIndex)     = [];
PLSmat.paths.Afni4D     {GroupIndex}(SubjIndex,:)   = [];
PLSmat.paths.NormSource {GroupIndex}(SubjIndex,:,:) = [];

% Update GUI:
handles.PLSmat = PLSmat;
handles = UpdatePLSmatInputData(handles);

% Save handles:
guidata(hObject, handles);


%--- Executes on button press in ButtonRmvPLSmatCondID. ---%
%----------------------------------------------------------%
function ButtonRmvPLSmatCondID_Callback(hObject, eventdata, handles)
PLSmat = handles.PLSmat;

if isempty(PLSmat.name.CondID)
	return;
end
if length(PLSmat.name.CondID) <= 1
    msgbox('Error: Cannot remove the last Condition.', 'Error:')
    return;
end

% Exclude selected CondID & data from PLSmat:
SelectedIndex = get(handles.ListboxPLSmatCondID, 'Value');

PLSmat.name.CondID(SelectedIndex) = [];

for g = 1:length(PLSmat.name.GroupID)
	PLSmat.paths.Afni4D     {g}(:,SelectedIndex)   = [];
    PLSmat.paths.NormSource {g}(:,SelectedIndex,:) = [];
end

% Update GUI:
handles.PLSmat = PLSmat;
handles = UpdatePLSmatInputData(handles);

% Save handles:
guidata(hObject, handles);


%--- Executes on button press in ButtonRestorePLSmatInput. ---%
%-------------------------------------------------------------%
function ButtonRestorePLSmatInput_Callback(hObject, eventdata, handles)
PLSmat = handles.PLSmat;

% Restore original Builder list of input data:
PLSmat.name.GroupID     = handles.name.GroupID;
PLSmat.name.SubjID      = handles.name.SubjID;
PLSmat.name.CondID      = handles.name.CondID;
PLSmat.paths.Afni4D     = handles.paths.Afni4DNormSource;
PLSmat.paths.NormSource = handles.paths.NormSource;

% Update GUI:
handles.PLSmat = PLSmat;
handles = UpdatePLSmatInputData(handles);

% Save handles:
guidata(hObject, handles);


%--- Textbox for name ID of PLS session. ---%
%-------------------------------------------%
function TextboxPLSmatSessionID_Callback(hObject, eventdata, handles)
InputID = deblank(get(handles.TextboxPLSmatSessionID, 'String'));
InputID = regexprep(InputID, '\s+', '_');
set(handles.TextboxPLSmatSessionID, 'String', InputID);

handles = UpdatePLSmatInputData(handles);
guidata(hObject, handles);


%--- Update PLSmat input data & GUI panel: ---%
%---------------------------------------------%
function OutputHandles = UpdatePLSmatInputData(InputHandles)
handles = InputHandles;
PLSmat  = handles.PLSmat;

% Read GUI to update settings:
PLSmat.SessionID = get(handles.TextboxPLSmatSessionID, 'String');

% Update GroupID listbox:
set(handles.ListboxPLSmatGroupID, 'String', PLSmat.name.GroupID);

GroupIndex    = get(handles.ListboxPLSmatGroupID, 'Value');
MaxGroupIndex = length(PLSmat.name.GroupID);

if isempty(GroupIndex) || GroupIndex == 0 || GroupIndex > MaxGroupIndex
    set(handles.ListboxPLSmatGroupID, 'Value', MaxGroupIndex);
    GroupIndex = MaxGroupIndex;
end

% Update SubjID listbox:
if isempty(PLSmat.name.SubjID)
    set(handles.ListboxPLSmatSubjID, 'String', []);
    MaxSubjIndex = 0;
else
    set(handles.ListboxPLSmatSubjID, 'String', PLSmat.name.SubjID{GroupIndex});
    MaxSubjIndex = length(PLSmat.name.SubjID{GroupIndex});
end

SubjIndex    = get(handles.ListboxPLSmatSubjID, 'Value');
if isempty(SubjIndex) || SubjIndex == 0 || SubjIndex > MaxSubjIndex
    set(handles.ListboxPLSmatSubjID, 'Value', MaxSubjIndex);
end

% Update CondID listbox:
set(handles.ListboxPLSmatCondID, 'String', PLSmat.name.CondID);

CondIndex    = get(handles.ListboxPLSmatCondID, 'Value');
MaxCondIndex = length(PLSmat.name.CondID);

if isempty(CondIndex) || CondIndex == 0 || CondIndex > MaxCondIndex
    set(handles.ListboxPLSmatCondID, 'Value', MaxCondIndex);
end

% Enable/Disable GUI components:
if get(handles.CheckboxMakePLSmat, 'Value') == 1
    set(findall(handles.PanelPLSmatInput, '-property', 'Enable'), 'Enable', 'on');
else
    set(findall(handles.PanelPLSmatInput, '-property', 'Enable'), 'Enable', 'off');
    
    set(handles.ListboxPLSmatGroupID, 'Enable', 'on');
    set(handles.ListboxPLSmatSubjID,  'Enable', 'on');
    set(handles.ListboxPLSmatCondID,  'Enable', 'on');
    
    set(handles.TextHeaderPLSmatGroupIDs, 'Enable', 'on');
    set(handles.TextHeaderPLSmatSubjIDs,  'Enable', 'on');
    set(handles.TextHeaderPLSmatCondIDs,  'Enable', 'on');
end 

% Set output handles:
handles.PLSmat = PLSmat;
OutputHandles  = handles;


%--- Reset PLSmat input data & GUI panel: ---%
%--------------------------------------------%
function OutputHandles = ResetPLSmatInputData(InputHandles)
handles = InputHandles;
PLSmat  = handles.PLSmat;

% Reset PLSmat input settings:
PLSmat.name.GroupID     = [];
PLSmat.name.SubjID      = [];
PLSmat.name.CondID      = [];
PLSmat.paths.Afni4D     = [];
PLSmat.paths.NormSource = [];
PLSmat.SessionID        = [];

% Reset GUI panel:
set(handles.ListboxPLSmatGroupID, 'String', []);
set(handles.ListboxPLSmatGroupID, 'Value',  1);

set(handles.ListboxPLSmatSubjID,  'String', []);
set(handles.ListboxPLSmatSubjID,  'Value',  1);

set(handles.ListboxPLSmatCondID,  'String', []);
set(handles.ListboxPLSmatCondID,  'Value',  1);

set(handles.TextboxPLSmatSessionID, 'String', []);

% Set output handles:
handles.PLSmat = PLSmat;
OutputHandles  = handles;




%======================================%
% FUNCTIONS FOR PLS MAT TIME SETTINGS: %
%======================================%

%--- Executes on slider movement for PLS start time. ---%
%-------------------------------------------------------%
function SliderTimeStartPLS_Callback(hObject, eventdata, handles)
if isempty(handles.gui.BuilderMat)
	msgbox('Warning: Load Builder .mat first.', 'Warning:');
    return;
end

StartIndex = round(get(handles.SliderTimeStartPLS, 'Value'));
EndIndex   = round(get(handles.SliderTimeEndPLS,   'Value'));
if StartIndex > EndIndex
    StartIndex = EndIndex;
end

set(handles.SliderTimeStartPLS,  'Value',  StartIndex);
set(handles.TextboxTimeStartPLS, 'String', handles.time.str.Windows{StartIndex,1});

handles = UpdateTimeSettings(handles);
guidata(hObject, handles);


%--- Executes on slider movement for PLS end time. ---%
%-----------------------------------------------------%
function SliderTimeEndPLS_Callback(hObject, eventdata, handles)
if isempty(handles.gui.BuilderMat)
    return;
end

EndIndex   = round(get(handles.SliderTimeEndPLS,   'Value'));
StartIndex = round(get(handles.SliderTimeStartPLS, 'Value'));
if EndIndex < StartIndex
    EndIndex = StartIndex;
end

set(handles.SliderTimeEndPLS,  'Value',  EndIndex);
set(handles.TextboxTimeEndPLS, 'String', handles.time.str.Windows{EndIndex,2});

handles = UpdateTimeSettings(handles);
guidata(hObject, handles);


%--- Textbox to display or enter PLS start time: ---%
%---------------------------------------------------%
function TextboxTimeStartPLS_Callback(hObject, eventdata, handles)
if isempty(handles.gui.BuilderMat)
    return;
end

% Check input start time:
InputStartTime = str2num(get(handles.TextboxTimeStartPLS, 'String'));
InputEndTime   = str2num(get(handles.TextboxTimeEndPLS,   'String'));

if isempty(InputStartTime) || InputStartTime > InputEndTime
    StartIndex = get(handles.SliderTimeStartPLS, 'Value');
    set(handles.TextboxTimeStartPLS, 'String', handles.time.str.Windows{StartIndex,1});
end

% Find closest time-index:
[~, ClosestIndex] = min(abs(InputStartTime - handles.time.Windows(:,1)));

StartIndex = ClosestIndex;
if StartIndex > EndIndex;
    StartIndex = EndIndex;
end

set(handles.SliderTimeStartPLS,  'Value',  StartIndex);
set(handles.TextboxTimeStartPLS, 'String', handles.time.str.Windows{StartIndex,1});

% Save handles:
handles = UpdateTimeSettings(handles);
guidata(hObject, handles);


%--- Textbox to display or enter PLS end time: ---%
%-------------------------------------------------%
function TextboxTimeEndPLS_Callback(hObject, eventdata, handles)
if isempty(handles.gui.BuilderMat)
    return;
end

% Check input end time:
InputStartTime = str2num(get(handles.TextboxTimeStartPLS, 'String'));
InputEndTime   = str2num(get(handles.TextboxTimeEndPLS,   'String'));

if isempty(InputEndTime) || InputEndTime < InputStartTime
    EndIndex = get(handles.SliderTimeEndPLS, 'Value');
    set(handles.TextboxTimeEndPLS, 'String', handles.time.str.Windows{EndIndex,2});
end

% Find closest time-index:
[~, ClosestIndex] = min(abs(EndTime - handles.time.Windows(:,2)));

EndIndex = ClosestIndex;
if EndIndex < StartIndex
    EndIndex = StartIndex;
end

set(handles.SliderTimeEndPLS,  'Value',  EndIndex);
set(handles.TextboxTimeEndPLS, 'String', handles.time.str.Windows{EndIndex,2});

% Save handles:
handles = UpdateTimeSettings(handles);
guidata(hObject, handles);


%--- Executes on button press in CheckboxManualTimeSelect. ---%
%---------------------------------------------------------------%
function CheckboxManualTimeSelect_Callback(hObject, eventdata, handles)
handles = UpdateTimeSettings(handles);
guidata(hObject, handles);

% --- Executes on selection change in ListboxManualTimeSelect.
function ListboxManualTimeSelect_Callback(hObject, eventdata, handles)


%--- Executes on button press in ButtonManualTimeSelect. ---%
%--------------------------------------------------------------%
function ButtonManualTimeSelect_Callback(hObject, eventdata, handles)
if isempty(handles.gui.BuilderMat)
    return;
end

SelectedIndices = listdlg('PromptString', 'Select time-indices for PLS analysis:',...
	'ListSize', [300, 300], 'SelectionMode', 'multiple',...
    'ListString', handles.time.str.Windows(:,3));

if isempty(SelectedIndices)
	return;  % If user cancels
end

SelectedIndices = sort(SelectedIndices);
handles.gui.ManualTimeIndices = SelectedIndices;  % Keep in memory for switch-back

handles = UpdateTimeSettings(handles);
guidata(hObject, handles);


%--- Updates time settings & GUI panel: ---%
%------------------------------------------%
function OutputHandles = UpdateTimeSettings(InputHandles)
handles = InputHandles;

% Read GUI to update PLSmat:
if get(handles.CheckboxManualTimeSelect, 'Value') == 0
    StartTimeIndex = round(get(handles.SliderTimeStartPLS, 'Value'));
    EndTimeIndex   = round(get(handles.SliderTimeEndPLS,   'Value'));
	
    handles.PLSmat.TimeIndices      = [StartTimeIndex:EndTimeIndex];
    handles.PLSmat.ManualTimeSelect = 'no';
else
    handles.PLSmat.TimeIndices      = handles.gui.ManualTimeIndices;
    handles.PLSmat.ManualTimeSelect = 'yes';
end

% Update listbox:
if isempty(handles.gui.ManualTimeIndices)
	set(handles.ListboxManualTimeSelect, 'String', []);
else
	set(handles.ListboxManualTimeSelect, 'String',...
		handles.time.str.Windows(handles.gui.ManualTimeIndices, 3));
end

SelectedIndex = get(handles.ListboxManualTimeSelect, 'Value');
MaxListIndex  = length(handles.gui.ManualTimeIndices);

if isempty(SelectedIndex) || SelectedIndex == 0 || SelectedIndex > MaxListIndex
    set(handles.ListboxManualTimeSelect, 'Value', MaxListIndex);
end

% Enable/Disable GUI time components:
if get(handles.CheckboxMakePLSmat, 'Value') == 1
	set(findall(handles.PanelPLSTimeSettings, '-property', 'Enable'), 'Enable', 'on');
else
	set(findall(handles.PanelPLSTimeSettings, '-property', 'Enable'), 'Enable', 'off');
end

if size(handles.time.Windows, 1) == 1
    set(handles.SliderTimeStartPLS,      'Enable', 'off');
    set(handles.SliderTimeEndPLS,        'Enable', 'off');
    set(handles.TextboxTimeStartPLS,     'Enable', 'off');
    set(handles.TextboxTimeEndPLS,       'Enable', 'off');
    set(handles.TextTimeStartHeaderPLS,  'Enable', 'off');
    set(handles.TextTimeEndHeaderPLS,    'Enable', 'off');
end

if get(handles.CheckboxManualTimeSelect, 'Value') == 0
	set(handles.ButtonManualTimeSelect,  'Enable', 'off');
	set(handles.ListboxManualTimeSelect, 'Enable', 'off');
else
    set(handles.SliderTimeStartPLS,      'Enable', 'off');
    set(handles.SliderTimeEndPLS,        'Enable', 'off');
    set(handles.TextboxTimeStartPLS,     'Enable', 'off');
    set(handles.TextboxTimeEndPLS,       'Enable', 'off');
    set(handles.TextTimeStartHeaderPLS,  'Enable', 'off');
    set(handles.TextTimeEndHeaderPLS,    'Enable', 'off');
end

% Set output handles:
OutputHandles = handles;


%--- Reset time settings & GUI panel: ---%
%----------------------------------------%
function OutputHandles = ResetTimeSettings(InputHandles)
handles = InputHandles;

% Reset PLSmat time settings:
handles.PLSmat.TimeIndices      = [1:size(handles.time.Windows, 1)];
handles.PLSmat.ManualTimeSelect = 'no';
handles.gui.ManualTimeIndices   = [];

% Reset GUI time panel back to default:
SliderStep = 1 / size(handles.time.Windows, 1);

set(handles.CheckboxManualTimeSelect, 'Value',  0);
set(handles.ListboxManualTimeSelect,  'String', []);

set(handles.SliderTimeStartPLS, 'Min', 1);
set(handles.SliderTimeStartPLS, 'Max', size(handles.time.Windows, 1));
set(handles.SliderTimeStartPLS, 'SliderStep', [SliderStep SliderStep]);
set(handles.SliderTimeStartPLS, 'Value', 1);

set(handles.SliderTimeEndPLS, 'Min', 1);
set(handles.SliderTimeEndPLS, 'Max', size(handles.time.Windows, 1));
set(handles.SliderTimeEndPLS, 'SliderStep', [SliderStep SliderStep]);
set(handles.SliderTimeEndPLS, 'Value', size(handles.time.Windows, 1));

set(handles.TextboxTimeStartPLS, 'String', handles.time.str.Windows{1,1});
set(handles.TextboxTimeEndPLS,   'String', handles.time.str.Windows{end,2});

% Set output handles:
OutputHandles = handles;




%======================================%
% FUNCTIONS FOR PLS MAT MASK SETTINGS: %
%======================================%

%--- Executes on button-group in PanelPLSMaskSettings. ---%
%---------------------------------------------------------%
function PanelPLSMaskSettings_SelectionChangeFcn(hObject, eventdata, handles)
handles = UpdateMaskSettings(handles);
guidata(hObject, handles);


%--- Executes on button press in CheckboxDataSpecificMask. ---%
%-------------------------------------------------------------%
function CheckboxDataSpecificMask_Callback(hObject, eventdata, handles)
handles = UpdateMaskSettings(handles);
guidata(hObject, handles);


%--- User enters % threshold to keep voxels for data-specific mask: ---%
%----------------------------------------------------------------------%
function TextboxKeepVoxelThresh_Callback(hObject, eventdata, handles)
if isempty(get(handles.TextboxKeepVoxelThresh, 'String'))
    msgbox('Error: Voxel inclusion threshold not specified. Default set to 90%.');
    set(handles.TextboxROISize, 'String', num2str(1));
end

handles = UpdateMaskSettings(handles);
guidata(hObject, handles);


%--- Executes on button press in ButtonSelectBrainMask. ---%
%----------------------------------------------------------%
function ButtonSelectBrainMask_Callback(hObject, eventdata, handles)

% Get initial search path:
if ~isempty(handles.PLSmat.BrainMaskFile)
    [SearchPath, ~, ~] = fileparts(handles.gui.DefaultBrainMask);
else
    [SearchPath, ~, ~] = fileparts(handles.PLSmat.BrainMaskFile);
end

% Select brain mask:
[MaskFile, MaskPath] = uigetfile([SearchPath,'/*+tlrc.BRIK'],...
    'Select Mask File:', 'MultiSelect', 'off');

if MaskFile == 0
    return;  % If user cancels
end

handles.PLSmat.BrainMaskFile = [MaskPath,MaskFile];
set(handles.TextboxBrainMaskFile, 'String', handles.PLSmat.BrainMaskFile);

% Save handles:
handles = UpdateMaskSettings(handles);
guidata(hObject, handles);


%--- Textbox to display selected brain mask. ---%
%-----------------------------------------------%
function TextboxBrainMaskFile_Callback(hObject, eventdata, handles)
EnteredText = get(handles.TextboxBrainMaskFile, 'String');

if ~isequal(EnteredText, handles.PLSmat.BrainMaskFile)
    set(handles.TextboxBrainMaskFile, 'String', handles.PLSmat.BrainMaskFile);
    msgbox('Note: Use the button to change brain mask file.', 'Note:');
end


%--- Executes on button press in ButtonSelectROIMask. ---%
%--------------------------------------------------------%
function ButtonSelectROIMask_Callback(hObject, eventdata, handles)

% Get initial search path:
if ~isempty(handles.PLSmat.ROIMaskFile)
    [SearchPath, ~, ~] = fileparts(handles.gui.DefaultROIMask);
else
    [SearchPath, ~, ~] = fileparts(handles.PLSmat.ROIMaskFile);
end

% Select ROI mask:
[MaskFile, MaskPath] = uigetfile([SearchPath,'/*.xls'],...
    'Select Mask File:', 'MultiSelect', 'off');

if MaskFile == 0
    return;  % If user cancels
end

handles.PLSmat.ROIMaskFile = [MaskPath,MaskFile];
set(handles.TextboxROIMaskFile, 'String', handles.PLSmat.ROIMaskFile);

% Save handles:
handles = UpdateMaskSettings(handles);
guidata(hObject, handles);


%--- Textbox to display selected ROI mask. ---%
%---------------------------------------------%
function TextboxROIMaskFile_Callback(hObject, eventdata, handles)
EnteredText = get(handles.TextboxROIMaskFile, 'String');

if ~isequal(EnteredText, handles.PLSmat.ROIMaskFile)
    set(handles.TextboxROIMaskFile, 'String', handles.PLSmat.ROIMaskFile);
    msgbox('Note: Use the button to change ROI mask file.', 'Note:');
end


%--- Textbox to specify ROI voxel size: ---%
%------------------------------------------%
function TextboxROISize_Callback(hObject, eventdata, handles)
if isempty(get(handles.TextboxROISize, 'String'))
    msgbox('Error: ROI size for ROI mask not specified. Default set to 1.');
    set(handles.TextboxROISize, 'String', num2str(1));
end
if str2num(get(handles.TextboxROISize, 'String')) == 0
    msgbox('Error: ROI size for ROI mask cannot be 0. Default set to 1.');
    set(handles.TextboxROISize, 'String', num2str(1));
end

handles = UpdateMaskSettings(handles);
guidata(hObject, handles);


%--- Updates mask settings & GUI panel: ---%
%------------------------------------------%
function OutputHandles = UpdateMaskSettings(InputHandles)
handles = InputHandles;

% Read GUI to update PLSmat:
if get(handles.ButtonBrainMask,   'Value') == 1
    handles.PLSmat.MaskType = 'BrainMask';
elseif get(handles.ButtonROIMask, 'Value') == 1
    handles.PLSmat.MaskType = 'ROIMask';
end

if get(handles.CheckboxDataSpecificMask, 'Value') == 1
    handles.PLSmat.DataSpecificMask = 'yes';
else
    handles.PLSmat.DataSpecificMask = 'no';
end

handles.PLSmat.MaskKeepVoxThresh = ...
    str2num(get(handles.TextboxKeepVoxelThresh, 'String')) / 100;

ROISize = round(str2num(get(handles.TextboxROISize, 'String')));
handles.PLSmat.ROIBlobSize = [-ROISize:ROISize];

% Enable/Disable GUI mask components:
if get(handles.CheckboxMakePLSmat, 'Value') == 1
	set(findall(handles.PanelPLSMaskSettings, '-property', 'Enable'), 'Enable', 'on');
	%set(handles.TextboxBrainMaskFile, 'Enable', 'inactive');
	%set(handles.TextboxROIMaskFile,   'Enable', 'inactive');
else
	set(findall(handles.PanelPLSMaskSettings, '-property', 'Enable'), 'Enable', 'off');
end

if get(handles.ButtonBrainMask,   'Value') == 1
    if get(handles.CheckboxDataSpecificMask, 'Value') == 1
        set(handles.ButtonSelectBrainMask, 'Enable', 'off');
        set(handles.TextboxBrainMaskFile,  'Enable', 'off');
    else
        set(handles.TextboxKeepVoxelThresh, 'Enable', 'off');
        set(handles.TextKeepVoxelThresh,    'Enable', 'off');
    end
    
    set(handles.ButtonSelectROIMask,   'Enable', 'off');
    set(handles.TextboxROIMaskFile,    'Enable', 'off');
    set(handles.TextboxROISize,        'Enable', 'off');
	set(handles.TextROISize,           'Enable', 'off');
    
elseif get(handles.ButtonROIMask, 'Value') == 1
    set(handles.CheckboxDataSpecificMask, 'Enable', 'off');
    set(handles.ButtonSelectBrainMask,    'Enable', 'off');
    set(handles.TextboxBrainMaskFile,     'Enable', 'off');
    set(handles.TextboxKeepVoxelThresh,   'Enable', 'off');
    set(handles.TextKeepVoxelThresh,      'Enable', 'off');
end

% Set output handles:
OutputHandles = handles;


%--- Reset mask settings & GUI panel: ---%
%----------------------------------------%
function OutputHandles = ResetMaskSettings(InputHandles)
handles = InputHandles;

% Reset PLSmat mask settings:
handles.PLSmat.MaskType          = 'BrainMask';
handles.PLSmat.DataSpecificMask  = 'yes';
handles.PLSmat.MaskKeepVoxThresh = 0.90; 

handles.PLSmat.BrainMaskFile = handles.gui.DefaultBrainMask;
handles.PLSmat.ROIMaskFile   = handles.gui.DefaultROIMask;
handles.PLSmat.ROIBlobSize   = [-1:1];

% Reset GUI mask panel back to default:
set(handles.ButtonBrainMask,      'Value', 1);
set(handles.ButtonROIMask,        'Value', 0);
set(handles.TextboxROISize,       'String', num2str(1));
set(handles.TextboxBrainMaskFile, 'String', handles.gui.DefaultBrainMask);
set(handles.TextboxROIMaskFile,   'String', handles.gui.DefaultROIMask);

set(handles.CheckboxDataSpecificMask, 'Value', 1);
set(handles.TextboxKeepVoxelThresh,   'String', '90');

% Set output handles:
OutputHandles = handles;




%====================================%
% FUNCTIONS FOR BEHAVIOR & SEED PLS: %
%====================================%

%--- Textbox for name ID of behavior files loaded. ---%
%-----------------------------------------------------%
function TextboxBhvSeedID_Callback(hObject, eventdata, handles)
InputID = deblank(get(handles.TextboxBhvSeedID, 'String'));
InputID = regexprep(InputID, '\s+', '_');
set(handles.TextboxBhvSeedID, 'String', InputID);

handles = UpdateBehaviorSettings(handles);
guidata(hObject, handles);


%--- Executes on button press in ButtonLoadBhvFile. ---%
%------------------------------------------------------%
function ButtonLoadBhvFile_Callback(hObject, eventdata, handles)
if ~isempty(handles.PLSmat.BhvFile)
    [SearchPath, ~, ~] = fileparts(handles.PLSmat.BhvFile);
    SearchPath         = [SearchPath,'/'];
else
    SearchPath = [];
end

[BhvFile, BhvPath] = uigetfile([SearchPath,'*.xls'],...
    'Select Behavior file:', 'MultiSelect', 'off');

if BhvFile == 0
    return;  % If user cancels
end

handles.PLSmat.BhvFile = [BhvPath,BhvFile];
set(handles.TextboxBhvFile, 'String', handles.PLSmat.BhvFile);

handles = UpdateBehaviorSettings(handles);
guidata(hObject, handles);


%--- Executes on button press in ButtonClearBhvFile. ---%
%-------------------------------------------------------%
function ButtonClearBhvFile_Callback(hObject, eventdata, handles)
handles.PLSmat.BhvFile = [];
set(handles.TextboxBhvFile, 'String', []);

handles = UpdateBehaviorSettings(handles);
guidata(hObject, handles);


%--- Textbox to display selected behavior .xls. ---%
%--------------------------------------------------%
function TextboxBhvFile_Callback(hObject, eventdata, handles)
EnteredText = get(handles.TextboxBhvFile, 'String');

if ~isequal(EnteredText, handles.PLSmat.BhvFile)
    set(handles.TextboxBhvFile, 'String', handles.PLSmat.BhvFile);
    msgbox('Note: Use button to change bhv .xls file.', 'Note:');
end


%--- Executes on button press in ButtonLoadSeedFile. ---%
%-------------------------------------------------------%
function ButtonLoadSeedFile_Callback(hObject, eventdata, handles)
if ~isempty(handles.PLSmat.SeedFile)
    [SearchPath, ~, ~] = fileparts(handles.PLSmat.SeedFile);
    SearchPath         = [SearchPath,'/'];
else
    SearchPath = [];
end

[SeedFile, SeedPath] = uigetfile([SearchPath,'*.xls'],...
    'Select Seed file:', 'MultiSelect', 'off');

if SeedFile == 0
    return;  % If user cancels
end

handles.PLSmat.SeedFile = [SeedPath,SeedFile];
set(handles.TextboxSeedFile, 'String', handles.PLSmat.SeedFile);

handles = UpdateBehaviorSettings(handles);
guidata(hObject, handles);


%--- Executes on button press in ButtonClearSeedFile. ---%
%--------------------------------------------------------%
function ButtonClearSeedFile_Callback(hObject, eventdata, handles)
handles.PLSmat.SeedFile = [];
set(handles.TextboxSeedFile, 'String', []);

handles = UpdateBehaviorSettings(handles);
guidata(hObject, handles);


%--- Textbox to display selected seed .xls. ---%
%----------------------------------------------%
function TextboxSeedFile_Callback(hObject, eventdata, handles)
EnteredText = get(handles.TextboxSeedFile, 'String');

if ~isequal(EnteredText, handles.PLSmat.SeedFile)
    set(handles.TextboxSeedFile, 'String', handles.PLSmat.SeedFile);
    msgbox('Note: Use button to change seed .xls file.');
end


%--- Textbox to specify seed size. ---%
%-------------------------------------%
function TextboxSeedSize_Callback(hObject, eventdata, handles)
if isempty(get(handles.TextboxSeedSize, 'String'))
    msgbox('Error: Seed size for seeds not specified. Default set to 1.', 'Error:')
    set(handles.TextboxSeedSize, 'String', num2str(1));
end
if str2num(get(handles.TextboxSeedSize, 'String')) == 0
    msgbox('Error: Seed size for seeds cannot be 0. Default set to 1.', 'Error:')
    set(handles.TextboxSeedSize, 'String', num2str(1));
end

handles = UpdateBehaviorSettings(handles);
guidata(hObject, handles);


%--- Updates behavior settings & GUI panel. ---%
%----------------------------------------------%
function OutputHandles = UpdateBehaviorSettings(InputHandles)
handles = InputHandles;

% Read GUI to update PLSmat:
handles.PLSmat.BhvSeedID = get(handles.TextboxBhvSeedID, 'String');

SeedSize = round(str2num(get(handles.TextboxSeedSize, 'String')));
handles.PLSmat.SeedBlobSize = [-SeedSize:SeedSize];

% Enable/disable GUI behavior components:
if get(handles.CheckboxMakePLSmat, 'Value') == 1
	set(findall(handles.PanelPLSBhvSeedSettings, '-property', 'Enable'), 'Enable', 'on');
	%set(handles.TextboxBhvFile,  'Enable', 'inactive');
	%set(handles.TextboxSeedFile, 'Enable', 'inactive');
else
	set(findall(handles.PanelPLSBhvSeedSettings, '-property', 'Enable'), 'Enable', 'off');
end

if isempty(handles.PLSmat.SeedFile)
    set(handles.TextboxSeedSize, 'Enable', 'off');
    set(handles.TextSeedSize,    'Enable', 'off');
end

% Set output handles:
OutputHandles = handles;


%--- Reset behavior settings & GUI panel. ---%
%--------------------------------------------%
function OutputHandles = ResetBehaviorSettings(InputHandles)
handles = InputHandles;

% Reset PLSmat behavior settings:
handles.PLSmat.BhvSeedID    = [];
handles.PLSmat.BhvFile      = [];
handles.PLSmat.SeedFile     = [];
handles.PLSmat.SeedBlobSize = [-1:1];

% Reset GUI behavior panel back to default:
set(handles.TextboxBhvSeedID, 'String', []);
set(handles.TextboxBhvFile,   'String', []);
set(handles.TextboxSeedFile,  'String', []);
set(handles.TextboxSeedSize,  'String', num2str(1));

% Set output handles:
OutputHandles = handles;




%=====================================%
% GUI FUNCTIONS FOR PLS RUN SETTINGS: %
%=====================================%

%--- Executes on button press in ButtonPLSReadme. ---%
%----------------------------------------------------%
function ButtonPLSReadme_Callback(hObject, eventdata, handles)
[PipelineDir, ~, ~] = fileparts(which('DriverGUI.m'));
open([PipelineDir,'/MEGPLS_TOOLBOX/PLS_Tools/pls_nk_functions/PLS_README.txt']);


%--- Executes on selection change in DropdownPLSMethod. ---%
%----------------------------------------------------------%
function DropdownPLSMethod_Callback(hObject, eventdata, handles)
handles = UpdatePLSrunSettings(handles);
guidata(hObject, handles);


%--- Executes on button press in CheckboxSplitHalfPLS. ---%
%---------------------------------------------------------%
function CheckboxSplitHalfPLS_Callback(hObject, eventdata, handles)
if get(handles.CheckboxSplitHalfPLS, 'Value') == 1
    set(handles.TextboxNumSplits,       'String', num2str(100));
    set(handles.TextboxNumPermutations, 'String', num2str(100));
    set(handles.TextboxNumBootstraps,   'String', num2str(200));
else
    set(handles.TextboxNumSplits,       'String', num2str(0));
    set(handles.TextboxNumPermutations, 'String', num2str(500));
    set(handles.TextboxNumBootstraps,   'String', num2str(500));
end 

handles = UpdatePLSrunSettings(handles);
guidata(hObject, handles);


%--- Textbox to specify number of PLS splits. ---%
%------------------------------------------------%
function TextboxNumSplits_Callback(hObject, eventdata, handles)
if get(handles.CheckboxSplitHalfPLS, 'Value') == 1
    if isempty(get(handles.TextboxNumSplits, 'String'))
        msgbox('Note: Number of splits not specified. Default set to 100.')
        set(handles.TextboxNumSplits, 'String', num2str(100));
    end
    if str2num(get(handles.TextboxNumSplits, 'String')) > 100
        msgbox('Warning: For Splithalf-PLS, recommend # splits to be < 100.')
    end

elseif get(handles.CheckboxSplitHalfPLS, 'Value') == 0
	set(handles.TextboxNumSplits, 'String', num2str(0));
end

handles = UpdatePLSrunSettings(handles);
guidata(hObject, handles);


%--- Textbox to specify number of PLS permutations. ---%
%------------------------------------------------------%
function TextboxNumPermutations_Callback(hObject, eventdata, handles)
if get(handles.CheckboxSplitHalfPLS, 'Value') == 1
    if isempty(get(handles.TextboxNumPermutations, 'String'))
        msgbox('Note: Number of permutations not specified. Default set to 100.')
        set(handles.TextboxNumPermutations, 'String', num2str(100));
    end
    if str2num(get(handles.TextboxNumPermutations, 'String')) > 100
        msgbox('Warning: For Splithalf-PLS, recommend # perm. to be < 100.')
    end
    
elseif get(handles.CheckboxSplitHalfPLS, 'Value') == 0
    if isempty(get(handles.TextboxNumPermutations, 'String'))
        msgbox('Note: Number of permutations not specified. Default set to 500.')
        set(handles.TextboxNumPermutations, 'String', num2str(500));
    end
end

handles = UpdatePLSrunSettings(handles);
guidata(hObject, handles);


%--- Textbox to specify number of PLS bootstraps. ---%
%------------------------------------------------------%
function TextboxNumBootstraps_Callback(hObject, eventdata, handles)
if get(handles.CheckboxSplitHalfPLS, 'Value') == 1
    if isempty(get(handles.TextboxNumBootstraps, 'String'))
        msgbox('Note: Number of bootstraps not specified. Default set to 200.')
        set(handles.TextboxNumBootstraps, 'String', num2str(200));
    end
    if str2num(get(handles.TextboxNumBootstraps, 'String')) > 200
        msgbox('Warning: For Splithalf-PLS, recommend # perm. to be < 200.')
    end

elseif get(handles.CheckboxSplitHalfPLS, 'Value') == 0
    if isempty(get(handles.TextboxNumBootstraps, 'String'))
        msgbox('Note: Number of bootstraps not specified. Default set to 500.')
        set(handles.TextboxNumBootstraps, 'String', num2str(500));
    end
end

handles = UpdatePLSrunSettings(handles);
guidata(hObject, handles);


%--- Textbox to enter PLS non-rotated contrast. ---%
%--------------------------------------------------%
function TextboxNRContrast_Callback(hObject, eventdata, handles)
handles = UpdatePLSrunSettings(handles);
guidata(hObject, handles);


%--- Textbox for name ID of PLS non-rotated contrast. ---%
%--------------------------------------------------------%
function TextboxNRContrastID_Callback(hObject, eventdata, handles)
InputID = deblank(get(handles.TextboxNRContrastID, 'String'));
InputID = regexprep(InputID, '\s+', '_');
set(handles.TextboxNRContrastID, 'String', InputID);

handles = UpdatePLSrunSettings(handles);
guidata(hObject, handles);


%--- Executes on button press in CheckboxSingleBhv. ---%
%------------------------------------------------------%
function CheckboxSingleBhv_Callback(hObject, eventdata, handles)
handles = UpdatePLSrunSettings(handles);
guidata(hObject, handles);


%--- Executes on button press in CheckboxKeepDatamat. ---%
%--------------------------------------------------------%
function CheckboxKeepDatamat_Callback(hObject, eventdata, handles)
prompt = {'WARNING:';
	'Saving datamat will consume a large amount of HDD space.';
	'Are you sure you want to enable setting?'};

SaveDatamat = questdlg(prompt, 'WARNING:', 'YES', 'NO', 'NO');
if strcmp(SaveDatamat, 'NO')
    set(handles.CheckboxKeepDatamat, 'Value', 0);
	return;
end

handles = UpdatePLSrunSettings(handles);
guidata(hObject, handles);


%--- Executes on selection change in DropdownMCentType. ---%
%----------------------------------------------------------%
function DropdownMCentType_Callback(hObject, eventdata, handles)
handles = UpdatePLSrunSettings(handles);
guidata(hObject, handles);


%--- Executes on selection change in DropdownCorrMode. ---%
%---------------------------------------------------------%
function DropdownCorrMode_Callback(hObject, eventdata, handles)
handles = UpdatePLSrunSettings(handles);
guidata(hObject, handles);


%--- Executes on selection change in DropdownPermType. ---%
%---------------------------------------------------------%
function DropdownPermType_Callback(hObject, eventdata, handles)
handles = UpdatePLSrunSettings(handles);
guidata(hObject, handles);


%--- Executes on selection change in DropdownBootType. ---%
%---------------------------------------------------------%
function DropdownBootType_Callback(hObject, eventdata, handles)
handles = UpdatePLSrunSettings(handles);
guidata(hObject, handles);


%--- Updates PLSrun settings & GUI panel. ---%
%----------------------------------------------%
function OutputHandles = UpdatePLSrunSettings(InputHandles)
handles = InputHandles;

% Read GUI to update PLSrun settings:
DropdownOptions = get(handles.DropdownPLSMethod, 'String');
PLSMethod = DropdownOptions{get(handles.DropdownPLSMethod, 'Value')};
switch PLSMethod
	case 'Mean-Centered'
		handles.PLSrun.Method = 'MCENT';
	case 'Non-Rotated'
		handles.PLSrun.Method = 'NROT';
	case 'Behavior/Seed'
		handles.PLSrun.Method = 'BHV';
	case 'Multi-Block'
		handles.PLSrun.Method = 'MBLOCK';
	case 'NRot. Behavior'
		handles.PLSrun.Method = 'NR-BHV';
	case 'NRot. Multi-Block'
		handles.PLSrun.Method = 'NR-MBLOCK';
end

handles.PLSrun.NumPerm  = str2num(get(handles.TextboxNumPermutations, 'String'));
handles.PLSrun.NumBoot  = str2num(get(handles.TextboxNumBootstraps,   'String'));
handles.PLSrun.NumSplit = str2num(get(handles.TextboxNumSplits,		  'String'));

handles.PLSrun.NRContrast   = str2num(get(handles.TextboxNRContrast, 'String'));
handles.PLSrun.NRContrastID = get(handles.TextboxNRContrastID, 'String');

if get(handles.CheckboxKeepDatamat, 'Value') == 1
	handles.PLSrun.KeepDatamat = 'yes';
else
	handles.PLSrun.KeepDatamat = 'no';
end

if get(handles.CheckboxSplitHalfPLS, 'Value') == 1
	handles.PLSrun.SplitHalf = 'yes';
else
	handles.PLSrun.SplitHalf = 'no';
    handles.PLSrun.NumSplit  = 0;
end

if get(handles.CheckboxSingleBhv, 'Value') == 1 && ...
		ismember(handles.PLSrun.Method, {'BHV', 'MBLOCK', 'NR-BHV', 'NR-MBLOCK'})
	handles.PLSrun.SingleBhv = 'yes';
else
	handles.PLSrun.SingleBhv = 'no';
end

DropdownOptions = get(handles.DropdownMCentType, 'String');
MCentType = DropdownOptions{get(handles.DropdownMCentType, 'Value')};
handles.PLSrun.MCentType = str2num(MCentType);

DropdownOptions = get(handles.DropdownCorrMode, 'String');
CorrMode = DropdownOptions{get(handles.DropdownCorrMode, 'Value')};
handles.PLSrun.CorrMode = str2num(CorrMode);

DropdownOptions = get(handles.DropdownPermType, 'String');
PermType = DropdownOptions{get(handles.DropdownPermType, 'Value')};
handles.PLSrun.PermType = str2num(PermType);

DropdownOptions = get(handles.DropdownBootType, 'String');
BootType = DropdownOptions{get(handles.DropdownBootType, 'Value')};
switch BootType
	case 'stratified'
		handles.PLSrun.BootType = 'strat';
	case 'non-stratified'
		handles.PLSrun.BootType = 'nonstrat';
	otherwise
		error('Unrecognized option.')  % error-check
end

% Enable/disable GUI PLSrun components:
if get(handles.CheckboxRunPLS, 'Value') == 1
	set(findall(handles.PanelPLSrunSettings, '-property', 'Enable'), 'Enable', 'on');
else
	set(findall(handles.PanelPLSrunSettings, '-property', 'Enable'), 'Enable', 'off');
end

if ~ismember(handles.PLSrun.Method, {'MCENT', 'NROT', 'MBLOCK', 'NR-MBLOCK'})
	set(handles.DropdownMCentType, 'Enable', 'off');
	set(handles.TextMCentType,	   'Enable', 'off');
end

if ~ismember(handles.PLSrun.Method, {'NROT', 'NR-BHV', 'NR-MBLOCK'})
	set(handles.TextboxNRContrast,   'Enable', 'off');
	set(handles.TextboxNRContrastID, 'Enable', 'off');
	set(handles.TextNRContrastID,	 'Enable', 'off');
end

if ~ismember(handles.PLSrun.Method, {'BHV', 'MBLOCK', 'NR-BHV', 'NR-MBLOCK'})
	set(handles.CheckboxSingleBhv, 'Enable', 'off');
end

if get(handles.CheckboxSplitHalfPLS, 'Value') == 0
	set(handles.TextboxNumSplits, 'Enable', 'off');
	set(handles.TextNumSplits,	  'Enable', 'off');
end

% Set output handles:
OutputHandles = handles;


%--- Reset PLSrun settings & GUI panel. ---%
%------------------------------------------%
function OutputHandles = ResetPLSrunSettings(InputHandles)
handles = InputHandles;

% Reset PLSrun settings:
handles.PLSrun.Method   = 'MCENT';
handles.PLSrun.NumPerm  = 500;
handles.PLSrun.NumBoot  = 500;
handles.PLSrun.NumSplit = 0;

handles.PLSrun.NRContrast   = [];
handles.PLSrun.NRContrastID = [];

handles.PLSrun.KeepDatamat = 'no';
handles.PLSrun.SplitHalf   = 'no';
handles.PLSrun.SingleBhv   = 'no';

handles.PLSrun.MCentType = 0;
handles.PLSrun.CorrMode  = 0;
handles.PLSrun.PermType  = 0;
handles.PLSrun.BootType = 'strat';

% Reset GUI PLSrun panel back to default:
DropdownOptions = get(handles.DropdownPLSMethod, 'String');
DropdownValue   = find(strcmp(DropdownOptions,   'Mean-Centered'));
set(handles.DropdownPLSMethod, 'Value', DropdownValue);
if DropdownValue == 0
	error('Option does not exist in dropdown list.');  % Error-check
end

set(handles.TextboxNumPermutations, 'String', num2str(500));
set(handles.TextboxNumBootstraps,   'String', num2str(500));
set(handles.TextboxNumSplits,		'String', num2str(0));

set(handles.TextboxNRContrast,   'String', []);
set(handles.TextboxNRContrastID, 'String', []);

set(handles.CheckboxSplitHalfPLS, 'Value', 0);
set(handles.CheckboxSingleBhv,	  'Value', 0);
set(handles.CheckboxKeepDatamat,  'Value', 0);

set(handles.DropdownMCentType, 'Value', 1);  % Sets type to 0.
set(handles.DropdownCorrMode,  'Value', 1);  % Sets type to 0.
set(handles.DropdownPermType,  'Value', 1);  % Sets type to 0.

DropdownOptions = get(handles.DropdownBootType, 'String');
DropdownValue   = find(strcmp(DropdownOptions,  'stratified'));
set(handles.DropdownBootType, 'Value', DropdownValue);
if DropdownValue == 0
	error('Option does not exist in dropdown list.');  % Error-check
end

% Set output handles:
OutputHandles = handles;




%=====================================%
% GUI FUNCTIONS FOR RUNNING ANALYSIS: %
%=====================================%

%--- Executes on button press in ButtonStartAnalysis. ---%
%--------------------------------------------------------%
function ButtonStartAnalysis_Callback(hObject, eventdata, handles)

% Check for errors and required settings:
if isempty(handles.gui.BuilderMat)
    msgbox('ERROR: Builder .mat file has not been loaded yet.', 'Error:')
    return;
end

if get(handles.CheckboxMakePLSmat, 'Value') == 1
    if isempty(handles.PLSmat.SessionID)
        msgbox('ERROR: Enter SessionID for PLS run.', 'Error:')
        return;
    end
    if isempty(handles.PLSmat.TimeIndices)
        msgbox('ERROR: Specify AFNI time-indices for PLS.', 'Error:')
        return;
    end
    if ismember(handles.PLSrun.Method, {'BHV', 'MBLOCK', 'NR-BHV', 'NR-MBLOCK'})
        if isempty(handles.PLSmat.BhvFile) && isempty(handles.PLSmat.SeedFile)
            msgbox('ERROR: Select behavior and/or seed .xls files.')
            return;
        end
        if isempty(handles.PLSmat.BhvSeedID)
            msgbox('ERROR: Enter ID for behavior and/or seeds selected.')
            return;
        end
    end
end

if get(handles.CheckboxRunPLS, 'Value') == 1
    if isempty(get(handles.TextboxNumPermutations, 'String'))
        msgbox('Error: Number of permutations not specified.', 'Error:');
        return;
    end
    if isempty(get(handles.TextboxNumBootstraps, 'String'))
        msgbox('Error: Number of bootstraps not specified.', 'Error:');
        return;
    end
    if isempty(get(handles.TextboxNumSplits, 'String')) && ...
            get(handles.CheckboxSplitHalfPLS, 'Value') == 1
        msgbox('Error: Number of splits not specified.', 'Error:');
        return;
    end

    if ismember(handles.PLSrun.Method, {'NROT', 'NR-BHV', 'NR-MBLOCK'})
        if isempty(handles.PLSrun.NRContrast) || isempty(handles.PLSrun.NRContrastID)
            msgbox('ERROR: Enter NR Contrast & Contrast ID.')
            return;
        end
    end
end

% Ask user to confirm:
ConfirmRun = questdlg('CONFIRM: Start analysis with current settings?',...
    'CONFIRM:', 'YES', 'NO', 'NO');
if strcmp(ConfirmRun, 'NO')
    return;
end

% Check for Matlab PCT:
CheckOpenPCT
commandwindow;

% Run timelock and/or frequency analysis:
if get(handles.CheckboxTimelockAnalysis, 'Value') == 1
    MEGpipeline_TimelockFreq (handles.gui.BuilderMat, 'Timelock')
end
if get(handles.CheckboxTimelockGrpAvg, 'Value') == 1
    MEGpipeline_TimelockFreqAvg (handles.gui.BuilderMat, 'Timelock')
end

if get(handles.CheckboxFreqAnalysis, 'Value') == 1
    MEGpipeline_TimelockFreq (handles.gui.BuilderMat, 'Freq')
end
if get(handles.CheckboxFreqGrpAvg, 'Value') == 1
    MEGpipeline_TimelockFreqAvg (handles.gui.BuilderMat, 'Freq')
end

% Run seg, headmodels & leadfields:
if get(handles.CheckboxSegmentMRI,   'Value') == 1
    MEGpipeline_SegmentMRI(handles.gui.BuilderMat)
end
if get(handles.CheckboxHdmLeadfield, 'Value') == 1
    MEGpipeline_HdmLeadfield(handles.gui.BuilderMat)
end

% Run source analysis sections:
if get(handles.CheckboxSourceFilter,    'Value') == 1
    MEGpipeline_SourceFilter_Wrap(handles.gui.BuilderMat)
end
if get(handles.CheckboxSourceAnalysis,  'Value') == 1
    MEGpipeline_SourceAnalysis_Wrap(handles.gui.BuilderMat)
end
if get(handles.CheckboxMakeSourceNifti, 'Value') == 1
    MEGpipeline_MakeInterpSourceNii_Wrap (handles.gui.BuilderMat)
    MEGpipeline_MakeMRINii_Wrap          (handles.gui.BuilderMat, 'PreprocMRI')
end
if get(handles.CheckboxMakeSourceAfni,  'Value') == 1
    MEGpipeline_Nifti4DFunc2AFNI_Wrap (handles.gui.BuilderMat, 'Nifti4DSource')
    MEGpipeline_NiftiAnat2AFNI_Wrap   (handles.gui.BuilderMat, 'PreprocMRINii')
end

% Run normalise source sections:
if get(handles.CheckboxNormaliseSource,  'Value') == 1
    MEGpipeline_NormaliseSPM_Wrap(handles.gui.BuilderMat)
end
if get(handles.CheckboxMakeNormSrcNifti, 'Value') == 1
    MEGpipeline_MakeNormSourceNii_Wrap (handles.gui.BuilderMat)
    MEGpipeline_MakeMRINii_Wrap        (handles.gui.BuilderMat, 'NormMRI')
end
if get(handles.CheckboxMakeNormSrcAfni,  'Value') == 1
    MEGpipeline_Nifti4DFunc2AFNI_Wrap (handles.gui.BuilderMat, 'Nifti4DNormSource')
    MEGpipeline_NiftiAnat2AFNI_Wrap   (handles.gui.BuilderMat, 'NormMRINii')
end

% Run group-average normsource sections:
if get(handles.CheckboxGroupAvgNormSrc, 'Value') == 1
    MEGpipeline_SourceAvg(handles.gui.BuilderMat)
end
if get(handles.CheckboxMakeGrpAvgNifti,    'Value') == 1
    MEGpipeline_MakeGrpAvgNii_Wrap(handles.gui.BuilderMat)
end
if get(handles.CheckboxMakeGrpAvgAfni,     'Value') == 1
    MEGpipeline_Nifti4DFunc2AFNI_Wrap(handles.gui.BuilderMat, 'Nifti4DGrpAvg')
end

% Run PLS sections:
if get(handles.CheckboxMakePLSmat, 'Value') == 1
    InputPLSmat = handles.PLSmat;
    
    if strcmp(InputPLSmat.MaskType, 'BrainMask')
        if strcmp(InputPLSmat.DataSpecificMask, 'yes')
            InputPLSmat.BrainMaskFile = [];
            InputPLSmat.ROIMaskFile   = [];
        else
            InputPLSmat.ROIMaskFile = [];
        end
        
    elseif strcmp(InputPLSmat.MaskType, 'ROIMask')
        InputPLSmat.BrainMaskFile = [];
    end
    
    PLSmatReady = MEGpipeline_MakePLSmat(InputPLSmat)
    InputPLSmat = [];  % Free memory
    
	if get(handles.CheckboxRunPLS, 'Value') == 0
		msgbox('Debug: Checking datamat without running PLS. Activating keyboard mode.')
		PLSmatReady
		keyboard;
	end
end
if get(handles.CheckboxRunPLS, 'Value') == 1
    MEGpipeline_RunPLS(PLSmatReady, handles.PLSrun)
end




%=================================%
% FUNCTION TO OPEN MODAL WAITBOX: %
%=================================%
function WaitBox = StartWaitBox(TextString)

WaitBox = dialog('Units', 'pixels', 'Position', [500 500 300 40],...
    'Windowstyle', 'modal', 'NextPlot', 'new', 'Name', 'Please Wait:');

uicontrol('Parent', WaitBox, 'Units', 'pixels', 'Position', [20 10 250 20],...
    'Style', 'text', 'String', TextString, 'FontWeight', 'bold');

movegui(WaitBox, 'center');




%==============================%
% GUIDE "CREATEFCN" FUNCTIONS: %
%==============================%

% --- Executes during object creation, after setting all properties.
function TextboxBuilderMat_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function ListboxPLSmatGroupID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function ListboxPLSmatSubjID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function ListboxPLSmatCondID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function TextboxBrainMaskFile_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxKeepVoxelThresh_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxROIMaskFile_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxROISize_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function SliderTimeStartPLS_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
% --- Executes during object creation, after setting all properties.
function SliderTimeEndPLS_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
% --- Executes during object creation, after setting all properties.
function TextboxTimeStartPLS_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxTimeEndPLS_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function ListboxManualTimeSelect_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function TextboxBhvSeedID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxBhvFile_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxSeedFile_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxSeedSize_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function TextboxPLSmatSessionID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function DropdownPLSMethod_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxNumPermutations_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxNumBootstraps_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxNumSplits_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function TextboxNRContrast_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxNRContrastID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function DropdownMCentType_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function DropdownCorrMode_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function DropdownPermType_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function DropdownBootType_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

