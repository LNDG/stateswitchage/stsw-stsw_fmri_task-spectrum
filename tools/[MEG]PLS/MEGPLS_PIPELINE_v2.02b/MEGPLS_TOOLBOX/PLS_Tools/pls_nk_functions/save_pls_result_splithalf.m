function save_pls_result_splithalf...
    (res,datamat_lst,dim4d,roi,boot_thresh,sal_thresh,bsrclamp_range,salclamp_range,Info,BSRprefix,SALprefix)

%** MODIFIED TO OUTPUT SALIENCE IMAGE AS WELL.

  %%%% save bootstrap & salience result as 4D brik files
  if numel(dim4d)~=4
    error('dim must be a vector of length 4');
  end
  num_times = dim4d(4);
  
  % Identify potentially significant LV's.
  sig_lvs = [];
  num_lvs = size(res.u,2);
  for lv=1:num_lvs
      if isfield(res, 'perm_splithalf')  %|| res.perm_splithalf.num_split > 0
          if (res.perm_splithalf.ucorr_prob(lv) < 0.1 || res.perm_splithalf.vcorr_prob(lv) < 0.1 || res.perm_result.sprob(lv) < 0.1)
              sig_lvs = [ sig_lvs lv];
          else break;
          end
          
      elseif ~isfield(res, 'perm_splithalf')  %|| res.perm_splithalf.num_split == 0
          if res.perm_result.sprob(lv) < 0.1
              sig_lvs = [ sig_lvs lv];
          end
      end
  end
    
  for lv = sig_lvs
    % Get BSR data for BSRimg:
    bsr = single(res.boot_result.compare_u(:,lv));
    bsr(find(abs(bsr)<boot_thresh)) = 0;
    bsr(find(bsr > bsrclamp_range(2))) = bsrclamp_range(2);  % clamp for display purposes
    bsr(find(bsr < bsrclamp_range(1))) = bsrclamp_range(1);
    bsr = reshape(bsr,[],dim4d(4));
    
    % Get salience data for SALIENCEimg:
    sal = single(res.u(:,lv));
    sal(find(abs(sal)<sal_thresh)) = 0;
    sal(find(sal > salclamp_range(2))) = salclamp_range(2);  % clamp for display purposes
    sal(find(sal < salclamp_range(1))) = salclamp_range(1);
    sal = reshape(sal,[],dim4d(4));
    
    % Get BSRimg:
    boot_img = zeros(dim4d); 
    boot_img(1,1,1,1) = -5; boot_img(1,1,1,2) = 5;  %** why is this here? can you remove?
    for t=1:dim4d(4)
      tmp = reshape(squeeze(boot_img(:,:,:,t)),1,[]);
      tmp(roi) = bsr(:,t);     
      boot_img(:,:,:,t) = reshape(tmp,dim4d(1),dim4d(2),dim4d(3));
    end
    
    % Get SALIENCEimg:
    sal_img = zeros(dim4d);
    sal_img(1,1,1,1) = -5; sal_img(1,1,1,2) = 5;  %** why is this here? can you remove?
    for t=1:dim4d(4)
      tmp = reshape(squeeze(sal_img(:,:,:,t)),1,[]);
      tmp(roi) = sal(:,t);     
      sal_img(:,:,:,t) = reshape(tmp,dim4d(1),dim4d(2),dim4d(3));
    end
    
    
    % Compile BRIK info:
    if isfield(res, 'perm_splithalf')
        if ismember(res.method,[1,2])
            Info.HISTORY_NOTE = [Info.HISTORY_NOTE ' *** PLS: LV ' num2str(lv) ...
                ' p = ' num2str(res.perm_result.sprob(lv)) ...
                ' ucorr_prob = ' num2str(res.perm_splithalf.ucorr_prob(lv)) ...
                ' vcorr_prob = ' num2str(res.perm_splithalf.vcorr_prob(lv)) ...
                ' v = ' num2str(reshape(squeeze(res.v(:,lv)),1,[]))];
        elseif ismember(res.method,[3,5,4,6])
            Info.HISTORY_NOTE = [Info.HISTORY_NOTE ' *** PLS: LV ' num2str(lv) ...
                ' p = ' num2str(res.perm_result.sprob(lv)) ...
                ' ucorr_prob = ' num2str(res.perm_splithalf.ucorr_prob(lv)) ...
                ' vcorr_prob = ' num2str(res.perm_splithalf.vcorr_prob(lv)) ...
                ' corr = ' num2str(reshape(squeeze(res.boot_result.orig_corr(:,lv)),1,[])) ...
                ' llcorr = ' num2str(reshape(squeeze(res.boot_result.llcorr(:,lv)),1,[])) ...
                ' ulcorr = ' num2str(reshape(squeeze(res.boot_result.ulcorr(:,lv)),1,[]))];
        end
    
    elseif ~isfield(res, 'perm_splithalf')
        if ismember(res.method,[1,2])
            Info.HISTORY_NOTE = [Info.HISTORY_NOTE ' *** PLS: LV ' num2str(lv) ...
                ' p = ' num2str(res.perm_result.sprob(lv)) ...
                ' v = ' num2str(reshape(squeeze(res.v(:,lv)),1,[]))];
        elseif ismember(res.method,[3,5,4,6])
            Info.HISTORY_NOTE = [Info.HISTORY_NOTE ' *** PLS: LV ' num2str(lv) ...
                ' p = ' num2str(res.perm_result.sprob(lv)) ...
                ' corr = ' num2str(reshape(squeeze(res.boot_result.orig_corr(:,lv)),1,[])) ...
                ' llcorr = ' num2str(reshape(squeeze(res.boot_result.llcorr(:,lv)),1,[])) ...
                ' ulcorr = ' num2str(reshape(squeeze(res.boot_result.ulcorr(:,lv)),1,[]))];
        end
    end
        
    clear Opt;
    Opt.scale = 0;
    Opt.view = '+tlrc';
    Opt.verbose = 1;
    Opt.AppendHistory = 1;
    Opt.NoCheck = 0;
    Opt.OverWrite = 'y';
    Opt.AdjustHeader = 'y';
    
    
    % Check input info for current BRIK:
    InputInfo = Info;
    [N(1), N(2), N(3), N(4)] = size(boot_img);
    
    if (isfield(InputInfo,'DATASET_RANK')),
        if (InputInfo.DATASET_RANK(2) ~= N(4)),
            InputInfo.TAXIS_NUMS    = [];  % Need to add these fields back so
            InputInfo.TAXIS_FLOATS  = [];  % WriteBrik won't error out when it
            InputInfo.TAXIS_OFFSETS = [];  % tries to remove these fields.
        end
    end
    
    InputInfo.RootName = [BSRprefix '_LV' num2str(lv)];
    Opt.Prefix         = InputInfo.RootName;
    [err, ErrMessage, InputInfo] = WriteBrik(boot_img, InputInfo, Opt);
    
    InputInfo.RootName = [SALprefix '_LV' num2str(lv)];
    Opt.Prefix         = InputInfo.RootName;
    [err, ErrMessage, InputInfo] = WriteBrik(sal_img, InputInfo, Opt);
  end  % lv
 
  
  % depending on type of PLS save group avrages or correlations or both
  num_groups = res.num_groups;
  num_conds = res.num_conditions;
  if ismember(res.method,[1,2,4,6])
      % save group average time courses
      for g=1:num_groups
          numsubs = res.num_subj_lst;
          data = reshape(datamat_lst{g},numsubs(g),num_conds,[],dim4d(4)); % (subj cond voxel time)
          data = squeeze(mean(data,1)); % (cond voxel time)
          for cond=1:num_conds
              container = zeros(dim4d); % (x y z time)
              for t=1:dim4d(4),
                  tmp = reshape(squeeze(container(:,:,:,t)),1,[]);
                  tmp(roi) = squeeze(data(cond,:,t));
                  container(:,:,:,t) = reshape(tmp,dim4d(1),dim4d(2),dim4d(3));
              end
              Info.RootName = [BSRprefix '_group' num2str(g) '_cond' num2str(cond) '_avgts'];
              Info.HISTORY_NOTE = [Info.HISTORY_NOTE ' Group/Cond average time series '];
              clear Opt;
              Opt.scale = 0;
              Opt.view = '+tlrc';
              Opt.verbose = 1;
              Opt.AppendHistory = 1;
              Opt.NoCheck = 0;
              Opt.OverWrite = 'y';
              Opt.Prefix = Info.RootName;
              Opt.AdjustHeader = 'y';
              
              % Check input info for current BRIK:
              InputInfo = Info;
              [N(1), N(2), N(3), N(4)] = size(container);
              
              if (isfield(InputInfo,'DATASET_RANK')),
                  if (InputInfo.DATASET_RANK(2) ~= N(4)),
                      InputInfo.TAXIS_NUMS    = [];  % Need to add these fields back so
                      InputInfo.TAXIS_FLOATS  = [];  % WriteBrik won't error out when it
                      InputInfo.TAXIS_OFFSETS = [];  % tries to remove these fields.
                  end
              end
              
              [err, ErrMessage, InputInfo] =  WriteBrik(container, InputInfo, Opt);
          end % for cond
      end % for g
  end
  if ismember(res.method,[3,5,4,6])
      % save group/condition/behavior specific correlation maps
      num_behavs = size(res.datamatcorrs_lst{1},1)/num_conds;
      for g=1:num_groups
          data = reshape(res.datamatcorrs_lst{g},num_behavs,num_conds,[],dim4d(4)); % (behav cond voxel time)
          for cond=1:num_conds
              for behav=1:num_behavs
                  
                  container = zeros(dim4d);
                  for t=1:dim4d(4),
                      tmp = reshape(squeeze(container(:,:,:,t)),1,[]);
                      tmp(roi) = squeeze(data(behav,cond,:,t));
                      container(:,:,:,t) = reshape(tmp,dim4d(1),dim4d(2),dim4d(3));
                  end
                  Info.RootName = [BSRprefix '_group' num2str(g) '_cond' num2str(cond) '_behav' num2str(behav) '_corrts'];
                  Info.HISTORY_NOTE = [Info.HISTORY_NOTE ' Group/Cond/Behav correlation time series '];
                  clear Opt;
                  Opt.scale = 0;
                  Opt.view = '+tlrc';
                  Opt.verbose = 1;
                  Opt.AppendHistory = 1;
                  Opt.NoCheck = 0;
                  Opt.OverWrite = 'y';
                  Opt.Prefix = Info.RootName;
                  Opt.AdjustHeader = 'y';
                  
                  % Check input info for current BRIK:
                  InputInfo = Info;
                  [N(1), N(2), N(3), N(4)] = size(container);
                  
                  if (isfield(InputInfo,'DATASET_RANK')),
                      if (InputInfo.DATASET_RANK(2) ~= N(4)),
                          InputInfo.TAXIS_NUMS    = [];  % Need to add these fields back so
                          InputInfo.TAXIS_FLOATS  = [];  % WriteBrik won't error out when it
                          InputInfo.TAXIS_OFFSETS = [];  % tries to remove these fields.
                      end
                  end
                  
                  [err, ErrMessage, InputInfo] =  WriteBrik(container, InputInfo, Opt);
              end % for behav
          end % for cond
      end % for g
  end
