function varargout = PreprocMRI(varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reads MRI data into Fieldtrip and runs preprocessing. %
% See: "help ft_read_mri" for supported filetypes.      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


% Last Modified by GUIDE v2.5 21-Aug-2014 12:07:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PreprocMRI_OpeningFcn, ...
                   'gui_OutputFcn',  @PreprocMRI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


%--- Executes just before PreprocMRI is made visible. ---%
%--------------------------------------------------------%
function PreprocMRI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PreprocMRI (see VARARGIN)

% Choose default command line output for PreprocMRI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% Make sure toolbox paths are added:
[PipelineDir, ~, ~] = fileparts(which('PreprocMRI.m'));

addpath(genpath(PipelineDir));
rmpath([PipelineDir,'/DEFAULT_SETTINGS']);  % Make sure its calling from AnalysisID
rmpath([PipelineDir,'/TEMPORARY_FIXES']);   % Make sure its calling from FT toolbox

CheckToolboxPaths(PipelineDir);

if exist('ErrorLog_PreprocMRI.txt', 'file')
    delete('ErrorLog_PreprocMRI.txt');
end


% Initialize variables:
handles.paths.Rootpath = [];
handles.paths.AnatID   = [];
handles.name.AnatID    = [];

handles.paths.CurrentGroupID = [];
handles.name.CurrentGroupID  = [];

handles.name.SubjID    = [];
handles.gui.Status     = [];

handles.paths.AnatFolder   = [];
handles.paths.AnatFile	   = [];
handles.paths.AnatFullpath = [];
handles.paths.PreprocMRI   = [];

handles.FTcfg = [];
handles.gui.ShowInstruct = 1;

% Save handles:
guidata(hObject, handles);


% UIWAIT makes PreprocMRI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


%--- Outputs from this function are returned to the command line. ---%
%--------------------------------------------------------------------%
function varargout = PreprocMRI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;




%===================================%
% FUNCTIONS FOR SELECTING ROOTPATH: %
%===================================%

%--- Executes on button press in ButtonSetRootpath. ---%
%------------------------------------------------------%
function ButtonSetRootpath_Callback(hObject, eventdata, handles)
if ~isempty(handles.name.AnatID)
	prompt = {'WARNING:';
		'Changing root directory will clear all current settings.'; '';
		'Do you wish to continue?'};
	
	ChangeDir = questdlg(prompt, 'WARNING:', 'YES', 'NO', 'NO');
	if strcmp(ChangeDir, 'NO')
		return;
	end
end

% Select and set rootpath:
SelectedPath = uigetdir;
if SelectedPath == 0
	return;  % If user cancels
end

% Check if selected path has spaces (AFNI functions cannot read spaces):
CheckSpaces = strfind(SelectedPath, ' ');
if ~isempty(CheckSpaces)
    message = {'Error: Target directory selected contains spaces.';
        'AFNI functions cannot read folder & file paths with spaces.';
        'AFNI functions are required for file conversions later on.'};
    msgbox(message, 'Error:')
    return;
end

handles.paths.Rootpath = SelectedPath;
set(handles.TextboxRootpath, 'String', handles.paths.Rootpath);

% Start Waitbox:
WaitBox = StartWaitBox('SETTING TARGET DIRECTORY:');

% Reset AnatID:
handles.name.AnatID  = [];
handles.paths.AnatID = [];
set(handles.TextboxAnatID, 'String', 'None Selected');

% Reset GroupID:
handles.name.CurrentGroupID  = [];
handles.paths.CurrentGroupID = [];
set(handles.TextboxGroupID, 'String', 'None Selected');

% Reset settings & GUI panels:
handles = ResetSubjIDSettings	 (handles);
handles = ResetInputAnatSettings (handles);
handles = ResetStatusSettings    (handles);
handles = ResetPreprocSettings	 (handles);

% Update settings & GUI panels (to enable/disable):
handles = UpdateSubjIDSettings	  (handles);
handles = UpdateInputAnatSettings (handles);
handles = UpdateStatusSettings    (handles);
handles = UpdatePreprocSettings	  (handles);

% Save handles:
guidata(hObject, handles);
close(WaitBox);


%--- Textbox to display selected Rootpath: ---%
%---------------------------------------------%
function TextboxRootpath_Callback(hObject, eventdata, handles)
EnteredText = get(handles.TextboxRootpath, 'String');

if ~isequal(EnteredText, handles.paths.Rootpath)
	set(handles.TextboxRootpath, 'String', handles.paths.Rootpath);
	msgbox('Note: Use button to change Rootpath.')
end




%===========================================%
% FUNCTIONS FOR CREATING OR LOADING ANATID: %
%===========================================%

%--- Executes on button press in ButtonCreateAnatID. ---%
%-------------------------------------------------------%
function ButtonCreateAnatID_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Select root directory first.', 'Warning:');
	return;
end

% Enter name of AnatID:
UserInput = inputdlg('Enter AnatID name:', 'Create new AnatID:');
UserInput = deblank(UserInput);
UserInput = regexprep(UserInput, '\s+', '_');
if isempty(char(UserInput))
    return;  % If user cancels
end

PathInputID = [handles.paths.Rootpath,'/AnatID_',UserInput{1}];
if exist(PathInputID, 'dir')
	msgbox('Error: Specified AnatID already exists!', 'Error:')
	return;
end

% Set AnatID & create folder:
status = mkdir(PathInputID);
if status == 1  % If mkdir successful
	handles.name.AnatID  = UserInput{1};
	handles.paths.AnatID = PathInputID;
	set(handles.TextboxAnatID, 'String', handles.name.AnatID);	
else
	msgbox('Error: Failed to create AnatID folder.', 'Error:')
	return;
end

% Start Waitbox:
WaitBox = StartWaitBox('CREATING ANAT ID:');

% Reset GroupID:
handles.name.CurrentGroupID  = [];
handles.paths.CurrentGroupID = [];
set(handles.TextboxGroupID, 'String', []);

% Reset settings & GUI panels:
handles = ResetSubjIDSettings	 (handles);
handles = ResetInputAnatSettings (handles);
handles = ResetStatusSettings    (handles);
handles = ResetPreprocSettings	 (handles);

% Update settings & GUI panels (to enable/disable):
handles = UpdateSubjIDSettings	  (handles);
handles = UpdateInputAnatSettings (handles);
handles = UpdateStatusSettings    (handles);
handles = UpdatePreprocSettings	  (handles);

% Save handles:
guidata(hObject, handles);
close(WaitBox);


%--- Executes on button press in ButtonLoadAnatID. ---%
%-----------------------------------------------------%
function ButtonLoadAnatID_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Select root directory first.', 'Warning:');
	return;
end

% Get AnatID folders in current rootpath:
DetectedFolders = dir([handles.paths.Rootpath,'/AnatID_*']);

RemoveIndex = [];
for d = 1:length(DetectedFolders)
	if DetectedFolders(d).isdir ~= 1
		RemoveIndex = [RemoveIndex, d];  % Get unwanted indices
	end
end

DetectedFolders(RemoveIndex) = [];
if isempty(DetectedFolders)
    msgbox('Error: No AnatID folders detected inside root dir.', 'Error:');
    return;
end

% List AnatID folders for selection:
DetectedFolders = {DetectedFolders.name};
SelectedIndex = listdlg('PromptString', 'Select AnatID to load:',...
    'ListSize', [300, 300], 'SelectionMode', 'single', 'ListString', DetectedFolders);			
if isempty(SelectedIndex)
	return;  % If user cancels.
end

SelectedAnatID		= DetectedFolders{SelectedIndex};
SelectedAnatID(1:7) = [];  % Remove "AnatID_" prefix to get AnatID suffix.
FullpathAnatID		= [handles.paths.Rootpath,'/AnatID_',SelectedAnatID];

% Set as current AnatID:
handles.name.AnatID  = SelectedAnatID;
handles.paths.AnatID = FullpathAnatID;
set(handles.TextboxAnatID, 'String', handles.name.AnatID);

% Start Waitbox:
WaitBox = StartWaitBox('LOADING ANAT ID:');

% Reset GroupID:
handles.name.CurrentGroupID  = [];
handles.paths.CurrentGroupID = [];
set(handles.TextboxGroupID, 'String', []);

% Reset settings & GUI panels:
handles = ResetSubjIDSettings	 (handles);
handles = ResetInputAnatSettings (handles);
handles = ResetStatusSettings    (handles);
handles = ResetPreprocSettings	 (handles);

% Update settings & GUI panels (to enable/disable):
handles = UpdateSubjIDSettings	  (handles);
handles = UpdateInputAnatSettings (handles);
handles = UpdateStatusSettings    (handles);
handles = UpdatePreprocSettings	  (handles);

% Save handles:
guidata(hObject, handles);
close(WaitBox);


%--- Textbox to display current AnatID: ---%
%------------------------------------------%
function TextboxAnatID_Callback(hObject, eventdata, handles)
EnteredText = get(handles.TextboxAnatID, 'String');

if ~isequal(EnteredText, handles.name.AnatID)
	set(handles.TextboxAnatID, 'String', handles.name.AnatID);
	msgbox('Note: Use button to change AnatID.')
end




%===========================================%
% FNCTIONS FOR CREATING OR LOADING GROUPID: %
%===========================================%

%--- Executes on button press in ButtonCreateGroupID. ---%
%--------------------------------------------------------%
function ButtonCreateGroupID_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Select root directory first.', 'Warning:');
	return;
end
if isempty(handles.name.AnatID) || isempty(handles.paths.AnatID)
	msgbox('Warning: Create or load AnatID first.', 'Warning:');
	return;
end

% Enter name of GroupID:
UserInput = inputdlg('Enter GroupID name:', 'Create new GroupID:');
UserInput = deblank(UserInput);
UserInput = regexprep(UserInput, '\s+', '_');
if isempty(char(UserInput))
    return;  % If user cancels
end

PathInputID = [handles.paths.AnatID,'/GroupID_',UserInput{1}];
if exist(PathInputID, 'dir')
    msgbox('Error: Specified GroupID already exists for this AnatID.', 'Error:')
    return;
end

% Set GroupID and create folder:
status = mkdir(PathInputID);
if status == 1  % If mkdir successful
    handles.name.CurrentGroupID  = UserInput{1};
    handles.paths.CurrentGroupID = PathInputID;
	set(handles.TextboxGroupID, 'String', handles.name.CurrentGroupID);
else
    msgbox('Error: Failed to create GroupID folder.', 'Error:')
    return;
end

% Start Waitbox:
WaitBox = StartWaitBox('CREATING GROUP ID:');

% Reset settings & GUI panels:
handles = ResetSubjIDSettings	 (handles);
handles = ResetInputAnatSettings (handles);
handles = ResetStatusSettings    (handles);
handles = ResetPreprocSettings	 (handles);

% Update settings & GUI panels (to enable/disable):
handles = UpdateSubjIDSettings	  (handles);
handles = UpdateInputAnatSettings (handles);
handles = UpdateStatusSettings    (handles);
handles = UpdatePreprocSettings	  (handles);

% Save handles:
guidata(hObject, handles);
close(WaitBox);


%--- Executes on button press in ButtonLoadGroupID. ---%
%------------------------------------------------------%
function ButtonLoadGroupID_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Select root directory first.', 'Warning:');
	return;
end
if isempty(handles.name.AnatID) || isempty(handles.paths.AnatID)
	msgbox('Warning: Create or load AnatID first.', 'Warning:');
	return;
end

% Acquire GroupID folders for current AnatID:
DetectedFolders = dir([handles.paths.AnatID,'/GroupID_*']);

RemoveIndex = [];
for g = 1:length(DetectedFolders)
    if DetectedFolders(g).isdir ~= 1
        RemoveIndex = [RemoveIndex, g];  % Get unwanted indices
    end
end

DetectedFolders(RemoveIndex) = [];
if isempty(DetectedFolders)
    msgbox('Error: No GroupID folders were detected for current AnatID.', 'Error:');
    return;
end

% List detected GroupID folders for selection:
DetectedFolders = {DetectedFolders.name};
SelectedIndex = listdlg('PromptString', 'Select GroupID to load:',...
    'ListSize', [300, 300], 'SelectionMode', 'single', 'ListString', DetectedFolders);
if isempty(SelectedIndex)
    return;  % If user cancels
end

SelectedGroupID		 = DetectedFolders{SelectedIndex};
SelectedGroupID(1:8) = [];  % Remove "GroupID_" prefix to get GroupID.

% Set as current GroupID:
handles.name.CurrentGroupID  = SelectedGroupID;
handles.paths.CurrentGroupID = [handles.paths.AnatID,'/GroupID_',SelectedGroupID];
set(handles.TextboxGroupID, 'String', handles.name.CurrentGroupID);

% Start Waitbox:
WaitBox = StartWaitBox('LOADING GROUP ID:');

% Reset settings & GUI panels:
handles = ResetSubjIDSettings	 (handles);
handles = ResetInputAnatSettings (handles);
handles = ResetStatusSettings    (handles);
handles = ResetPreprocSettings	 (handles);

% Load PreprocInputMRI .mat for GroupID if available:
PreprocInputMat = [handles.paths.CurrentGroupID,...
	'/PreprocInputMRI_',handles.name.CurrentGroupID,'.mat'];

if exist(PreprocInputMat, 'file')
	LoadedMat = load(PreprocInputMat);
	handles = LoadSubjIDSettings    (handles, LoadedMat);
	handles = LoadInputAnatSettings (handles, LoadedMat);
else
	message = {'WARNING:';
		'PreprocInputMRI .mat file not found for GroupID.';
		'SubjIDs and input MRI files will need to be selected again.'};
	msgbox(message, 'Warning:');
end

% Update settings & GUI panels (to enable/disable):
handles = UpdateSubjIDSettings	  (handles);
handles = UpdateInputAnatSettings (handles);
handles = UpdatePreprocSettings	  (handles);

% Update status for all loaded subjects:
StatusWaitbar = waitbar(0, ...
    'Please wait. Loading files from session.', 'Windowstyle', 'modal');
for s = 1:length(handles.name.SubjID)
    waitbar(s/length(handles.name.SubjID), StatusWaitbar);
    handles = DetectStatus(handles, s);
end
delete(StatusWaitbar)
handles = UpdateStatusSettings(handles);

% Save handles:
guidata(hObject, handles);
close(WaitBox);


%--- Textbox to display current GroupID: ---%
%-------------------------------------------%
function TextboxGroupID_Callback(hObject, eventdata, handles)
EnteredText = get(handles.TextboxGroupID, 'String');

if ~isequal(EnteredText, handles.name.CurrentGroupID)
	set(handles.TextboxGroupID, 'String', handles.name.CurrentGroupID);
	msgbox('Note: Use button to change GroupID.')
end




%============================================%
% FUNCTIONS FOR ADDING OR REMOVING SUBJECTS: %
%============================================%

%--- Executes on selection change in ListboxSubjID. ---%
%------------------------------------------------------%
function ListboxSubjID_Callback(hObject, eventdata, handles)
SelectedIndex = get(handles.ListboxSubjID, 'Value');
set(handles.ListboxInputAnat, 'Value', SelectedIndex);
set(handles.ListboxStatus,    'Value', SelectedIndex);

handles = UpdatePreprocSettings(handles);  % For enable/disable
guidata(hObject, handles);


%--- User enters SubjID into textbox. ---%
%----------------------------------------%
function TextboxSubjID_Callback(hObject, eventdata, handles)
keypress = get(gcf, 'CurrentCharacter');
if isequal(keypress, char(13))
	ButtonAddSubjID_Callback(hObject, eventdata, handles);
end


%--- Executes on button press in ButtonAddSubjID. ---%
%----------------------------------------------------%
function ButtonAddSubjID_Callback(hObject, eventdata, handles)
if isempty(handles.name.AnatID) || isempty(handles.paths.AnatID)
	msgbox('Warning: Create or load AnatID first.', 'Warning:');
	return;
end
if isempty(handles.name.CurrentGroupID)
	msgbox('Warning: Create or load GroupID first.', 'Warning:');
	return;
end

% Reads and set SubjID:
InputSubjID = get(handles.TextboxSubjID, 'String');
InputSubjID = deblank(InputSubjID);
InputSubjID = regexprep(InputSubjID, '\s+', '_');
InputSubjID = cellstr(InputSubjID);
if isempty(InputSubjID{1})
	return;
end
if ~isempty(handles.name.SubjID) && ismember(InputSubjID{1}, handles.name.SubjID)
	return;  % If SubjID already added
end

handles.name.SubjID = [handles.name.SubjID; InputSubjID];
NewIndex = length(handles.name.SubjID);

% Initialize cells for new subject:
handles.paths.AnatFolder   {NewIndex} = [];
handles.paths.AnatFile     {NewIndex} = [];
handles.paths.AnatFullpath {NewIndex} = [];
handles.paths.PreprocMRI   {NewIndex} = [];
handles.gui.Status         {NewIndex} = [];

% Update GUI:
handles = UpdateSubjIDSettings    (handles);
handles = UpdateInputAnatSettings (handles);
handles = DetectStatus            (handles, NewIndex);
handles = UpdateStatusSettings	  (handles);
handles = UpdatePreprocSettings   (handles);  % For enable/disable

% Save handles:
set(handles.ListboxSubjID, 'Value', length(handles.name.SubjID));
if ~isempty(handles.paths.AnatFullpath)
    set(handles.ListboxInputAnat, 'Value', length(handles.name.SubjID));
end
if ~isempty(handles.gui.Status)
    set(handles.ListboxStatus,    'Value', length(handles.name.SubjID));
end
guidata(hObject, handles);


%--- Executes on button press in ButtonRmvSubjID. ---%
%----------------------------------------------------%
function ButtonRmvSubjID_Callback(hObject, eventdata, handles)
if isempty(handles.name.SubjID)
	return;
end

SelectedIndex = get(handles.ListboxSubjID, 'Value');
handles.name.SubjID(SelectedIndex) = [];

% Remove data & status cells for removed subject:
handles.paths.AnatFolder   (SelectedIndex) = [];
handles.paths.AnatFile     (SelectedIndex) = [];
handles.paths.AnatFullpath (SelectedIndex) = [];
handles.paths.PreprocMRI   (SelectedIndex) = [];
handles.gui.Status         (SelectedIndex) = [];

% Update GUI:
handles = UpdateSubjIDSettings    (handles);
handles = UpdateInputAnatSettings (handles);
handles = UpdateStatusSettings	  (handles);
handles = UpdatePreprocSettings   (handles);  % For enable/disable

% Save handles:
guidata(hObject, handles);


%--- Updates SubjID settings & GUI panel: ---%
%--------------------------------------------%
function OutputHandles = UpdateSubjIDSettings(InputHandles)
handles = InputHandles;

% Update PreprocMRI paths (Location of FT MRIs to be read in):
for s = 1:length(handles.name.SubjID)
	handles.paths.PreprocMRI{s} = ...
		[handles.paths.CurrentGroupID,'/',handles.name.SubjID{s},'_PreprocMRI.mat'];
end

% Update listbox:
set(handles.ListboxSubjID, 'String', handles.name.SubjID);
CurrentIndex = get(handles.ListboxSubjID, 'Value');
MaxSubjIndex = length(handles.name.SubjID);

if isempty(CurrentIndex) || CurrentIndex == 0 || CurrentIndex > MaxSubjIndex
    set(handles.ListboxSubjID,    'Value', MaxSubjIndex);
    set(handles.ListboxInputAnat, 'Value', MaxSubjIndex);
	set(handles.ListboxStatus,    'Value', MaxSubjIndex);
end

% Set output handles:
OutputHandles = handles;


%--- Load SubjID settings & GUI panel: ---%
%-----------------------------------------%
function OutputHandles = LoadSubjIDSettings(InputHandles, LoadedMat)
handles = InputHandles;

% Load SubjID and respective PreprocMRI paths:
handles.name.SubjID      = LoadedMat.name.SubjID;
handles.paths.PreprocMRI = LoadedMat.paths.PreprocMRI;

% Load GUI:
set(handles.ListboxSubjID, 'String', handles.name.SubjID);
set(handles.ListboxSubjID, 'Value',  1);
handles = UpdatePreprocSettings(handles);  % For enable/disable

% Set output handles:
OutputHandles = handles;



%--- Reset SubjID settings & GUI panel: ---%
%------------------------------------------%
function OutputHandles = ResetSubjIDSettings(InputHandles)
handles = InputHandles;

handles.paths.PreprocMRI = [];

handles.name.SubjID = [];
set(handles.ListboxSubjID, 'String', []);
set(handles.ListboxSubjID, 'Value',  1);

% Set output handles:
OutputHandles = handles;




%=============================================%
% FUNCTIONS FOR MRI FILE SELECTION & DISPLAY: %
%=============================================%

%--- Executes on selection change in ListboxInputAnat. ---%
%---------------------------------------------------------%
function ListboxInputAnat_Callback(hObject, eventdata, handles)
SelectedIndex = get(handles.ListboxInputAnat, 'Value');
set(handles.ListboxSubjID, 'Value', SelectedIndex);
set(handles.ListboxStatus, 'Value', SelectedIndex);

handles = UpdatePreprocSettings(handles);  % For enable/disable
guidata(hObject, handles);


%--- Updates settings & GUI panel for input MRI files: ---%
%---------------------------------------------------------%
function OutputHandles = UpdateInputAnatSettings(InputHandles)
handles = InputHandles;

% Make sure cells initialized:
for s = 1:length(handles.name.SubjID)
	try
		handles.paths.AnatFolder{s};
		handles.paths.AnatFile{s};
		handles.paths.AnatFullpath{s};
	catch
		handles.paths.AnatFolder{s}   = [];
		handles.paths.AnatFile{s}     = [];
		handles.paths.AnatFullpath{s} = [];
	end
end

% Update listbox:
set(handles.ListboxInputAnat, 'String', handles.paths.AnatFullpath);

% Set output handles:
OutputHandles = handles;


%--- Load settings & GUI panel for input MRI files: ---%
%------------------------------------------------------%
function OutputHandles = LoadInputAnatSettings(InputHandles, LoadedMat)
handles = InputHandles;

% Load input MRI files:
handles.paths.AnatFolder   = LoadedMat.paths.AnatFolder;
handles.paths.AnatFile	   = LoadedMat.paths.AnatFile;
handles.paths.AnatFullpath = LoadedMat.paths.AnatFullpath;

% Update GUI:
set(handles.ListboxInputAnat, 'String', handles.paths.AnatFullpath);
set(handles.ListboxInputAnat, 'Value',  1);
handles = UpdatePreprocSettings(handles);  % For enable/disable

% Set output handles:
OutputHandles = handles;


%--- Reset settings & GUI panel for input MRI files: ---%
%-------------------------------------------------------%
function OutputHandles = ResetInputAnatSettings(InputHandles)
handles = InputHandles;

% Reset input anat:
handles.paths.AnatFolder   = [];
handles.paths.AnatFile     = [];
handles.paths.AnatFullpath = [];

for s = 1:length(handles.name.SubjID)
	handles.paths.AnatFolder{s}   = [];
	handles.paths.AnatFile{s}     = [];
	handles.paths.AnatFullpath{s} = [];
end

% Reset GUI panel:
set(handles.ListboxInputAnat, 'String', []);
set(handles.ListboxInputAnat, 'Value',  1);

% Set output handles:
OutputHandles = handles;




%===============================%
% FUNCTIONS FOR STATUS UPDATES: %
%===============================%

%--- Executes on selection change in ListboxStatus. ---%
%------------------------------------------------------%
function ListboxStatus_Callback(hObject, eventdata, handles)
SelectedIndex = get(handles.ListboxStatus, 'Value');
set(handles.ListboxSubjID,    'Value', SelectedIndex);
set(handles.ListboxInputAnat, 'Value', SelectedIndex);

handles = UpdatePreprocSettings(handles);  % For enable/disable
guidata(hObject, handles);


%--- Executes on button press in ButtonRefreshStatus. ---%
%--------------------------------------------------------%
function ButtonRefreshStatus_Callback(hObject, eventdata, handles)
StatusWaitbar = waitbar(0, ...
    'Please wait. Loading files from session.', 'Windowstyle', 'modal');

for s = 1:length(handles.name.SubjID)
    waitbar(s/length(handles.name.SubjID), StatusWaitbar);
    handles = DetectStatus(handles, s);
end
delete(StatusWaitbar)

handles = UpdateStatusSettings(handles);
guidata(hObject, handles);


%--- Function to acquire status: ---%
%-----------------------------------%
function OutputHandles = DetectStatus(InputHandles, SubjIndex)
handles = InputHandles;

% Get selected subject and update status.
% Unlike PreprocMEG, only selected subject gets worked on, so
% update on a subject-by-subject basis for efficiency.
s = SubjIndex;

% Check if MRI file has been selected:
if isempty(handles.paths.AnatFullpath{s})
    handles.gui.Status{s} = 'Select MRI file to load into FT.';
else
    handles.gui.Status{s} = [];
end

% Check if read into FT and coregistration:
if exist(handles.paths.PreprocMRI{s}, 'file')
    MRIdata = LoadFTmat(handles.paths.PreprocMRI{s}, 'PreprocMRI');
    
    if isempty(MRIdata)
        handles.gui.Status{s} = 'FT file detected, but could not be read. See ErrorLog.';
        
    else
        DimStr = num2str(MRIdata.dim);
        if isfield(MRIdata, 'coordsys')
            Coordsys = num2str(MRIdata.coordsys);
            handles.gui.Status{s} = ...
                ['FT file detected. Coordsys: ',Coordsys,'. Dims: ',DimStr];
        else
            handles.gui.Status{s} = ...
                ['FT file detected. No Coordsys found. Dims: ',DimStr];
        end
    end
end

% Save as output:
OutputHandles = handles;


%--- Update status settings & GUI panel: ---%
%-------------------------------------------%
function OutputHandles = UpdateStatusSettings(InputHandles)
handles = InputHandles;

% Make sure cells initialized:
for s = 1:length(handles.name.SubjID)
	try
		handles.gui.Status{s};
	catch
		handles.gui.Status{s} = [];
	end
end

% Update listbox:
set(handles.ListboxStatus, 'String', handles.gui.Status);

% Set output handles:
OutputHandles = handles;


%--- Reset status settings & GUI panel: ---%
%------------------------------------------%
function OutputHandles = ResetStatusSettings(InputHandles)
handles = InputHandles;

% Reset status:
handles.gui.Status = [];

for s = 1:length(handles.name.SubjID)
    handles.gui.Status{s} = [];
end

% Reset GUI status panel:
set(handles.ListboxStatus, 'String', []);
set(handles.ListboxStatus, 'Value',  1);

% Set output handles:
OutputHandles = handles;




%===============================================%
% FUNCTIONS FOR SAVING & LOADING PREPROC INPUT: %
%===============================================%

%--- Executes on button press in ButtonSaveInput. ---%
%----------------------------------------------------%
function ButtonSaveInput_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Select root directory first.', 'Warning:');
	return;
end
if isempty(handles.name.AnatID) || isempty(handles.paths.AnatID)
	msgbox('Warning: Create or load AnatID first.', 'Warning:');
	return;
end
if isempty(handles.name.CurrentGroupID)
	msgbox('Warning: Create or load GroupID first.', 'Warning:');
	return;
end
if isempty(handles.name.SubjID) || isempty(handles.paths.PreprocMRI)
	msgbox('Warning: Need to specify SubjIDs.', 'Warning:');
	return;
end
if isempty(handles.paths.AnatFullpath)
	msgbox('Warning: MRI files have not been read into FT yet.', 'Warning:')
	return;
end

% Make sure Rootpath & AnatID don't have spaces (will cause issues for AFNI later):
CheckSpaces1 = strfind(handles.paths.Rootpath, ' ');
CheckSpaces2 = strfind(handles.paths.AnatID, ' ');
if ~isempty(CheckSpaces1) || ~isempty(CheckSpaces2)
    message = {'Error: Target directory or AnatID contains spaces.';
        'AFNI functions cannot read folder & file paths with spaces.';
        'AFNI functions are required for file conversions later on.'};
    msgbox(message, 'Error:')
    return;
end

% Check save path:
PreprocInputMat = [handles.paths.CurrentGroupID,...
	'/PreprocInputMRI_',handles.name.CurrentGroupID,'.mat'];

if exist(PreprocInputMat, 'file')
	prompt = {'WARNING:';
		'PreprocInputMRI .mat already exists for this GroupID.';
		'Do you wish to continue and overwrite it?'};
	
	OverwriteMat = questdlg(prompt, 'WARNING:', 'YES', 'NO', 'NO');
	if strcmp(OverwriteMat, 'NO')
		return;
    else
        delete(PreprocInputMat);
	end
end

% Save input parameters for current GroupID:
name  = handles.name;
paths = handles.paths;

paths = rmfield(paths, 'Rootpath');  % Rmv GUI only paths that aren't in load fcns.
paths = rmfield(paths, 'AnatID');    % - These paths auto-adapt if folders moved.
paths = rmfield(paths, 'CurrentGroupID');

CheckSavePath(PreprocInputMat, 'PreprocMRI');
save(PreprocInputMat, 'name', 'paths');

% Feedback:
if exist(PreprocInputMat, 'file')
    msgbox('Current input parameters successfully saved.')
else
    msgbox('Error: Failed to save input parameters.')
end


%--- Executes on button press in ButtonLoadInput. ---%
%----------------------------------------------------%
function ButtonLoadInput_Callback(hObject, eventdata, handles)
if isempty(handles.paths.Rootpath)
	msgbox('Warning: Select root directory first.', 'Warning:');
	return;
end
if isempty(handles.name.AnatID) || isempty(handles.paths.AnatID)
	msgbox('Warning: Create or load AnatID first.', 'Warning:');
	return;
end
if isempty(handles.name.CurrentGroupID)
	msgbox('Warning: Create or load GroupID first.', 'Warning:');
	return;
end

[matfile, matpath] = uigetfile([handles.paths.CurrentGroupID,...
	'/PreprocInputMRI_*.mat'], 'Select PreprocInputMRI .mat:', 'MultiSelect', 'off');

if matfile == 0
	msgbox('Note: Load cancelled. File not selected.', 'Note:');
	return;
end

% Reset settings & GUI panels:
handles = ResetSubjIDSettings	 (handles);
handles = ResetInputAnatSettings (handles);
handles = ResetStatusSettings    (handles);
handles = ResetPreprocSettings	 (handles);

% Load input settings:
LoadedMat = load([matpath,matfile]);
handles = LoadSubjIDSettings    (handles, LoadedMat);
handles = LoadInputAnatSettings (handles, LoadedMat);

% Update settings & GUI panels (to enable/disable):
handles = UpdateSubjIDSettings	  (handles);
handles = UpdateInputAnatSettings (handles);
handles = UpdatePreprocSettings	  (handles);

% Update status for all loaded subjects:
StatusWaitbar = waitbar(0, ...
    'Please wait. Loading files from session.', 'Windowstyle', 'modal');
for s = 1:length(handles.name.SubjID)
    waitbar(s/length(handles.name.SubjID), StatusWaitbar);
    handles = DetectStatus(handles, s);
end
delete(StatusWaitbar)
handles = UpdateStatusSettings(handles);

% Save handles:
guidata(hObject, handles);




%======================================%
% FUNCTIONS TO READ IN & PROCESS MRIs: %
%======================================%

%--- Executes on button press in ButtonReadMRItoFT. ---%
%------------------------------------------------------%
function ButtonReadMRItoFT_Callback(hObject, eventdata, handles)
if isempty(handles.name.AnatID) || isempty(handles.paths.AnatID)
	msgbox('Warning: Create or load AnatID first.', 'Warning:');
	return;
end
if isempty(handles.name.CurrentGroupID)
	msgbox('Warning: Create or load GroupID first.', 'Warning:');
	return;
end
if isempty(handles.name.SubjID)
	msgbox('Warning: Add SubjID first.', 'Warning:');
	return;
end

% Make sure Rootpath & AnatID don't have spaces (will cause issues for AFNI later):
CheckSpaces1 = strfind(handles.paths.Rootpath, ' ');
CheckSpaces2 = strfind(handles.paths.AnatID, ' ');
if ~isempty(CheckSpaces1) || ~isempty(CheckSpaces2)
    message = {'Error: Target directory or AnatID contains spaces.';
        'AFNI functions cannot read folder & file paths with spaces.';
        'AFNI functions are required for file conversions later on.'};
    msgbox(message, 'Error:')
    return;
end

% Check for existing files:
SubjIndex = get(handles.ListboxSubjID, 'Value');

if exist(handles.paths.PreprocMRI{SubjIndex}, 'file')
	prompt = {'Warning: PreprocMRI already exists for this SubjID.';
		'Do you wish to continue and overwrite?'};
	
	Overwrite = questdlg(prompt, 'WARNING:', 'YES', 'NO', 'NO');
	if strcmp(Overwrite, 'NO')
		return;
	end
end

% Select raw MRI file and read into FT:
[File, Folder] = uigetfile([handles.paths.Rootpath,'/*']);
if File == 0
	return;  % If user cancels
end

WaitBox = StartWaitBox('READING MRI FILE INTO FT:');
MRIdata = ft_read_mri([Folder,File])
close(WaitBox);

CheckSavePath(handles.paths.PreprocMRI{SubjIndex}, 'PreprocMRI');
save(handles.paths.PreprocMRI{SubjIndex}, 'MRIdata');

handles.paths.AnatFolder   {SubjIndex} = Folder;
handles.paths.AnatFile     {SubjIndex} = File;
handles.paths.AnatFullpath {SubjIndex} = [Folder,File];

% Update GUI:
handles = UpdateInputAnatSettings (handles);
handles = UpdatePreprocSettings   (handles);

handles = DetectStatus			  (handles, SubjIndex);
handles = UpdateStatusSettings	  (handles);

% Save handles:
guidata(hObject, handles);


%--- Executes on button press in ButtonClearMRI. ---%
%---------------------------------------------------%
function ButtonClearMRI_Callback(hObject, eventdata, handles)
if isempty(handles.name.SubjID)
	return;
end

% Remove FT MRI file for selected SubjID:
SubjIndex = get(handles.ListboxSubjID, 'Value');

if exist(handles.paths.PreprocMRI{SubjIndex}, 'file')
    delete(handles.paths.PreprocMRI{SubjIndex});
end

if ~isempty(handles.paths.AnatFullpath)
	handles.paths.AnatFolder   {SubjIndex} = [];
	handles.paths.AnatFile     {SubjIndex} = [];
	handles.paths.AnatFullpath {SubjIndex} = [];
end

% Update GUI:
handles = UpdateInputAnatSettings (handles);
handles = UpdatePreprocSettings   (handles);

handles = DetectStatus			  (handles, SubjIndex);
handles = UpdateStatusSettings	  (handles);

% Save handles:
guidata(hObject, handles);


%--- Executes on button press in ButtonCheckTransformHand. ---%
%-------------------------------------------------------------%
function ButtonCheckTransformHand_Callback(hObject, eventdata, handles)
if isempty(handles.name.SubjID) || isempty(handles.paths.PreprocMRI)
	msgbox('Warning: Add and select SubjID first.', 'Warning:')
	return;
end

% Load MRI file:
SubjIndex = get(handles.ListboxSubjID, 'Value');
if ~exist(handles.paths.PreprocMRI{SubjIndex}, 'file')
	msgbox('Warning: MRI file for SubjID has not been read into FT yet.', 'Warning:')
	return;
end

MRIdata = LoadFTmat(handles.paths.PreprocMRI{SubjIndex}, 'PreprocMRI');
if isempty(MRIdata)
    msgbox('Error: Failed to load PreprocMRI file for SubjID. See ErrorLog.', 'Error:')
    return;
end

% Check handedness of transformation matrix:
temp = det(MRIdata.transform(1:3, 1:3));

if temp < 0
    message = {'Transformation matrix is left-handed.';
        'If +XYZ is also right-handed, viewer shows radiological view.'};
    msgbox(message);
else
    message = {'Transformation matrix is right-handed.';
        'If +XYZ is also right-handed, viewer shows neurological view.'};
    msgbox(message);
end


%--- Executes on selection change in DropdownCoordsys. ---%
%---------------------------------------------------------%
function DropdownCoordsys_Callback(hObject, eventdata, handles)

% Note: By default, if cfg.coordsys is not specified for ft_volumerealign,
% the output coordinate system will always default to "ctf" or "spm".
% User specifies desired coordinate system here to fill in cfg.coordsys field.
guidata(hObject, handles);


%--- Executes on button press in ButtonCoregisterMRI. ---%
%--------------------------------------------------------%
function ButtonCoregisterMRI_Callback(hObject, eventdata, handles)
if isempty(handles.name.SubjID) || isempty(handles.paths.PreprocMRI)
	msgbox('Warning: Add and select SubjID first.', 'Warning:')
	return;
end

% Load MRI file:
SubjIndex = get(handles.ListboxSubjID, 'Value');
if ~exist(handles.paths.PreprocMRI{SubjIndex}, 'file')
	msgbox('Warning: MRI file for SubjID has not been read into FT yet.', 'Warning:')
	return;
end

MRIdata = LoadFTmat(handles.paths.PreprocMRI{SubjIndex}, 'PreprocMRI');
if isempty(MRIdata)
    msgbox('Error: Failed to load PreprocMRI file for SubjID. See ErrorLog.', 'Error:')
    return;
end

% Check for coordsys (coregistration):
if isfield(MRIdata, 'coordsys')
	prompt = {'WARNING:'; '';
        'Field "coordsys" detected. MRI may already be coregistered.';
        'If so, it is recommended you DO NOT coregister again.'; '';
        'If you wish to redo the coregistration, go back and click the';
        '"Clear Subj FT MRI" button first.'; '';
		'Do you wish to continue and run coregistration again?'; ''};
	
	RunCoreg = questdlg(prompt, 'WARNING:', 'YES', 'NO', 'NO');
	if strcmp(RunCoreg, 'NO')
		return;
	end
end

% Show instructions:
if handles.gui.ShowInstruct == 1;
    Instructions = {'----------------------------'; 
        'COREGISTRATION INSTRUCTIONS:'; 
        '----------------------------'; '';
        'NOTE:';
        'If the crosshair does not appear, push "C" to toggle its visibility.'; ''; '';
        'IMPORTANT:'; 
        ' Push "F" to toggle FIDUCIAL & LANDMARK visibility:  When you place';
        ' fiducials and landmarks, they should appear as coloured circles.'; ''; '';
        'If placing FIDUCIALS (ex: for CTF):';
        ' 1) Push "F" to toggle fiducial visibility if it is not on.';
        ' 2) Place fiducials with keys:  "L" (left), "R" (right), and "N" (nasion).';
        ' 3) Set extra Z-point in positive Z-coordinates using "Z" (see below).';
        ' 4) Once finished, push "Q" to save the coregistration.'; '';
        '    In most cases, Z-point can normally be placed at the top-of-head.';
        '    Additional Z-point ensures left & right fiducials are not flipped.';
        '    Note: Only works with MRI formats supported by Fieldtrip.'; ''; '';
        'If placing LANDMARKS (ex: for SPM):';
        ' 1) Place landmarks with keys:  "A", "P", and "Z".';
        ' 2) Once finished, push "Q" to save the coregistration.'; ''; ''; ''};
    
    Show = questdlg(Instructions, 'INSTRUCTIONS:', 'OK', 'Don''t show again', 'OK');
    if strcmp(Show, 'Don''t show again')
        handles.gui.ShowInstruct = 0;
    end
end

% Get target coordinate system from coordsys dropdown:
% Note: Defaults to "ctf" or "spm" if cfg.coordsys is left empty.
DropdownOptions = get(handles.DropdownCoordsys, 'String');
CoregCoordsys   = DropdownOptions{get(handles.DropdownCoordsys, 'Value')};

ViableOptions = {'ctf', 'spm', 'tal', '4d', 'yokogawa', 'neuromag', 'itab'};
if ismember(CoregCoordsys, ViableOptions)
    cfgRealign.coordsys = CoregCoordsys;
else
    error('Unrecognized Option.');  % error-check
end

% Coregister anatomy:
cfgRealign.method    = 'interactive';
cfgRealign.parameter = 'anatomy';

MRIdata = ft_volumerealign(cfgRealign, MRIdata);

CheckSavePath(handles.paths.PreprocMRI{SubjIndex}, 'PreprocMRI');
save(handles.paths.PreprocMRI{SubjIndex}, 'MRIdata');

% Update GUI:
handles = DetectStatus		   (handles, SubjIndex);
handles = UpdateStatusSettings (handles);

% Save handles:
guidata(hObject, handles);


%--- Executes on button press in ButtonCheckCoordsys. ---%
%--------------------------------------------------------%
function ButtonCheckCoordsys_Callback(hObject, eventdata, handles)
if isempty(handles.name.SubjID) || isempty(handles.paths.PreprocMRI)
	msgbox('Warning: Add and select SubjID first.', 'Warning:')
	return;
end

% Load MRI file:
SubjIndex = get(handles.ListboxSubjID, 'Value');
if ~exist(handles.paths.PreprocMRI{SubjIndex}, 'file')
	msgbox('Warning: MRI file for SubjID has not been read into FT yet.', 'Warning:')
	return;
end

MRIdata = LoadFTmat(handles.paths.PreprocMRI{SubjIndex}, 'PreprocMRI');
if isempty(MRIdata)
    msgbox('Error: Failed to load PreprocMRI file for SubjID. See ErrorLog.', 'Error:')
    return;
end

% Check coregistration:
ft_determine_coordsys(MRIdata, 'interactive', 'no');


%--- Executes on button press in ButtonPlotMRI. ---%
%--------------------------------------------------%
function ButtonPlotMRI_Callback(hObject, eventdata, handles)
if isempty(handles.name.SubjID) || isempty(handles.paths.PreprocMRI)
	msgbox('Warning: Add and select SubjID first.', 'Warning:')
	return;
end

% Load MRI file:
SubjIndex = get(handles.ListboxSubjID, 'Value');
if ~exist(handles.paths.PreprocMRI{SubjIndex}, 'file')
	msgbox('Warning: MRI file for SubjID has not been read into FT yet.', 'Warning:')
	return;
end

CheckInput = CheckPipelineMat(handles.paths.PreprocMRI{SubjIndex}, 'PreprocMRI');
if CheckInput == 0
    msgbox('Error: Failed to load PreprocMRI file for SubjID. See ErrorLog.', 'Error:')
    return;
end

% Plot MRI in ortho view:
cfgPlot.method       = 'ortho';
cfgPlot.anaparameter = 'anatomy';
cfgPlot.location	 = 'center';
cfgPlot.crosshair	 = 'yes';
cfgPlot.interactive  = 'no';
cfgPlot.axis		 = 'on';
cfgPlot.inputfile    = handles.paths.PreprocMRI{SubjIndex};

ft_sourceplot(cfgPlot);


%--- Executes on button press in ButtonResliceMRI. ---%
%-----------------------------------------------------%
function ButtonResliceMRI_Callback(hObject, eventdata, handles)
if isempty(handles.name.SubjID) || isempty(handles.paths.PreprocMRI)
	msgbox('Warning: Add and select SubjID first.', 'Warning:')
	return;
end

% Check for required inputs:
FTcfg = handles.FTcfg;

if isempty(FTcfg.Reslice.resolution)
    msgbox('Warning: Specify resolution in physical units for resliced MRI.');
    return;
end

if get(handles.ButtonResliceCustomRange, 'Value') == 1
    if isempty(FTcfg.Reslice.xrange) || ...
            isempty(FTcfg.Reslice.yrange) || isempty(FTcfg.Reslice.zrange)
        msgbox('Warning: XYZ ranges not specified.');
        return;
    end
    
    XRange = str2num(get(handles.TextboxResliceXRange, 'String'));
    if ~isequal(size(XRange), [1 2]) || (XRange(1) > XRange(2))
        set(handles.TextboxResliceXRange, 'String', []);
        msgbox('Error: X-range should be: [min max] physical units.', 'Error:')
        return;
    end
    
    YRange = str2num(get(handles.TextboxResliceYRange, 'String'));
    if ~isequal(size(YRange), [1 2]) || (YRange(1) > YRange(2))
        set(handles.TextboxResliceYRange, 'String', []);
        msgbox('Error: Y-range should be: [min max] physical units.', 'Error:')
        return;
    end
    
    ZRange = str2num(get(handles.TextboxResliceZRange, 'String'));
    if ~isequal(size(ZRange), [1 2]) || (ZRange(1) > ZRange(2))
        set(handles.TextboxResliceZRange, 'String', []);
        msgbox('Error: Z-range should be: [min max] physical units.', 'Error:')
        return;
    end
end
    
if get(handles.ButtonResliceCustomDims, 'Value') == 1 && isempty(FTcfg.Reslice.dim)
    msgbox('Warning: Custom dimensions not specified.');
    return;
end

% Load selected MRI file:
SubjIndex = get(handles.ListboxSubjID, 'Value');
if ~exist(handles.paths.PreprocMRI{SubjIndex}, 'file')
	msgbox('Warning: MRI file for SubjID has not been read into FT yet.', 'Warning:')
	return;
end

MRIdata = LoadFTmat(handles.paths.PreprocMRI{SubjIndex}, 'PreprocMRI');
if isempty(MRIdata)
    msgbox('Error: Failed to load PreprocMRI file for SubjID. See ErrorLog.', 'Error:')
    return;
end

% Reslice MRI:
WaitBox = StartWaitBox('RESLICING MRI:');
MRIdata = ft_volumereslice(FTcfg.Reslice, MRIdata);
close(WaitBox);

% Plot resliced MRI for user to check:
cfgPlot.method       = 'ortho';
cfgPlot.anaparameter = 'anatomy';
cfgPlot.location	 = 'center';
cfgPlot.crosshair	 = 'yes';
cfgPlot.interactive  = 'no';
cfgPlot.axis		 = 'on';

ft_sourceplot(cfgPlot, MRIdata);

% Ask user if resliced MRI should be saved:
KeepReslice = questdlg('Keep resliced MRI or revert back to previous?', ...
    'SAVE RESLICE?', 'KEEP', 'REVERT', 'REVERT');

if strcmp(KeepReslice, 'KEEP')
    close(1);
    CheckSavePath(handles.paths.PreprocMRI{SubjIndex}, 'PreprocMRI');
    save(handles.paths.PreprocMRI{SubjIndex}, 'MRIdata');
else
    close(1);
    return;
end

% Update GUI:
handles = DetectStatus		   (handles, SubjIndex);
handles = UpdateStatusSettings (handles);

% Save handles:
guidata(hObject, handles);


%--- Executes when selected object is changed in PanelResliceMRISettings. ---%
%----------------------------------------------------------------------------%
function PanelResliceMRISettings_SelectionChangeFcn(hObject, eventdata, handles)
handles = UpdatePreprocSettings(handles);
guidata(hObject, handles);


%--- User enters X-range for reslice here: ---%
%---------------------------------------------%
function TextboxResliceXRange_Callback(hObject, eventdata, handles)
handles = UpdatePreprocSettings(handles);
guidata(hObject, handles);


%--- User enters Y-range for reslice here: ---%
%---------------------------------------------%
function TextboxResliceYRange_Callback(hObject, eventdata, handles)
handles = UpdatePreprocSettings(handles);
guidata(hObject, handles);


%--- User enters Z-range for reslice here: ---%
%---------------------------------------------%
function TextboxResliceZRange_Callback(hObject, eventdata, handles)
handles = UpdatePreprocSettings(handles);
guidata(hObject, handles);


%--- User enters custom volume dimensions for reslice here: ---%
%--------------------------------------------------------------%
function TextboxResliceCustomDims_Callback(hObject, eventdata, handles)
ResliceDims = str2num(get(handles.TextboxResliceCustomDims, 'String'));

if ~isequal(size(ResliceDims), [1 3])
	set(handles.TextboxResliceCustomDims, 'String', '256, 256, 256');
	msgbox('Error: Dim. of volume should be specified as: [nx, ny, nz].', 'Error:')
	return;
end

handles = UpdatePreprocSettings(handles);
guidata(hObject, handles);


%--- User enters resolution for reslice here (physical units): ---%
%-----------------------------------------------------------------%
function TextboxResliceRes_Callback(hObject, eventdata, handles)
prompt = {'WARNING:';
    'Are you sure you wish to change resolution of MRI?';
    'Should not change resolution unless required.'};

ChangeRes = questdlg(prompt, 'WARNING:', 'YES', 'NO', 'NO');
if strcmp(ChangeRes, 'NO')
    set(handles.TextboxResliceRes, 'String', '1');
    return;
end

ResliceRes = str2num(get(handles.TextboxResliceRes, 'String'));
if ~isequal(size(ResliceRes), [1 1])
    set(handles.TextboxResliceRes, 'String', '1');
    msgbox('Error: Resolution should be single # in physical units.', 'Error:')
    return;
end

handles = UpdatePreprocSettings(handles);
guidata(hObject, handles);


%--- Updates PreprocMRI settings & GUI panel: ---%
%------------------------------------------------%
function OutputHandles = UpdatePreprocSettings(InputHandles)
handles = InputHandles;
FTcfg   = handles.FTcfg;

% Update reslice settings:
FTcfg.Reslice            = [];
FTcfg.Reslice.resolution = str2num(get(handles.TextboxResliceRes, 'String'));
	
if get(handles.ButtonResliceCustomDims, 'Value') == 1
    FTcfg.Reslice.dim = str2num(get(handles.TextboxResliceCustomDims, 'String'));
	
elseif get(handles.ButtonResliceCustomRange, 'Value') == 1
	FTcfg.Reslice.xrange = str2num(get(handles.TextboxResliceXRange, 'String'));
	FTcfg.Reslice.yrange = str2num(get(handles.TextboxResliceYRange, 'String'));
	FTcfg.Reslice.zrange = str2num(get(handles.TextboxResliceZRange, 'String'));
end

% Enable/Disable GUI components:
set(handles.ButtonReadMRItoFT,        'Enable', 'on');
set(handles.ButtonClearMRI,           'Enable', 'on');
set(handles.ButtonCheckTransformHand, 'Enable', 'on');
set(handles.ButtonCoregisterMRI,      'Enable', 'on');
set(handles.ButtonCheckCoordsys,      'Enable', 'on');
set(handles.ButtonPlotMRI,            'Enable', 'on');
set(handles.ButtonResliceMRI,         'Enable', 'on');
set(findall(handles.PanelResliceMRISettings, '-property', 'Enable'), 'Enable', 'on');

SubjIndex = get(handles.ListboxSubjID, 'Value');

if isempty(handles.paths.PreprocMRI) || ~exist(handles.paths.PreprocMRI{SubjIndex}, 'file')
	set(handles.ButtonClearMRI,           'Enable', 'off');
    set(handles.ButtonCheckTransformHand, 'Enable', 'off');
	set(handles.ButtonCoregisterMRI,      'Enable', 'off');
	set(handles.ButtonCheckCoordsys,      'Enable', 'off');
	set(handles.ButtonPlotMRI,            'Enable', 'off');
	set(handles.ButtonResliceMRI,         'Enable', 'off');
	set(findall(handles.PanelResliceMRISettings, '-property', 'Enable'), 'Enable', 'off');
end

if get(handles.ButtonResliceDefaultDims, 'Value') == 1
	set(handles.TextboxResliceXRange,     'Enable', 'off');
	set(handles.TextboxResliceYRange,     'Enable', 'off');
	set(handles.TextboxResliceZRange,     'Enable', 'off');
	set(handles.TextboxResliceCustomDims, 'Enable', 'off');
	set(handles.TextXRange,               'Enable', 'off');
	set(handles.TextYRange,               'Enable', 'off');
	set(handles.TextZRange,               'Enable', 'off');
	set(handles.TextResliceCustomDims,	  'Enable', 'off');
	
elseif get(handles.ButtonResliceCustomDims, 'Value') == 1
	set(handles.TextboxResliceXRange, 'Enable', 'off');
	set(handles.TextboxResliceYRange, 'Enable', 'off');
	set(handles.TextboxResliceZRange, 'Enable', 'off');
	set(handles.TextXRange,			  'Enable', 'off');
	set(handles.TextYRange,			  'Enable', 'off');
	set(handles.TextZRange,			  'Enable', 'off');
	
elseif get(handles.ButtonResliceCustomRange, 'Value') == 1
	set(handles.TextboxResliceCustomDims,   'Enable', 'off');
	set(handles.TextResliceCustomDims,	  'Enable', 'off');
	
end

% Set output handles:
handles.FTcfg = FTcfg;
OutputHandles = handles;


%--- Reset PreprocMRI settings % GUI panel: ---%
%----------------------------------------------%
function OutputHandles = ResetPreprocSettings(InputHandles)
handles = InputHandles;

% Reset GUI reslice:
set(handles.ButtonResliceDefaultDims, 'Value', 1);

set(handles.TextboxResliceXRange, 'String', []);
set(handles.TextboxResliceYRange, 'String', []);
set(handles.TextboxResliceZRange, 'String', []);

set(handles.TextboxResliceCustomDims, 'String', '256, 256, 256');
set(handles.TextboxResliceRes,        'String', '1');

% Set output handles:
OutputHandles = handles;




%=================================%
% FUNCTION TO OPEN MODAL WAITBOX: %
%=================================%
function WaitBox = StartWaitBox(TextString)

WaitBox = dialog('Units', 'pixels', 'Position', [500 500 300 40],...
    'Windowstyle', 'modal', 'NextPlot', 'new', 'Name', 'Please Wait:');

uicontrol('Parent', WaitBox, 'Units', 'pixels', 'Position', [20 10 250 20],...
    'Style', 'text', 'String', TextString, 'FontWeight', 'bold');

movegui(WaitBox, 'center');




%============================%
% GUIDE CREATEFCN FUNCTIONS: %
%============================%

% --- Executes during object creation, after setting all properties.
function TextboxRootpath_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxAnatID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxGroupID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function ListboxSubjID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxSubjID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function ListboxInputAnat_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function ListboxStatus_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function DropdownCoordsys_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxResliceRes_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxResliceCustomDims_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxResliceXRange_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxResliceYRange_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function TextboxResliceZRange_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
